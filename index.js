import {AppRegistry,I18nManager} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import { Navigation } from "react-native-navigation";
import registerScreens from './src/screens'
import {Dimensions} from 'react-native'


console.disableYellowBox = true
I18nManager.allowRTL(false)
I18nManager.forceRTL(false)
registerScreens()

Navigation.events().registerAppLaunchedListener(() => {

  //determine properities of topBar,layout,statusBar of all screens 
  Navigation.setDefaultOptions({
    topBar: {
      visible: false
    },
    layout: {
      backgroundColor: 'white',
      orientation: ['portrait'] // An array of supported orientations
    },
    statusBar: {
      //visible: false,
       backgroundColor: '#0B763B',  //in android only
       blur: true // in ios only
    },
    animations: {
      // animation for root screen only
      setRoot: {
        enabled: 'true' , //| 'false', // Optional, used to enable/disable the animation
        alpha: {
          from: 0,
          to: 1,
         // duration: 500,
          startDelay: 0,
          interpolation: 'accelerate'
        }
      },
       // animation for push screens
      push: {
        enabled: 'true',  //| 'false', // Optional, used to enable/disable the animation
        content: {
          // animation from right to left
          /*x: {
            from: 800,
            to: 0,
            startDelay: 0,
            duration: 1500,
          }*/
          //animation from bottom to top
          y: {
            from: 2000,
            to: 0,
            startDelay: 0,
           // duration: 500,
          }
        },
    },
    pop:{
      enabled:true,
      //animation from bottom to top
      content:{
      y: {
        from: 0,
        to: 2000,
        startDelay: 0,
       // duration: 500,
      }
    }
    }
  }

  })

  Navigation.setRoot({
    root: {
      sideMenu:{
          id:'slideMenu',
          animationType: 'parallax',
          openGestureMode: 'bezel',
          left: {
       
            component: {
                id: 'MenuContent',
                name: 'MenuContent'
            }
          },

          center: {
              stack: {
                 id: 'AppStack',
                  children: [
                    {
                        component: {
                            id: 'SplashScreen',
                            name: "SplashScreen"
                        }
                    },
                   
                ]
                },
          },

          right: {    
            component: {
              id: 'MenuContent',
              name: 'MenuContent'
            }
          }
        }
    }
  });
});


