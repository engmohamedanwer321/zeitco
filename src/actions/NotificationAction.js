import axios from 'axios';
import Strigs from '../assets/strings';
import { BASE_END_POINT} from '../AppConfig';
import {
    UNREAD_NOTIFICATIONS_COUNT,FIREBASE_TOKEN
} from './types';
import { RNToasty } from 'react-native-toasty';

export function getUnreadNotificationsCount(token) {
    return dispatch => {
        axios.get(`${BASE_END_POINT}notif/unreadCount`, {
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${token}`  
            },
        })
        .then(response=>{
            console.log('noti count 22')
            console.log(response.data);
            dispatch({type:UNREAD_NOTIFICATIONS_COUNT,payload:response.data.unread})
        }).catch(error=>{
            console.log('noti count error 22')
            console.log(error.response);
        })
    }
}


export function setFirebaseToken(token) {
    return dispatch => {
        console.log("FBT ACTION  ",token)
        dispatch({type:FIREBASE_TOKEN,payload:token})
    }
}



