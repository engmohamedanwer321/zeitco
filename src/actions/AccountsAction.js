//import { AsyncStorage } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios';
import {
    LOGOUT, ACCOUNTS_COUNT
} from './types';
import { BASE_END_POINT } from '../AppConfig';
import { RNToasty } from 'react-native-toasty';
import Strigs from '../assets/strings';
import { resetTo } from '../controlls/NavigationControll'


export function AccountsCount(status) {

    return dispatch => {
        var Url = `${BASE_END_POINT}restaurant/accountCount?`

        if (status != null) {
            Url = Url + `&status=${status}`
        }
        axios.get(Url)
            .then(response => {
                console.log('Done   ', response.data)

                dispatch({ type: ACCOUNTS_COUNT, payload: response.data.accountCount })
            })
            .catch(error => {
                console.log('Error   ', error)
                // this.setState({ accounts404: true, accountsLoading: false, })
            })
    }
}



