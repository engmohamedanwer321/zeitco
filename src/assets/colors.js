export const white = 'white';
export const black = '#373737';
export const darkBlue = '#16476A'
export const sky = '#55B1C2'
export const lightBlue = '#2196F3'
export const lightGray = '#f2f2f0'
export const darkGray = '#595959'
export const green = 'green'
export const greenApp = '#80ad30'
export const grayButton = '#6b6b6b'

export const grayColor1 ='#737373'
export const placeholderGray = '#A9A9A9'

export const lightGreen = '#31AA48'
export const medimGreen = '#249843'
export const darkGreen = '#0B763B'
export const greenButton='#1e8b06'
export const lightGreenButton ='#a9d3a0'
export const darkRed ='#df0000'

