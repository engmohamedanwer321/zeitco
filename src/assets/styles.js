import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import * as colors from '../assets/colors';

export const whiteButton = {width: responsiveWidth(40),  height: responsiveHeight(6), justifyContent: 'center', alignItems: 'center', backgroundColor: 'white', borderWidth:1, borderColor:colors.lightGreenButton}
export const greenButton = {width: responsiveWidth(40),  height: responsiveHeight(6), justifyContent: 'center', alignItems: 'center', backgroundColor: colors.lightGreenButton}