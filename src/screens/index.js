import { Navigation } from 'react-native-navigation';
import { HOC } from '../controlls/NavigationControll'

//resturant
import CancelOrder from './resturant/CancelOrder'
import ResturantHome from './resturant/ResturantHome'

//survey
import SurveyHome from './survey/SurveyHome'

//purchasing
import PurchasingHome from './purchasing/PurchasingHome'
import PurchasingAccountsList from './purchasing/PurchasingAccountsList'
import PurchasingOutlitesInfo from './purchasing/PurchasingOutlitesInfo'


//driver
import DriverHome from './driver/DriverHome'
import DriverOrders from './driver/DriverOrders'
import DriverUpdateOrder from './driver/DriverUpdateOrder'
import DriverCompletedOrders from './driver/DriverCompletedOrders'
import DriverOrderDetails from './driver/DriverOrderDetails'

//operation
import OperationHome from './operation/OperationHome';
import OperationAccounts from './operation/OperationAccounts'
import OperationOrders from './operation/OperationOrders'
import OperationAssignAccountToPurchasing from './operation/OperationAssignAccountToPurchasing'
import OperationConfirmResturantAccount from './operation/OperationConfirmResturantAccount'
import OperationDrivers from './operation/OperationDrivers'
import OperationAssignOrderToDriver from './operation/OperationAssignOrderToDriver'
import OperationEdit_Reassign_DeleteOrder from './operation/OperationEdit_Reassign_DeleteOrder'
import OperationActiveAccounts from './operation/OperationActiveAccounts'
import OperationCollectedOrderDeails from './operation/OperationCollectedOrderDeails'
import OperationShowContacts from './operation/OperationShowContacts'
import OperationSurveys from './operation/OperationSurveys'
import OperationPurchasings from './operation/OperationPurchasings'
import OperationAssignResturantToPurchasing from './operation/OperationAssignResturantToPurchasing'
import OperationWarehouses from './operation/OperationWarehouses'
import OperationWarehouseMember from './operation/OperationWarehouseMember'

//admin
import AdminHome from './admin/AdminHome'
import AdminAddMember from './admin/AdminAddMember'
import AdminNewsList from './admin/AdminNewsList'
import AdminAddNews from './admin/AdminAddNews'
import AdminEditSlider from './admin/AdminEditSlider'
import AdminUpdateNews from './admin/AdminUpdateNews'
import AdimnShowAllMembers from './admin/AdimnShowAllMembers'
import AdminUpdateMemberProfile from './admin/AdminUpdateMemberProfile'
import AdimnShowDriversLocation from './admin/AdimnShowDriversLocation'
import AdminWarehouses from './admin/AdminWarehouses'
import AdminAssignTankToWarehouse from './admin/AdminAssignTankToWarehouse'
import AdminDriversSystemChecking from './admin/AdminDriversSystemChecking'
import AdminShowCallCenters from './admin/AdminShowCallCenters'
import AdminCallCenterOrdersAndNoOrders from './admin/AdminCallCenterOrdersAndNoOrders'
import AdminCallCenterAccountDetails from './admin/AdminCallCenterAccountDetails'
import AdminGetAllShipping from './admin/AdminGetAllShipping'
import AdminUpdateShipping from './admin/AdminUpdateShipping'

//public

import SplashScreen from './SplashScreen';
import MenuContent from "../components/MenuContent";
import Login from './Login';
import Visits from './Visits';
import SelectLanguage from './SelectLanguage';
import ContactUs from './ContactUs'
import Notifications from './Notifications'
import UpdateProfile from './UpdateProfile'
import History from './History'
import AddResturantAccount from './AddResturantAccount'
import UpdateResturantAccount from './UpdateResturantAccount'
import ResturantAccountDetails from './ResturantAccountDetails'
import MakeOrder from './MakeOrder'
import AccountDetails from './AccountDetails'
import RegisterResturantAccount from './RegisterResturantAccount'

//warehouse
import WarehouseHome from './warehouse/WarehouseHome'
import WarehouseDriverCollections from './warehouse/WarehouseDriverCollections'
import WarehouseShipment from './warehouse/WarehouseShipment'
import WarehouseGetMyOrders from './warehouse/WarehouseGetMyOrders'
import WarehouseGetMyShipment from "./warehouse/WarehouseGetMyShipment"
import WarehouseDrivers from './warehouse/WarehouseDrivers'
import AdminWarehouseDetails from './admin/AdminWarehouseDetails'

//callcenter
import CallCenterHome from './callcenter/CallCenterHome'
import CallCenterMakeOrder from './CallCenterMakeOrder'
import CallCenterActiveAccounts from './callcenter/CallCenterActiveAccounts'
import CallCenterAccountDetails from './callcenter/CallCenterAccountDetails'


export default function registerScreens() {
     Navigation.registerComponent("AdminCallCenterAccountDetails", () => HOC(AdminCallCenterAccountDetails), () => AdminCallCenterAccountDetails);
     Navigation.registerComponent("AdminCallCenterOrdersAndNoOrders", () => HOC(AdminCallCenterOrdersAndNoOrders), () => AdminCallCenterOrdersAndNoOrders);
     Navigation.registerComponent("AdminShowCallCenters", () => HOC(AdminShowCallCenters), () => AdminShowCallCenters);
     Navigation.registerComponent("CallCenterAccountDetails", () => HOC(CallCenterAccountDetails), () => CallCenterAccountDetails);
     Navigation.registerComponent("CallCenterActiveAccounts", () => HOC(CallCenterActiveAccounts), () => CallCenterActiveAccounts);
     Navigation.registerComponent("CallCenterMakeOrder", () => HOC(CallCenterMakeOrder), () => CallCenterMakeOrder);
     Navigation.registerComponent("CallCenterHome", () => HOC(CallCenterHome), () => CallCenterHome);
     Navigation.registerComponent("AdminWarehouseDetails", () => HOC(AdminWarehouseDetails), () => AdminWarehouseDetails);
     Navigation.registerComponent("WarehouseDrivers", () => HOC(WarehouseDrivers), () => WarehouseDrivers);
     Navigation.registerComponent("AdminDriversSystemChecking", () => HOC(AdminDriversSystemChecking), () => AdminDriversSystemChecking);
     Navigation.registerComponent("AdminAssignTankToWarehouse", () => HOC(AdminAssignTankToWarehouse), () => AdminAssignTankToWarehouse);
     Navigation.registerComponent("AdminWarehouses", () => HOC(AdminWarehouses), () => AdminWarehouses);
     Navigation.registerComponent("DriverOrderDetails", () => HOC(DriverOrderDetails), () => DriverOrderDetails);
     Navigation.registerComponent("OperationWarehouseMember", () => HOC(OperationWarehouseMember), () => OperationWarehouseMember);
     Navigation.registerComponent("OperationWarehouses", () => HOC(OperationWarehouses), () => OperationWarehouses);
     Navigation.registerComponent("WarehouseGetMyShipment", () => HOC(WarehouseGetMyShipment), () => WarehouseGetMyShipment);
     Navigation.registerComponent("WarehouseGetMyOrders", () => HOC(WarehouseGetMyOrders), () => WarehouseGetMyOrders);
     Navigation.registerComponent("WarehouseShipment", () => HOC(WarehouseShipment), () => WarehouseShipment);
     Navigation.registerComponent("WarehouseDriverCollections", () => HOC(WarehouseDriverCollections), () => WarehouseDriverCollections);
     Navigation.registerComponent("WarehouseHome", () => HOC(WarehouseHome), () => WarehouseHome);
     Navigation.registerComponent("OperationAssignResturantToPurchasing", () => HOC(OperationAssignResturantToPurchasing), () => OperationAssignResturantToPurchasing);
     Navigation.registerComponent("DriverCompletedOrders", () => HOC(DriverCompletedOrders), () => DriverCompletedOrders);
     Navigation.registerComponent("OperationPurchasings", () => HOC(OperationPurchasings), () => OperationPurchasings);
     Navigation.registerComponent("OperationSurveys", () => HOC(OperationSurveys), () => OperationSurveys);
     Navigation.registerComponent("PurchasingOutlitesInfo", () => HOC(PurchasingOutlitesInfo), () => PurchasingOutlitesInfo);
     Navigation.registerComponent("SplashScreen", () => HOC(SplashScreen), () => SplashScreen);
     Navigation.registerComponent("MenuContent", () => HOC(MenuContent), () => MenuContent);
     Navigation.registerComponent("Login", () => HOC(Login), () => Login);
     Navigation.registerComponent("OperationHome", () => HOC(OperationHome), () => OperationHome)
     Navigation.registerComponent("Visits", () => HOC(Visits), () => Visits);
     Navigation.registerComponent("SelectLanguage", () => HOC(SelectLanguage), () => SelectLanguage);
     Navigation.registerComponent("ContactUs", () => HOC(ContactUs), () => ContactUs);
     Navigation.registerComponent("Notifications", () => HOC(Notifications), () => Notifications);
     Navigation.registerComponent("UpdateProfile", () => HOC(UpdateProfile), () => UpdateProfile);
     Navigation.registerComponent("PurchasingAccountsList", () => HOC(PurchasingAccountsList), () => PurchasingAccountsList);
     Navigation.registerComponent("AdminAddMember", () => HOC(AdminAddMember), () => AdminAddMember);
     Navigation.registerComponent("History", () => HOC(History), () => History);
     Navigation.registerComponent("CancelOrder", () => HOC(CancelOrder), () => CancelOrder);
     Navigation.registerComponent("AddResturantAccount", () => HOC(AddResturantAccount), () => AddResturantAccount);
     Navigation.registerComponent("DriverOrders", () => HOC(DriverOrders), () => DriverOrders);
     Navigation.registerComponent("OperationAccounts", () => HOC(OperationAccounts), () => OperationAccounts);
     Navigation.registerComponent("ResturantHome", () => HOC(ResturantHome), () => ResturantHome);
     Navigation.registerComponent("Users", () => HOC(Users), () => Users);
     Navigation.registerComponent("AccountDetails", () => HOC(AccountDetails), () => AccountDetails);
     Navigation.registerComponent("DriverUpdateOrder", () => HOC(DriverUpdateOrder), () => DriverUpdateOrder);
     Navigation.registerComponent("DriverHome", () => HOC(DriverHome), () => DriverHome);
     Navigation.registerComponent("AdminHome", () => HOC(AdminHome), () => AdminHome);
     Navigation.registerComponent("PurchasingHome", () => HOC(PurchasingHome), () => PurchasingHome);
     Navigation.registerComponent("SurveyHome", () => HOC(SurveyHome), () => SurveyHome);
     Navigation.registerComponent("MakeOrder", () => HOC(MakeOrder), () => MakeOrder);
     Navigation.registerComponent("OperationAssignAccountToPurchasing", () => HOC(OperationAssignAccountToPurchasing), () => OperationAssignAccountToPurchasing);
     Navigation.registerComponent("OperationConfirmResturantAccount", () => HOC(OperationConfirmResturantAccount), () => OperationConfirmResturantAccount);
     Navigation.registerComponent("OperationAssignDrivers", () => HOC(OperationAssignDrivers), () => OperationAssignDrivers);
     Navigation.registerComponent("OperationOrders", () => HOC(OperationOrders), () => OperationOrders);
     Navigation.registerComponent("OperationEdit_Reassign_DeleteOrder", () => HOC(OperationEdit_Reassign_DeleteOrder), () => OperationEdit_Reassign_DeleteOrder);
     Navigation.registerComponent("OrderDetails", () => HOC(OrderDetails), () => OrderDetails);
     Navigation.registerComponent("OperationAssignOrderToDriver", () => HOC(OperationAssignOrderToDriver), () => OperationAssignOrderToDriver);
     Navigation.registerComponent("OperationDrivers", () => HOC(OperationDrivers), () => OperationDrivers);
     Navigation.registerComponent("OperationActiveAccounts", () => HOC(OperationActiveAccounts), () => OperationActiveAccounts);
     Navigation.registerComponent("ResturantAccountDetails", () => HOC(ResturantAccountDetails), () => ResturantAccountDetails);
     Navigation.registerComponent("AdminNewsList", () => HOC(AdminNewsList), () => AdminNewsList);
     Navigation.registerComponent("AdminAddNews", () => HOC(AdminAddNews), () => AdminAddNews);
     Navigation.registerComponent("AdminEditSlider", () => HOC(AdminEditSlider), () => AdminEditSlider);
     Navigation.registerComponent("UpdateResturantAccount", () => HOC(UpdateResturantAccount), () => UpdateResturantAccount);
     Navigation.registerComponent("AdminUpdateNews", () => HOC(AdminUpdateNews), () => AdminUpdateNews);
     Navigation.registerComponent("OperationCollectedOrderDeails", () => HOC(OperationCollectedOrderDeails), () => OperationCollectedOrderDeails);
     Navigation.registerComponent("AdimnShowAllMembers", () => HOC(AdimnShowAllMembers), () => AdimnShowAllMembers);
     Navigation.registerComponent("AdminUpdateMemberProfile", () => HOC(AdminUpdateMemberProfile), () => AdminUpdateMemberProfile);
     Navigation.registerComponent("AdimnShowDriversLocation", () => HOC(AdimnShowDriversLocation), () => AdimnShowDriversLocation);
     Navigation.registerComponent("OperationShowContacts", () => HOC(OperationShowContacts), () => OperationShowContacts);
     Navigation.registerComponent("RegisterResturantAccount", () => HOC(RegisterResturantAccount), () => RegisterResturantAccount);
     Navigation.registerComponent("AdminGetAllShipping", () => HOC(AdminGetAllShipping), () => AdminGetAllShipping);
     Navigation.registerComponent("AdminUpdateShipping", () => HOC(AdminUpdateShipping), () => AdminUpdateShipping);

}

