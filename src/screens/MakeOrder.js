import React, { Component } from 'react';
import {
    View, TouchableOpacity, Image, Text, ScrollView, TextInput, Alert, Platform
} from 'react-native';
import { connect } from 'react-redux';
import { Icon, Button } from 'native-base';
import { login } from '../actions/AuthActions'
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import Strings from '../assets/strings';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import { enableSideMenu, resetTo, push, pop } from '../controlls/NavigationControll'
import { arrabicFont, englishFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import * as colors from '../assets/colors';
import { removeItem } from '../actions/MenuActions';
import CollaspeAppHeader from '../common/CollaspeAppHeader'
import LinearGradient from 'react-native-linear-gradient';
import FastImage from 'react-native-fast-image'
import CommanHeader from '../common/CommanHeader'
import ImagePicker from 'react-native-image-crop-picker';
import AppFooter from '../components/AppFooter'
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import io from 'socket.io-client';
import axios from 'axios';
import { BASE_END_POINT } from '../AppConfig'
import { RNToasty } from 'react-native-toasty';
import DateTimePicker from '@react-native-community/datetimepicker';
import { TextInputMask } from 'react-native-masked-text'
import DateTimePickerModal from "react-native-modal-datetime-picker";
import { whiteButton, greenButton } from '../assets/styles';
import RNPickerSelect from 'react-native-picker-select';
import Carousel from 'react-native-snap-carousel';
import Header from '../common/Header'

const dataa = [
    { label: '10', value: 10 },
    { label: '10', value: 10 },
    { label: '10', value: 10 },
    { label: '10', value: 10 },
    { label: '10', value: 10 },
    { label: '10', value: 10 },
    { label: '20', value: 10 },
]

class MakeOrder extends Component {

    socket = null;
    state = {
        confirmOnly: false,
        index: null,
        amount: null,
        price: null,
        date: null,
        selectedResturant: false,
        resturants: [],
        loading: true,
        branch: 0, // null if more branches
        branchAddress: null,
        disable: false,
        datePicker: new Date((new Date().getFullYear()) + '/' + (new Date().getMonth() + 1) + '/' + (parseInt(new Date().getDate()) + 2)),
        showDatePicker: false,
        selectedDate: Strings.selectDate,
        calendr: '',
        selectedResturantPrice: '00.00',
        minimumDate: (new Date().getFullYear()) + '/' + new Date().getMonth() + '/' + (parseInt(new Date().getDate()) + 2),
        indexState: 0,

        sliderIndex: 0,

        id: this.props.currentUser.user.type == 'USER' ? this.props.currentUser.user.restaurant.id : null,
    }

    componentDidMount() {
        var d = new Date()
        //console.log('jjjjjjjjjj',d)
        // Wed Feb 29 2012 11:00:00 GMT+1100 (EST)

        d.setDate(d.getDate() + 1)
        console.log(d)
        const { selectedResturant } = this.state
        enableSideMenu(false, null)
        this.getResutrants()

        this.socket = io("https://api.zeitcoapp.com/order", { query: `id=${this.props.currentUser.user.id}` });
        //this.socket = io("https://zeitco.herokuapp.com/order", { query: `id=${this.props.currentUser.user.id}` });

        this.socket.once('newOrder', (data) => {

            console.log("ORDER  newOrder  ", data)
            if (this.state.confirmOnly) {
                pop()
            } else {
                if (this.props.currentUser.user.type == 'USER') {
                    pop()
                } else {
                    const d = {
                        home: true,
                        ...data.order[0]
                    }
                    push('OperationDrivers', d)

                }
            }

            this.setState({ disable: false })

        });



    }


    getResutrants = () => {
        const { isRTL } = this.props
        axios.get(`${BASE_END_POINT}restaurant?status=APPROVED&withoutPagenation=true`)
            .then(response => {
                // Alert.alert("Done")
                console.log('All All RES : ', response.data)
                var rests = []
                response.data.map((item) => {
                    //rests.push({ label: item.restaurantName, value: item.id, resturant: item, price: item.price, branches: item.branches, id: item.id })
                    console.log('item', item)
                    rests.push({ id: item.id, name: isRTL ? item.restaurantName_ar : item.restaurantName, resturant: item, price: item.price, branches: item.branches })
                })
                console.log('rests')
                this.setState({
                    loading: false,
                    resturants: [
                        {
                            name: Strings.restaurants,
                            id: 0,
                            children: rests
                        }
                    ],
                    // resturants: rests
                })
            })
            .catch(error => {
                console.log("ERROR  ", error.response)
                this.setState({ loading: false })
            })
    }


    resturantPicker = () => {
        const { isRTL } = this.props
        const { selectedResturant, resturants, loading } = this.state
        return (
            <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', width: responsiveWidth(80), height: responsiveHeight(6), marginTop: moderateScale(5), alignItems: 'center', justifyContent: 'center', alignSelf: 'center' }} >

                <View style={{ marginTop: moderateScale(12), width: responsiveWidth(80), height: responsiveHeight(6), }}>

                    <View style={{ alignItems: 'center', justifyContent: 'center', backgroundColor: colors.lightGray, width: responsiveWidth(80), height: responsiveHeight(5), alignSelf: 'center' }}>



                        <SectionedMultiSelect
                            expandDropDowns
                            //modalAnimationType='slide'
                            //loading={loading}
                            showDropDowns={false}
                            modalWithTouchable
                            hideConfirm
                            searchPlaceholderText={Strings.search}
                            styles={{
                                selectToggle: { width: responsiveWidth(74), borderRadius: moderateScale(3), alignItems: 'center', justifyContent: 'center', },
                                selectToggleText: { textAlign: isRTL ? 'right' : 'left', fontSize: responsiveFontSize(6), color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont },
                                subItemText: { textAlign: isRTL ? 'right' : 'left' },
                                itemText: { fontSize: responsiveFontSize(10), textAlign: isRTL ? 'right' : 'left' },
                                container: { height: responsiveHeight(60), position: 'absolute', width: responsiveWidth(80), top: responsiveHeight(18), alignSelf: 'center' },
                                searchTextInput: { textAlign: isRTL ? 'right' : 'left', marginHorizontal: moderateScale(5) },
                            }}
                            items={resturants}
                            alwaysShowSelectText
                            single
                            searchPlaceholderText={Strings.search}
                            uniqueKey="id"
                            subKey="children"
                            selectText={this.state.selectedResturant ? this.state.selectedResturant : Strings.restaurants}

                            readOnlyHeadings={true}
                            onSelectedItemsChange={(selectedItems) => {
                                // this.setState({ countries: selectedItems });
                            }
                            }
                            onSelectedItemObjectsChange={(selectedItems) => {
                                console.log("ITEM2   ", selectedItems[0])
                                this.setState({ id: selectedItems[0].id, selectedResturant: selectedItems[0].name, selectedResturantPrice: selectedItems[0].price });
                            }
                            }

                        //onConfirm={() => this.setState({ selecTitle: '' })}
                        />

                    </View>
                    {selectedResturant == null &&
                        <Text style={{ color: 'red', marginTop: moderateScale(1), marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                    }
                </View>
            </View>
        )
    }

    resturantPickerold = () => {
        const { isRTL } = this.props
        const { selectedResturant, resturants, loading } = this.state
        return (
            <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', width: responsiveWidth(80), alignSelf: 'center' }} >

                <View animation={isRTL ? "slideInRight" : "slideInLeft"} duration={1000} style={{ marginTop: moderateScale(12), width: responsiveWidth(80), }}>

                    {/* 
                    <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.sel}</Text>
                    */}

                    <View style={{ alignItems: 'center', justifyContent: 'center', backgroundColor: colors.lightGray, width: responsiveWidth(80), height: responsiveHeight(6), alignSelf: 'center' }}>


                        <RNPickerSelect
                            onValueChange={
                                (item, index) => {
                                    this.setState({
                                        selectedResturantPrice: resturants[index - 1].price,
                                        selectedResturant: resturants[index - 1]
                                    });
                                    //  alert(item.label)
                                    console.log(resturants[index - 1].resturant)
                                    // this.addRequiredAddress(item)
                                    //this.pushCoordinate(item)
                                }}
                            items={resturants}

                            placeholder={{ label: Strings.selectResutant, value: '' }}
                            style={{
                                inputIOS: { textAlign: isRTL ? 'right' : 'left', fontSize: responsiveFontSize(6), height: responsiveHeight(6), fontFamily: isRTL ? arrabicFont : englishFont, paddingVertical: 9, paddingHorizontal: moderateScale(3), paddingRight: responsiveWidth(5), color: 'gray', },
                                inputAndroid: { textAlign: isRTL ? 'right' : 'left', fontSize: responsiveFontSize(6), height: responsiveHeight(6), fontFamily: isRTL ? arrabicFont : englishFont, paddingVertical: 9, paddingHorizontal: moderateScale(3), paddingRight: responsiveWidth(5), color: 'gray', }
                            }}
                            placeholderTextColor={'gray'}
                            Icon={() => { return (<Icon name='down' type='AntDesign' style={{ color: 'black', fontSize: responsiveFontSize(6), top: responsiveHeight(2), right: responsiveWidth(1) }} />) }}
                        />


                        {/*<SectionedMultiSelect
                            styles={{
                                selectToggle: { width: responsiveWidth(78), height: responsiveHeight(6), borderRadius: moderateScale(3), alignItems: 'center', justifyContent: 'center' },
                                selectToggleText: { textAlign: isRTL ? 'right' : 'left', fontSize: responsiveFontSize(6), color: 'gray', marginTop: moderateScale(8), fontFamily: isRTL ? arrabicFont : englishFont },
                                subItemText: { textAlign: isRTL ? 'right' : 'left' },
                                itemText: { fontSize: responsiveFontSize(10), textAlign: isRTL ? 'right' : 'left' },
                                container: { height: responsiveHeight(75), position: 'absolute', width: responsiveWidth(85), top: responsiveHeight(13), alignSelf: 'center' },
                                searchTextInput: { textAlign: isRTL ? 'right' : 'left', marginHorizontal: moderateScale(5) },

                            }}

                            items={resturants}
                            alwaysShowSelectText
                            single
                            highlightChildren
                            uniqueKey="id"
                            subKey="children"
                            displayKey="restaurantName"

                            expandDropDowns
                            //modalAnimationType='slide'
                            loading={loading}
                            showDropDowns={false}
                            modalWithTouchable
                            hideConfirm
                            searchPlaceholderText={Strings.search}
                            selectText={selectedResturant ? selectedResturant.restaurantName : Strings.selectResutant}
                            readOnlyHeadings={true}
                            onSelectedItemsChange={(selectedItems) => {
                                // this.setState({ countries: selectedItems });
                            }
                            }
                            onSelectedItemObjectsChange={(selectedItems) => {
                                console.log("ITEM2   ", selectedItems[0].name)
                                this.setState({ selectedResturant: selectedItems[0], selectedResturantPrice: selectedItems[0].price });

                            }
                            }

                            onConfirm={() => this.setState({ selectedResturant: null })}
                        />*/}

                    </View>
                    {selectedResturant == null &&
                        <Text style={{ color: 'red', marginTop: moderateScale(1), marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                    }
                </View>
            </View>
        )
    }




    estimatedPrice = () => {
        const { isRTL } = this.props
        const { selectedResturantPrice, amount } = this.state
        return (
            <View style={{ width: responsiveWidth(80), alignSelf: 'center', alignItems: 'center', justifyContent: 'center', marginTop: moderateScale(20) }}>
                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(10), textAlign: 'center', color: colors.green }}> {Strings.estimatedPrice}</Text>
                <View style={{ borderRadius: moderateScale(1), alignSelf: 'center', alignItems: 'center', justifyContent: 'center', marginTop: moderateScale(5) }} >

                    <TextInput
                        editable={false}
                        value={(((selectedResturantPrice * amount).toFixed(1)).toString())}
                        style={{ color: colors.darkGray, width: responsiveWidth(35), paddingHorizontal: moderateScale(3), paddingVertical: 0, fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(6), textAlign: 'center', borderColor: 'black', borderWidth: 0, fontSize: responsiveFontSize(7) }}
                    />
                </View>
            </View>
        )
    }

    quantity = () => {
        const { isRTL } = this.props
        const { amount, index, price, sliderIndex } = this.state
        const data = []
        for (let index = 10; index < 1001; index = index + 10) {
            data.push({ amount: index, price: 10 })

        }
        return (
            <View style={{ width: responsiveWidth(100) }}>
                <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(10), alignSelf: 'center', marginTop: moderateScale(12), color: colors.green }} >{Strings.orderQtyKg}</Text>
                <Icon name='arrowdown' type='AntDesign' style={{ color: '#a9d3a0', fontWeight: 'bold', marginTop: moderateScale(5), fontSize: responsiveFontSize(12), alignSelf: 'center' }} />
                <View style={{ marginTop: moderateScale(1) }}>
                    <Carousel
                        activeSlideOffset={1}
                        inactiveSlideScale={0.8}
                        firstItem={0}
                        ref={(c) => { this._carousel = c; }}
                        data={data}
                        renderItem={({ item, index }) => <Text style={{ borderColor: colors.lightGray, borderWidth: sliderIndex == index ? 0 : 1, backgroundColor: sliderIndex == index ? '#a9d3a0' : 'transparent', textAlign: 'center', paddingVertical: moderateScale(sliderIndex == index ? 3 : 2), fontSize: responsiveFontSize(10), fontWeight: 'bold', color: sliderIndex == index ? 'white' : colors.darkGray, fontFamily: isRTL ? arrabicFont : englishFont, }} >{item.amount}</Text>}
                        sliderWidth={responsiveWidth(100)}
                        itemWidth={responsiveWidth(15)}
                        onSnapToItem={(index) => {

                            this.setState({ sliderIndex: index, index: index, amount: data[index].amount, price: data[index].price })
                        }}
                    />
                </View>
                {/*<ScrollView style={{ marginTop: moderateScale(10), marginHorizontal: moderateScale(5), height: responsiveHeight(6), borderColor: 'gray', borderTopWidth: 0.5, borderBottomWidth: 0.5 }} horizontal showsHorizontalScrollIndicator={false} >
                    {data.map((val, i) => (

                        <TouchableOpacity onPress={() => {
                            this.setState({ index: i, amount: val.amount, price: val.price })

                        }} style={{ padding: moderateScale(5), alignSelf: 'center', marginHorizontal: moderateScale(2), height: responsiveHeight(5.8), justifyContent: 'center', alignItems: 'center', backgroundColor: index == i ? '#a9d3a0' : 'white' }} >
                            <Text style={{ color: colors.darkGray, fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(10), height: responsiveHeight(5.8), lineHeight: responsiveHeight(5.8) }}>{val.amount}</Text>
                        </TouchableOpacity>
                    ))}
                    </ScrollView>*/}
            </View>
        )
    }



    resturantBranches = () => {
        const { isRTL, currentUser } = this.props
        const { selectedResturant, branch, branchAddress } = this.state

        return (
            <View style={{ width: responsiveWidth(100) }}>
                <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(15), alignSelf: 'center', marginTop: moderateScale(12), }} >{Strings.branch}</Text>

                <ScrollView contentContainerStyle={{ justifyContent: 'center', alignItems: 'center' }} style={{ alignSelf: 'center', marginTop: moderateScale(10), marginHorizontal: moderateScale(5) }} horizontal showsHorizontalScrollIndicator={false} >
                    {
                        selectedResturant.branches.map((val, i) => (
                            <TouchableOpacity onPress={() => {
                                console.log('ADDRESSS    ', val)
                                this.setState({ branch: i, branchAddress: val.address })
                                console.log('ADDRESSS2    ', this.state.branchAddress)
                            }} style={{ padding: moderateScale(5), alignSelf: 'center', marginHorizontal: moderateScale(2), height: responsiveHeight(6), justifyContent: 'center', alignItems: 'center', backgroundColor: branch == i ? colors.lightGreenButton : 'white', borderWidth: 1, borderColor: colors.lightGreenButton, borderRadius: moderateScale(1) }} >
                                <Text style={{ color: branch == i ? colors.white : colors.green, fontFamily: isRTL ? arrabicFont : englishFont }}>{i + 1}</Text>
                            </TouchableOpacity>
                        ))
                    }
                </ScrollView>
                {branch != null &&
                    <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), alignSelf: 'center', marginTop: moderateScale(12) }}>{branchAddress}</Text>
                }
            </View>
        )
    }

    myBranches = () => {
        const { isRTL, currentUser } = this.props
        const { selectedResturant, branch, branchAddress } = this.state

        return (
            <View style={{ width: responsiveWidth(100) }}>
                <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(15), alignSelf: 'center', marginTop: moderateScale(12), }} >{Strings.branch}</Text>

                <ScrollView contentContainerStyle={{ justifyContent: 'center', alignItems: 'center' }} style={{ alignSelf: 'center', marginTop: moderateScale(10), marginHorizontal: moderateScale(5) }} horizontal showsHorizontalScrollIndicator={false} >
                    {
                        currentUser.user.restaurant.branches.map((val, i) => (
                            <TouchableOpacity onPress={() => {
                                console.log('ADDRESSS    ', val)
                                this.setState({ branch: i, branchAddress: val.address })
                                console.log('ADDRESSS2    ', this.state.branchAddress)
                            }} style={{ padding: moderateScale(5), alignSelf: 'center', marginHorizontal: moderateScale(2), height: responsiveHeight(6), justifyContent: 'center', alignItems: 'center', backgroundColor: branch == i ? colors.lightGreenButton : 'white', borderWidth: 1, borderColor: colors.lightGreenButton, borderRadius: moderateScale(1) }} >
                                <Text style={{ color: branch == i ? colors.white : colors.green, fontFamily: isRTL ? arrabicFont : englishFont }}>{i + 1}</Text>
                            </TouchableOpacity>
                        ))
                    }

                </ScrollView>
                {branch != null &&
                    <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), alignSelf: 'center', marginTop: moderateScale(12) }}>{branchAddress}</Text>
                }
            </View>
        )
    }


    date = () => {
        const { isRTL } = this.props
        const { date, selectedDate, dateActive } = this.state

        return (
            <View style={{ width: responsiveWidth(100), marginTop: moderateScale(15), }}>
                <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(10), alignSelf: 'center', color: colors.green }} >{Strings.date}</Text>

                <View style={{ alignSelf: 'center', margin: moderateScale(10), flexDirection: isRTL ? 'row-reverse' : 'row' }} >

                    <TouchableOpacity onPress={() => {
                        this.setState({ selectedDate: Strings.selectDate, dateActive: 'today', date: new Date().getFullYear() + "-" + ((new Date().getMonth() + 1) < 10 ? ('0'+(new Date().getMonth() + 1)): (new Date().getMonth() + 1)) + "-" + (new Date().getDate() <10 ? ('0'+new Date().getDate()) : new Date().getDate())})
                    }} style={{ borderRadius: 0, padding: moderateScale(2), alignSelf: 'center', height: responsiveHeight(6), width: responsiveWidth(30), justifyContent: 'center', alignItems: 'center', backgroundColor: dateActive == 'today' ? '#a9d3a0' : 'white', borderColor: colors.darkGray, borderWidth: 0.5 }} >
                        <Text style={{ color: colors.darkGray, fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(7), height: responsiveHeight(6), lineHeight: responsiveHeight(6) }}>{Strings.today}</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => {
                        var date = new Date()
                        date.setDate(date.getDate() + 1)
                        this.setState({ selectedDate: Strings.selectDate, dateActive: 'tomorrow', date: date.getFullYear() + "-" + ((date.getMonth() + 1) < 10 ? ('0'+(date.getMonth() + 1)): (date.getMonth() + 1)) + "-" + (date.getDate() <10 ? ('0'+date.getDate()) : date.getDate()) })
                    }} style={{ borderRadius: 0, padding: moderateScale(2), alignSelf: 'center', height: responsiveHeight(6), width: responsiveWidth(30), justifyContent: 'center', alignItems: 'center', backgroundColor: dateActive == 'tomorrow' ? '#a9d3a0' : 'white', borderColor: colors.darkGray, borderWidth: 0.5 }} >
                        <Text style={{ color: colors.darkGray, fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(7), height: responsiveHeight(6), lineHeight: responsiveHeight(6) }}>{Strings.tomorrow}</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => {
                        this.setState({ dateActive: 'aftertomorrow', date: 'aftertomorrow', showDatePicker: true })
                    }} style={{ borderRadius: 0, padding: moderateScale(2), alignSelf: 'center', height: responsiveHeight(6), width: responsiveWidth(30), justifyContent: 'center', alignItems: 'center', backgroundColor: dateActive == 'aftertomorrow' ? '#a9d3a0' : 'white', borderColor: colors.darkGray, borderWidth: 0.5 }} >
                        <Text style={{ color: colors.darkGray, fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(7), height: responsiveHeight(6), lineHeight: responsiveHeight(6) }}>{selectedDate}</Text>
                    </TouchableOpacity>

                </View>

            </View>
        )
    }


    fromDatePicker = () => {
        const { showFromDataPicker, fromDate, from } = this.state
        return (
            showFromDataPicker ?
                <DateTimePicker
                    testID="dateTimePicker"
                    timeZoneOffsetInMinutes={0}
                    value={fromDate}
                    //mode={mode}
                    is24Hour={true}
                    display="default"
                    onChange={(event, date) => {
                        console.log("DATE   ", date.getMonth())
                        const d = date.getFullYear() + "-" + ((date.getMonth() + 1) < 10 ? ('0'+(date.getMonth() + 1)): (date.getMonth() + 1)) + "-" + (date.getDate() <10 ? ('0'+date.getDate()) : date.getDate())
                        this.setState({ from: d, fromDate: date, showFromDataPicker: false })
                    }}
                />
                :
                null
        )
    }

    datePickerr = () => {
        const { showDatePicker, datePicker } = this.state
        return (
            showDatePicker ?
                <DateTimePicker
                    testID="dateTimePicker"
                    timeZoneOffsetInMinutes={0}
                    value={datePicker}
                    //mode={mode}
                    is24Hour={true}
                    display="default"
                    onChange={(event, date) => {
                        console.log("DATE   ", date.getMonth())
                        const d = date.getFullYear() + "-" + ((date.getMonth() + 1) < 10 ? ('0'+(date.getMonth() + 1)): (date.getMonth() + 1)) + "-" + (date.getDate() <10 ? ('0'+date.getDate()) : date.getDate())
                        this.setState({ date: d, datePicker: date, selectedDate: d, showDatePicker: false })
                    }}
                />
                :
                null
        )
    }



    datePicker = () => {
        const { showDatePicker, datePicker, minimumDate } = this.state
        return (<DateTimePickerModal
            isVisible={showDatePicker}
            mode="date"
            date={datePicker}
            onConfirm={(date) => {
                const d = date.getFullYear() + "-" + ((date.getMonth() + 1) < 10 ? ('0'+(date.getMonth() + 1)): (date.getMonth() + 1))+ "-" + (date.getDate() <10 ? ('0'+date.getDate()) : date.getDate())
                this.setState({ showDatePicker: false, date: d, selectedDate: d, datePicker: date })
                console.log(d)
            }
            }
            onCancel={() => this.setState({ showDatePicker: false })}
            minimumDate={new Date(minimumDate)}
            confirmTextIOS={Strings.confirm}
            cancelTextIOS={Strings.cancel}
            headerTextIOS={Strings.chooseDate}
        />
        )
    }

    sendOrder = () => {
        const { currentUser } = this.props
        const { amount, price, date, selectedResturant, branch, id } = this.state
        console.log('branch:::', id + "   ", selectedResturant, " ", amount, " ", date, " ", price, " ", currentUser.user.id, " ", branch)
        if (price && date && id && branch != null) {
            //Alert.alert(currentUser.user.type=='USER'?""+currentUser.user.id:""+selectedResturant.id,)
            this.setState({ disable: true })
            this.socket.emit("newOrder", {
                restaurant: currentUser.user.type == 'USER' ? currentUser.user.restaurant.id : id, //resturant id,
                average: amount,
                date: date,
                //price: price ,
                userId: currentUser.user.id,
                branch: branch
            });
            console.log("Done")
            RNToasty.Success({ title: Strings.addOrderSuccessfuly })
            if (this.props.currentUser.user.type == 'USER') {
                pop()
            }
        } else {
            RNToasty.Error({ title: Strings.insertTheRequiredData })
        }
        //pop()
    }


    submit_cancel_buttons = () => {
        const { isRTL } = this.props
        return (
            <View style={{ alignSelf: 'center', margin: moderateScale(10), marginBottom: moderateScale(20), flexDirection: isRTL ? 'row-reverse' : 'row' }} >

                {this.props.currentUser.user.type != 'USER' &&
                    <TouchableOpacity onPress={() => {
                        this.sendOrder()
                    }} disabled={this.state.enable} style={{ ...whiteButton, width: responsiveWidth(35) }} >
                        <Text style={{ color: colors.green, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.confirmAndAsign}</Text>
                    </TouchableOpacity>
                }

                <TouchableOpacity onPress={() => {
                    this.setState({ confirmOnly: true })
                    setTimeout(() => {
                        this.sendOrder()
                    }, 1000)

                }} disabled={this.state.enable} style={{ ...whiteButton, width: responsiveWidth(25) }} >
                    <Text style={{ color: colors.green, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.confirm}</Text>
                </TouchableOpacity>

                {/*<TouchableOpacity onPress={() => {
                    pop()
                }} style={{...greenButton,width:responsiveWidth(25)}} >
                    <Text style={{ color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.cancel}</Text>
            </TouchableOpacity>*/}


            </View>
        )
    }




    render() {
        const { isRTL, userToken, currentUser } = this.props;
        const { selectedResturant, sliderIndex } = this.state;
        return (
            <LinearGradient

                colors={[colors.white, colors.white, colors.white]}
                style={{ flex: 1 }}
            >
                <Header title={currentUser.user.type != 'USER' ? Strings.manualOrder : Strings.collectionRequest} />
                <ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: colors.white, borderTopRightRadius: moderateScale(20), borderTopLeftRadius: moderateScale(20), marginTop: moderateScale(0), width: responsiveWidth(100), marginBottom: moderateScale(20) }} >




                    {currentUser.user.type != 'USER' && this.resturantPicker()}
                    {/*this.props.currentUser.user.type == 'USER' && this.myBranches()*/}
                    {/*selectedResturant && this.resturantBranches()*/}

                    {this.quantity()}
                    {currentUser.user.type != 'USER' && this.estimatedPrice()}
                    {this.date()}
                    {this.submit_cancel_buttons()}
                    {this.datePicker()}
                </ScrollView>

                <AppFooter />
            </LinearGradient>
        );
    }
}
const mapDispatchToProps = {

}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
    //userToken: state.auth.userToken,
})


export default connect(mapToStateProps, mapDispatchToProps)(MakeOrder);

