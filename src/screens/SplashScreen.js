import React, { Component } from 'react';
import AsyncStorage from '@react-native-community/async-storage'
import {
    View, Image, ImageBackground, TouchableNativeFeedback, Text, ScrollView
} from 'react-native';
import { connect } from 'react-redux';
import { responsiveHeight, responsiveWidth, responsiveFontSize, moderateScale } from "../utils/responsiveDimensions";
import Strings from '../assets/strings';
import { setUser, userToken } from '../actions/AuthActions';
import { changeLanguage, changeColor } from '../actions/LanguageActions';
import { enableSideMenu, push, resetTo } from '../controlls/NavigationControll'
import { selectMenu } from '../actions/MenuActions';
//import firebase,{Notification } from 'react-native-firebase';
import FastImage from 'react-native-fast-image'
import LinearGradient from 'react-native-linear-gradient'
import { Button } from 'native-base'
import { arrabicFont, englishFont } from '../common/AppFont'
import * as colors from '../assets/colors'
import {
    getFirebaseNotificationToken, 
    showFirebaseNotifcation,
    clickOnFirebaseNotification,
} from '../controlls/FirebasePushNotificationControll'

class SplashScreen extends Component {


    
    componentDidMount() {
        this.checkLanguage();
        showFirebaseNotifcation()
        clickOnFirebaseNotification()
        enableSideMenu(false, null)  
        setTimeout(()=>{ this.checkLogin()},3000)
    }

    checkLogin = async () => {
        const userJSON = await AsyncStorage.getItem('USER');
        if (userJSON) {
            console.log("Exist User")
            const userInfo = JSON.parse(userJSON);
            this.props.setUser(userInfo);

            console.log(userInfo)
            if (userInfo.user.type == 'SURVEY') {
                resetTo('SurveyHome')
            } else if (userInfo.user.type == 'ADMIN') {
                resetTo('AdminHome')
            } else if (userInfo.user.type == 'USER') {
                resetTo('ResturantHome')
            } else if (userInfo.user.type == 'DRIVER') {
                resetTo('DriverHome')
            }
            else if (userInfo.user.type == 'OPERATION') {
                resetTo('OperationHome')
            }
            else if (userInfo.user.type == 'PURCHASING') {
                resetTo('PurchasingHome')
            }else if (userInfo.user.type == 'WAREHOUSE') {
                resetTo('WarehouseHome')
            }else if (userInfo.user.type == 'CALL-CENTER') {
                resetTo('CallCenterHome')
            }
            //resetTo('Home')
        } else {
            console.log("no User")
            resetTo('Login')
        }

    }

    checkLanguage = async () => {
        console.log("lang0   " + lang)
        const lang = await AsyncStorage.getItem('@lang');
        console.log("lang   " + lang)
        if (lang === 'en') {
            this.props.changeLanguage(false);
            Strings.setLanguage('en')
        } else {
            this.props.changeLanguage(true);
            Strings.setLanguage('ar')
        }

    }
    //source={require('../assets/imgs/splashBackground.png')}

    render() {
        return (
            <View
                style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>

                <FastImage
                    resizeMode='contain'
                    style={{ marginTop: moderateScale(12), height: responsiveHeight(40), width: responsiveWidth(45) }}
                    source={require('../assets/imgs/splashLogo.png')}
                />

                {/*<Text style={{ fontFamily: englishFont, color: colors.black, marginTop: moderateScale(2), fontSize: responsiveFontSize(22) }}>Zeitco</Text>
                <Text style={{ fontFamily: englishFont, color: colors.black, marginTop: moderateScale(-4), fontSize: responsiveFontSize(8) }}>OIL RECYCLING</Text>*/}


                {/*<Button onPress={() => {
                    this.checkLogin()
                }}
                    style={{ width: responsiveWidth(60), height: responsiveHeight(7), justifyContent: 'center', alignItems: 'center', borderRadius: moderateScale(3), backgroundColor: colors.sky }}
                >
                    <Text style={{ fontFamily: arrabicFont, color: colors.white }}>Admin</Text>
                </Button>*/}

            </View>
        );
    }
}

const mapToStateProps = state => ({
    currentUser: state.auth.currentUser,
    userTokens: state.auth.userToken,
})

const mapDispatchToProps = {
    setUser,
    changeLanguage,
    userToken,
    selectMenu,
}

export default connect(mapToStateProps, mapDispatchToProps)(SplashScreen);


