import React, { Component } from 'react';
import {
    View, TouchableOpacity, Image, Text, ScrollView, TextInput, Alert, Platform
} from 'react-native';
import { connect } from 'react-redux';
import { Icon, Button } from 'native-base';
import { login } from '../../actions/AuthActions'
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../../utils/responsiveDimensions";
import Strings from '../../assets/strings';
import LoadingDialogOverlay from '../../components/LoadingDialogOverlay';
import { enableSideMenu, resetTo, push, pop } from '../../controlls/NavigationControll'
import { arrabicFont, englishFont } from '../../common/AppFont'
import * as Animatable from 'react-native-animatable';
import * as colors from '../../assets/colors';
import { removeItem } from '../../actions/MenuActions';
import CollaspeAppHeader from '../../common/CollaspeAppHeader'
import LinearGradient from 'react-native-linear-gradient';
import FastImage from 'react-native-fast-image'
import CommanHeader from '../../common/CommanHeader'
import ImagePicker from 'react-native-image-crop-picker';
import AppFooter from '../../components/AppFooter'
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import io from 'socket.io-client';
import axios from 'axios';
import { BASE_END_POINT } from '../../AppConfig'
import { RNToasty } from 'react-native-toasty';
import DateTimePicker from '@react-native-community/datetimepicker';
import { TextInputMask } from 'react-native-masked-text'
import DateTimePickerModal from "react-native-modal-datetime-picker";
import { whiteButton, greenButton } from '../../assets/styles';
import RNPickerSelect from 'react-native-picker-select';
import Carousel from 'react-native-snap-carousel';
import { SpinPicker } from 'react-native-spin-picker'
import Header from '../../common/Header'


class AdminAssignTankToWarehouse extends Component {

    socket = null;
    vhicleNumber=null
    waybill=null
    tankNumber=null
    tankCapacity=null
    driverName=null
    state = {
        
        datePicker: new Date((new Date().getFullYear()) + '/' + (new Date().getMonth() + 1) + '/' + (parseInt(new Date().getDate()) + 2)),
        showDatePicker: false,
        selectedDate: null,
        calendr: '',
        
        tankNumber:' ',
        tankCapacity:' ',
        vehicleNumber:' ',
        waybill:' ',
        driverName:' ',
        loading:false,

        selectedItem1: 0,
        selectedItem2: 0,
        selectedItem3: 0,
        selectedItem4: 0,
        selectedItem5: 0,
        selectedKg: '0',
        kg:' ',

        selectedWarehouse:null,
        warehouses: [],
        warehouses2: [],
    }

    componentDidMount() {
        var d = new Date()
        d.setDate(d.getDate() + 1)
        console.log(d)
        enableSideMenu(false, null)
        this.getWarehouses()
    }

    getWarehouses = () => {
        axios.get(`${BASE_END_POINT}warehouses`,{
            headers:{
                Authorization: `Bearer ${this.props.currentUser.token}`
            }
        })
        .then(response => {
            console.log('Done    ', response.data)
            this.setState({warehouses:response.data})
            var w=[]
            response.data.filter((val)=>{
                w.push({ label: val.name, value: val.name })
            })
            this.setState({warehouses2:w})
        })
        .catch(error => {
            console.log('Error   ', error.response)
        })
    }

    warehousesPicker = () => {
        const { isRTL } = this.props
        const {warehouses2, warehouses, selectedWarehouse } = this.state
        return (
            <View style={{width: responsiveWidth(80), alignSelf: 'center' }} >
                 <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.selectWarehouse}</Text>
                <View animation={isRTL ? "slideInRight" : "slideInLeft"} duration={1000} style={{ marginTop: moderateScale(0), width: responsiveWidth(80), }}>

                    <View style={{ alignItems: 'center', justifyContent: 'center', backgroundColor: colors.lightGray, width: responsiveWidth(80), height: responsiveHeight(6), alignSelf: 'center' }}>


                        <RNPickerSelect
                            onValueChange={
                                (item, index) => {
                                    this.setState({
                                        selectedWarehouse: warehouses[index - 1]
                                    });               
                                    
                            }}
                            items={warehouses2}
                            placeholder={{ label: '', value: '' }}
                            style={{
                                inputIOS: { textAlign: isRTL ? 'right' : 'left', fontSize: responsiveFontSize(6), height: responsiveHeight(6), fontFamily: isRTL ? arrabicFont : englishFont, paddingVertical: 9, paddingHorizontal: moderateScale(3), paddingRight: responsiveWidth(5), color: 'gray', },
                                inputAndroid: { textAlign: isRTL ? 'right' : 'left', fontSize: responsiveFontSize(6), height: responsiveHeight(6), fontFamily: isRTL ? arrabicFont : englishFont, paddingVertical: 9, paddingHorizontal: moderateScale(3), paddingRight: responsiveWidth(5), color: 'gray', }
                            }}
                            placeholderTextColor={'gray'}
                            Icon={() => { return (<Icon name='down' type='AntDesign' style={{ color: 'black', fontSize: responsiveFontSize(6), top: responsiveHeight(2), right: responsiveWidth(1) }} />) }}
                        />


                       
                    </View>
                    {selectedWarehouse == false &&
                        <Text style={{ color: 'red', marginTop: moderateScale(1), marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                    }
                </View>
            </View>
        )
    }


    spinKg = () => {
        const { isRTL } = this.props
        const { selectedItem1, selectedItem2, selectedItem3, selectedItem4, selectedItem5 } = this.state
        return (
            <View style={{ width: responsiveWidth(20), alignSelf: 'center', marginTop: moderateScale(-5), justifyContent: 'center', alignItems: 'center' }}>
                <View style={{ flexDirection: 'row', width: responsiveWidth(20), alignSelf: 'center', marginTop: moderateScale(0), justifyContent: 'center', alignItems: 'center' }}>
                    <View style={{ alignSelf: 'center', marginTop: moderateScale(6), justifyContent: 'center', alignItems: 'center' }}>
                        <SpinPicker
                            data={[...Array(10).keys()]}
                            value={this.state.selectedItem1}
                            onValueChange={selectedItem => this.setState({ selectedItem1: selectedItem, kg: parseFloat(selectedItem1 + '' + selectedItem2 + '' + selectedItem3 + '' + selectedItem4 + '.' + selectedItem5) })}
                            keyExtractor={number => number.toString()}
                            //showArrows={Platform.OS=='android'?true:false}
                            onInputValueChanged={this.onValueChanged}
                            renderItem={item => <Text style={{color: colors.darkGray, textAlign: 'center', width: responsiveWidth(17), height: responsiveHeight(6), lineHeight: responsiveHeight(6), fontSize: responsiveFontSize(8) }}>{item.toString()}</Text>} />
                    </View>
                    <View style={{ alignSelf: 'center', marginTop: moderateScale(6), justifyContent: 'center', alignItems: 'center' }}>
                        <SpinPicker
                            data={[...Array(10).keys()]}
                            value={this.state.selectedItem2}
                            onValueChange={selectedItem => this.setState({ selectedItem2: selectedItem, kg: parseFloat(selectedItem1 + '' + selectedItem2 + '' + selectedItem3 + '' + selectedItem4 + '.' + selectedItem5) })}
                            keyExtractor={number => number.toString()}
                            //showArrows={Platform.OS=='android'?true:false}
                            onInputValueChanged={this.onValueChanged}
                            renderItem={item => <Text style={{color: colors.darkGray, textAlign: 'center', width: responsiveWidth(17), height: responsiveHeight(6), lineHeight: responsiveHeight(6), fontSize: responsiveFontSize(8) }}>{item.toString()}</Text>} />
                    </View>
                    <View style={{ alignSelf: 'center', marginTop: moderateScale(6), justifyContent: 'center', alignItems: 'center' }}>
                        <SpinPicker
                            data={[...Array(10).keys()]}
                            value={this.state.selectedItem3}
                            onValueChange={selectedItem => this.setState({ selectedItem3: selectedItem, kg: parseFloat(selectedItem1 + '' + selectedItem2 + '' + selectedItem3 + '' + selectedItem4 + '.' + selectedItem5) })}
                            keyExtractor={number => number.toString()}
                            //showArrows={Platform.OS=='android'?true:false}
                            onInputValueChanged={this.onValueChanged}
                            renderItem={item => <Text style={{color: colors.darkGray, textAlign: 'center', width: responsiveWidth(17), height: responsiveHeight(6), lineHeight: responsiveHeight(6), fontSize: responsiveFontSize(8) }}>{item.toString()}</Text>} />
                    </View>
                    <View style={{ alignSelf: 'center', marginTop: moderateScale(6), justifyContent: 'center', alignItems: 'center' }}>
                        <SpinPicker
                            data={[...Array(10).keys()]}
                            value={this.state.selectedItem4}
                            onValueChange={selectedItem => this.setState({ selectedItem4: selectedItem, kg: parseFloat(selectedItem1 + '' + selectedItem2 + '' + selectedItem3 + '' + selectedItem4 + '.' + selectedItem5) })}
                            keyExtractor={number => number.toString()}
                            //showArrows={Platform.OS=='android'?true:false}
                            onInputValueChanged={this.onValueChanged}
                            renderItem={item => <Text style={{color: colors.darkGray, textAlign: 'center', width: responsiveWidth(16), height: responsiveHeight(6), lineHeight: responsiveHeight(6), fontSize: responsiveFontSize(8) }}>{item.toString()}</Text>} />
                    </View>

                   <View style={{borderTopWidth:0.7, borderBottomWidth:0.7, borderColor:'black',  marginTop: moderateScale(6)}}>
                   <Text style={{color: colors.darkGray,height:responsiveHeight(6), lineHeight:responsiveHeight(7), fontSize:responsiveFontSize(9) }}>.</Text>
                   </View>
                    
                    <View style={{ alignSelf: 'center', marginTop: moderateScale(6), justifyContent: 'center', alignItems: 'center' }}>
                    
                        <SpinPicker
                            data={[...Array(10).keys()]}
                            value={this.state.selectedItem5}
                            onValueChange={selectedItem => this.setState({ selectedItem5: selectedItem, kg: parseFloat(selectedItem1 + '' + selectedItem2 + '' + selectedItem3 + '' + selectedItem4 + '.' + selectedItem5) })}
                            keyExtractor={number => number.toString()}
                            //showArrows={Platform.OS=='android'?true:false}
                            onInputValueChanged={this.onValueChanged}
                            renderItem={item => <Text style={{color: colors.darkGray, textAlign: 'center', width: responsiveWidth(17), height: responsiveHeight(6), lineHeight: responsiveHeight(6), fontSize: responsiveFontSize(8) }}>{item.toString()}</Text>} />
                    </View>
                </View>
            </View>
        )
    }


    date = () => {
        const { isRTL } = this.props
        const { date, selectedDate, dateActive } = this.state
        return (
            <View style={{ width: responsiveWidth(100), marginTop: moderateScale(0), }}>
                <View style={{ alignSelf: 'center', margin: moderateScale(10), flexDirection: isRTL ? 'row-reverse' : 'row' }} >
                    <TouchableOpacity onPress={() => {
                        this.setState({showDatePicker: true })
                    }} style={{ borderRadius: 0, padding: moderateScale(2), alignSelf: 'center', height: responsiveHeight(6), width: responsiveWidth(30), justifyContent: 'center', alignItems: 'center', backgroundColor: dateActive == 'aftertomorrow' ? '#a9d3a0' : 'white', borderColor: colors.darkGray, borderWidth: 0.5 }} >
                        <Text style={{ color: colors.darkGray, fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(7), height: responsiveHeight(6), lineHeight: responsiveHeight(6) }}>{selectedDate?selectedDate:Strings.selectDate}</Text>
                    </TouchableOpacity>

                </View>

            </View>
        )
    }


    datePicker = () => {
        const { showDatePicker, datePicker, minimumDate } = this.state
        return (
        <DateTimePickerModal
            isVisible={showDatePicker}
            mode="date"
            //date={datePicker}
            onConfirm={(date) => {
                const d = date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear()
                this.setState({ showDatePicker: false, date: d, selectedDate: d, datePicker: date })
                console.log(d)
            }
            }
            onCancel={() => this.setState({ showDatePicker: false })}
            //minimumDate={new Date(minimumDate)}
            confirmTextIOS={Strings.confirm}
            cancelTextIOS={Strings.cancel}
            headerTextIOS={Strings.chooseDate}
        />
        )
    }

    send = () => {
        const { currentUser} = this.props
        const {selectedWarehouse,vehicleNumber,waybill,driverName, kg,selectedDate,tankNumber,tankCapacity} = this.state
        if (!tankNumber.toString().replace(/\s/g, '').length) {
            this.setState({tankNumber: '' })
        }
        /*if (!tankCapacity.toString().replace(/\s/g, '').length) {
            this.setState({tankCapacity: '' })
        }*/
        if (!vehicleNumber.toString().replace(/\s/g, '').length) {
            this.setState({vehicleNumber: '' })
        }
        if (!waybill.toString().replace(/\s/g, '').length) {
            this.setState({waybill: '' })
        }
        if (!driverName.replace(/\s/g, '').length) {
            this.setState({driverName: '' })
        }
        if (!kg.toString().replace(/\s/g, '').length) {
            this.setState({kg: '' })
        }
        if (!selectedDate) {
            RNToasty.Error({title:Strings.selectDate})
        }
        if (!selectedWarehouse) {
           this.setState({selectedWarehouse:false})
        }

        if (selectedWarehouse&&driverName.replace(/\s/g, '').length&&waybill.toString().replace(/\s/g, '').length&&vehicleNumber.toString().replace(/\s/g, '').length&&kg.toString().replace(/\s/g, '').length&&tankNumber.toString().replace(/\s/g, '').length&&selectedDate) {
            
            
                this.setState({loading: true })
                const data = {
                    shippingDay:selectedDate,
                    tankNumber:tankNumber,
                    //tankCapacity:tankCapacity,
                    //director:currentUser.user.id,
                    warehouse:selectedWarehouse.id,
                    amount:kg,
                    driverName:driverName,
                    waybill:waybill,
                    vehicleNumber:vehicleNumber,
                }
                axios.post(`${BASE_END_POINT}shipping`,JSON.stringify(data),{
                    headers: {
                        "Content-Type": 'application/json',
                        "Authorization": `Bearer ${this.props.currentUser.token}`,
                    }
                })
                .then(response=>{
                    resetTo('AdminHome')
                    RNToasty.Success({title:Strings.done})
                    this.setState({loading: false })
                })
                .catch(error=>{
                    console.log("ERROR  ",error.response)
                    this.setState({loading: false })
                })
            
        }

    }


    submit_cancel_buttons = () => {
        const { isRTL } = this.props
        return (
            <View style={{ alignSelf: 'center', margin: moderateScale(0), marginBottom: moderateScale(20), flexDirection: isRTL ? 'row-reverse' : 'row' }} >

                <TouchableOpacity onPress={() => {
                    this.send()
                }} disabled={this.state.enable} style={whiteButton} >
                    <Text style={{ color: colors.green, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.confirm}</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => {
                    pop()
                }} style={greenButton} >
                    <Text style={{ color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.cancel}</Text>
                </TouchableOpacity>


            </View>
        )
    }

    tanKNumberInput = () => {
        const { isRTL } = this.props
        const { tankNumber } = this.state
        return (
            <View style={{ marginTop: moderateScale(5), width: responsiveWidth(80), alignSelf: 'center' }}>

                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.tankNumber}</Text>
                <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                    <TextInput
                        onChangeText={(val) => { this.setState({ tankNumber: val,}) }}
                        style={{ width: responsiveWidth(80), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(6), textAlign: isRTL ? 'right' : 'left' }}
                        placeholder={Strings.tankNumber}
                        ref={(input) => { this.tankNumber = input; }}
                        returnKeyType='next'
                        onSubmitEditing={() => {this.tankCapacity.focus(); }}
                        
                       
                    />
                </View>
                {tankNumber.length == 0 &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }
    

    tankCapacityInput = () => {
            const { isRTL } = this.props
            const { tankCapacity} = this.state
            return (
                <View style={{ marginTop: moderateScale(5), width: responsiveWidth(80), alignSelf: 'center' }}>
    
                    <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.tankCapacity}</Text>
                    <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                        <TextInput
                            onChangeText={(val) => { this.setState({ tankCapacity: val}) }}
                            style={{ width: responsiveWidth(80), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(6), textAlign: isRTL ? 'right' : 'left' }}
                            placeholder={Strings.tankCapacity}
                            keyboardType={'phone-pad'}
                            ref={(input) => { this.tankCapacity = input; }}
                            returnKeyType='next'
                            onSubmitEditing={() => {this.driverName.focus(); }}
                        />
                    </View>
                    {tankCapacity.length == 0 &&
                        <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                    }
                </View>
            )
        
    }

    vehicleNumberInput = () => {
        const { isRTL } = this.props
        const { vehicleNumber } = this.state
        return (
            <View style={{ marginTop: moderateScale(5), width: responsiveWidth(80), alignSelf: 'center' }}>

                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.vehicleNumber}</Text>
                <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                    <TextInput
                        onChangeText={(val) => { this.setState({ vehicleNumber: val}) }}
                        style={{ width: responsiveWidth(80), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(6), textAlign: isRTL ? 'right' : 'left' }}
                        placeholder={Strings.vehicleNumber}
                        returnKeyType='next'
                        onSubmitEditing={() => {this.waybill.focus(); }}
                       
                    />
                </View>
                {vehicleNumber.length == 0 &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    
    }

    waybillInput = () => {
        const { isRTL } = this.props
        const { waybill } = this.state
        return (
            <View style={{ marginTop: moderateScale(5), width: responsiveWidth(80), alignSelf: 'center' }}>

                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.waybill}</Text>
                <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                    <TextInput
                        onChangeText={(val) => { this.setState({ waybill: val}) }}
                        style={{ width: responsiveWidth(80), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(6), textAlign: isRTL ? 'right' : 'left' }}
                        placeholder={Strings.waybill}
                        ref={(input) => { this.waybill = input; }}
                        returnKeyType='next'
                        onSubmitEditing={() => {this.tankNumber.focus(); }}
                        
                    />
                </View>
                {waybill.length == 0 &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    
    }

    driverNameInput = () => {
        const { isRTL } = this.props
        const { driverName } = this.state
        return (
            <View style={{ marginTop: moderateScale(5), width: responsiveWidth(80), alignSelf: 'center' }}>

                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.driver}</Text>
                <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                    <TextInput
                        onChangeText={(val) => { this.setState({ driverName: val}) }}
                        style={{ width: responsiveWidth(80), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(6), textAlign: isRTL ? 'right' : 'left' }}
                        placeholder={Strings.driver}
                        ref={(input) => { this.driverName = input; }}
                        
                    />
                </View>
                {driverName.length == 0 &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    
    }

    amountInput = () => {
        const { isRTL } = this.props
        const { kg } = this.state
        return (
            <View style={{ marginTop: moderateScale(5), width: responsiveWidth(80), alignSelf: 'center' }}>

                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.quantity}</Text>
                <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                    <TextInput
                        onChangeText={(val) => { this.setState({ kg: val}) }}
                        style={{ width: responsiveWidth(80), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(6), textAlign: isRTL ? 'right' : 'left' }}
                        placeholder={Strings.quantity}
                        keyboardType={'phone-pad'}
                        ref={(input) => { this.tankCapacity = input; }}
                     returnKeyType='next'
                     onSubmitEditing={() => {this.driverName.focus(); }}
                    />
                </View>
                {kg.length == 0 &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    
    }


    render() {
        const { isRTL, currentUser } = this.props
        return (
            <LinearGradient
                colors={[colors.white, colors.white, colors.white]}
                style={{ flex: 1 }}
            >
                {/*<Header  title={Strings.assignTank} />*/}
                <ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: colors.white, borderTopRightRadius: moderateScale(20), borderTopLeftRadius: moderateScale(20), marginTop: moderateScale(10), width: responsiveWidth(100), marginBottom: moderateScale(15),flex:1, }} >
                    {this.warehousesPicker()}
                    {this.vehicleNumberInput()}
                    {this.waybillInput()}
                    {this.tanKNumberInput()}
                    {/*this.tankCapacityInput()*/}
                    {this.amountInput()}
                    {this.driverNameInput()}
                    {/*this.spinKg()*/}
                    {this.date()}
                    {this.submit_cancel_buttons()}
                    {this.datePicker()}
                </ScrollView>
                {this.state.loading&&<LoadingDialogOverlay title={Strings.wait} />}
                <AppFooter />
            </LinearGradient>
        );
    }
}
const mapDispatchToProps = {

}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
    //userToken: state.auth.userToken,
})


export default connect(mapToStateProps, mapDispatchToProps)(AdminAssignTankToWarehouse);

