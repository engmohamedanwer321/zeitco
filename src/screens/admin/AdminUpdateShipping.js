import React, { Component } from 'react';
import {
    View, TouchableOpacity, Image, Text, ScrollView, TextInput, Alert, Platform
} from 'react-native';
import { connect } from 'react-redux';
import { Icon, Button } from 'native-base';
import { login } from '../../actions/AuthActions'
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../../utils/responsiveDimensions";
import Strings from '../../assets/strings';
import LoadingDialogOverlay from '../../components/LoadingDialogOverlay';
import { enableSideMenu, resetTo, push, pop } from '../../controlls/NavigationControll'
import { arrabicFont, englishFont } from '../../common/AppFont'
import * as Animatable from 'react-native-animatable';
import * as colors from '../../assets/colors';
import { removeItem } from '../../actions/MenuActions';
import CollaspeAppHeader from '../../common/CollaspeAppHeader'
import LinearGradient from 'react-native-linear-gradient';
import FastImage from 'react-native-fast-image'
import CommanHeader from '../../common/CommanHeader'
import ImagePicker from 'react-native-image-crop-picker';
import AppFooter from '../../components/AppFooter'
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import io from 'socket.io-client';
import axios from 'axios';
import { BASE_END_POINT } from '../../AppConfig'
import { RNToasty } from 'react-native-toasty';
import DateTimePicker from '@react-native-community/datetimepicker';
import { TextInputMask } from 'react-native-masked-text'
import DateTimePickerModal from "react-native-modal-datetime-picker";
import { whiteButton, greenButton } from '../../assets/styles';
import RNPickerSelect from 'react-native-picker-select';
import Carousel from 'react-native-snap-carousel';
import { SpinPicker } from 'react-native-spin-picker'
import moment from 'moment'
import Header from '../../common/Header'


class AdminUpdateShipping extends Component {

    socket = null;
    state = {
        loading: false,
        kg: this.props.data.amount,
        selectedItem1: 0,
        selectedItem2: 0,
        selectedItem3: 0,
        selectedItem4: 0,
        selectedItem5: 0,
        selectedKg: '0',
        date: null,
        datePicker: new Date((new Date().getFullYear()) + '/' + (new Date().getMonth() + 1) + '/' + (parseInt(new Date().getDate()) + 2)),
        showDatePicker: false,
        selectedDate: this.props.data.shippingDay,
        calendr: '',
        showKgPicker: false,
        firstMeasure: this.props.data.firstMeasure ? this.props.data.firstMeasure.toString() : '',
        secondMeasure: this.props.data.secondMeasure ? this.props.data.secondMeasure.toString() : ''
    }

    componentDidMount() {
        enableSideMenu(false, null)
        this.socket = io("https://api.zeitcoapp.com/order", { query: `id=${this.props.currentUser.user.id}` });
    }

    amountView = () => {
        const { isRTL, data } = this.props
        const { showKgPicker } = this.state

        return (
            <View style={{ marginTop: moderateScale(10), backgroundColor: colors.lightGray, alignSelf: 'center', width: responsiveWidth(90), height: responsiveHeight(5), justifyContent: 'center', }} >
                <Text style={{ marginHorizontal: moderateScale(2), fontFamily: englishFont, color: 'black', textAlign: isRTL ? 'right' : 'left', }}> {Strings.quantity} : {data.amount} {Strings.Kg} </Text>
                {/*<TouchableOpacity
                    onPress={() => {
                        this.setState({ showKgPicker: !showKgPicker })
                    }}
                    style={{ width: responsiveWidth(30), height: responsiveHeight(7), justifyContent: 'center', alignItems: 'center', backgroundColor: colors.green }} >
                    <Text style={{ color: 'white' }}>{showKgPicker ? Strings.close : Strings.update}</Text>
                </TouchableOpacity>*/}
            </View>
        )
    }

    spinKg = () => {
        const { isRTL } = this.props
        const { selectedItem1, selectedItem2, selectedItem3, selectedItem4, selectedItem5 } = this.state
        return (
            <View style={{ width: responsiveWidth(20), alignSelf: 'center', marginTop: moderateScale(0), justifyContent: 'center', alignItems: 'center' }}>
                <View style={{ flexDirection: 'row', width: responsiveWidth(20), alignSelf: 'center', marginTop: moderateScale(0), justifyContent: 'center', alignItems: 'center' }}>
                    <View style={{ alignSelf: 'center', marginTop: moderateScale(6), justifyContent: 'center', alignItems: 'center' }}>
                        <SpinPicker
                            data={[...Array(10).keys()]}
                            value={this.state.selectedItem1}
                            onValueChange={selectedItem => this.setState({ selectedItem1: selectedItem, kg: parseFloat(selectedItem1 + '' + selectedItem2 + '' + selectedItem3 + '' + selectedItem4 + '.' + selectedItem5) })}
                            keyExtractor={number => number.toString()}
                            //showArrows={Platform.OS=='android'?true:false}
                            onInputValueChanged={this.onValueChanged}
                            renderItem={item => <Text style={{ color: colors.darkGray, textAlign: 'center', width: responsiveWidth(17), height: responsiveHeight(6), lineHeight: responsiveHeight(6), fontSize: responsiveFontSize(8) }}>{item.toString()}</Text>} />
                    </View>
                    <View style={{ alignSelf: 'center', marginTop: moderateScale(6), justifyContent: 'center', alignItems: 'center' }}>
                        <SpinPicker
                            data={[...Array(10).keys()]}
                            value={this.state.selectedItem2}
                            onValueChange={selectedItem => this.setState({ selectedItem2: selectedItem, kg: parseFloat(selectedItem1 + '' + selectedItem2 + '' + selectedItem3 + '' + selectedItem4 + '.' + selectedItem5) })}
                            keyExtractor={number => number.toString()}
                            //showArrows={Platform.OS=='android'?true:false}
                            onInputValueChanged={this.onValueChanged}
                            renderItem={item => <Text style={{ color: colors.darkGray, textAlign: 'center', width: responsiveWidth(17), height: responsiveHeight(6), lineHeight: responsiveHeight(6), fontSize: responsiveFontSize(8) }}>{item.toString()}</Text>} />
                    </View>
                    <View style={{ alignSelf: 'center', marginTop: moderateScale(6), justifyContent: 'center', alignItems: 'center' }}>
                        <SpinPicker
                            data={[...Array(10).keys()]}
                            value={this.state.selectedItem3}
                            onValueChange={selectedItem => this.setState({ selectedItem3: selectedItem, kg: parseFloat(selectedItem1 + '' + selectedItem2 + '' + selectedItem3 + '' + selectedItem4 + '.' + selectedItem5) })}
                            keyExtractor={number => number.toString()}
                            //showArrows={Platform.OS=='android'?true:false}
                            onInputValueChanged={this.onValueChanged}
                            renderItem={item => <Text style={{ color: colors.darkGray, textAlign: 'center', width: responsiveWidth(17), height: responsiveHeight(6), lineHeight: responsiveHeight(6), fontSize: responsiveFontSize(8) }}>{item.toString()}</Text>} />
                    </View>
                    <View style={{ alignSelf: 'center', marginTop: moderateScale(6), justifyContent: 'center', alignItems: 'center' }}>
                        <SpinPicker
                            data={[...Array(10).keys()]}
                            value={this.state.selectedItem4}
                            onValueChange={selectedItem => this.setState({ selectedItem4: selectedItem, kg: parseFloat(selectedItem1 + '' + selectedItem2 + '' + selectedItem3 + '' + selectedItem4 + '.' + selectedItem5) })}
                            keyExtractor={number => number.toString()}
                            //showArrows={Platform.OS=='android'?true:false}
                            onInputValueChanged={this.onValueChanged}
                            renderItem={item => <Text style={{ color: colors.darkGray, textAlign: 'center', width: responsiveWidth(16), height: responsiveHeight(6), lineHeight: responsiveHeight(6), fontSize: responsiveFontSize(8) }}>{item.toString()}</Text>} />
                    </View>

                    <View style={{ borderTopWidth: 0.7, borderBottomWidth: 0.7, borderColor: 'black', marginTop: moderateScale(6) }}>
                        <Text style={{ color: colors.darkGray, height: responsiveHeight(6), lineHeight: responsiveHeight(7), fontSize: responsiveFontSize(9) }}>.</Text>
                    </View>

                    <View style={{ alignSelf: 'center', marginTop: moderateScale(6), justifyContent: 'center', alignItems: 'center' }}>

                        <SpinPicker
                            data={[...Array(10).keys()]}
                            value={this.state.selectedItem5}
                            onValueChange={selectedItem => this.setState({ selectedItem5: selectedItem, kg: parseFloat(selectedItem1 + '' + selectedItem2 + '' + selectedItem3 + '' + selectedItem4 + '.' + selectedItem5) })}
                            keyExtractor={number => number.toString()}
                            //showArrows={Platform.OS=='android'?true:false}
                            onInputValueChanged={this.onValueChanged}
                            renderItem={item => <Text style={{ color: colors.darkGray, textAlign: 'center', width: responsiveWidth(17), height: responsiveHeight(6), lineHeight: responsiveHeight(6), fontSize: responsiveFontSize(8) }}>{item.toString()}</Text>} />
                    </View>
                </View>
            </View>
        )
    }


    vehicleNumberInput = () => {
        const { isRTL } = this.props
        //const {tankNumber} = this.state
        return (
            <View style={{ marginTop: moderateScale(6), width: responsiveWidth(90), alignSelf: 'center' }}>

                <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                    <TextInput
                        editable={false}
                        value={Strings.vehicleNumber + " : " + this.props.data.vehicleNumber}
                        //onChangeText={(val)=>{this.setState({tankNumber:val})}}
                        style={{ width: responsiveWidth(90), paddingHorizontal: moderateScale(3), paddingVertical: moderateScale(0), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(5), padding: 0 }}
                        placeholder={Strings.vehicleNumber}
                    />
                </View>
                {/*tankNumber.length==0&&
                <Text style={{color:'red',marginHorizontal:moderateScale(2),fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {Strings.require}</Text>
                */}
            </View>
        )
    }

    willBayInput = () => {
        const { isRTL } = this.props
        //const {tankCapacity} = this.state
        return (
            <View style={{ marginTop: moderateScale(6), width: responsiveWidth(90), alignSelf: 'center' }}>

                <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                    <TextInput
                        editable={false}
                        value={Strings.waybill + " : " + this.props.data.waybill}
                        //onChangeText={(val)=>{this.setState({tankCapacity:val})}}
                        style={{ width: responsiveWidth(90), paddingHorizontal: moderateScale(3), paddingVertical: moderateScale(0), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(5), padding: 0 }}
                        placeholder={Strings.waybill}
                    />
                </View>
                {/*tankCapacity.length==0&&
                <Text style={{color:'red',marginHorizontal:moderateScale(2),fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {Strings.require}</Text>
                */}
            </View>
        )
    }

    firstMeasureInput = () => {
        const { isRTL, data } = this.props
        //const {tankNumber} = this.state
        console.log('DATA: ', data)
        return (
            <View style={{ marginTop: moderateScale(6), width: responsiveWidth(90), alignSelf: 'center' }}>

                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', borderRadius: moderateScale(1), backgroundColor: colors.lightGray, justifyContent: 'space-between' }} >
                    <TextInput

                        value={this.state.firstMeasure}
                        onChangeText={(val) => { this.setState({ firstMeasure: val }) }}
                        style={{ width: responsiveWidth(55), paddingHorizontal: moderateScale(3), paddingVertical: moderateScale(0), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(5), padding: 0, textAlign: isRTL ? 'right' : 'left' }}
                        placeholder={Strings.firstMeasure}
                    />

                    <TouchableOpacity
                        onPress={() => {
                            this.updateFirstMeasure()
                        }}
                        style={{ width: responsiveWidth(30), height: responsiveHeight(5), justifyContent: 'center', alignItems: 'center', backgroundColor: colors.green }} >
                        <Text style={{ color: 'white' }}>{Strings.update}</Text>
                    </TouchableOpacity>

                </View>

                {/*tankNumber.length==0&&
                <Text style={{color:'red',marginHorizontal:moderateScale(2),fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {Strings.require}</Text>
                */}
            </View>
        )
    }

    secondMeasureInput = () => {
        const { isRTL, data } = this.props
        //const {tankNumber} = this.state
        return (
            <View style={{ marginTop: moderateScale(6), width: responsiveWidth(90), alignSelf: 'center' }}>

                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                    <TextInput

                        value={this.state.secondMeasure}
                        onChangeText={(val) => { this.setState({ secondMeasure: val }) }}
                        style={{ width: responsiveWidth(60), paddingHorizontal: moderateScale(3), paddingVertical: moderateScale(0), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(5), padding: 0, textAlign: isRTL ? 'right' : 'left' }}
                        placeholder={Strings.secondMeasure}
                    />

                    <TouchableOpacity
                        onPress={() => {
                            this.updateSecondMeasure()
                        }}
                        style={{ width: responsiveWidth(30), height: responsiveHeight(5), justifyContent: 'center', alignItems: 'center', backgroundColor: colors.green }} >
                        <Text style={{ color: 'white' }}>{Strings.update}</Text>
                    </TouchableOpacity>
                </View>
                {/*tankNumber.length==0&&
                <Text style={{color:'red',marginHorizontal:moderateScale(2),fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {Strings.require}</Text>
                */}
            </View>
        )
    }


    datePicker = () => {
        const { showDatePicker, datePicker } = this.state
        return (<DateTimePickerModal
            isVisible={showDatePicker}
            mode="date"
            date={datePicker}
            onConfirm={(date) => {
                const d = date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear()
                this.setState({ showDatePicker: false, date: d, selectedDate: d, datePicker: date })
                console.log(d)
            }
            }
            onCancel={() => this.setState({ showDatePicker: false })}
            //minimumDate={new Date(minimumDate)}
            confirmTextIOS={Strings.confirm}
            cancelTextIOS={Strings.cancel}
            headerTextIOS={Strings.chooseDate}
        />
        )
    }


    updateFirstMeasure = () => {
        const { currentUser } = this.props
        const { firstMeasure, date } = this.state
        console.log('FirstMeasure', firstMeasure)
        if (!firstMeasure) {
            RNToasty.Error({ title: Strings.enterFirstMeasure })
        }
        if (firstMeasure) {
            this.setState({ loading: true })
            const data = {
                firstMeasure: firstMeasure,
            }
            console.log("DATA  ", data)
            axios.put(`${BASE_END_POINT}shipping/${this.props.data.id}/firstMeasure`, JSON.stringify(data), {
                headers: {
                    "Authorization": `Bearer ${currentUser.token}`,
                    "Content-Type": "application/json"
                }
            })
                .then(respose => {
                    this.setState({ loading: false })
                    //resetTo('AdminHome')
                    RNToasty.Success({ title: Strings.firstMeasureHasUpdated })
                })
                .catch(error => {
                    console.log('ERROR  ', error.response)
                    console.log('ERROR  ', error)
                    this.setState({ loading: false })
                })
        }
    }


    updateSecondMeasure = () => {
        const { currentUser } = this.props
        const { secondMeasure, date } = this.state
        if (!secondMeasure) {
            RNToasty.Error({ title: Strings.enterSecondMeasure })
        }
        if (secondMeasure) {
            this.setState({ loading: true })
            const data = {
                secondMeasure: secondMeasure,
            }
            console.log("DATA  ", data)
            axios.put(`${BASE_END_POINT}shipping/${this.props.data.id}/secondMeasure`, JSON.stringify(data), {
                headers: {
                    "Authorization": `Bearer ${currentUser.token}`,
                    "Content-Type": "application/json"
                }
            })
                .then(respose => {
                    this.setState({ loading: false })
                    //resetTo('AdminHome')
                    RNToasty.Success({ title: Strings.secondMeasureHasUpdated })
                })
                .catch(error => {
                    console.log('ERROR  ', error.respose)
                    console.log('ERROR  ', error)
                    this.setState({ loading: false })
                })
        }
    }



    send = () => {
        const { currentUser } = this.props
        const { kg, date } = this.state
        if (!kg) {
            RNToasty.Error({ title: Strings.enterKg })
        }
        if (kg) {
            this.setState({ loading: true })
            const data = {
                amount: kg,
            }
            console.log("DATA  ", data)
            axios.put(`${BASE_END_POINT}shipping/${this.props.data.id}/done`, JSON.stringify(data), {
                headers: {
                    "Authorization": `Bearer ${currentUser.token}`,
                    "Content-Type": "application/json"
                }
            })
                .then(respose => {
                    this.setState({ loading: false })
                    resetTo('WarehouseHome')
                    RNToasty.Success({ title: Strings.done })
                })
                .catch(error => {
                    console.log('ERROR  ', error.respose)
                    console.log('ERROR  ', error)
                    this.setState({ loading: false })
                })
        }

        /*if (price && date && branch != null) {
            //Alert.alert(currentUser.user.type=='USER'?""+currentUser.user.id:""+selectedResturant.id,)
            this.setState({ disable: true })
            this.socket.emit("newOrder", {
                restaurant: currentUser.user.type == 'USER' ? currentUser.user.restaurant.id : selectedResturant.id, //resturant id,
                average: amount,
                date: date,
                //price: price ,
                userId: currentUser.user.id,
                branch: branch
            });
            console.log("Done")
            RNToasty.Success({ title: Strings.addOrderSuccessfuly })
            if (this.props.currentUser.user.type == 'USER') {
                pop()
            }
        } else {
            RNToasty.Error({ title: Strings.insertTheRequiredData })
        }*/
        //pop()
    }


    submit_cancel_buttons = () => {
        const { isRTL } = this.props
        return (
            <View style={{ alignSelf: 'center', margin: moderateScale(10), marginBottom: moderateScale(15), flexDirection: isRTL ? 'row-reverse' : 'row' }} >

                <TouchableOpacity onPress={() => {
                    this.send()
                }} disabled={this.state.enable} style={whiteButton} >
                    <Text style={{ color: colors.green, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.confirm}</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => {
                    pop()
                }} style={greenButton} >
                    <Text style={{ color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.cancel}</Text>
                </TouchableOpacity>


            </View>
        )
    }


    dataButton = () => {
        const { isRTL } = this.props
        const { selectedDate } = this.state
        return (
            <TouchableOpacity onPress={() => {
                //this.setState({showDatePicker: true })
            }} style={{ borderRadius: 0, padding: moderateScale(2), alignSelf: isRTL ? 'flex-end' : 'flex-start', marginHorizontal: moderateScale(10), height: responsiveHeight(6), width: responsiveWidth(30), justifyContent: 'center', alignItems: 'center', backgroundColor: 'white', borderColor: colors.green, borderWidth: 0.5, marginTop: moderateScale(10) }} >
                <Text style={{ color: colors.green, fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(7), height: responsiveHeight(6), lineHeight: responsiveHeight(6) }}>{selectedDate}</Text>
            </TouchableOpacity>
        )
    }



    render() {
        const { isRTL, userToken, currentUser } = this.props;
        const { loading, showKgPicker } = this.state;
        return (
            <LinearGradient
                colors={[colors.white, colors.white, colors.white]}
                style={{ flex: 1 }}
            >
                <Header title={Strings.Shipment} />

                <ScrollView nestedScrollEnabled showsVerticalScrollIndicator={false} style={{ backgroundColor: colors.white, borderTopRightRadius: moderateScale(20), borderTopLeftRadius: moderateScale(20), marginTop: moderateScale(15), width: responsiveWidth(100), marginBottom: moderateScale(20) }} >
                    {this.amountView()}
                    {showKgPicker && <Text style={{ marginTop: moderateScale(10), color: colors.green, marginHorizontal: moderateScale(10), alignSelf: 'center', fontFamily: arrabicFont, fontSize: responsiveFontSize(8) }} >{Strings.enterShipmentQty}</Text>}
                    {showKgPicker && this.spinKg()}
                    {this.vehicleNumberInput()}
                    {this.willBayInput()}
                    {this.firstMeasureInput()}
                    {this.secondMeasureInput()}

                    {/*this.datePicker()*/}

                </ScrollView>
                {loading && <LoadingDialogOverlay title={Strings.wait} />}
                <AppFooter />
            </LinearGradient>
        );
    }
}
const mapDispatchToProps = {

}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
    //userToken: state.auth.userToken,
})


export default connect(mapToStateProps, mapDispatchToProps)(AdminUpdateShipping);

