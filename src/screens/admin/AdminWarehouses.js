import React, { Component } from 'react';
import { View, RefreshControl, ScrollView, FlatList, Alert, Text, TouchableOpacity } from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../../utils/responsiveDimensions';
import { connect } from "react-redux";
import Strings from '../../assets/strings';

import axios from 'axios';
import { BASE_END_POINT } from '../../AppConfig';
import ListFooter from '../../components/ListFooter';
import { Icon, Thumbnail, Button } from 'native-base'
import { selectMenu, removeItem } from '../../actions/MenuActions';
import { enableSideMenu, pop } from '../../controlls/NavigationControll'
import NetInfo from "@react-native-community/netinfo";
import Loading from "../../common/Loading"
import NetworError from '../../common/NetworError'
import NoData from '../../common/NoData'
import * as colors from '../../assets/colors'
import { arrabicFont, englishFont } from '../../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image'
import AdminWarehouseCard from '../../components/AdminWarehouseCard'
import LinearGradient from 'react-native-linear-gradient';
import CommanHeader from '../../common/CommanHeader'
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';
import AppFooter from '../../components/AppFooter'
import io from 'socket.io-client';
import Header from '../../common/Header'
import AdminAssignTankToWarehouse from './AdminAssignTankToWarehouse'


class AdminWarehouses extends Component {

    _menu = null;
    page = 1;
    state = {
        tabNumber:1,
        networkError: null,
        members: [],
        membersLoading: true,
        membersRefresh: false,
        members404: false,
        pages: null,

        datePicker: new Date((new Date().getFullYear()) + '/' + (new Date().getMonth() + 1) + '/' + (parseInt(new Date().getDate()) + 2)),
        showDatePicker: false,
        selectedDate: null,
        calendr: '',
        
        tankNumber:' ',
        tankCapacity:' ',
        vehicleNumber:' ',
        waybill:' ',
        driverName:' ',
        loading:false,

        selectedItem1: 0,
        selectedItem2: 0,
        selectedItem3: 0,
        selectedItem4: 0,
        selectedItem5: 0,
        selectedKg: '0',
        kg:' ',
    }


    componentDidMount() {
        var d = new Date()
        d.setDate(d.getDate() + 1)
        enableSideMenu(false, null)
        this.getMembers(false, 1)
    }

    tabs = () => {
        const {isRTL} = this.props;
        const {tabNumber} = this.state;
        console.log(tabNumber);
        return (
          <View style={{width: responsiveWidth(100),flexDirection: 'row',alignItems: 'center',}}
            >
            <Button
              onPress={() => {
                if (this.state.tabNumber != 1) {
                  this.setState({tabNumber: 1});
                }
              }}
              style={{flexDirection: isRTL ? 'row-reverse' : 'row',marginLeft: 1,flex: 1,height: responsiveHeight(8),justifyContent: 'center',alignItems: 'center',alignSelf: isRTL ? 'flex-start' : 'flex-end',backgroundColor: tabNumber == 1 ? colors.white : colors.lightGreen,}}
              >
              <Text style={{color: tabNumber == 1 ? colors.darkGray : colors.white,fontFamily: isRTL ? arrabicFont : englishFont,}}>{Strings.warehouses}</Text>
            </Button>
    
            <Button
              onPress={() => {
                if (this.state.tabNumber != 2) {
                  this.setState({tabNumber: 2,});
                }
              }}
              style={{marginRight: 1,flex: 1,height: responsiveHeight(8),justifyContent: 'center',alignItems: 'center',alignSelf: isRTL ? 'flex-start' : 'flex-end',backgroundColor: tabNumber == 2 ? colors.white : colors.lightGreen,}}
              >
              <Text style={{color: tabNumber == 2 ? colors.darkGray : colors.white,fontFamily: isRTL ? arrabicFont : englishFont,}}> {Strings.assignTank}</Text>
            </Button>
          </View>
        );
      }

    getMembers = (refresh, page) => {
        if (refresh) {
            this.setState({ membersRefresh: true })
        }

        axios.get(`${BASE_END_POINT}warehouses/get/withPaginated?page=${page}`,{
            headers:{
                Authorization: `Bearer ${this.props.currentUser.token}`
            }
        })
            .then(response => {
                console.log('Done    ', response.data.data)
                this.setState({
                    members: refresh ? response.data.data : [...this.state.members, ...response.data.data],
                    membersLoading: false,
                    membersRefresh: false,
                    members404: false,
                    pages: response.data.pageCount,
                })
            })
            .catch(error => {
                console.log('Error   ', error.response)
                this.setState({ members404: true, membersLoading: false, })
            })
    }

    membersPage = () => {
        const { pages, members, members404, membersLoading, membersRefresh } = this.state
        return (
            members404 ?
                <NetworError />
                :
                membersLoading ?
                    <Loading />
                    :
                    members.length > 0 ?
                        <FlatList
                            showsVerticalScrollIndicator={false}
                            style={{maxHeight: responsiveHeight(79), marginBottom: moderateScale(0) }}
                            data={members}
                            renderItem={({ item }) => <AdminWarehouseCard data={item} />}
                            onEndReachedThreshold={.5}
                            //ListFooterComponent={()=>notificationsLoad&&<ListFooter />}
                            onEndReached={() => {
                                if (this.page <= pages) {
                                    this.page = this.page + 1;
                                    this.getMembers(false, this.page)
                                    console.log('page  ', this.page)
                                }
                            }}
                            refreshControl={
                                <RefreshControl
                                    //colors={["#B7ED03",colors.darkBlue]} 
                                    refreshing={membersRefresh}
                                    onRefresh={() => {
                                        this.page = 1
                                        this.getMembers(true, 1)
                                    }}
                                />
                            }

                        />
                        :
                        <NoData />
        )
    }

    
   

    render() {
        const { categoryName, isRTL } = this.props;
        const { tabNumber } = this.state;
       
        return (
            <LinearGradient
                colors={[colors.white, colors.white, colors.white]}
                style={{ flex: 1 }}
            >
                <Header  title={Strings.warehouses} />
                {this.tabs()}
                <View style={{ flex: 1, backgroundColor: colors.white, marginTop: moderateScale(0), width: responsiveWidth(100) }} >

                    {tabNumber==1&&this.membersPage()}
                    {tabNumber==2&&<AdminAssignTankToWarehouse/>}
                </View>

                <AppFooter />

            </LinearGradient>
        );
    }
}

const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    barColor: state.lang.color,
    currentUser: state.auth.currentUser,
})

const mapDispatchToProps = {
    removeItem,
}

export default connect(mapStateToProps, mapDispatchToProps)(AdminWarehouses);
