import React, { Component } from 'react';
import { View, RefreshControl, Alert, Modal, ScrollView, FlatList, Text, TouchableOpacity, Platform } from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../../utils/responsiveDimensions';
import { connect } from "react-redux";
import Strings from '../../assets/strings';

import axios from 'axios';
import { BASE_END_POINT } from '../../AppConfig';
import ListFooter from '../../components/ListFooter';
import { Icon, Thumbnail, Button } from 'native-base'
import { selectMenu, removeItem } from '../../actions/MenuActions';
import { enableSideMenu, pop } from '../../controlls/NavigationControll'
import NetInfo from "@react-native-community/netinfo";
import Loading from "../../common/Loading"
import NetworError from '../../common/NetworError'
import NoData from '../../common/NoData'
import * as colors from '../../assets/colors'
import { arrabicFont, englishFont } from '../../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image'
import NotificationCard from '../../components/NotificationCard'
import NewOrderCard from '../../components/NewOrderCard';
import AdminCallCenterNoOrdersCard from '../../components/AdminCallCenterNoOrdersCard'
import CollaspeAppHeader from '../../common/CollaspeAppHeader'
import LinearGradient from 'react-native-linear-gradient';
import CommanHeader from '../../common/CommanHeader'
import MapView, { Marker } from 'react-native-maps';
import io from 'socket.io-client';
import AppFooter from '../../components/AppFooter';
import { OrdersCount } from '../../actions/OrdersActions';
import Header from '../../common/Header'

class AdminCallCenterOrdersAndNoOrders extends Component {

    ordersPage = 1;
    noOrdersPage = 1;

    state = {
        networkError: null,
        tabNumber: 1,

        orders: [],
        ordersLoading: true,
        ordersRefresh: false,
        orders404: false,
        ordersPages: null,
        

        noOrders: [],
        noOrdersLoading: true,
        noOrdersRefresh: false,
        noOrders404: false,
        noOrdersPages: null,
       
    }



    componentDidMount() {
        enableSideMenu(false, null)
        //connect to sicket
        this.socket = io("https://api.zeitcoapp.com/order", { query: `id=${this.props.currentUser.user.id}` });
        this.getOrders(false, 1)
        this.getNoOrders(false, 1)
    }


    getOrders = (refresh, page) => {
        if (refresh) {
            this.setState({ ordersRefresh: true })
        }
        
        axios.get(`${BASE_END_POINT}orders?didUser=${this.props.data.id}&page=${page}`)
            .then(response => {
                console.log('Done   ', response.data.data)
                //Alert.alert('Done')
                this.setState({
                    orders: refresh ? response.data.data : [...this.state.orders, ...response.data.data],
                    ordersLoading: false,
                    ordersRefresh: false,
                    ordersLoading: false,
                    ordersPages: response.data.pageCount,
                })
            })
            .catch(error => {
                //Alert.alert('error  ',error)
                console.log('Error   ', error.response)
                this.setState({ orders404: true, ordersLoading: false, })
            })
    }

    getNoOrders = (refresh, page) => {
        if (refresh) {
            this.setState({ assignedOrdersRefresh: true })
        }
        axios.get(`${BASE_END_POINT}restaurant?noOrderEventUser=${this.props.data.id}&noOrder=true&page=${page}`)
            .then(response => {
                console.log('NO ORDERS   ', response.data.data)
                //Alert.alert('ASS Done')
                this.setState({
                    noOrders: refresh ? response.data.data : [...this.state.noOrders, ...response.data.data],
                    noOrdersLoading: false,
                    noOrders404: false,
                    noOrdersRefresh: false,
                    noOrdersPages: response.data.pageCount,
                })
            })
            .catch(error => {
                //Alert.alert('error  ',error)
                console.log('Error ASSIGN   ', error)
                this.setState({ noOrders404: true, noOrdersLoading: false, })
            })
    }



    tabs = () => {
        const { categoryName, isRTL, ordersCount } = this.props;
        const { tabNumber } = this.state;
        return (
            <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', width: responsiveWidth(100), alignItems: 'center' }} >
                <Button onPress={() => {
                    if (tabNumber != 1) {
                        this.setState({ tabNumber: 1 })
                        this.ordersPage = 1
                        this.getOrders(true, 1)
                    }

                }} style={{ flexDirection: isRTL ? 'row-reverse' : 'row', marginLeft: 1, flex: 1, height: responsiveHeight(8), justifyContent: 'center', alignItems: 'center', alignSelf: isRTL ? 'flex-start' : 'flex-end', backgroundColor: tabNumber == 1 ? colors.white : colors.darkGreen }}>
                    <Text style={{ color: tabNumber == 1 ? colors.darkGray : colors.white, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.orders}</Text>
                </Button>

                <Button onPress={() => {
                    if (tabNumber != 2) {
                        this.setState({ tabNumber: 2 })
                        this.noOrdersPage = 1
                        this.getNoOrders(true, 1)
                    }
                }} style={{ marginRight: 1, flex: 1, height: responsiveHeight(8), justifyContent: 'center', alignItems: 'center', alignSelf: isRTL ? 'flex-start' : 'flex-end', backgroundColor: tabNumber == 2 ? colors.white : colors.darkGreen }}>
                    <Text style={{ color: tabNumber == 2 ? colors.darkGray : colors.white, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.noOrders}</Text>
                </Button>

            </View>
        )
    }


    ordersPagee = () => {
        const {orders,newOrderType, ordersPages, orders404, ordersLoading, ordersRefresh } = this.state
        return (
        <View style={{flex:1,}} >
            {
            orders404 ?
                <NetworError />
                :
                ordersLoading ?
                    <Loading />
                    :
                    orders.length > 0 ?
                        <FlatList
                            showsVerticalScrollIndicator={false}
                            style={{ marginBottom: moderateScale(10) }}
                            data={orders}
                            renderItem={({ item }) => <NewOrderCard hideButtons data={item} />}
                            keyExtractor={item => item.id}
                            onEndReachedThreshold={.5}
                            //ListFooterComponent={()=>notificationsLoad&&<ListFooter />}
                            onEndReached={() => {
                                if (this.ordersPage <= ordersPages) {
                                    this.ordersPage = this.ordersPage + 1;
                                    this.getOrders(false, this.ordersPage)
                                }
                            }}
                            refreshControl={
                                <RefreshControl
                                    //colors={["#B7ED03",colors.darkBlue]} 
                                    refreshing={ordersRefresh}
                                    onRefresh={() => {
                                        this.ordersPage = 1
                                        this.getOrders(true, 1)
                                    }}
                                />
                            }

                        />
                        :
                        <NoData />
            }
        </View>   
        )
    }

    norOdersPage = () => {
        const { noOrders, noOrders404, noOrdersLoading, noOrdersRefresh, noOrdersPages } = this.state
        return (
            noOrders404 ?
                <NetworError />
                :
                noOrdersLoading ?
                    <Loading />
                    :
                    noOrders.length > 0 ?
                        <FlatList
                            showsVerticalScrollIndicator={false}
                            style={{ marginBottom: moderateScale(10) }}
                            data={noOrders}
                            renderItem={({ item }) => <AdminCallCenterNoOrdersCard data={item} />}
                            onEndReachedThreshold={.5}
                            //ListFooterComponent={()=>notificationsLoad&&<ListFooter />}
                            onEndReached={() => {
                                if (this.noOrdersPage <= noOrdersPages) {
                                    this.noOrdersPage = this.noOrdersPage + 1;
                                    this.getNoOrders(false, this.noOrdersPage)
                                    console.log('page  ', this.noOrdersPage)
                                }
                            }}
                            refreshControl={
                                <RefreshControl
                                    //colors={["#B7ED03",colors.darkBlue]} 
                                    refreshing={noOrdersRefresh}
                                    onRefresh={() => {
                                        this.noOrdersPage = 1
                                        this.getNoOrders(true, 1)
                                    }}
                                />
                            }

                        />
                        :
                        <NoData />
        )
    }


    render() {
        const { categoryName, isRTL } = this.props;
        const { tabNumber } = this.state;
        return (
            <LinearGradient
                colors={[colors.white, colors.white, colors.white]}
                style={{ flex: 1 }}
            >
                
                <Header  title={Strings.orders} />

                <View style={{ flex:1, height: responsiveHeight(75), backgroundColor: colors.white, marginTop: moderateScale(0), width: responsiveWidth(100),marginBottom:responsiveHeight(7) }} >
                    {this.tabs()}
                    {tabNumber == 1 && this.ordersPagee()}
                    {tabNumber == 2 && this.norOdersPage()}

                </View>
                <AppFooter />
            </LinearGradient>
        );
    }
}

const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    barColor: state.lang.color,
    currentUser: state.auth.currentUser,
    ordersCount: state.orders.ordersCount,

})

const mapDispatchToProps = {
    removeItem,
    OrdersCount,

}

export default connect(mapStateToProps, mapDispatchToProps)(AdminCallCenterOrdersAndNoOrders);
