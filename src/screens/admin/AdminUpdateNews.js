import React, { Component } from 'react';
import {
    View, TouchableOpacity, Image, Text, ScrollView, TextInput, Alert, Platform
} from 'react-native';
import { connect } from 'react-redux';
import { Icon, Button } from 'native-base';
import { login } from '../../actions/AuthActions'
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../../utils/responsiveDimensions";
import Strings from '../../assets/strings';
import LoadingDialogOverlay from '../../components/LoadingDialogOverlay';
import { enableSideMenu, resetTo, push, pop } from '../../controlls/NavigationControll'
import { arrabicFont, englishFont } from '../../common/AppFont'
import * as Animatable from 'react-native-animatable';
import * as colors from '../../assets/colors';
import { removeItem } from '../../actions/MenuActions';
import CollaspeAppHeader from '../../common/CollaspeAppHeader'
import LinearGradient from 'react-native-linear-gradient';
import FastImage from 'react-native-fast-image'
import CommanHeader from '../../common/CommanHeader'
import ImagePicker from 'react-native-image-crop-picker';
import AppFooter from '../../components/AppFooter'
import axios from 'axios';
import {BASE_END_POINT} from '../../AppConfig';
import { RNToasty } from 'react-native-toasty';
import {whiteButton, greenButton} from '../../assets/styles'
import Header from '../../common/Header'


class AdminUpdateNews extends Component {

    state = {
        newsTitle:this.props.data.title,
        newsDescription:this.props.data.description,
        image:null,
        load:false,
    }

    componentDidMount() {
        enableSideMenu(false, null)
    }

    newsTitleInput = () => {
        const { isRTL } = this.props
        const { newsTitle } = this.state
        return (
            <View style={{ marginTop: moderateScale(15), width: responsiveWidth(80), alignSelf: 'center' }}>
                <Text style={{marginBottom:moderateScale(3),fontFamily:isRTL?arrabicFont:englishFont, color:colors.black,marginHorizontal:moderateScale(0),fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.title}</Text>
                <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                    <TextInput
                        value={newsTitle}
                        onChangeText={(val) => { this.setState({ newsTitle: val }) }}
                        style={{ width: responsiveWidth(80), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(5) }}
                        placeholder={Strings.title}
                    />
                </View>
                {newsTitle.length == 0 &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }

    newsDescriptionInput = () => {
        const { isRTL } = this.props
        const { newsDescription } = this.state
        return (
            <View style={{ marginTop: moderateScale(7), width: responsiveWidth(80),  alignSelf: 'center' }}>

                <Text style={{marginBottom:moderateScale(3),fontFamily:isRTL?arrabicFont:englishFont, color:colors.black,marginHorizontal:moderateScale(0),fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.description}</Text>
                <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                    <TextInput
                        multiline
                        value={newsDescription}
                        onChangeText={(val) => { this.setState({ newsDescription: val }) }}
                        style={{ width: responsiveWidth(80), height: responsiveHeight(30), textAlignVertical: 'top', paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont }}
                        placeholder={Strings.description}
                    />
                </View>
                {newsDescription.length == 0 &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }

    newsImage = () => {
        const { isRTL } = this.props
        const { image } = this.state
        return (
            <View style={{width: responsiveWidth(80),alignSelf:'center'}} >
            <View style={{ marginTop: moderateScale(10), width: responsiveWidth(80), height: responsiveHeight(30), alignSelf: 'center', backgroundColor: colors.lightGray }} >
                <FastImage
                    resizeMode='cover'
                    source={{ uri:image?image:this.props.data.img[0] }}
                    style={{ borderWidth: 2, borderColor: colors.lightGray, width: responsiveWidth(80), height: responsiveHeight(30) }}
                />
                <TouchableOpacity onPress={this.pickImage} style={{ flexDirection: isRTL ? 'row-reverse' : 'row', alignSelf: 'center', marginTop: responsiveHeight(-30), justifyContent: 'center', alignItems: 'center', width: responsiveWidth(80), height: responsiveHeight(30), }}>
                    <Icon name='plus' type='AntDesign' style={{ fontSize: responsiveFontSize(7), color: colors.grayColor1, marginHorizontal: moderateScale(2) }} />
                    <Text style={{ color: colors.grayColor1, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.upload}</Text>
                </TouchableOpacity>
            </View>
            {image == false &&
            <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
            }
            </View>
        )
    }

    pickImage = () => {
        ImagePicker.openPicker({
            width: 600,
            height: 600,
            cropping: true
        }).then(image => {
            this.setState({ image: image.path })
            console.log(image);
        });
    }

    submitCancelButtons = () => {
        const { isRTL } = this.props

        return (
            <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', marginTop: moderateScale(7), width: responsiveWidth(77), alignSelf: 'center', justifyContent: 'space-between' }}>
                <TouchableOpacity onPress={() => { this.updatedAds() }} style={[whiteButton, {  marginVertical: moderateScale(15) }]} >
                    <Text style={{ color: colors.green, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.submit}</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => { pop() }} style={[greenButton, {  marginVertical: moderateScale(15) }]} >
                    <Text style={{ color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.cancel}</Text>
                </TouchableOpacity>
            </View>
        )
    }


    updatedAds = () => {
        const { newsTitle, newsDescription,image } = this.state
        if (!newsTitle.replace(/\s/g, '').length) {
            this.setState({ newsTitle: '' })
        }
        if (!newsDescription.replace(/\s/g, '').length) {
            this.setState({ newsDescription: '' })
        }

        if(newsDescription.replace(/\s/g, '').length&&newsTitle.replace(/\s/g, '').length){
            this.setState({load:true})
            var data = new FormData()
            data.append('title',newsTitle)
            data.append('description',newsDescription)
            if(image){
                data.append('img', {
                    uri: image,
                    type: 'multipart/form-data',
                    name: 'photoImage'
                })
            }
            axios.put(`${BASE_END_POINT}news/${this.props.data.id}`,data,{
                headers:{
                    "Content-Type":'application/json',
                    "Authorization": `Bearer ${this.props.currentUser.token}`,
                }
            })
            .then(response=>{
                this.setState({load:false})
                RNToasty.Success({title:Strings.updateNewsSuccessfuly})
                resetTo('AdminHome')
            })
            .catch(error=>{
                this.setState({load:false})
                //Alert.alert(''+error.response)
            })
        }
    }



    

    



    render() {
        const { isRTL, userToken } = this.props;
        return (
            <LinearGradient
                colors={[colors.white, colors.white, colors.white]}
                style={{ flex: 1 }}
            >
                <Header  title={Strings.updateNews} />

                <ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: colors.white, borderTopRightRadius: moderateScale(20), borderTopLeftRadius: moderateScale(20), marginTop: moderateScale(12), width: responsiveWidth(100), marginBottom: moderateScale(20) }} >
                    {this.newsTitleInput()}
                    {this.newsDescriptionInput()}
                    {this.newsImage()}
                    <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', width: responsiveWidth(80), justifyContent: 'space-between', alignSelf: 'center', }}>
                    </View>
                    {this.submitCancelButtons()}
                </ScrollView>
                {this.state.load&&<LoadingDialogOverlay title={Strings.wait} />}

                <AppFooter />

            </LinearGradient>
        );
    }
}
const mapDispatchToProps = {
    login,
    removeItem
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    currentUser:state.auth.currentUser, 
    //userToken: state.auth.userToken,
})


export default connect(mapToStateProps, mapDispatchToProps)(AdminUpdateNews);

