import React, { Component } from 'react';
import { View, RefreshControl, Alert, Modal, ScrollView, FlatList, Text, TouchableOpacity } from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../../utils/responsiveDimensions';
import { connect } from "react-redux";
import Strings from '../../assets/strings';

import axios from 'axios';
import { BASE_END_POINT } from '../../AppConfig';
import ListFooter from '../../components/ListFooter';
import { Icon, Thumbnail, Button } from 'native-base'
import { selectMenu, removeItem } from '../../actions/MenuActions';
import { enableSideMenu, pop } from '../../controlls/NavigationControll'
import NetInfo from "@react-native-community/netinfo";
import Loading from "../../common/Loading"
import NetworError from '../../common/NetworError'
import NoData from '../../common/NoData'
import * as colors from '../../assets/colors'
import { arrabicFont, englishFont } from '../../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image'
import NotificationCard from '../../components/NotificationCard'
import SliderCard from '../../components/SliderCard';
import AssignedOrderCard from '../../components/AssignedOrderCard'
import CollaspeAppHeader from '../../common/CollaspeAppHeader'
import LinearGradient from 'react-native-linear-gradient';
import CommanHeader from '../../common/CommanHeader'
import MapView, { Marker } from 'react-native-maps';
import ImagePicker from 'react-native-image-crop-picker';
import AppFooter from '../../components/AppFooter'
import LoadingDialogOverlay from '../../components/LoadingDialogOverlay';
import { RNToasty } from 'react-native-toasty';
import Header from '../../common/Header'


class AdminEditSlider extends Component {

    page = 1;
    state = {
        image: null,
        detailImage: null,
        networkError: null,
        sliderImgs: [],
        sliderImgsLoading: true,
        sliderImgsRefresh: false,
        sliderImgs404: false,
        pages: null,
    }

    componentDidMount() {
        enableSideMenu(false, null)
        this.getSliderImgs(false, 1)
    }

    getSliderImgs(refresh, page) {

        if (refresh) {
            this.setState({ accountListRefresh: true })
        }
        axios.get(`${BASE_END_POINT}slider?page=${page}`)
            .then(response => {
                console.log('Done   ', response.data.data)
                this.setState({
                    sliderImgs: refresh ? response.data.data : [...this.state.sliderImgs, ...response.data.data],
                    sliderImgsLoading: false,
                    sliderImgsRefresh: false,
                    pages: response.data.pageCount,
                    image:null,
                })
            })
            .catch(error => {
                console.log('Error   ', error)
                this.setState({ sliderImgs404: true, sliderImgsLoading: false, })
            })
    }


    addNewImg = () => {
        const { image } = this.state
        console.log('Image : ', image)
        if (!image) {
            this.setState({ image: false })
        }

        if (image) {
            this.setState({ load: true })
            var data = new FormData()
            data.append('title', 'Test')
            data.append('img', {
                uri: image,
                type: 'multipart/form-data',
                name: 'photoImage'
            })
            axios.post(`${BASE_END_POINT}slider`, data, {
                headers: {
                    "Content-Type": 'application/json',
                    "Authorization": `Bearer ${this.props.currentUser.token}`,
                }
            })
                .then(response => {
                    this.setState({ load: false })
                    RNToasty.Success({ title: Strings.addImageSuccessfuly })
                    this.getSliderImgs(true, 1)
                    //resetTo('AdminHome')
                })
                .catch(error => {
                    this.setState({ load: false })
                    console.log(error.response.data.errors)
                    //RNToasty.Error({ title: error })
                    //Alert.alert(''+error.response)
                })
        }
    }

    renderFooter = () => {
        return (
            this.state.loading ?
                <View style={{ alignSelf: 'center', margin: moderateScale(5) }}>
                    <ListFooter />
                </View>
                : null
        )
    }

    pickImage = () => {
        ImagePicker.openPicker({
            width: 600,
            height: 600,
            cropping: true
        }).then(image => {
            this.setState({ image: image.path })
            console.log(image);
        });
    }

    addSlideSection = () => {
        const { isRTL } = this.props;
        const { image } = this.state
        return (
            <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', alignSelf: 'center', justifyContent: 'space-between', alignItems: 'center', width: responsiveWidth(94), marginTop: moderateScale(6) }}>
                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', }}>
                    <FastImage
                        resizeMode='center'
                        source={image ? { uri: image } : require('../../assets/imgs/back-gray.jpg')}
                        style={{ borderWidth: 2, borderColor: colors.lightGray, width: 80, height: 80, borderRadius: 40 }}
                    />
                    <TouchableOpacity onPress={this.pickImage} style={{ flexDirection: isRTL ? 'row-reverse' : 'row', alignSelf: 'center', justifyContent: 'center', alignItems: 'center' }}>
                        <Icon name='plus' type='AntDesign' style={{ fontSize: responsiveFontSize(7), color: colors.grayColor1, marginHorizontal: moderateScale(2) }} />
                        <Text style={{ color: colors.grayColor1, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.upload}</Text>
                    </TouchableOpacity>
                </View>

                <Button onPress={() => {
                    this.addNewImg()

                }} style={{ width: responsiveWidth(35), height: responsiveHeight(5), justifyContent: 'center', alignItems: 'center', alignSelf: 'center', borderRadius: moderateScale(3), backgroundColor: colors.greenButton }}>
                    <Text style={{ color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(7) }}>{Strings.addNew}</Text>
                </Button>
            </View>
        )
    }



    sliderListPage = () => {
        const { sliderImgs, sliderImgs404, sliderImgsLoading } = this.state
        return (
            <View>
                {sliderImgs404 ?
                    <NetworError />
                    :
                    sliderImgsLoading ?
                        <Loading />
                        :
                        sliderImgs.length > 0 ?
                            <FlatList
                                showsVerticalScrollIndicator={false}
                                style={{ marginBottom: moderateScale(40) }}
                                data={sliderImgs}
                                renderItem={({ item }) => <SliderCard data={item} />}
                            />
                            :
                            <NoData />

                }
            </View>
        )
    }

    render() {
        const { categoryName, isRTL } = this.props;
        const { tabNumber } = this.state;
        return (
            <LinearGradient
                colors={[colors.white, colors.white, colors.white]}
                style={{ flex: 1 }}
            >
                <Header  title={Strings.editSlider} />

                <View style={{flex:1, height: responsiveHeight(82), backgroundColor: colors.white, borderTopRightRadius: moderateScale(20), borderTopLeftRadius: moderateScale(20), marginTop: moderateScale(15), width: responsiveWidth(100), marginBottom:responsiveHeight(10) }} >
                    {this.addSlideSection()}
                    {this.sliderListPage()}
                </View>
                {this.state.load && <LoadingDialogOverlay title={Strings.wait} />}
                <AppFooter />

            </LinearGradient>
        );
    }
}

const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    barColor: state.lang.color,
    currentUser: state.auth.currentUser,
})

const mapDispatchToProps = {
    removeItem,
}

export default connect(mapStateToProps, mapDispatchToProps)(AdminEditSlider);
