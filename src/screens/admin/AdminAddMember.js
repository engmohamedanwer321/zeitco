import React, { Component } from 'react';
import {
    View, TouchableOpacity, Image, Text, ScrollView, TextInput, Alert, Platform, KeyboardAvoidingView
} from 'react-native';
import { connect } from 'react-redux';
import { Icon, Button } from 'native-base';
import { login } from '../../actions/AuthActions'
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../../utils/responsiveDimensions";
import Strings from '../../assets/strings';
import LoadingDialogOverlay from '../../components/LoadingDialogOverlay';
import { enableSideMenu, resetTo, push, pop } from '../../controlls/NavigationControll'
import { arrabicFont, englishFont } from '../../common/AppFont'
import * as Animatable from 'react-native-animatable';
import * as colors from '../../assets/colors';
import { removeItem } from '../../actions/MenuActions';
import CollaspeAppHeader from '../../common/CollaspeAppHeader'
import LinearGradient from 'react-native-linear-gradient';
import FastImage from 'react-native-fast-image'
import CommanHeader from '../../common/CommanHeader'
import ImagePicker from 'react-native-image-crop-picker';
import AppFooter from '../../components/AppFooter'
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import InputValidations from '../../common/InputValidations';
import { BASE_END_POINT } from '../../AppConfig';
import { RNToasty } from 'react-native-toasty'
import axios from 'axios'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { whiteButton } from '../../assets/styles'
import RNPickerSelect from 'react-native-picker-select';
import Header from '../../common/Header'


class AdminAddMember extends Component {

    firstName = null
    lastName = null
    email = null
    phone = null
    userName = null
    jopTitle = null
    password = null
    confirmPassword = null
    type = null


    state = {
        firstName: '', firstNameValidate: ' ',
        lastName: '', lastNameValidate: ' ',
        phone: ' ', phoneValidate: ' ',
        email: '',
        userName: '', userNameValidate: ' ',
        password: '', passwordValidate: ' ',
        warehouse: '', warehouseValidate: ' ',
        confirmPassword: ' ',
        job: ' ',
        title: ' ',
        image: null,
        hidePassword: true,
        type: '', typeValidate: ' ',
        addMemberLoading: false,
        allWarehouses: [],
        warehouseLoading: false,
        showWareHousePicker: false
    }

    users = [
        // this is the parent or 'item'
        // {
        //  name: Strings.users,
        // id: 0,
        // these are the children or 'sub items'
        // children: [
        {
            label: 'ADMIN',
            value: 'ADMIN',
        },
        {
            label: 'OPERATION',
            value: 'OPERATION',
        },
        {
            label: 'SURVEY',
            value: 'SURVEY',
        },
        {
            label: 'PURCHASING',
            value: 'PURCHASING',
        },
        {
            label: 'DRIVER',
            value: 'DRIVER',
        },
        {
            label: 'WAREHOUSE',
            value: 'WAREHOUSE',
        },
        {
            label: 'CALL CENTER',
            value: 'CALL-CENTER',
        },
        //  ],
        //  },
    ];

    componentDidMount() {
        enableSideMenu(false, null)
        this.getWareHouses()
    }

    getWareHouses = () => {


        axios.get(`${BASE_END_POINT}warehouses`)
            .then(response => {
                //   console.log('Type    ', type)
                console.log('Done    ', response.data)
                var rests = []
                response.data.map((item) => {
                    rests.push({ label: item.name, value: item.id })
                })

                this.setState({
                    allWarehouses: rests,
                    warehouseLoading: false,
                })
            })
            .catch(error => {
                console.log('Error   ', error)
                this.setState({ warehouseLoading: false, })
            })
    }


    addMember = () => {
        const { firstName, lastName, title, image, job, phone, type, confirmPassword, userName, password, email, warehouse } = this.state
        const { currentUser } = this.props
        console.log(currentUser.token)

        var status = ''
        var warehouseRequired = false
        if (type == 'WAREHOUSE' || type == 'DRIVER') {
            if (!(warehouse.toString()).replace(/\s/g, '').length) { 
                this.setState({ warehouseValidate: '' }); 
                warehouseRequired = true
             }
            
            
        }

        /*if (!title.replace(/\s/g, '').length) { this.setState({ title: '' }); }*/
        if (!firstName.replace(/\s/g, '').length) { this.setState({ firstNameValidate: '' }); }
        if (!lastName.replace(/\s/g, '').length) { this.setState({ lastNameValidate: '' }); }
        if (!phone.replace(/\s/g, '').length) { this.setState({ phone: '' }); }
        if (!type.replace(/\s/g, '').length) { this.setState({ typeValidate: '' }); }
        if (!userName.replace(/\s/g, '').length) { this.setState({ userNameValidate: '' }); }
        if (!password.replace(/\s/g, '').length) { this.setState({ passwordValidate: '' }); }
        if (email.replace(/\s/g, '').length) {
            if (InputValidations.validateEmail(email) == false) { RNToasty.Error({ title: Strings.emailNotValid }); }
        }
        if (!confirmPassword.replace(/\s/g, '').length) {
            this.setState({ confirmPassword: '' });
        }
        if (!job.replace(/\s/g, '').length) {
            this.setState({ job: '' });
        }
        if (password != confirmPassword) {
            RNToasty.Error({ title: Strings.confirmPasswordMustBeLikePassword })
        }

        if (job.replace(/\s/g, '').length && password == confirmPassword && userName.replace(/\s/g, '').length && firstName.replace(/\s/g, '').length && lastName.replace(/\s/g, '').length && phone.replace(/\s/g, '').length && type.replace(/\s/g, '').length && password.replace(/\s/g, '').length && warehouseRequired == false) {
            this.setState({ addMemberLoading: true })
            var data = new FormData()
            data.append('firstname', firstName)
            if (image) {
                data.append('img', {
                    uri: image,
                    type: 'multipart/form-data',
                    name: 'Img'
                })
            }
            data.append('lastname', lastName)
            if (email.replace(/\s/g, '').length) {
                data.append('email', email)
            }
            data.append('phone', phone)
            //data.append('title', title)
            data.append('type', type)
            data.append('job', job)
            data.append('username', userName.toLowerCase())
            data.append('password', password)
            if (type == 'WAREHOUSE' || type == 'DRIVER') {
                data.append('warehouse', warehouse)
            }


            axios.post(`${BASE_END_POINT}signup`, data, {
                headers: {
                    Authorization: `Bearer ${currentUser.token}`
                }
            })
                .then(response => {
                    this.setState({ addMemberLoading: false })
                    //const res = response.data
                    pop()
                    console.log('Done: ', response)
                    RNToasty.Success({ title: Strings.dataInsertedSuccessfuly })
                })
                .catch(error => {
                    const resError = error.response.data.errors[0]
                    console.log('Error  ', error.response.data.errors)
                    RNToasty.Error({ title: resError.msg })
                    this.setState({ addMemberLoading: false })
                })
        } else { RNToasty.Error({ title: Strings.insertTheRequiredData }); }


    }



    firstNameInput = () => {
        const { isRTL } = this.props
        const { firstName, firstNameValidate } = this.state
        return (
            <View style={{ marginTop: moderateScale(15), width: responsiveWidth(37), }}>

                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, marginHorizontal: moderateScale(0), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.firstName}</Text>
                <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                    <TextInput
                        onChangeText={(val) => { this.setState({ firstName: val, firstNameValidate: val }) }}
                        style={{ width: responsiveWidth(37), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(5), padding: 0 }}
                        placeholder={Strings.firstName}
                        returnKeyType="next"
                        onSubmitEditing={() => { this.lastName.focus(); }}
                    />
                </View>
                {firstNameValidate.length == 0 &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }

    lastNameInput = () => {
        const { isRTL } = this.props
        const { lastName, lastNameValidate } = this.state
        return (
            <View style={{ marginTop: moderateScale(15), width: responsiveWidth(37) }}>

                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, marginHorizontal: moderateScale(0), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.lastName}</Text>
                <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                    <TextInput
                        onChangeText={(val) => { this.setState({ lastName: val, lastNameValidate: val }) }}
                        style={{ width: responsiveWidth(37), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(5), padding: 0 }}
                        placeholder={Strings.lastName}
                        ref={(input) => { this.lastName = input; }}
                        returnKeyType="next"
                        onSubmitEditing={() => { this.email.focus(); }}
                    />
                </View>
                {lastNameValidate.length == 0 &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }


    titleInput = () => {
        const { isRTL } = this.props
        const { title } = this.state
        return (
            <View style={{ marginTop: moderateScale(6), width: responsiveWidth(80), alignSelf: 'center' }}>

                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.title}</Text>
                <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                    <TextInput
                        onChangeText={(val) => { this.setState({ title: val, }) }}
                        style={{ width: responsiveWidth(80), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(5), padding: 0 }}
                        placeholder={Strings.title}

                    //ref={(input) => { this.phone = input; }}
                    />
                </View>
                {title.length == 0 &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(1), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }

    jobInput = () => {
        const { isRTL } = this.props
        const { job } = this.state
        return (
            <View style={{ marginTop: moderateScale(6), width: responsiveWidth(80), alignSelf: 'center' }}>

                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.job}</Text>
                <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                    <TextInput
                        onChangeText={(val) => { this.setState({ job: val }) }}
                        style={{ width: responsiveWidth(80), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(5), padding: 0 }}
                        placeholder={Strings.job}
                        ref={(input) => { this.jopTitle = input; }}
                        returnKeyType="next"
                        onSubmitEditing={() => { this.password.focus(); }}
                    //onSubmitEditing={() => { this.phone.focus(); }}

                    />
                </View>
                {job.length == 0 &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }

    emailInput = () => {
        const { isRTL } = this.props
        const { email } = this.state
        return (
            <View style={{ marginTop: moderateScale(6), width: responsiveWidth(80), alignSelf: 'center' }}>

                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.email}</Text>
                <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                    <TextInput
                        onChangeText={(val) => { this.setState({ email: val }) }}
                        style={{ width: responsiveWidth(80), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(5), padding: 0 }}
                        placeholder={Strings.email}
                        keyboardType={'email-address'}
                        ref={(input) => { this.email = input; }}
                        returnKeyType="next"
                        onSubmitEditing={() => { this.phone.focus(); }}
                    //onSubmitEditing={() => { this.phone.focus(); }}

                    />
                </View>
                {/*email.length == 0 &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                */}
            </View>
        )
    }

    phoneInput = () => {
        const { isRTL } = this.props
        const { phone, phoneValidate } = this.state
        return (
            <View style={{ marginTop: moderateScale(6), width: responsiveWidth(80), alignSelf: 'center' }}>

                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.phone}</Text>
                <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                    <TextInput
                        onChangeText={(val) => { this.setState({ phone: val, phoneNoValidate: val }) }}
                        style={{ width: responsiveWidth(80), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(5), padding: 0 }}
                        placeholder={Strings.phone}
                        keyboardType={'phone-pad'}
                        ref={(input) => { this.phone = input; }}
                        returnKeyType="next"
                        onSubmitEditing={() => { this.userName.focus(); }}
                    //ref={(input) => { this.phone = input; }}
                    />
                </View>
                {phone.length == 0 &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(1), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }


    userNameInput = () => {
        const { isRTL } = this.props
        const { userName, userNameValidate } = this.state
        return (
            <View style={{ marginTop: moderateScale(6), width: responsiveWidth(80), alignSelf: 'center' }}>

                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.userName}</Text>
                <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                    <TextInput
                        onChangeText={(val) => { this.setState({ userName: val, userNameValidate: val }) }}
                        style={{ width: responsiveWidth(80), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(5), padding: 0 }}
                        placeholder={Strings.userName}
                        ref={(input) => { this.userName = input; }}
                        returnKeyType="next"
                        onSubmitEditing={() => { this.jopTitle.focus(); }}
                    />
                </View>
                {userNameValidate.length == 0 &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }


    passwordInput = () => {
        const { isRTL } = this.props
        const { password, passwordValidate } = this.state
        return (
            <View style={{ marginTop: moderateScale(6), width: responsiveWidth(80), alignSelf: 'center' }}>

                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.password}</Text>
                <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                    <TextInput
                        secureTextEntry
                        onChangeText={(val) => { this.setState({ password: val, passwordValidate: val }) }}
                        style={{ width: responsiveWidth(80), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(5), padding: 0, textAlign: isRTL ? 'right' : 'left' }}
                        placeholder={Strings.password}
                        ref={(input) => { this.password = input; }}
                        returnKeyType="next"
                        onSubmitEditing={() => { this.confirmPassword.focus(); }}
                    />
                </View>
                {passwordValidate.length == 0 &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }

    typePicker = () => {
        const { isRTL } = this.props
        const { type, typeValidate } = this.state
        return (
            <View style={{ marginTop: moderateScale(7), alignSelf: 'center', width: responsiveWidth(80), }}>

                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.type}</Text>

                <RNPickerSelect
                    ref={(input) => { this.type = input; }}
                    onValueChange={
                        (item) => {
                            this.setState({
                                type: item,
                                typeValidate: item
                            });
                            if (item == 'WAREHOUSE' || item == 'DRIVER') { this.setState({ showWareHousePicker: true }) } else { this.setState({ showWareHousePicker: false }) }
                            //if (item == 'DRIVER') { this.setState({ showWareHousePicker: true }) } else { this.setState({ showWareHousePicker: false }) }
                        }}
                    items={this.users}

                    placeholder={{ label: Strings.type, value: '' }}
                    style={{
                        inputIOS: { textAlign: isRTL ? 'right' : 'left', fontSize: responsiveFontSize(6), height: responsiveHeight(6), fontFamily: isRTL ? arrabicFont : englishFont, paddingHorizontal: moderateScale(3), paddingRight: responsiveWidth(5), justifyContent: 'center', alignItems: 'center', color: 'gray', backgroundColor: colors.lightGray },
                        inputAndroid: { textAlign: isRTL ? 'right' : 'left', fontSize: responsiveFontSize(6), height: responsiveHeight(6), fontFamily: isRTL ? arrabicFont : englishFont, paddingHorizontal: moderateScale(3), paddingRight: responsiveWidth(5), justifyContent: 'center', alignItems: 'center', color: 'gray', backgroundColor: colors.lightGray }
                    }}
                    placeholderTextColor={'gray'}
                    Icon={() => { return (<Icon name='down' type='AntDesign' style={{ color: 'black', fontSize: responsiveFontSize(6), top: responsiveHeight(2), right: responsiveWidth(1) }} />) }}
                />


                {/*<SectionedMultiSelect
                    expandDropDowns
                    //modalAnimationType='slide'
                    showDropDowns={false}
                    modalWithTouchable
                    hideConfirm
                    searchPlaceholderText={Strings.search}
                    styles={{
                        selectToggle: { width: responsiveWidth(80), height: responsiveHeight(6), borderRadius: moderateScale(1), alignItems: 'center', justifyContent: 'center', backgroundColor: colors.lightGray },
                        selectToggleText: { textAlign: isRTL ? 'right' : 'left', color: 'black', marginTop: moderateScale(6), marginLeft: moderateScale(4), fontFamily: isRTL ? arrabicFont : englishFont },
                        subItemText: { textAlign: isRTL ? 'right' : 'left' },
                        itemText: { fontSize: responsiveFontSize(10), textAlign: isRTL ? 'right' : 'left' },
                        container: { height: responsiveHeight(75), position: 'absolute', width: responsiveWidth(85), top: responsiveHeight(13), alignSelf: 'center' },
                        searchTextInput: { textAlign: isRTL ? 'right' : 'left', marginHorizontal: moderateScale(5) },
                    }}
                    items={this.users}
                    alwaysShowSelectText
                    single
                    uniqueKey="id"
                    subKey="children"

                    searchPlaceholderText='Search type'
                    //hideSearch
                    selectText={this.state.type ? this.state.type : Strings.type}
                    showDropDowns={false}
                    readOnlyHeadings={true}
                    onSelectedItemsChange={(selectedItems) => {
                        // this.setState({ countries: selectedItems });
                    }
                    }
                    onSelectedItemObjectsChange={(types) => {
                        console.log("ITEM2   ", types[0].name)
                        this.setState({ type: types[0].name, typeValidate: types[0].name });
                    }
                    }
                    onConfirm={() => {
                        this.setState({ type: null, typeValidate: ' ' });
                    }}
                />*/}
                {typeValidate.length == 0 &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }


    wareHousePicker = () => {
        const { isRTL } = this.props
        const { warehouse, warehouseValidate, allWarehouses } = this.state
        return (
            <View style={{ marginTop: moderateScale(7), alignSelf: 'center', width: responsiveWidth(80), }}>

                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.warehouses}</Text>

                <RNPickerSelect
                    onValueChange={
                        (item) => {
                            this.setState({
                                warehouse: item,
                                warehouseValidate: item
                            });

                        }}
                    items={allWarehouses}

                    placeholder={{ label: Strings.warehouses, value: '' }}
                    style={{
                        inputIOS: { textAlign: isRTL ? 'right' : 'left', fontSize: responsiveFontSize(6), height: responsiveHeight(6), fontFamily: isRTL ? arrabicFont : englishFont, paddingHorizontal: moderateScale(3), paddingRight: responsiveWidth(5), justifyContent: 'center', alignItems: 'center', color: 'gray', backgroundColor: colors.lightGray },
                        inputAndroid: { textAlign: isRTL ? 'right' : 'left', fontSize: responsiveFontSize(6), height: responsiveHeight(6), fontFamily: isRTL ? arrabicFont : englishFont, paddingHorizontal: moderateScale(3), paddingRight: responsiveWidth(5), justifyContent: 'center', alignItems: 'center', color: 'gray', backgroundColor: colors.lightGray }
                    }}
                    placeholderTextColor={'gray'}
                    Icon={() => { return (<Icon name='down' type='AntDesign' style={{ color: 'black', fontSize: responsiveFontSize(6), top: responsiveHeight(2), right: responsiveWidth(1) }} />) }}
                />

                {warehouseValidate.length == 0 &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }


    confirmPasswordInput = () => {
        const { isRTL } = this.props
        const { confirmPassword } = this.state
        return (
            <View style={{ marginTop: moderateScale(6), width: responsiveWidth(80), alignSelf: 'center' }}>

                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.confirmPassword}</Text>
                <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                    <TextInput
                        secureTextEntry
                        onChangeText={(val) => { this.setState({ confirmPassword: val }) }}
                        style={{ width: responsiveWidth(80), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(5), padding: 0, textAlign: isRTL ? 'right' : 'left' }}
                        placeholder={Strings.confirmPassword}
                        ref={(input) => { this.confirmPassword = input; }}

                    />
                </View>
                {confirmPassword.length == 0 &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }


    save = () => {
        const { firstName, lastName, phone, password, confirmPassword, email, type } = this.state
        if (!email.replace(/\s/g, '').length) {
            this.setState({ email: '' })
        }
        if (!password.replace(/\s/g, '').length) {
            this.setState({ password: '' })
        }
        if (!confirmPassword.replace(/\s/g, '').length) {
            this.setState({ confirmPassword: '' })
        }

        if (!firstName.replace(/\s/g, '').length) {
            this.setState({ firstName: '' })
        }

        if (!lastName.replace(/\s/g, '').length) {
            this.setState({ lastName: '' })
        }

        if (!phone.replace(/\s/g, '').length) {
            this.setState({ phone: '' })
        }

        if (!type) {
            this.setState({ type: '' })
        }

        // this.loadingButton.showLoading(true);
        //this.props.login(phone,password);
    }

    saveButton = () => {
        const { isRTL } = this.props

        return (
            <TouchableOpacity onPress={() => { this.addMember() }} style={[whiteButton, { marginVertical: moderateScale(15), marginBottom: moderateScale(35), alignSelf: 'center' }]} >
                <Text style={{ color: colors.green, fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(7) }}>{Strings.submit}</Text>
            </TouchableOpacity>
        )
    }

    pickImage = () => {
        ImagePicker.openPicker({
            width: 600,
            height: 600,
            cropping: true
        }).then(image => {
            this.setState({ image: image.path })
            console.log(image);
        });
    }

    profileImage = () => {
        const { isRTL } = this.props
        const { image } = this.state
        return (
            <View style={{ marginTop: moderateScale(10), alignSelf: 'center' }} >
                <FastImage
                    resizeMode='center'
                    source={image ? { uri: image } : require('../../assets/imgs/profileicon.jpg')}
                    style={{ borderWidth: 2, borderColor: colors.lightGray, width: 100, height: 100, borderRadius: 50 }}
                />
                <TouchableOpacity onPress={this.pickImage} style={{ alignSelf: 'flex-end', marginTop: moderateScale(-11), justifyContent: 'center', alignItems: 'center', backgroundColor: colors.darkGreen, height: 40, width: 40, borderRadius: 20 }}>
                    <Icon name='photo' type='FontAwesome' style={{ fontSize: responsiveFontSize(7), color: colors.white }} />
                </TouchableOpacity>
            </View>
        )
    }




    render() {
        const { isRTL, userToken } = this.props;
        const { phone, password, hidePassword, email, showWareHousePicker } = this.state
        return (
            <LinearGradient
                colors={[colors.white, colors.white, colors.white]}
                style={{ flex: 1 }}
            >
                <Header title={Strings.addMember} />

                <KeyboardAwareScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: colors.white, borderTopRightRadius: moderateScale(20), borderTopLeftRadius: moderateScale(20), marginTop: moderateScale(5), width: responsiveWidth(100) }}>

                    {this.profileImage()}
                    <View style={{ width: responsiveWidth(80), justifyContent: 'space-between', alignSelf: 'center', flexDirection: isRTL ? 'row-reverse' : 'row' }}>
                        {this.firstNameInput()}
                        {this.lastNameInput()}
                    </View>
                    {this.emailInput()}
                    {this.phoneInput()}
                    {this.userNameInput()}
                    {this.jobInput()}
                    {this.passwordInput()}
                    <View style={{ borderRadius: moderateScale(1), height: 0.1 }} >
                        <TextInput
                            style={{ height: 0.1 }}
                        />
                    </View>
                    {this.confirmPasswordInput()}
                    {this.typePicker()}
                    {showWareHousePicker == true && this.wareHousePicker()}
                    {/*this.titleInput()*/}
                    {this.saveButton()}
                    {this.state.addMemberLoading == true ?
                        <LoadingDialogOverlay title={Strings.wait} />
                        :
                        null
                    }


                </KeyboardAwareScrollView>
                <AppFooter />
            </LinearGradient>
        );
    }
}
const mapDispatchToProps = {
    login,
    removeItem
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    loading: state.auth.loading,
    errorText: state.auth.errorText,
    currentUser: state.auth.currentUser,
    //userToken: state.auth.userToken,
})


export default connect(mapToStateProps, mapDispatchToProps)(AdminAddMember);

