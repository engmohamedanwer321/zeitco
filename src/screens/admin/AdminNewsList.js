import React, { Component } from 'react';
import { View, RefreshControl, Alert, Modal, ScrollView, FlatList, Text, TouchableOpacity } from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../../utils/responsiveDimensions';
import { connect } from "react-redux";
import Strings from '../../assets/strings';

import axios from 'axios';
import { BASE_END_POINT } from '../../AppConfig';
import ListFooter from '../../components/ListFooter';
import { Icon, Thumbnail, Button } from 'native-base'
import { selectMenu, removeItem } from '../../actions/MenuActions';
import { enableSideMenu, pop,push } from '../../controlls/NavigationControll'
import NetInfo from "@react-native-community/netinfo";
import Loading from "../../common/Loading"
import NetworError from '../../common/NetworError'
import NoData from '../../common/NoData'
import * as colors from '../../assets/colors'
import { arrabicFont, englishFont } from '../../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image'
import NotificationCard from '../../components/NotificationCard'
import NewsCard from '../../components/NewsCard';
import AssignedOrderCard from '../../components/AssignedOrderCard'
import CollaspeAppHeader from '../../common/CollaspeAppHeader'
import LinearGradient from 'react-native-linear-gradient';
import CommanHeader from '../../common/CommanHeader'
import MapView, { Marker } from 'react-native-maps';
import AppFooter from '../../components/AppFooter'
import {whiteButton, greenButton} from '../../assets/styles'
import Header from '../../common/Header'


class AdminNewsList extends Component {

    page = 1;
    state = {
        networkError: null,
        news:[],
        newsLoading: true,
        newsRefresh:false,
        news404:false,
        pages: null,
    }

    componentDidMount() {
        enableSideMenu(false, null)
        this.getNews(false,1)
    }

    getNews = (refresh,page) => {
        if(refresh){
            this.setState({newsRefresh:true})
        }
        axios.get(`${BASE_END_POINT}news?page=${page}`)
        .then(response=>{
          console.log('Done   ',response.data.data)
          this.setState({
              news:refresh?response.data.data:[...this.state.news,...response.data.data],
              newsLoading:false,
              newsRefresh:false,
              pages:response.data.pageCount,
            })
          })
        .catch(error=>{
          console.log('Error   ',error.response)
          this.setState({news404:true,newsLoading:false,})
        })
    }

    addNewsButton = () => {
        const { isRTL } = this.props;
        return (
            <View style={{marginTop:moderateScale(12), alignSelf: 'center', justifyContent: 'center', alignItems: 'center', width: responsiveWidth(40) }}>
                <TouchableOpacity onPress={() => {
                    push('AdminAddNews')
                }} style={[whiteButton,{ width: responsiveWidth(35), height: responsiveHeight(5), justifyContent: 'center', alignItems: 'center', alignSelf: 'center' }]}>
                    <Text style={{ color: colors.green, fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(7) }}>{Strings.addNews}</Text>
                </TouchableOpacity>
            </View>
        )
    }

    render() {
        const { categoryName, isRTL } = this.props;
        const {newsLoading,news,news404,newsRefresh,pages} = this.state
        return (
            <LinearGradient
                colors={[colors.white, colors.white, colors.white]}
                style={{ flex: 1 }}
            >
                <Header  title={Strings.news} />

                <View style={{flex:1, height:responsiveHeight(73), backgroundColor: colors.white, borderTopRightRadius: moderateScale(20), borderTopLeftRadius: moderateScale(20),marginTop: moderateScale(15), width: responsiveWidth(100), marginBottom:responsiveHeight(7) }} >
                    {this.addNewsButton()}
                    {
                          news404?
                          <NetworError />
                          :
                          newsLoading?
                          <Loading />
                          :
                          news.length>0?
                          <FlatList 
                          contentContainerStyle={{paddingBottom:moderateScale(5)}}
                          style={{}}
                          data={news}
                          renderItem={({item})=><NewsCard data={item}/>}
                          onEndReachedThreshold={.5}
                          //ListFooterComponent={()=>notificationsLoad&&<ListFooter />}
                          onEndReached={() => {     
                              if(this.page <= pages){
                                  this.page=this.page+1;
                                  this.getNews(false,this.page)
                                  console.log('page  ',this.page)
                              }  
                          }}
                          refreshControl={
                          <RefreshControl 
                          //colors={["#B7ED03",colors.darkBlue]} 
                          refreshing={newsRefresh}
                          onRefresh={() => {
                              this.page = 1
                              this.getNews(true,1)
                          }}
                          />
                          }
                          
                          />
                          :
                          <NoData />
                    }

                </View>

                <AppFooter/>

            </LinearGradient>
        );
    }
}

const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    barColor: state.lang.color,
    currentUser: state.auth.currentUser,
})

const mapDispatchToProps = {
    removeItem,
}

export default connect(mapStateToProps, mapDispatchToProps)(AdminNewsList);
