import React, { Component } from 'react';
import {
    View, TouchableOpacity, Linking, Text, ScrollView, TextInput, Alert, Platform
} from 'react-native';
import { connect } from 'react-redux';
import { Icon, Button } from 'native-base';
import { login } from '../../actions/AuthActions'
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../../utils/responsiveDimensions";
import Strings from '../../assets/strings';
import LoadingDialogOverlay from '../../components/LoadingDialogOverlay';
import { enableSideMenu, resetTo, push } from '../../controlls/NavigationControll'
import { arrabicFont, englishFont } from '../../common/AppFont'
import * as Animatable from 'react-native-animatable';
import * as colors from '../../assets/colors';
import { removeItem } from '../../actions/MenuActions';
import CollaspeAppHeader from '../../common/CollaspeAppHeader'
import LinearGradient from 'react-native-linear-gradient';
import FastImage from 'react-native-fast-image'
import CommanHeader from '../../common/CommanHeader'
import ImagePicker from 'react-native-image-crop-picker';
import AppFooter from '../../components/AppFooter'
import { BASE_END_POINT } from '../../AppConfig';
import axios from 'axios';
import { RNToasty } from 'react-native-toasty';
import Dialog, { DialogContent, DialogTitle } from 'react-native-popup-dialog';
import { whiteButton, greenButton } from '../../assets/styles'
import Header from '../../common/Header'


class AdminWarehouseDetails extends Component {

    state = {
        loading: false,
        showDeleteDialog: false,
        reason:'',
    }

    componentDidMount() {
        enableSideMenu(false, null)
        console.log('FFFF : ', this.props.data)
    }

    confirmResturant = () => {
        const { currentUser, data } = this.props

        this.setState({ loading: true })
        axios.put(`${BASE_END_POINT}restaurant/${this.props.data.id}/approved`, null, {
            headers: {
                "Authorization": `Bearer ${this.props.currentUser.token}`,
            }
        })
            .then(response => {
                RNToasty.Success({ title: Strings.resturantApprovedSuccessfuly })
                this.setState({ loading: false })
                if (currentUser.user.type == 'ADMIN') { resetTo('AdminHome') }
                else if (currentUser.user.type == 'OPERATION') { resetTo('OperationHome') }
            })
            .catch(error => {
                this.setState({ loading: false })
                RNToasty.Error({ title: Strings.setUserNamepassword })
                push('UpdateResturantAccount', data)
            })
    }


    deleteResturant = () => {
        const { currentUser } = this.props
        this.setState({ showDeleteDialog: false })
        axios.delete(`${BASE_END_POINT}restaurant/${this.props.data.id}`, {
            headers: {
                "Authorization": `Bearer ${this.props.currentUser.token}`,
            }
        })
            .then(response => {
                RNToasty.Success({ title: Strings.resturantDeletedSuccessfuly })
                if (currentUser.user.type == 'ADMIN') { resetTo('AdminHome') }
                else if (currentUser.user.type == 'OPERATION') { resetTo('OperationHome') }
            })
            .catch(error => {

                RNToasty.Error({ title: Strings.errorInDelete })
            })
    }

    declineResturant() {
        const { currentUser } = this.props
        const {reason} = this.state
        if(!reason.replace(/\s/g, '').length){
            RNToasty.Error({ title: Strings.pleaseEnterDeclineReason })
        }else{
            const data = {
                declineReason:reason,
            }
            axios.put(`${BASE_END_POINT}restaurant/${this.props.data.id}/ignore`, JSON.stringify({data}), {
                headers: {
                    "Authorization": `Bearer ${this.props.currentUser.token}`,
                    "Content-Type": "application/json"
                }
            })
            .then(response => {
                console.log('DONE')
                this.setState({ loading: false })
                if (currentUser.user.type == 'ADMIN') { resetTo('AdminHome') }
                else if (currentUser.user.type == 'OPERATION') { resetTo('OperationHome') }
                RNToasty.Success({ title: Strings.resturantDeclinedSuccessfuly })
            })
            .catch(error => {
                this.setState({ loading: false })
                console.log('ERROR   ', error)
                RNToasty.Error({ title: Strings.errorInDelete })
            })
        }
    }



    deleteDialog = () => {
        const { isRTL, data } = this.props;
        return (
            <Dialog
                containerStyle={{ backgroundColor: 'rgba(1,1,1,0.1)', }}
                visible={this.state.showDeleteDialog}
                onTouchOutside={() => {
                    this.setState({ showDeleteDialog: false });
                }}
                onHardwareBackPress={() => {
                    this.setState({ showDeleteDialog: false });
                }}
            >
                <View style={{ backgroundColor: 'white', justifyContent: 'center', alignItems: 'center', width: responsiveWidth(90), height: responsiveHeight(30) }}>
                    <Text style={{ color: colors.darkGreen, fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6) }}>{Strings.areYouSureToDeclineAccount}</Text>
                    
                    <TextInput
                    placeholder={Strings.declineReason}
                    style={{color:'black', marginVertical:moderateScale(10),width:responsiveWidth(80),backgroundColor:colors.lightGray,height:responsiveHeight(7),paddingVertical:0,borderRadius:moderateScale(3)}}
                    onChangeText={(val)=>this.setState({reason:val})}
                    />
                    
                    <View style={{  flexDirection: isRTL ? 'row-reverse' : 'row', width: responsiveWidth(60), justifyContent: 'space-between', alignItems: 'center' }}>
                        <Button onPress={() => {
                            this.setState({ showDeleteDialog: false })
                        }} style={{ width: responsiveWidth(28), height: responsiveHeight(6), justifyContent: 'center', alignItems: 'center', alignSelf: isRTL ? 'flex-start' : 'flex-end', borderRadius: moderateScale(0), backgroundColor: 'white' }}>
                            <Text style={{ color: colors.black, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.no}</Text>
                        </Button>
                        <Button onPress={() => {
                            this.declineResturant()
                        }} style={{ width: responsiveWidth(28), height: responsiveHeight(6), justifyContent: 'center', alignItems: 'center', alignSelf: isRTL ? 'flex-start' : 'flex-end', borderRadius: moderateScale(0), backgroundColor: colors.lightGreenButton }}>
                            <Text style={{ color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.yes}</Text>
                        </Button>

                    </View>
                </View>
            </Dialog>
        )
    }

    basicsData = () => {
        const { isRTL, data } = this.props
        return (
            <View style={{ width: responsiveWidth(90), alignItems: isRTL ? 'flex-end' : 'flex-start', alignSelf: 'center', marginTop: moderateScale(5) }}>
                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row' }}>
                    <View style={{ width: responsiveWidth(80) }}>
                        <Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), marginTop: moderateScale(5), textAlign: isRTL ? 'right' : 'left' }} >{Strings.id} : {data.id}</Text>
                        <Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), marginTop: moderateScale(5), textAlign: isRTL ? 'right' : 'left' }} >{Strings.name} : {data.name} </Text>
                        <Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), marginTop: moderateScale(5), textAlign: isRTL ? 'right' : 'left' }} >{Strings.quantity} : {data.oilQuantity} {Strings.Kg}</Text>
                        <Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), marginTop: moderateScale(5), textAlign: isRTL ? 'right' : 'left' }} >{Strings.receiveOliQuantity} : {data.receiveOli} {Strings.Kg}</Text>
                        <Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), marginTop: moderateScale(5), textAlign: isRTL ? 'right' : 'left' }} >{Strings.exportOliQuantity} : {data.exportOli} {Strings.Kg}</Text>
                        
                    </View>
                </View>
            </View>
        )
    }




    submit_cancel_buttons = () => {
        const { isRTL, data } = this.props
        return (
            <View style={{ alignSelf: 'center', marginVertical: moderateScale(20), }} >
                <View style={{ width: responsiveWidth(80) }}>

                    {data.status != 'APPROVED' &&
                        <TouchableOpacity onPress={() => {
                            this.confirmResturant()
                        }} style={whiteButton} >
                            <Text style={{ paddingHorizontal: moderateScale(5), color: colors.green, fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(7) }}>{Strings.approve}</Text>
                        </TouchableOpacity>
                    }
                </View>

                <View style={{ width: responsiveWidth(80), flexDirection: isRTL ? 'row-reverse' : 'row', marginVertical: moderateScale(10), justifyContent: 'space-between' }}>
                    <TouchableOpacity onPress={() => {
                        push('UpdateResturantAccount', data)
                    }} style={whiteButton} >
                        <Text style={{ paddingHorizontal: moderateScale(5), color: colors.green, fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(7) }}>{Strings.edit}</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => {
                        this.setState({ showDeleteDialog: true })
                    }} style={greenButton} >
                        <Text style={{ paddingHorizontal: moderateScale(5), color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(7) }}>{Strings.Decline}</Text>
                    </TouchableOpacity>
                </View>


            </View>
        )
    }




    render() {
        const { isRTL, userToken } = this.props;
        return (
            <LinearGradient
                colors={[colors.white, colors.white, colors.white]}
                style={{ flex: 1 }}
            >
                <Header title={Strings.warehouseDetails} />

                <ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: colors.white, borderTopRightRadius: moderateScale(20), borderTopLeftRadius: moderateScale(20), marginTop: moderateScale(5), width: responsiveWidth(100), marginBottom: moderateScale(20) }} >
                    {this.basicsData()}
                </ScrollView>
            
                <AppFooter />

            </LinearGradient>
        );
    }
}
const mapDispatchToProps = {
    login,
    removeItem
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    loading: state.auth.loading,
    errorText: state.auth.errorText,
    currentUser: state.auth.currentUser,
    //userToken: state.auth.userToken,
})


export default connect(mapToStateProps, mapDispatchToProps)(AdminWarehouseDetails);

