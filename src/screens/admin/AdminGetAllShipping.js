import React, { Component } from 'react';
import { View, RefreshControl, ScrollView, FlatList, Text, TouchableOpacity } from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../../utils/responsiveDimensions';
import { connect } from "react-redux";
import Strings from '../../assets/strings';

import axios from 'axios';
import { BASE_END_POINT } from '../../AppConfig';
import ListFooter from '../../components/ListFooter';
import { Icon, Thumbnail, Button } from 'native-base'
import { selectMenu, removeItem } from '../../actions/MenuActions';
import { enableSideMenu, pop } from '../../controlls/NavigationControll'
import NetInfo from "@react-native-community/netinfo";
import Loading from "../../common/Loading"
import NetworError from '../../common/NetworError'
import NoData from '../../common/NoData'
import * as colors from '../../assets/colors'
import { arrabicFont, englishFont } from '../../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image'
import WarehouseShipmentCard from '../../components/WarehouseShipmentCard';
import CollaspeAppHeader from '../../common/CollaspeAppHeader'
import LinearGradient from 'react-native-linear-gradient';
import CommanHeader from '../../common/CommanHeader'
import AppFooter from '../../components/AppFooter'
import Header from '../../common/Header'


class AdminGetAllShipping extends Component {

    page = 1;
    state = {
        networkError: null,
        shipments: [],
        shipmentsLoading: true,
        shipmentsRefresh: false,
        shipments404: false,
        pages: null,
    }



    componentDidMount() {
        enableSideMenu(false, null)
        this.getShipments(false, 1)
    }


    renderFooter = () => {
        return (
            this.state.loading ?
                <View style={{ alignSelf: 'center', margin: moderateScale(5) }}>
                    <ListFooter />
                </View>
                : null
        )
    }

    getShipments = (refresh, page) => {
        if (refresh) {
            this.setState({ shipmentsRefresh: true })
        }
        //done=false&
        axios.get(`${BASE_END_POINT}shipping?page=${page}`, {
            headers: {
                Authorization: `Bearer ${this.props.currentUser.token}`
            }
        })
            .then(response => {
                console.log('Done   ', response.data.data)
                this.setState({
                    shipments: refresh ? response.data.data : [...this.state.shipments, ...response.data.data],
                    shipmentsLoading: false,
                    shipmentsRefresh: false,
                    pages: response.data.pageCount,
                })
            })
            .catch(error => {
                console.log('Error   ', error.response)
                this.setState({ shipments404: true, shipmentsLoading: false, })
            })
    }


    render() {
        const { categoryName, isRTL } = this.props;
        const { pages, shipments, shipmentsLoading, shipments404, shipmentsRefresh } = this.state
        return (
            <LinearGradient
                colors={[colors.white, colors.white, colors.white]}
                style={{ flex: 1 }}
            >
                <Header title={Strings.shipping} />


                <View style={{ flex: 1, height: responsiveHeight(73), backgroundColor: colors.white, borderTopRightRadius: moderateScale(20), borderTopLeftRadius: moderateScale(20), marginTop: moderateScale(5), width: responsiveWidth(100), marginBottom: responsiveHeight(7.5) }} >
                    {
                        shipments404 ?
                            <NetworError />
                            :
                            shipmentsLoading ?
                                <Loading />
                                :
                                shipments.length > 0 ?
                                    <FlatList
                                        showsVerticalScrollIndicator={false}
                                        style={{ marginBottom: moderateScale(10) }}
                                        data={shipments}
                                        renderItem={({ item }) => <WarehouseShipmentCard data={item} />}
                                        onEndReachedThreshold={.5}
                                        //ListFooterComponent={()=>notificationsLoad&&<ListFooter />}
                                        onEndReached={() => {
                                            if (this.page <= pages) {
                                                this.page = this.page + 1;
                                                this.getShipments(false, this.page)
                                                console.log('page  ', this.page)
                                            }
                                        }}
                                        refreshControl={
                                            <RefreshControl
                                                //colors={["#B7ED03",colors.darkBlue]} 
                                                refreshing={shipmentsRefresh}
                                                onRefresh={() => {
                                                    this.page = 1
                                                    this.getShipments(true, 1)
                                                }}
                                            />
                                        }

                                    />
                                    :
                                    <NoData />
                    }

                </View>
                <AppFooter />
            </LinearGradient>
        );
    }
}

const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    barColor: state.lang.color,
    currentUser: state.auth.currentUser,
})

const mapDispatchToProps = {
    removeItem,
}

export default connect(mapStateToProps, mapDispatchToProps)(AdminGetAllShipping);
