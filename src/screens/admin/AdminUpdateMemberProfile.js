import React, { Component } from 'react';
import {
    View, TouchableOpacity, Image, Text, ScrollView, TextInput, Alert, Platform
} from 'react-native';
import { connect } from 'react-redux';
import { Icon, Button } from 'native-base';
import { setUser } from '../../actions/AuthActions'
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../../utils/responsiveDimensions";
import Strings from '../../assets/strings';
import LoadingDialogOverlay from '../../components/LoadingDialogOverlay';
import { enableSideMenu, resetTo, push } from '../../controlls/NavigationControll'
import { arrabicFont, englishFont } from '../../common/AppFont'
import * as Animatable from 'react-native-animatable';
import * as colors from '../../assets/colors';
import { removeItem } from '../../actions/MenuActions';
import CollaspeAppHeader from '../../common/CollaspeAppHeader'
import LinearGradient from 'react-native-linear-gradient';
import FastImage from 'react-native-fast-image'
import CommanHeader from '../../common/CommanHeader'
import ImagePicker from 'react-native-image-crop-picker';
import AppFooter from '../../components/AppFooter'
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios';
import { BASE_END_POINT } from '../../AppConfig';
import { RNToasty } from 'react-native-toasty';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { whiteButton, greenButton } from '../../assets/styles'
import RNPickerSelect from 'react-native-picker-select';
import Header from '../../common/Header'
import AppCheckBox from '../../common/AppCheckBox'


class AdminUpdateMemberProfile extends Component {

    firstName=null
    lastName=null
    email=null
    phone=null
    userName=null
    confirmPassword=null

    state = {
        firstName: this.props.data.firstname,
        lastName: this.props.data.lastname,
        phone: this.props.data.phone,
        email: this.props.data.email ? this.props.data.email : '',
        userName: this.props.data.username,
        job: this.props.data.job ? this.props.data.job : ' ',
        password: ' ',
        confirmPassword: ' ',
        title: this.props.data.title ? this.props.data.title : ' ',
        warehouse: this.props.data.warehouse, warehouseValidate: ' ',
        image: null,
        hidePassword: true,
        loading: false,
        allWarehouses: [],
        warehouseLoading: false,
        showWareHousePicker: false,
        enablePassword: false

    }

    componentDidMount() {
        enableSideMenu(false, null)
        this.getWareHouses()
    }

    getWareHouses = () => {


        axios.get(`${BASE_END_POINT}warehouses`)
            .then(response => {
                //   console.log('Type    ', type)
                console.log('Done    ', response.data)
                var rests = []
                response.data.map((item) => {
                    rests.push({ label: item.name, value: item.id })
                })

                this.setState({
                    allWarehouses: rests,
                    warehouseLoading: false,
                })
            })
            .catch(error => {
                console.log('Error   ', error)
                this.setState({ warehouseLoading: false, })
            })
    }


    firstNameInput = () => {
        const { isRTL } = this.props
        const { firstName, } = this.state
        return (
            <View style={{ marginTop: moderateScale(15), width: responsiveWidth(37), }}>

                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, marginHorizontal: moderateScale(0), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.firstName}</Text>
                <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                    <TextInput
                        value={firstName}
                        onChangeText={(val) => { this.setState({ firstName: val }) }}
                        style={{ width: responsiveWidth(37), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(5), padding: 0 }}
                        placeholder={Strings.firstName}
                        returnKeyType='next'
                        onSubmitEditing={() => {this.lastName.focus(); }}
                    />
                </View>
                {firstName.length == 0 &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }

    lastNameInput = () => {
        const { isRTL } = this.props
        const { lastName } = this.state
        return (
            <View style={{ marginTop: moderateScale(15), width: responsiveWidth(37) }}>

                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, marginHorizontal: moderateScale(0), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.lastName}</Text>
                <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                    <TextInput
                        value={lastName}
                        onChangeText={(val) => { this.setState({ lastName: val }) }}
                        style={{ width: responsiveWidth(37), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(5), padding: 0 }}
                        placeholder={Strings.lastName}
                        ref={(input) => { this.lastName = input; }}
                        returnKeyType='next'
                        onSubmitEditing={() => {this.email.focus(); }}
                    />
                </View>
                {lastName.length == 0 &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }

    emailInput = () => {
        const { isRTL } = this.props
        const { email } = this.state
        return (
            <View style={{ marginTop: moderateScale(6), width: responsiveWidth(80), alignSelf: 'center' }}>

                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.email}</Text>
                <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                    <TextInput
                        value={email.replace(/\s/g, '').length ? email : null}
                        onChangeText={(val) => { this.setState({ email: val }) }}
                        style={{ width: responsiveWidth(80), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(5), padding: 0 }}
                        placeholder={Strings.email}
                        ref={(input) => { this.email = input; }}
                        returnKeyType='next'
                        onSubmitEditing={() => {this.phone.focus(); }}
                        
                    />
                </View>
                {/*email.length == 0 &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                */}
            </View>
        )
    }

    titleInput = () => {
        const { isRTL } = this.props
        const { title } = this.state
        return (
            <View style={{ marginTop: moderateScale(6), width: responsiveWidth(80), alignSelf: 'center' }}>

                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.title}</Text>
                <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                    <TextInput
                        value={title.replace(/\s/g, '').length ? title : null}
                        onChangeText={(val) => { this.setState({ title: val, }) }}
                        style={{ width: responsiveWidth(80), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(5), padding: 0 }}
                        placeholder={Strings.title}

                    //ref={(input) => { this.phone = input; }}
                    />
                </View>
                {title.length == 0 &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(1), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }

    phoneInput = () => {
        const { isRTL } = this.props
        const { phone } = this.state
        return (
            <View style={{ marginTop: moderateScale(6), width: responsiveWidth(80), alignSelf: 'center' }}>

                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.phone}</Text>
                <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                    <TextInput
                        value={phone}
                        onChangeText={(val) => { this.setState({ phone: val }) }}
                        style={{ width: responsiveWidth(80), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(5), padding: 0 }}
                        placeholder={Strings.phone}
                        ref={(input) => { this.phone = input; }}
                        returnKeyType='next'
                        onSubmitEditing={() => {this.userName.focus(); }}
                        keyboardType='phone-pad'
                    />
                </View>
                {phone.length == 0 &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }


    userNameInput = () => {
        const { isRTL } = this.props
        const { userName } = this.state
        return (
            <View style={{ marginTop: moderateScale(6), width: responsiveWidth(80), alignSelf: 'center' }}>

                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.userName}</Text>
                <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                    <TextInput
                        value={userName}
                        onChangeText={(val) => { this.setState({ userName: val }) }}
                        style={{ width: responsiveWidth(80), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(5), padding: 0 }}
                        placeholder={Strings.userName}
                        ref={(input) => { this.userName = input; }}

                    />
                </View>
                {userName.length == 0 &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }


    jobInput = () => {
        const { isRTL } = this.props
        const { job, userName } = this.state
        return (
            <View style={{ marginTop: moderateScale(6), width: responsiveWidth(80), alignSelf: 'center' }}>

                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.job}</Text>
                <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                    <TextInput
                        value={job.replace(/\s/g, '').length ? job : ''}

                        onChangeText={(val) => { this.setState({ job: val }) }}
                        style={{ width: responsiveWidth(80), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(5), padding: 0 }}
                        placeholder={Strings.job}

                    //onSubmitEditing={() => { this.phone.focus(); }}

                    />
                </View>
                {job.length == 0 &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }

    passwordInput = () => {
        const { isRTL } = this.props
        const { password } = this.state
        return (
            <View style={{ marginTop: moderateScale(6), width: responsiveWidth(80), alignSelf: 'center' }}>

                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.password}</Text>
                <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                    <TextInput
                        secureTextEntry
                        onChangeText={(val) => { this.setState({ password: val }) }}
                        style={{ width: responsiveWidth(80), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(5), padding: 0, textAlign: isRTL ? 'right' : 'left' }}
                        placeholder={Strings.password}
                        returnKeyType='next'
                        onSubmitEditing={() => {this.confirmPassword.focus(); }}
                    />
                </View>

            </View>
        )
    }

    confirmPasswordInput = () => {
        const { isRTL } = this.props
        const { confirmPassword } = this.state
        return (
            <View style={{ marginTop: moderateScale(6), width: responsiveWidth(80), alignSelf: 'center' }}>

                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.confirmPassword}</Text>
                <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                    <TextInput
                        secureTextEntry
                        onChangeText={(val) => { this.setState({ confirmPassword: val }) }}
                        style={{ width: responsiveWidth(80), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(5), padding: 0, textAlign: isRTL ? 'right' : 'left' }}
                        placeholder={Strings.confirmPassword}
                        ref={(input) => { this.confirmPassword = input; }}
                    />
                </View>

            </View>
        )
    }

    wareHousePicker = () => {
        const { isRTL } = this.props
        const { warehouse, warehouseValidate, allWarehouses } = this.state
        return (
            <View style={{ marginTop: moderateScale(7), alignSelf: 'center', width: responsiveWidth(80), }}>

                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.warehouses}</Text>

                <RNPickerSelect
                    onValueChange={
                        (item) => {
                            this.setState({
                                warehouse: item,
                                warehouseValidate: item
                            });

                        }}
                    items={allWarehouses}
                    value={warehouse}
                    placeholder={{ label: Strings.warehouses, value: '' }}
                    style={{
                        inputIOS: { textAlign: isRTL ? 'right' : 'left', fontSize: responsiveFontSize(6), height: responsiveHeight(5), fontFamily: isRTL ? arrabicFont : englishFont, paddingHorizontal: moderateScale(3), paddingRight: responsiveWidth(5), justifyContent: 'center', alignItems: 'center', color: 'gray', backgroundColor: colors.lightGray },
                        inputAndroid: { textAlign: isRTL ? 'right' : 'left', fontSize: responsiveFontSize(6), height: responsiveHeight(5), fontFamily: isRTL ? arrabicFont : englishFont, paddingHorizontal: moderateScale(3), paddingRight: responsiveWidth(5), justifyContent: 'center', alignItems: 'center', color: 'gray', backgroundColor: colors.lightGray }
                    }}
                    placeholderTextColor={'gray'}
                    Icon={() => { return (<Icon name='down' type='AntDesign' style={{ color: 'black', fontSize: responsiveFontSize(6), top: responsiveHeight(2), right: responsiveWidth(1) }} />) }}
                />

                {warehouseValidate.length == 0 &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }


    save = () => {
        const { firstName, title, lastName, job, phone, confirmPassword, userName, password, email, image, warehouse , enablePassword} = this.state
        const { currentUser, data } = this.props



        /*if (!job.replace(/\s/g, '').length) {
            this.setState({ job: '' }); 
       }*/

        if (!firstName.replace(/\s/g, '').length) {
            this.setState({ firstName: '' })
        }
        /*if (!title.replace(/\s/g, '').length) {
            this.setState({ title: '' })
        }*/

        if (!lastName.replace(/\s/g, '').length) {
            this.setState({ lastName: '' })
        }

        if (!phone.replace(/\s/g, '').length) {
            this.setState({ phone: '' })
        }

        if (firstName.replace(/\s/g, '').length && lastName.replace(/\s/g, '').length && phone.replace(/\s/g, '').length) {
            var user = new FormData()

            user.append('firstname', firstName)
            if (job.replace(/\s/g, '').length) {
                user.append('job', job)
            }
            user.append('lastname', lastName)
            //user.append('title', title)
            if (email.replace(/\s/g, '').length) {
                user.append('email', email)
            }
            user.append('phone', phone)
            user.append('username', userName.toLowerCase())

            user.append('type', this.props.data.type)
            if (image) {
                user.append('img', {
                    uri: image,
                    type: 'multipart/form-data',
                    name: 'photoImage'
                })
            }
            if (this.props.data.type == 'WAREHOUSE' || this.props.data.type == 'DRIVER') {
                user.append('warehouse', warehouse)
            }

            if (enablePassword){
            if (password.replace(/\s/g, '').length) {
                if (confirmPassword.replace(/\s/g, '').length) {
                    this.setState({ confirmPassword: ' ' })
                }

                if (password != confirmPassword) {
                    RNToasty.Error({ title: Strings.confirmPasswordMustBeLikePassword })
                } else {
                    user.append('password', password)
                    this.updateProfile(user)

                }
            }else{
                RNToasty.Error({ title: Strings.insertNewPassword })
            }

        }else{
            this.updateProfile(user)
        }



        }
    }

    updateProfile = (user)=>{
        const { currentUser, data } = this.props
        this.setState({ loading: true })
        axios.put(`${BASE_END_POINT}user/${data.id}/updateAdminInfo`, user, {
            headers: {
                "Authorization": `Bearer ${currentUser.token}`,
                "Content-Type": "application/json"
            }
        })
            .then(response => {
                console.log('DONE')
                this.setState({ loading: false })
                resetTo('AdminHome')
                RNToasty.Success({ title: Strings.profileUpdatSuccessfuly })
            })
            .catch(error => {
                this.setState({ loading: false })
                console.log('ERROR   ', error.response)
                RNToasty.Error({ title: Strings.incorrectPassword })
            })
    }


    saveButton = () => {
        const { isRTL } = this.props

        return (
            <TouchableOpacity onPress={() => { this.save() }} style={[whiteButton, { marginVertical: moderateScale(15), marginBottom: moderateScale(35), alignSelf: 'center' }]} >
                <Text style={{ color: colors.green, fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(7) }}>{Strings.update}</Text>
            </TouchableOpacity>
        )
    }

    pickImage = () => {
        ImagePicker.openPicker({
            width: 600,
            height: 600,
            cropping: true
        }).then(image => {
            this.setState({ image: image.path })
            console.log(image);
        });
    }

    profileImage = () => {
        const { isRTL, currentUser, data } = this.props
        const { image } = this.state
        return (
            <View style={{ marginTop: moderateScale(10), alignSelf: 'center' }} >
                <FastImage
                    resizeMode='center'
                    source={image ? { uri: image } : data.img ? { uri: data.img } : require('../../assets/imgs/profileicon.jpg')}
                    style={{ borderWidth: 2, borderColor: colors.lightGray, width: 100, height: 100, borderRadius: 50 }}
                />
                <TouchableOpacity onPress={this.pickImage} style={{ alignSelf: 'flex-end', marginTop: moderateScale(-11), justifyContent: 'center', alignItems: 'center', backgroundColor: colors.darkGreen, height: 40, width: 40, borderRadius: 20 }}>
                    <Icon name='photo' type='FontAwesome' style={{ fontSize: responsiveFontSize(7), color: colors.white }} />
                </TouchableOpacity>
            </View>
        )
    }

    reasonCheckBox = () => {
        const { enablePassword } = this.state
        return (
            <View style={{ alignSelf: 'center', width: responsiveWidth(80), marginTop: moderateScale(10) }} >
                <AppCheckBox
                    title={Strings.updatePassword}
                    check={this.state.enablePassword}
                    onPress={() => this.setState({ enablePassword: !this.state.enablePassword })}
                    textColor='black'
                    boxColor={colors.darkGreen}
                    style={{ alignSelf: this.props.isRTL ? 'flex-end' : 'flex-start' }}
                />
            </View>
        )
    }


    render() {
        const { isRTL, userToken } = this.props;
        const { loading, enablePassword } = this.state
        return (
            <LinearGradient
                colors={[colors.white, colors.white, colors.white]}
                style={{ flex: 1 }}
            >

                <Header title={Strings.editUser} />

                <KeyboardAwareScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: colors.white, borderTopRightRadius: moderateScale(20), borderTopLeftRadius: moderateScale(20), marginTop: moderateScale(15), width: responsiveWidth(100) }}>

                    {this.profileImage()}
                    <View style={{ width: responsiveWidth(80), justifyContent: 'space-between', alignSelf: 'center', flexDirection: isRTL ? 'row-reverse' : 'row' }}>
                        {this.firstNameInput()}
                        {this.lastNameInput()}
                    </View>
                    {this.emailInput()}
                    {this.phoneInput()}
                    {this.userNameInput()}
                    {/*this.jobInput()*/}
                    {this.reasonCheckBox()}
                    {enablePassword && this.passwordInput()}
                    <View style={{ borderRadius: moderateScale(1), height: 0.1 }} >
                        <TextInput
                            style={{ height: 0.1 }}
                        />
                    </View>
                    {enablePassword && this.confirmPasswordInput()}

                    {(this.props.data.type == 'WAREHOUSE' || this.props.data.type == 'DRIVER' ) && this.wareHousePicker()}
                    {/*this.titleInput()*/}
                    {this.saveButton()}
                </KeyboardAwareScrollView>
                {loading && <LoadingDialogOverlay title={Strings.wait} />}
                <AppFooter />
            </LinearGradient>
        );
    }
}
const mapDispatchToProps = {
    setUser,
    removeItem
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
    //userToken: state.auth.userToken,
})


export default connect(mapToStateProps, mapDispatchToProps)(AdminUpdateMemberProfile);

