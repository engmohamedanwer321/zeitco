import React, { Component } from 'react';
import { View, RefreshControl, ScrollView, FlatList, Alert, Text, TouchableOpacity } from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../../utils/responsiveDimensions';
import { connect } from "react-redux";
import Strings from '../../assets/strings';

import axios from 'axios';
import { BASE_END_POINT } from '../../AppConfig';
import ListFooter from '../../components/ListFooter';
import { Icon, Thumbnail, Button } from 'native-base'
import { selectMenu, removeItem } from '../../actions/MenuActions';
import { enableSideMenu, pop } from '../../controlls/NavigationControll'
import NetInfo from "@react-native-community/netinfo";
import Loading from "../../common/Loading"
import NetworError from '../../common/NetworError'
import NoData from '../../common/NoData'
import * as colors from '../../assets/colors'
import { arrabicFont, englishFont } from '../../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image'
import AccountCard from '../../components/AccountCard'
import LinearGradient from 'react-native-linear-gradient';
import CommanHeader from '../../common/CommanHeader'
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';
import AppFooter from '../../components/AppFooter'
import Header from '../../common/Header'

class AdimnShowAllMembers extends Component {

    _menu = null;
    page = 1;
    state = {
        networkError: null,
        type: 'ADMIN',
        members: [],
        membersLoading: true,
        membersRefresh: false,
        members404: false,
        pages: null,
    }


    componentDidMount() {
        enableSideMenu(false, null)
        this.getMembers(false, 1, this.state.type)
    }

    getMembers = (refresh, page, type) => {
        if (refresh) {
            this.setState({ membersRefresh: true })
        }
        var uri;
        if (type == 'ALL') {
            uri = `${BASE_END_POINT}find?page=${page}`
        } else {
            uri = `${BASE_END_POINT}find?type=${type}&page=${page}`
        }

        axios.get(uri)
            .then(response => {
                console.log('Type    ', type)
                console.log('Done    ', response.data.data)
                this.setState({
                    members: refresh ? response.data.data : [...this.state.members, ...response.data.data],
                    membersLoading: false,
                    membersRefresh: false,
                    members404: false,
                    pages: response.data.pageCount,
                })
            })
            .catch(error => {
                console.log('Error   ', error.response)
                this.setState({ members404: true, membersLoading: false, })
            })
    }

    membersPage = () => {
        const { pages, type, members, members404, membersLoading, membersRefresh } = this.state
        return (
            members404 ?
                <NetworError />
                :
                membersLoading ?
                    <Loading />
                    :
                    members.length > 0 ?
                        <FlatList
                            showsVerticalScrollIndicator={false}
                            style={{marginBottom: moderateScale(0) }}
                            data={members}
                            renderItem={({ item }) => <AccountCard order={this.props.data} data={item} socket={this.socket} />}
                            onEndReachedThreshold={.5}
                            //ListFooterComponent={()=>notificationsLoad&&<ListFooter />}
                            onEndReached={() => {
                                if (this.page <= pages) {
                                    this.page = this.page + 1;
                                    this.getMembers(false, this.page, type)
                                    console.log('page  ', this.page)
                                }
                            }}
                            refreshControl={
                                <RefreshControl
                                    //colors={["#B7ED03",colors.darkBlue]} 
                                    refreshing={membersRefresh}
                                    onRefresh={() => {
                                        this.page = 1
                                        this.getMembers(true, 1, type)
                                    }}
                                />
                            }

                        />
                        :
                        <NoData />
        )
    }

    setMenuRef = ref => {
        this._menu = ref;
    };

    hideMenu = () => {
        this._menu.hide();
    };

    showMenu = () => {
        this._menu.show();
    };


    render() {
        const { categoryName, isRTL } = this.props;
        const { type } = this.state;
        return (
            <LinearGradient
                colors={[colors.white, colors.white, colors.white]}
                style={{ flex: 1 }}
            >
                <Header  title={Strings.user} />

                <View style={{ flex: 1,backgroundColor: colors.white,  marginTop: moderateScale(0), width: responsiveWidth(100) }} >

                    <Menu
                        style={{ marginLeft: moderateScale(40), width: responsiveWidth(50) }}
                        ref={this.setMenuRef}
                        button={
                            <TouchableOpacity onPress={() => this.showMenu()} style={{ flexDirection: 'row', borderBottomColor: colors.lightGray, borderBottomWidth: 3, borderTopRightRadius: moderateScale(20), borderTopLeftRadius: moderateScale(20), width: responsiveWidth(100), height: responsiveHeight(8), justifyContent: 'center', alignItems: 'center', backgroundColor: 'white' }}>
                                <Text style={{ marginHorizontal: moderateScale(10), color: colors.black, fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont }}>{type}</Text>
                                <Icon name='ios-arrow-down' type='Ionicons' style={{ fontSize: responsiveFontSize(8) }} />
                            </TouchableOpacity>
                        }
                    >
                        {/*<MenuItem onPress={()=>{
                        this.hideMenu()
                        this.setState({type:'All'})
                    }}>All</MenuItem>*/}
                        <MenuItem onPress={() => {
                            this.hideMenu()
                            this.setState({ type: 'ADMIN' })
                            this.page = 1
                            this.getMembers(true, 1, "ADMIN")
                        }}>Admins</MenuItem>
                        <MenuDivider />


                        <MenuItem onPress={() => {
                            this.hideMenu()
                            this.setState({ type: 'SURVEY' })
                            this.page = 1
                            this.getMembers(true, 1, "SURVEY")
                        }}>Surveyors</MenuItem>
                        <MenuDivider />

                        <MenuItem onPress={() => {
                            this.hideMenu()
                            this.setState({ type: 'PURCHASING' })
                            this.page = 1
                            this.getMembers(true, 1, "PURCHASING")
                        }}>Purchasers</MenuItem>
                        <MenuDivider />
                        

                        <MenuDivider />
                        <MenuItem onPress={() => {
                            this.hideMenu()
                            this.setState({ type: 'OPERATION' })
                            this.page = 1
                            this.getMembers(true, 1, "OPERATION")
                        }}>Operations</MenuItem>
                        <MenuDivider />

                        <MenuItem onPress={() => {
                            this.hideMenu()
                            this.setState({ type: 'CALL-CENTER' })
                            this.page = 1
                            this.getMembers(true, 1, "CALL-CENTER")
                        }}>Call Center</MenuItem>
                        <MenuDivider />

                        <MenuItem onPress={() => {
                            this.hideMenu()
                            this.setState({ type: 'DRIVER' })
                            this.page = 1
                            this.getMembers(true, 1, "DRIVER")
                        }}>Drivers</MenuItem>
                        <MenuDivider />

                        <MenuItem onPress={() => {
                            this.hideMenu()
                            this.setState({ type: 'WAREHOUSE' })
                            this.page = 1
                            this.getMembers(true, 1, "WAREHOUSE")
                        }}>WareHouse</MenuItem>
                        <MenuDivider />


                        <MenuItem onPress={() => {
                            this.hideMenu()
                            this.setState({ type: 'RESTURANTS' })
                            this.page = 1
                            this.getMembers(true, 1, "USER")
                        }}>Outlets</MenuItem>

                    </Menu>

                    {this.membersPage()}
                </View>

                <AppFooter />

            </LinearGradient>
        );
    }
}

const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    barColor: state.lang.color,
    currentUser: state.auth.currentUser,
})

const mapDispatchToProps = {
    removeItem,
}

export default connect(mapStateToProps, mapDispatchToProps)(AdimnShowAllMembers);
