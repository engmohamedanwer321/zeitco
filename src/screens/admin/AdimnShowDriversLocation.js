import React, { Component } from 'react';
import { View, RefreshControl, ScrollView, FlatList, Alert, Text, TouchableOpacity } from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../../utils/responsiveDimensions';
import { connect } from "react-redux";
import Strings from '../../assets/strings';

import axios from 'axios';
import { BASE_END_POINT } from '../../AppConfig';
import ListFooter from '../../components/ListFooter';
import { Icon, Thumbnail, Button } from 'native-base'
import { selectMenu, removeItem } from '../../actions/MenuActions';
import { enableSideMenu, pop } from '../../controlls/NavigationControll'
import NetInfo from "@react-native-community/netinfo";
import Loading from "../../common/Loading"
import NetworError from '../../common/NetworError'
import NoData from '../../common/NoData'
import * as colors from '../../assets/colors'
import { arrabicFont, englishFont } from '../../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image'
import AccountCard from '../../components/AccountCard'
import LinearGradient from 'react-native-linear-gradient';
import CommanHeader from '../../common/CommanHeader'
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';
import AppFooter from '../../components/AppFooter'
import DriverLocationCard from '../../components/DriverLocationCard'
import Header from '../../common/Header'

class AdimnShowDriversLocation extends Component {

    page = 1;
    state = {
        networkError: null,
        type: 'ADMIN',
        drivers: [],
        driversLoading: true,
        driversRefresh: false,
        drivers404: false,
        pages: null,
    }


    componentDidMount() {
        enableSideMenu(false, null)
        this.getDrivers(false, 1)
    }

    getDrivers = (refresh, page) => {
        if (refresh) {
            this.setState({ driversRefresh: true })
        }

        axios.get(`${BASE_END_POINT}find?type=DRIVER&page=${page}`)
            .then(response => {
                console.log('Done    ', response.data.data)
                this.setState({
                    drivers: refresh ? response.data.data : [...this.state.drivers, ...response.data.data],
                    driversLoading: false,
                    driversRefresh: false,
                    drivers404: false,
                    pages: response.data.pageCount,
                })
            })
            .catch(error => {
                console.log('Error   ', error.response)
                this.setState({ drivers404: true, driversLoading: false, })
            })
    }

    driversList = () => {
        const { pages, drivers, drivers404, driversLoading, driversRefresh } = this.state
        return (
            drivers404 ?
                <NetworError />
                :
                driversLoading ?
                    <Loading />
                    :
                    drivers.length > 0 ?
                        <FlatList
                            showsVerticalScrollIndicator={false}
                            style={{ marginBottom: responsiveHeight(8) }}
                            data={drivers}
                            renderItem={({ item }) => <DriverLocationCard order={this.props.data} data={item} socket={this.socket} />}
                            onEndReachedThreshold={.5}
                            //ListFooterComponent={()=>notificationsLoad&&<ListFooter />}
                            onEndReached={() => {
                                if (this.page <= pages) {
                                    this.page = this.page + 1;
                                    this.getDrivers(false, this.page)
                                    console.log('page  ', this.page)
                                }
                            }}
                            refreshControl={
                                <RefreshControl
                                    //colors={["#B7ED03",colors.darkBlue]} 
                                    refreshing={driversRefresh}
                                    onRefresh={() => {
                                        this.page = 1
                                        this.getDrivers(true, 1)
                                    }}
                                />
                            }

                        />
                        :
                        <NoData />
        )
    }

    render() {
        const { categoryName, isRTL } = this.props;
        const { type } = this.state;
        return (
            <LinearGradient
                colors={[colors.white, colors.white, colors.white]}
                style={{ flex: 1 }}
            >
                <Header  title={Strings.driversLocation} />
                <View style={{ flex: 1, backgroundColor: colors.white,  marginTop: moderateScale(0), width: responsiveWidth(100), marginBottom: (7) }} >
                    {this.driversList()}
                </View>

                <AppFooter />

            </LinearGradient>
        );
    }
}

const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    barColor: state.lang.color,
    currentUser: state.auth.currentUser,
})

const mapDispatchToProps = {
    removeItem,
}

export default connect(mapStateToProps, mapDispatchToProps)(AdimnShowDriversLocation);
