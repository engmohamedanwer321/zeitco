import React,{Component} from 'react';
import {View,RefreshControl,ScrollView,FlatList,Text,TouchableOpacity} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight ,responsiveFontSize} from '../../utils/responsiveDimensions';
import { connect } from "react-redux";
import Strings from '../../assets/strings';

import axios from 'axios';
import { BASE_END_POINT} from '../../AppConfig';
import ListFooter from '../../components/ListFooter';
import {Icon,Thumbnail,Button} from 'native-base'
import {selectMenu,removeItem} from '../../actions/MenuActions';
import {enableSideMenu,pop} from '../../controlls/NavigationControll'
import NetInfo from "@react-native-community/netinfo";
import Loading from "../../common/Loading"
import NetworError from '../../common/NetworError'
import NoData from '../../common/NoData'
import * as colors from '../../assets/colors'
import {arrabicFont,englishFont} from  '../../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image'
import NotificationCard from '../../components/NotificationCard'
import DriverCheckingCard from '../../components/DriverCheckingCard';
import CollaspeAppHeader from '../../common/CollaspeAppHeader'
import LinearGradient from 'react-native-linear-gradient';
import CommanHeader from '../../common/CommanHeader'
import AppFooter from '../../components/AppFooter'
import io from 'socket.io-client';
import Header from '../../common/Header'
import DateTimePickerModal from "react-native-modal-datetime-picker";
import moment from 'moment'


class AdminDriversSystemChecking extends Component {

    page=1;
    socket=null;
    state= {
        networkError:null,
        drivers:[],
        driversLoading: true,
        driversRefresh:false,
        drivers404:false,
        pages: null,

        date: null,
        datePicker: new Date((new Date().getFullYear()) + '/' + (new Date().getMonth() + 1) + '/' + (parseInt(new Date().getDate()) )),
        showDatePicker: false,
        selectedDate:Strings.selectDate,
        calendr: '',
    }

  
    componentDidMount(){
        enableSideMenu(false, null)
        this.getDrivers(false,1)
    }


    getDrivers = (refresh,page,date) => {
        if(refresh){
            this.setState({driversRefresh:true})
        }
        var url;
        if(date){
            url=`${BASE_END_POINT}getAttends?start=${date}&end=${date}&page=${page}`
        }else{
            url=`${BASE_END_POINT}getAttends?today=true&page=${page}`
        }
        axios.get(url)
        .then(response=>{
          console.log('Done   ',response.data.data)
          this.setState({
              drivers:refresh?response.data.data:[...this.state.drivers,...response.data.data],
              driversLoading:false,
              driversRefresh:false,
              newOrdersLoading:false,
              pages:response.data.pageCount,
            })
          })
        .catch(error=>{
          console.log('Error   ',error.response)
          this.setState({drivers404:true,driversLoading:false,})
        })
    }

    driversPage = () => {
        const {pages,drivers,driversLoading,drivers404,driversRefresh} = this.state
        return (
            drivers404?
            <NetworError />
            :
            driversLoading?
            <Loading />
            :
            drivers.length>0?
            <FlatList
            showsVerticalScrollIndicator={false} 
            style={{marginVertical:moderateScale(0)}}
            data={drivers}
            renderItem={({item})=><DriverCheckingCard data={item} />}
            onEndReachedThreshold={.5}
            //ListFooterComponent={()=>notificationsLoad&&<ListFooter />}
            onEndReached={() => {     
                if(this.page <= pages){
                    this.page=this.page+1;
                    this.getDrivers(false,this.page)
                    console.log('page  ',this.page)
                }  
            }}
            refreshControl={
            <RefreshControl 
            //colors={["#B7ED03",colors.darkBlue]} 
            refreshing={driversRefresh}
            onRefresh={() => {
                this.page = 1
                this.getDrivers(true,1)
            }}
            />
            }
            
            />
            :
            <NoData />
        )
    }

    datePicker = () => {
        const { showDatePicker, datePicker } = this.state
        return (<DateTimePickerModal
            isVisible={showDatePicker}
            mode="date"
            date={datePicker}
            onConfirm={(date) => {
                const d = moment(date).format('YYYY-MM-DD')
                this.setState({ showDatePicker: false, date: d, selectedDate: d, datePicker: date })
                this.page=1
                this.getDrivers(true,1,d)
            }
            }
            onCancel={() => this.setState({ showDatePicker: false })}
            //minimumDate={new Date(minimumDate)}
            confirmTextIOS={Strings.confirm}
            cancelTextIOS={Strings.cancel}
            headerTextIOS={Strings.chooseDate}
            maximumDate={new Date()}
        />
        )
    }

    dateButton = () =>{
        const {isRTL} =  this.props
        const {selectedDate} =  this.state
        return(
            <TouchableOpacity onPress={() => {
                this.setState({showDatePicker: true })
            }} style={{ borderRadius: 0, padding: moderateScale(2),alignSelf:'center' ,marginHorizontal:moderateScale(10), height: responsiveHeight(6), width: responsiveWidth(30), justifyContent: 'center', alignItems: 'center', backgroundColor: 'white', borderColor: colors.green, borderWidth: 0.5,marginTop:moderateScale(10) }} >
                <Text style={{ color: colors.green, fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(7), height: responsiveHeight(6), lineHeight: responsiveHeight(6) }}>{selectedDate}</Text>
            </TouchableOpacity>
        )
    }

    render(){
        const {categoryName,isRTL} = this.props;
        moment.locale('en')
        return(
            <LinearGradient 
            colors={[colors.white, colors.white, colors.white]} 
            style={{flex:1}}
            >
             <Header title={Strings.checkingSystem} />
             {this.dateButton()}
            <ScrollView showsVerticalScrollIndicator={false} style={{marginBottom:moderateScale(20), backgroundColor:colors.white,  marginTop:moderateScale(5), width:responsiveWidth(100)}} >
              {this.driversPage()}
            </ScrollView>
            {this.datePicker()}
            <AppFooter />
            </LinearGradient> 
        );
    }
}

const mapStateToProps = state => ({
    isRTL: state.lang.RTL, 
    barColor: state.lang.color ,
    currentUser:state.auth.currentUser, 
})

const mapDispatchToProps = {
    removeItem,
}

export default connect(mapStateToProps,mapDispatchToProps)(AdminDriversSystemChecking);
