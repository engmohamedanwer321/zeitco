import React, { Component } from 'react';
import {
    View, TouchableOpacity, Image, Text, ScrollView, TextInput, Alert, Platform
} from 'react-native';
import { connect } from 'react-redux';
import { Icon, Button } from 'native-base';
import { login } from '../actions/AuthActions'
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import Strings from '../assets/strings';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import { enableSideMenu, resetTo, push } from '../controlls/NavigationControll'
import { arrabicFont, englishFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import * as colors from '../assets/colors';
import { removeItem } from '../actions/MenuActions';
import CollaspeAppHeader from '../common/CollaspeAppHeader'
import LinearGradient from 'react-native-linear-gradient';
import FastImage from 'react-native-fast-image'
import CommanHeader from '../common/CommanHeader'
import ImagePicker from 'react-native-image-crop-picker';
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import AppFooter from '../components/AppFooter'
import MapView, { Marker } from 'react-native-maps';
import Header from '../common/Header'

const oil_types = [
    {
        name: 'Oil Types',
        id: 0,
        children: [
            {
                name: 'Type 1',
                id: 1,
            },
            {
                name: 'Type 2',
                id: 2,
            },
            {
                name: 'Type 3',
                id: 3,
            },
        ],
    }
]

const branchsNo = [
    {
        name: 'Branchs No',
        id: 0,
        children: [
            {
                name: '1',
                id: 1,
            },
            {
                name: '2',
                id: 2,
            },
            {
                name: '3',
                id: 3,
            },
            {
                name: '4',
                id: 4,
            },
        ],
    }
]

const titles = [
    {
        name: 'Titles',
        id: 0,
        children: [
            {
                name: 'Title 1',
                id: 1,
            },
            {
                name: 'Title 2',
                id: 2,
            },
            {
                name: 'Title 3',
                id: 3,
            },
        ],
    }
]

const spaces = [
    {
        name: 'Spaces',
        id: 0,
        children: [
            {
                name: 'space 1',
                id: 1,
            },
            {
                name: 'space 2',
                id: 2,
            },
            {
                name: 'space 3',
                id: 3,
            },
        ],
    }
]



class ResturantAccountDetails extends Component {

    state = {
        restaurantName: ' ',
        price: ' ',
        addressByDetails: ' ',
        contactNo: ' ',
        address: ' ',
        firstName: ' ',
        lastName: ' ',
        phone: ' ',
        email: ' ',
        password: ' ',
        image: null,
        hidePassword: true,
        addressCount: [1],
        selectOilType: '',
        selecTitle: '',
        selectAvgKg: '',
        selectBranchNo: '',
        selectResturantSpace: '',
        Coordinate: {
            latitude: 0,
            longitude: 0,
        },
        Latitude: 30.35689489993212,
        Longitude: 31.1944606972275,
    }

    componentDidMount() {
        enableSideMenu(false, null)
    }

    restaurantNameInput = () => {
        const { isRTL } = this.props
        const { restaurantName } = this.state
        return (
            <View style={{ marginTop: moderateScale(15), width: responsiveWidth(80), alignSelf: 'center' }}>

                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.restaurantName}</Text>
                <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                    <TextInput
                        onChangeText={(val) => { this.setState({ restaurantName: val }) }}
                        style={{ width: responsiveWidth(80), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(6) }}
                        placeholder={Strings.restaurantName}
                    />
                </View>
                {restaurantName.length == 0 &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }

    oilTypePicker = () => {
        const { isRTL } = this.props
        const { selectedItems } = this.state
        return (
            <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', width: responsiveWidth(80), alignSelf: 'center' }} >

                {/*Oil Type */}
                <Animatable.View animation={isRTL ? "slideInRight" : "slideInLeft"} duration={1000} style={{ marginTop: moderateScale(5), width: responsiveWidth(80), }}>

                    <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.oilType}</Text>

                    <View style={{ alignItems: 'center', justifyContent: 'center', backgroundColor: colors.lightGray, width: responsiveWidth(80), height: responsiveHeight(6), alignSelf: 'center' }}>

                        <SectionedMultiSelect
                            styles={{
                                selectToggle: { width: responsiveWidth(78), height: responsiveHeight(6), borderRadius: moderateScale(3), alignItems: 'center', justifyContent: 'center', },
                                selectToggleText: { fontSize: responsiveFontSize(6), color: 'gray', marginTop: moderateScale(8), fontFamily: isRTL ? arrabicFont : englishFont },
                                itemText: { fontSize: responsiveFontSize(10), textAlign: isRTL ? 'right' : 'left' },
                                container: { height: responsiveHeight(75), position: 'absolute', width: responsiveWidth(85), top: responsiveHeight(13), alignSelf: 'center' },
                                searchTextInput: { textAlign: isRTL ? 'right' : 'left', marginHorizontal:moderateScale(5) },
                            }}
                            items={oil_types}
                            alwaysShowSelectText
                            single
                            highlightChildren
                            uniqueKey="id"
                            subKey="children"
                            selectText={this.state.selectOilType ? this.state.selectOilType : Strings.oilType}
                            showDropDowns={true}
                            readOnlyHeadings={true}
                            onSelectedItemsChange={(selectedItems) => {
                                // this.setState({ countries: selectedItems });
                            }
                            }
                            onSelectedItemObjectsChange={(selectedItems) => {
                                console.log("ITEM2   ", selectedItems[0].name)
                                this.setState({ selectOilType: selectedItems[0].name });
                            }
                            }

                            onConfirm={() => this.setState({ selectOilType: null })}
                        />

                    </View>
                    {this.state.selectOilType == null &&
                        <Text style={{ color: 'red', marginTop: moderateScale(1), marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                    }
                </Animatable.View>
            </View>
        )
    }

    PriceInputAvgKgPicker = () => {
        const { isRTL } = this.props
        const { price } = this.state
        return (
            <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'space-between', width: responsiveWidth(80), alignSelf: 'center', marginTop: moderateScale(7), }} >

                {/*Oil Type */}
                <Animatable.View animation={isRTL ? "slideInRight" : "slideInLeft"} duration={1000} style={{ width: responsiveWidth(37), }}>

                    <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.averageKg}</Text>
                    <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: colors.lightGray, width: responsiveWidth(37), height: responsiveHeight(6) }}>
                        <SectionedMultiSelect
                            styles={{
                                selectToggle: { width: responsiveWidth(35), height: responsiveHeight(6), borderRadius: moderateScale(3), alignItems: 'center', justifyContent: 'center', },
                                selectToggleText: { fontSize: responsiveFontSize(6), color: 'gray', marginTop: moderateScale(8), fontFamily: isRTL ? arrabicFont : englishFont },
                            }}
                            items={oil_types}
                            alwaysShowSelectText
                            single
                            uniqueKey="id"
                            subKey="children"
                            selectText={this.state.selectAvgKg ? this.state.selectAvgKg : Strings.averageKg}
                            showDropDowns={true}
                            readOnlyHeadings={true}
                            onSelectedItemsChange={(selectedItems) => {
                                // this.setState({ countries: selectedItems });
                            }
                            }
                            onSelectedItemObjectsChange={(selectedItems) => {
                                console.log("ITEM2   ", selectedItems[0].name)
                                this.setState({ selectAvgKg: selectedItems[0].name });
                            }
                            }

                            onConfirm={() => this.setState({ selectAvgKg: null })}

                        />
                    </View>
                    {this.state.selectAvgKg == null &&
                        <Text style={{ color: 'red', marginTop: moderateScale(1), marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                    }
                </Animatable.View>


                <View style={{ width: responsiveWidth(37) }}>
                    <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.price}</Text>
                    <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                        <TextInput
                            onChangeText={(val) => { this.setState({ price: val }) }}
                            style={{ width: responsiveWidth(37), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(6) }}
                            placeholder={Strings.price}
                        />
                    </View>
                    {price.length == 0 &&
                        <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                    }
                </View>
            </View>
        )
    }

    addressByDetailsInput = () => {
        const { isRTL } = this.props
        const { addressByDetails } = this.state
        return (
            <View style={{ marginTop: moderateScale(7), width: responsiveWidth(80), alignSelf: 'center' }}>

                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.addressByDetails}</Text>
                <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                    <TextInput
                        onChangeText={(val) => { this.setState({ addressByDetails: val }) }}
                        style={{ width: responsiveWidth(80), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(6) }}
                        placeholder={Strings.addressByDetails}
                    />
                </View>
                {addressByDetails.length == 0 &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }

    ResSpacebranchsNoPicker = () => {
        const { isRTL } = this.props
        const { selectedItems } = this.state
        return (


            <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'space-between', width: responsiveWidth(80), alignSelf: 'center', marginTop: moderateScale(7), }} >

                {/*Oil Type */}
                <Animatable.View animation={isRTL ? "slideInRight" : "slideInLeft"} duration={1000} style={{ width: responsiveWidth(37) }}>

                    <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.resSpace}</Text>

                    <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: colors.lightGray, width: responsiveWidth(37), height: responsiveHeight(6) }}>

                        <SectionedMultiSelect
                            styles={{
                                selectToggle: { width: responsiveWidth(35), height: responsiveHeight(6), borderRadius: moderateScale(3), alignItems: 'center', justifyContent: 'center', },
                                selectToggleText: { fontSize: responsiveFontSize(6), color: 'gray', marginTop: moderateScale(8), fontFamily: isRTL ? arrabicFont : englishFont },
                            }}
                            items={spaces}
                            alwaysShowSelectText
                            single
                            uniqueKey="id"
                            subKey="children"
                            selectText={this.state.selectResturantSpace ? this.state.selectResturantSpace : Strings.resSpace}
                            showDropDowns={true}
                            readOnlyHeadings={true}
                            onSelectedItemsChange={(selectedItems) => {
                                // this.setState({ countries: selectedItems });
                            }
                            }
                            onSelectedItemObjectsChange={(selectedItems) => {
                                console.log("ITEM2   ", selectedItems[0].name)
                                this.setState({ selectResturantSpace: selectedItems[0].name });
                            }
                            }

                            onConfirm={() => this.setState({ selectResturantSpace: null })}
                        />
                    </View>
                    {this.state.selectResturantSpace == null &&
                        <Text style={{ color: 'red', marginTop: moderateScale(1), marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                    }
                </Animatable.View>


                <Animatable.View animation={isRTL ? "slideInRight" : "slideInLeft"} duration={1000} style={{ width: responsiveWidth(37) }}>

                    <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.branchesNo}</Text>

                    <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: colors.lightGray, width: responsiveWidth(37), height: responsiveHeight(6), alignSelf: 'center' }}>

                        <SectionedMultiSelect
                            styles={{
                                selectToggle: { width: responsiveWidth(35), height: responsiveHeight(6), borderRadius: moderateScale(3), alignItems: 'center', justifyContent: 'center', },
                                selectToggleText: { fontSize: responsiveFontSize(6), color: 'gray', marginTop: moderateScale(8), fontFamily: isRTL ? arrabicFont : englishFont },
                            }}
                            items={branchsNo}
                            alwaysShowSelectText
                            single
                            uniqueKey="id"
                            subKey="children"
                            selectText={this.state.selectBranchNo ? this.state.selectBranchNo : Strings.branchesNo}
                            showDropDowns={true}
                            readOnlyHeadings={true}
                            onSelectedItemsChange={(selectedItems) => {
                                // this.setState({ countries: selectedItems });
                            }
                            }
                            onSelectedItemObjectsChange={(selectedItems) => {

                                console.log("ITEM2   ", selectedItems[0].id)
                                this.addRequiredAddress(selectedItems[0].id)
                                this.setState({ selectBranchNo: selectedItems[0].name });

                            }
                            }

                            onConfirm={() => this.setState({ selectBranchNo: null })}
                        />
                    </View>
                    {this.state.selectBranchNo == null &&
                        <Text style={{ color: 'red', marginTop: moderateScale(1), marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                    }
                </Animatable.View>
            </View>
        )
    }

    addRequiredAddress = (count) => {
        this.setState({ addressCount: [] })
        var NewAddressCount = []
        for (var i = 1; i <= count; i++) {
            NewAddressCount.push(i)
        }
        this.setState({ addressCount: NewAddressCount })
        console.log('NewAddressCount : ' + NewAddressCount)
    }


    firstNameInput = () => {
        const { isRTL } = this.props
        const { firstName } = this.state
        return (
            <View style={{ marginTop: moderateScale(7), width: responsiveWidth(37), }}>

                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, marginHorizontal: moderateScale(0), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.firstName}</Text>
                <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                    <TextInput
                        onChangeText={(val) => { this.setState({ firstName: val }) }}
                        style={{ width: responsiveWidth(37), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(6) }}
                        placeholder={Strings.firstName}
                    />
                </View>
                {firstName.length == 0 &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }

    lastNameInput = () => {
        const { isRTL } = this.props
        const { lastName } = this.state
        return (
            <View style={{ marginTop: moderateScale(7), width: responsiveWidth(37) }}>

                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, marginHorizontal: moderateScale(0), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.lastName}</Text>
                <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                    <TextInput
                        onChangeText={(val) => { this.setState({ lastName: val }) }}
                        style={{ width: responsiveWidth(37), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(6) }}
                        placeholder={Strings.lastName}
                    />
                </View>
                {lastName.length == 0 &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }

    contactNoInput = () => {
        const { isRTL } = this.props
        const { contactNo } = this.state
        return (
            <View style={{ marginTop: moderateScale(6), width: responsiveWidth(80), alignSelf: 'center' }}>

                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.contactNo}</Text>
                <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                    <TextInput
                        onChangeText={(val) => { this.setState({ contactNo: val }) }}
                        style={{ width: responsiveWidth(80), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(6) }}
                        placeholder={Strings.contactNo}
                    />
                </View>
                {contactNo.length == 0 &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }

    titlePicker = () => {
        const { isRTL } = this.props
        const { title } = this.state
        return (
            <View style={{ marginTop: moderateScale(5), width: responsiveWidth(80), alignSelf: 'center' }} >

                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.title}</Text>
                {/*Oil Type */}
                <Animatable.View animation={isRTL ? "slideInRight" : "slideInLeft"} duration={1000} style={{ width: responsiveWidth(80), marginLeft: 'auto', marginRight: 'auto' }}>


                    <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: colors.lightGray, width: responsiveWidth(80), height: responsiveHeight(6), alignSelf: 'center' }}>

                        <SectionedMultiSelect
                            styles={{
                                selectToggle: { width: responsiveWidth(74), height: responsiveHeight(6), borderRadius: moderateScale(3), alignItems: 'center', justifyContent: 'center', },
                                selectToggleText: { fontSize: responsiveFontSize(6), color: 'gray', marginTop: moderateScale(8), fontFamily: isRTL ? arrabicFont : englishFont },
                            }}
                            items={titles}
                            alwaysShowSelectText
                            single
                            uniqueKey="id"
                            subKey="children"
                            selectText={this.state.selecTitle ? this.state.selecTitle : Strings.title}
                            showDropDowns={true}
                            readOnlyHeadings={true}
                            onSelectedItemsChange={(selectedItems) => {
                                // this.setState({ countries: selectedItems });
                            }
                            }
                            onSelectedItemObjectsChange={(selectedItems) => {
                                console.log("ITEM2   ", selectedItems[0].name)
                                this.setState({ selecTitle: selectedItems[0].name });
                            }
                            }

                            onConfirm={() => this.setState({ selecTitle: null })}
                        />
                    </View>
                    {this.state.selecTitle == null &&
                        <Text style={{ color: 'red', marginTop: moderateScale(1), marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                    }
                </Animatable.View>
            </View>
        )
    }


    addressMapInput = () => {
        const { isRTL } = this.props
        const { address } = this.state

        return (
            <>
                {this.state.addressCount.map((item, index) => (
                    <View>
                        <View style={{ marginTop: moderateScale(6), width: responsiveWidth(80), alignSelf: 'center' }}>

                            <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', width: responsiveWidth(80), height: responsiveHeight(6) }}>
                                <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), lineHeight: responsiveHeight(7), alignSelf: isRTL ? 'flex-end' : 'flex-start', }}>{item}</Text>
                                <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), lineHeight: responsiveHeight(7), alignSelf: isRTL ? 'flex-end' : 'flex-start', }}>. </Text>
                                <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                                    <TextInput
                                        onChangeText={(val) => { this.setState({ address: val }) }}
                                        style={{ width: responsiveWidth(77), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(6) }}
                                        placeholder={Strings.address}
                                    />
                                </View>
                            </View>
                            {address.length == 0 && item == 1 &&
                                <Text style={{ color: 'red', marginHorizontal: moderateScale(5), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                            }
                        </View>

                        <View style={{ marginTop: moderateScale(6), width: responsiveWidth(80), height: responsiveHeight(30), alignSelf: 'center' }}>
                            <MapView style={{ position: 'absolute', top: 0, left: 0, right: 0, bottom: 0 }}
                                initialRegion={{
                                    latitude: this.state.Latitude,
                                    longitude: this.state.Longitude,
                                    latitudeDelta: 0.0922,
                                    longitudeDelta: 0.0421,
                                }}
                                onPress={(event) => [this.setState({ Coordinate: event.nativeEvent.coordinate }), console.log(event.nativeEvent.coordinate)]}
                            >
                                <MapView.Marker style={{ width: 4, height: 4 }}

                                    coordinate={{
                                        // latitude: 30.35689489993212,
                                        // longitude: 31.1944606972275
                                        latitude: parseFloat(this.state.Coordinate.latitude),
                                        longitude: parseFloat(this.state.Coordinate.longitude),
                                    }}
                                    //image={require('../../../../Assets/map-pin.png')}
                                    height={5}
                                    width={5}
                                />

                            </MapView>
                        </View>
                    </View>
                ))
                }
            </>
        )
    }




    save = () => {
        const { firstName, lastName, phone, password, email } = this.state
        if (!email.replace(/\s/g, '').length) {
            this.setState({ email: '' })
        }
        if (!password.replace(/\s/g, '').length) {
            this.setState({ password: '' })
        }

        if (!firstName.replace(/\s/g, '').length) {
            this.setState({ firstName: '' })
        }

        if (!lastName.replace(/\s/g, '').length) {
            this.setState({ lastName: '' })
        }

        if (!phone.replace(/\s/g, '').length) {
            this.setState({ phone: '' })
        }

        // this.loadingButton.showLoading(true);
        //this.props.login(phone,password);
    }

    saveButton = () => {
        const { isRTL } = this.props

        return (
            <Button onPress={() => { this.save() }} style={{ alignSelf: 'center', marginVertical: moderateScale(15), height: responsiveHeight(7), width: responsiveWidth(40), justifyContent: 'center', alignItems: 'center', backgroundColor: colors.darkGreen, borderRadius: moderateScale(5) }} >
                <Text style={{ color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.submit}</Text>
            </Button>
        )
    }

    pickImage = () => {
        ImagePicker.openPicker({
            width: 600,
            height: 600,
            cropping: true
        }).then(image => {
            this.setState({ image: image.path })
            console.log(image);
        });
    }

    profileImage = () => {
        const { isRTL } = this.props
        const { image } = this.state
        return (
            <View style={{ marginTop: moderateScale(10), alignSelf: 'center' }} >
                <FastImage
                    resizeMode='center'
                    source={image ? { uri: image } : require('../assets/imgs/profileicon.jpg')}
                    style={{ borderWidth: 2, borderColor: colors.lightGray, width: 100, height: 100, borderRadius: 50 }}
                />
                <TouchableOpacity onPress={this.pickImage} style={{ alignSelf: 'flex-end', marginTop: moderateScale(-11), justifyContent: 'center', alignItems: 'center', backgroundColor: colors.darkGreen, height: 40, width: 40, borderRadius: 20 }}>
                    <Icon name='photo' type='FontAwesome' style={{ fontSize: responsiveFontSize(7), color: colors.white }} />
                </TouchableOpacity>
            </View>
        )
    }



    render() {
        const { isRTL, userToken } = this.props;
        const { phone, password, hidePassword, email } = this.state
        return (
            <LinearGradient
                colors={[colors.white, colors.white, colors.white]}
                style={{ flex: 1 }}
            >
                <Header  title={Strings.addNewAccount} /> 

                <ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: colors.white, borderTopRightRadius: moderateScale(20), borderTopLeftRadius: moderateScale(20), marginTop: moderateScale(12), width: responsiveWidth(100), marginBottom: moderateScale(20) }} >
                    <View style={{ alignSelf: 'center', width: responsiveWidth(60), borderBottomColor: colors.darkGreen, borderBottomWidth: 2, justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ marginTop: moderateScale(5), marginBottom: moderateScale(3), color: colors.darkGreen, fontSize: responsiveFontSize(8), fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.restaurantDetails}</Text>
                    </View>
                    {this.restaurantNameInput()}
                    {this.oilTypePicker()}
                    {this.PriceInputAvgKgPicker()}
                    {this.ResSpacebranchsNoPicker()}
                    {this.addressByDetailsInput()}
                    <View style={{ marginTop: moderateScale(15), alignSelf: 'center', width: responsiveWidth(60), borderBottomColor: colors.darkGreen, borderBottomWidth: 2, justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ marginBottom: moderateScale(3), color: colors.darkGreen, fontSize: responsiveFontSize(8), fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.contactDetails}</Text>
                    </View>
                    {this.profileImage()}
                    <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', width: responsiveWidth(80), justifyContent: 'space-between', alignSelf: 'center', }}>
                        {this.firstNameInput()}
                        {this.lastNameInput()}
                    </View>

                    {this.contactNoInput()}
                    {this.titlePicker()}
                    {this.addressMapInput()}




                    {this.saveButton()}
                </ScrollView>

                <AppFooter />

            </LinearGradient>
        );
    }
}
const mapDispatchToProps = {
    login,
    removeItem
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    loading: state.auth.loading,
    errorText: state.auth.errorText,
    //userToken: state.auth.userToken,
})


export default connect(mapToStateProps, mapDispatchToProps)(ResturantAccountDetails);

