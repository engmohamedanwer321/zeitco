import React, { Component } from 'react';
import {
  View,TouchableOpacity,Image,Text,ScrollView,TextInput,Alert,Platform,DeviceEventEmitter
} from 'react-native';
import { connect } from 'react-redux';
import {Icon,Button} from 'native-base';
import {setUser} from '../actions/AuthActions'
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import Strings from '../assets/strings';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import {enableSideMenu,resetTo,push} from '../controlls/NavigationControll'
import {arrabicFont,englishFont} from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import * as colors from '../assets/colors';
import {removeItem} from '../actions/MenuActions';
import CollaspeAppHeader from '../common/CollaspeAppHeader'
import LinearGradient from 'react-native-linear-gradient';
import FastImage from 'react-native-fast-image'
import CommanHeader from '../common/CommanHeader'
import ImagePicker from 'react-native-image-crop-picker';
import AppFooter from '../components/AppFooter'
import AsyncStorage  from '@react-native-community/async-storage'
import axios from 'axios';
import {BASE_END_POINT} from '../AppConfig';
import { RNToasty } from 'react-native-toasty';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { whiteButton, greenButton } from '../assets/styles';
import Header from '../common/Header'

class UpdateProfile extends Component {

    firstName=null
    lastName=null
    email=null
    phone=null
    userName=null
    password=null
    confirmPassword=null

    state={
        firstName:this.props.currentUser.user.firstname,
        lastName:this.props.currentUser.user.lastname,
        phone:this.props.currentUser.user.phone,
        userName:this.props.currentUser.user.username,
        email:this.props.currentUser.user.email,
        password: ' ',
        confirmPassword:' ',
        image:null,
        hidePassword: true,
        loading:false,
        showFooter:true

    }

    componentDidMount(){
        enableSideMenu(false, null)
        /*DeviceEventEmitter.addListener('keyboardDidShow',()=>{
            Alert.alert('Show')
        })*/
    }
    

    firstNameInput = () => {
        const {isRTL} = this.props
        const {firstName,} = this.state
        return(
            <View style={{marginTop:moderateScale(15),width:responsiveWidth(37),}}>
                
                <Text style={{marginBottom:moderateScale(3),fontFamily:isRTL?arrabicFont:englishFont, color:colors.black,marginHorizontal:moderateScale(0),fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.firstName}</Text>
                <View style={{borderRadius:moderateScale(1), backgroundColor:colors.lightGray }} >
                <TextInput 
                    value={firstName}
                     onChangeText={(val)=>{this.setState({firstName:val})}}
                     style={{width:responsiveWidth(37), paddingHorizontal:moderateScale(3),fontFamily:isRTL?arrabicFont:englishFont,height:responsiveHeight(5),padding:0}}
                     placeholder={Strings.firstName}
                     returnKeyType='next'
                    onSubmitEditing={() => {this.lastName.focus(); }}
                />
                </View>
                {firstName.length==0&&
                <Text style={{color:'red',marginHorizontal:moderateScale(2),fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {Strings.require}</Text>
                }
            </View>
        )
    }

    lastNameInput = () => {
        const {isRTL} = this.props
        const {lastName} = this.state
        return(
            <View style={{ marginTop:moderateScale(15),width:responsiveWidth(37)}}>
                
                <Text style={{marginBottom:moderateScale(3),fontFamily:isRTL?arrabicFont:englishFont, color:colors.black,marginHorizontal:moderateScale(0),fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.lastName}</Text>
                <View style={{borderRadius:moderateScale(1), backgroundColor:colors.lightGray }} >
                <TextInput 
                    value={lastName}
                     onChangeText={(val)=>{this.setState({lastName:val})}}
                     style={{width:responsiveWidth(37), paddingHorizontal:moderateScale(3),fontFamily:isRTL?arrabicFont:englishFont,height:responsiveHeight(5),padding:0}}
                     placeholder={Strings.lastName}
                     ref={(input) => { this.lastName = input; }}
                     returnKeyType='next'
                    onSubmitEditing={() => {this.userName.focus(); }}
                />
                </View>
                {lastName.length==0&&
                <Text style={{color:'red',marginHorizontal:moderateScale(2),fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {Strings.require}</Text>
                }
            </View>
        )
    }

    userNameInput = () => {
        const {isRTL} = this.props
        const {userName} = this.state
        return(
            <View style={{marginTop:moderateScale(6),width:responsiveWidth(80), alignSelf:'center'}}>
                
                <Text style={{marginBottom:moderateScale(3),fontFamily:isRTL?arrabicFont:englishFont, color:colors.black,fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.userName}</Text>
                <View style={{borderRadius:moderateScale(1), backgroundColor:colors.lightGray }} >
                <TextInput 
                    value={userName}
                     onChangeText={(val)=>{this.setState({userName:val})}}
                     style={{width:responsiveWidth(80),padding:0, paddingHorizontal:moderateScale(3),fontFamily:isRTL?arrabicFont:englishFont,height:responsiveHeight(5)}}
                     placeholder={Strings.userName}
                     ref={(input) => { this.userName = input; }}
                    returnKeyType='next'
                    onSubmitEditing={() => {this.email.focus(); }}
                />
                </View>
                {userName.length==0&&
                <Text style={{color:'red',marginHorizontal:moderateScale(2),fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {Strings.require}</Text>
                }
            </View>
        )
    }

    emailInput = () => {
        const {isRTL} = this.props
        const {email} = this.state
        return(
            <View style={{marginTop:moderateScale(6),width:responsiveWidth(80), alignSelf:'center'}}>
                
                <Text style={{marginBottom:moderateScale(3),fontFamily:isRTL?arrabicFont:englishFont, color:colors.black,fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.email}</Text>
                <View style={{borderRadius:moderateScale(1), backgroundColor:colors.lightGray }} >
                <TextInput 
                    value={email}
                     onChangeText={(val)=>{this.setState({email:val})}}
                     style={{padding:0,width:responsiveWidth(80), paddingHorizontal:moderateScale(3),fontFamily:isRTL?arrabicFont:englishFont,height:responsiveHeight(5)}}
                     placeholder={Strings.email}
                     ref={(input) => { this.email = input; }}
                        returnKeyType='next'
                        onSubmitEditing={() => {this.phone.focus(); }}
                />
                </View>
                {email.length==0&&
                <Text style={{color:'red',marginHorizontal:moderateScale(2),fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {Strings.require}</Text>
                }
            </View>
        )
    }

    phoneInput = () => {
        const {isRTL} = this.props
        const {phone} = this.state
        return(
            <View style={{marginTop:moderateScale(6),width:responsiveWidth(80), alignSelf:'center'}}>
                
                <Text style={{marginBottom:moderateScale(3),fontFamily:isRTL?arrabicFont:englishFont, color:colors.black,fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.phone}</Text>
                <View style={{borderRadius:moderateScale(1), backgroundColor:colors.lightGray }} >
                <TextInput 
                    value={phone}
                     onChangeText={(val)=>{this.setState({phone:val})}}
                     style={{padding:0,width:responsiveWidth(80), paddingHorizontal:moderateScale(3),fontFamily:isRTL?arrabicFont:englishFont,height:responsiveHeight(5)}}
                     placeholder={Strings.phone}
                     ref={(input) => { this.phone = input; }}
                        returnKeyType='next'
                        onSubmitEditing={() => {this.password.focus(); }}
                    keyboardType='phone-pad'
                />
                </View>
                {phone.length==0&&
                <Text style={{color:'red',marginHorizontal:moderateScale(2),fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {Strings.require}</Text>
                }
            </View>
        )
    }

    passwordInput = () => {
        const {isRTL} = this.props
        const {password} = this.state
        return(
            <View style={{marginTop:moderateScale(6),width:responsiveWidth(80), alignSelf:'center'}}>
                
                <Text style={{marginBottom:moderateScale(3),fontFamily:isRTL?arrabicFont:englishFont, color:colors.black,fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.password}</Text>
                <View style={{borderRadius:moderateScale(1), backgroundColor:colors.lightGray }} >
                <TextInput 
                     onChangeText={(val)=>{this.setState({password:val})}}
                     style={{width:responsiveWidth(80),padding:0, paddingHorizontal:moderateScale(3),fontFamily:isRTL?arrabicFont:englishFont,height:responsiveHeight(5), textAlign:isRTL?'right':'left'}}
                     placeholder={Strings.password}
                     secureTextEntry
                     ref={(input) => { this.password = input; }}
                     returnKeyType='next'
                     onSubmitEditing={() => {this.confirmPassword.focus(); }}
                />
                </View>
                {password.length==0&&
                <Text style={{color:'red',marginHorizontal:moderateScale(2),fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {Strings.require}</Text>
                }
            </View>
        )
    }


    confirmPasswordInput = () => {
        const {isRTL} = this.props
        const {confirmPassword} = this.state
        return(
            <View style={{marginTop:moderateScale(6),width:responsiveWidth(80), alignSelf:'center'}}>
                
                <Text style={{marginBottom:moderateScale(3),fontFamily:isRTL?arrabicFont:englishFont, color:colors.black,fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.confirmPassword}</Text>
                <View style={{borderRadius:moderateScale(1), backgroundColor:colors.lightGray }} >
                <TextInput 
                     onChangeText={(val)=>{this.setState({confirmPassword:val})}}
                     style={{width:responsiveWidth(80),padding:0, paddingHorizontal:moderateScale(3),fontFamily:isRTL?arrabicFont:englishFont,height:responsiveHeight(5), textAlign:isRTL?'right':'left'}}
                     placeholder={Strings.confirmPassword}
                     secureTextEntry
                     ref={(input) => { this.confirmPassword = input; }}
                />
                </View>
                {confirmPassword.length==0&&
                <Text style={{color:'red',marginHorizontal:moderateScale(2),fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {Strings.require}</Text>
                }
            </View>
        )
    }


    

    save = () => {
        const {firstName,lastName,userName,phone,confirmPassword, password,email,image} = this.state
        const {currentUser} = this.props

        if(!email.replace(/\s/g, '').length){
            this.setState({email:''})
        }
        if(!confirmPassword.replace(/\s/g, '').length){
            this.setState({confirmPassword:''})
        }
        if(!userName.replace(/\s/g, '').length){
            this.setState({userName:''})
        }
        if(!password.replace(/\s/g, '').length){
            this.setState({password:''})
        }

        if(!firstName.replace(/\s/g, '').length){
            this.setState({firstName:''})
        }

        if(!lastName.replace(/\s/g, '').length){
            this.setState({lastName:''})
        }

        if(!phone.replace(/\s/g, '').length){
            this.setState({phone:''})
        }

        if(password!=confirmPassword){
            RNToasty.Error({title:Strings.confirmPasswordMustBeLikePassword})
        }
        if(password==confirmPassword&&userName.replace(/\s/g, '').length&&firstName.replace(/\s/g, '').length&&lastName.replace(/\s/g, '').length&&email.replace(/\s/g, '').length&&phone.replace(/\s/g, '').length&&password.replace(/\s/g, '').length){
            var data = new FormData()
            this.setState({loading:true})
            data.append('firstname',firstName)
            data.append('lastname',lastName)
            data.append('username',userName.toLowerCase())
            data.append('email',email)
            data.append('phone',phone)
            data.append('password',password)
            data.append('type',currentUser.user.type)
            if(image){
                data.append('img', {
                    uri: image,
                    type: 'multipart/form-data',
                    name: 'photoImage'
                })
            }

            axios.put(`${BASE_END_POINT}user/${currentUser.user.id}/updateInfo`,data,{
                headers:{
                    "Authorization": `Bearer ${currentUser.token}`,
                    "Content-Type": "application/json"
                  }
            })
            .then(response=>{
                console.log('DONE')
                this.setState({loading:false})
                var user = {...currentUser,user:response.data.user}
                this.props.setUser(user)
                AsyncStorage.setItem('USER',JSON.stringify(user))
                if(user.user.type=='SURVEY'){
                    resetTo('SurveyHome')
                }else if(user.user.type=='USER'){
                    resetTo('ResturantHome')
                }else if(user.user.type=='ADMIN'){
                    resetTo('AdminHome')
                }else if(user.user.type=='OPERATION'){
                    resetTo('OperationHome')
                }else if(user.user.type=='DRIVER'){
                    resetTo('DrivertHome')
                }else if(user.user.type=='PURCHASING'){
                    resetTo('PurchasingHome')
                }
                RNToasty.Success({title:Strings.profileUpdatSuccessfuly})
            })
            .catch(error=>{
                this.setState({loading:false})
                console.log('ERROR   ',error.response)
                RNToasty.Error({title:Strings.incorrectPassword})
            })
            

        }

       
    }

    saveButton = () => {
        const {isRTL} = this.props

        return(
        <TouchableOpacity onPress={()=>{this.save()}} 
        style={[whiteButton,{alignSelf:'center', marginVertical:moderateScale(15), height:responsiveHeight(7),width:responsiveWidth(40),justifyContent:'center',alignItems:'center',marginBottom:moderateScale(35)}]} >
            <Text style={{color:colors.green,fontFamily:isRTL?arrabicFont:englishFont, fontSize:responsiveFontSize(7)}}>{Strings.update}</Text>
        </TouchableOpacity>
        )
    }

    pickImage = () =>{
        ImagePicker.openPicker({
            width: 600,
            height: 600,
            cropping: true
          }).then(image => {
              this.setState({image:image.path})
            console.log(image);
          });
    }

    profileImage = () => {
        const {isRTL,currentUser} = this.props
        const {image} = this.state
        return(
            <View style={{marginTop:moderateScale(10), alignSelf:'center'}} >
                <FastImage 
                resizeMode='center'
                source={image?{uri:image}:currentUser.user.img?{uri:currentUser.user.img}:require('../assets/imgs/profileicon.jpg')}
                style={{borderWidth:2,borderColor:colors.lightGray, width:100,height:100,borderRadius:50}}
                />
                <TouchableOpacity onPress={this.pickImage} style={{alignSelf:'flex-end',marginTop:moderateScale(-11), justifyContent:'center',alignItems:'center',backgroundColor:colors.darkGreen,height:40,width:40,borderRadius:20}}>
                    <Icon name='photo' type='FontAwesome' style={{fontSize:responsiveFontSize(7),color:colors.white}} />
                </TouchableOpacity>
            </View>
        )
    }


    

    render(){
        const  { isRTL,userToken } = this.props;
        const  {loading} = this.state
        return(
        <LinearGradient 
       
        colors={[colors.white, colors.white, colors.white]} 
        style={{flex:1}}
        >
         <Header  title={Strings.editProfile} /> 
  
         <KeyboardAwareScrollView  
         showsVerticalScrollIndicator={false}
         style={{ backgroundColor: colors.white, borderTopRightRadius: moderateScale(20), borderTopLeftRadius: moderateScale(20), marginTop: moderateScale(15), width: responsiveWidth(100)}}>
       
            <View style={{alignSelf:'center', width:responsiveWidth(60),borderBottomColor:colors.darkGreen,borderBottomWidth:2, justifyContent:'center',alignItems:'center'}}>
                <Text style={{marginTop:moderateScale(5),marginBottom:moderateScale(3), color:colors.darkGreen,fontSize:responsiveFontSize(8),fontFamily:isRTL?arrabicFont:englishFont}}>{Strings.updateYourProfile}</Text>
            </View>
            {this.profileImage()}
            <View style={{width:responsiveWidth(80),justifyContent:'space-between', alignSelf:'center',flexDirection:isRTL?'row-reverse':'row'}}>
                {this.firstNameInput()}
                {this.lastNameInput()}
            </View>
            {this.userNameInput()}
            {this.emailInput()}
            {this.phoneInput()}
            {this.passwordInput()}
            <View style={{ borderRadius: moderateScale(1), height: 0.1 }} >
                        <TextInput
                            style={{ height: 0.1 }}
                        />
                    </View>
            {this.confirmPasswordInput()}
            {this.saveButton()}
        </KeyboardAwareScrollView>
        {loading&&<LoadingDialogOverlay title={Strings.wait} />}
        {this.state.showFooter&&<AppFooter />}
        </LinearGradient> 
        );
    }
}
const mapDispatchToProps = {
    setUser,
    removeItem
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL, 
    currentUser:state.auth.currentUser, 
    //userToken: state.auth.userToken,
})


export default connect(mapToStateProps,mapDispatchToProps)(UpdateProfile);

