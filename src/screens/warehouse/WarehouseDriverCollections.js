import React, { Component } from 'react';
import {
    View, TouchableOpacity, Image, Text, ScrollView, TextInput, Alert, Platform
} from 'react-native';
import { connect } from 'react-redux';
import { Icon, Button } from 'native-base';
import { login } from '../../actions/AuthActions'
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../../utils/responsiveDimensions";
import Strings from '../../assets/strings';
import LoadingDialogOverlay from '../../components/LoadingDialogOverlay';
import { enableSideMenu, resetTo, push, pop } from '../../controlls/NavigationControll'
import { arrabicFont, englishFont } from '../../common/AppFont'
import * as Animatable from 'react-native-animatable';
import * as colors from '../../assets/colors';
import { removeItem } from '../../actions/MenuActions';
import CollaspeAppHeader from '../../common/CollaspeAppHeader'
import LinearGradient from 'react-native-linear-gradient';
import FastImage from 'react-native-fast-image'
import CommanHeader from '../../common/CommanHeader'
import ImagePicker from 'react-native-image-crop-picker';
import AppFooter from '../../components/AppFooter'
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import io from 'socket.io-client';
import axios from 'axios';
import { BASE_END_POINT } from '../../AppConfig'
import { RNToasty } from 'react-native-toasty';
import DateTimePicker from '@react-native-community/datetimepicker';
import { TextInputMask } from 'react-native-masked-text'
import DateTimePickerModal from "react-native-modal-datetime-picker";
import { whiteButton, greenButton } from '../../assets/styles';
import RNPickerSelect from 'react-native-picker-select';
import Carousel from 'react-native-snap-carousel';
import { SpinPicker } from 'react-native-spin-picker'
import Header from '../../common/Header'



class WarehouseDriverCollections extends Component {

    socket = null;
    state = {
        selectedDriver: false,
        drivers: [],
        loading: false,
        selectedItem1: 0,
        selectedItem2: 0,
        selectedItem3: 0,
        selectedItem4: 0,
        selectedItem5: 0,
        selectedKg: '0',
        kg: this.props.data.average,
        showKgPicker: false
    }

    componentDidMount() {
        enableSideMenu(false, null)
        this.getDrivers()
        this.socket = io("https://api.zeitcoapp.com/order", { query: `id=${this.props.currentUser.user.id}` });
    }


    getDrivers = () => {
        const { isRTL } = this.props
        axios.get(`${BASE_END_POINT}restaurant?status=APPROVED`)
            .then(response => {
                // Alert.alert("Done")
                console.log('All All RES : ', response.data.data)
                var rests = []
                response.data.data.map((item) => {
                    //rests.push({ label: item.restaurantName, value: item.id, resturant: item, price: item.price, branches: item.branches, id: item.id })
                    console.log(item)
                    rests.push({ id: item.id, name: isRTL ? item.restaurantName_ar : item.restaurantName, resturant: item, price: item.price, branches: item.branches })
                })
                console.log('rests')
                this.setState({
                    loading: false,
                    drivers: [
                        {
                            name: Strings.drivers,
                            id: 0,
                            children: rests
                        }
                    ],
                    // resturants: rests
                })
            })
            .catch(error => {
                console.log("ERROR  ", error.response)
                this.setState({ loading: false })
            })
    }


    driversPicker = () => {
        const { isRTL } = this.props
        const { selectedDriver, drivers, loading } = this.state
        return (
            <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', width: responsiveWidth(80), alignSelf: 'center' }} >

                <View animation={isRTL ? "slideInRight" : "slideInLeft"} duration={1000} style={{ marginTop: moderateScale(12), width: responsiveWidth(80), }}>

                    <View style={{ alignItems: 'center', justifyContent: 'center', backgroundColor: colors.lightGray, width: responsiveWidth(80), height: responsiveHeight(5), alignSelf: 'center' }}>



                        <SectionedMultiSelect
                            expandDropDowns
                            //modalAnimationType='slide'
                            //loading={loading}
                            showDropDowns={false}
                            modalWithTouchable
                            hideConfirm
                            searchPlaceholderText={Strings.search}
                            styles={{
                                selectToggle: { width: responsiveWidth(74), height: responsiveHeight(6), borderRadius: moderateScale(3), alignItems: 'center', justifyContent: 'center', },
                                selectToggleText: { textAlign: isRTL ? 'right' : 'left', fontSize: responsiveFontSize(6), color: 'gray', marginTop: moderateScale(8), fontFamily: isRTL ? arrabicFont : englishFont },
                                subItemText: { textAlign: isRTL ? 'right' : 'left' },
                                itemText: { fontSize: responsiveFontSize(10), textAlign: isRTL ? 'right' : 'left' },
                                container: { height: responsiveHeight(60), position: 'absolute', width: responsiveWidth(80), top: responsiveHeight(18), alignSelf: 'center' },
                                searchTextInput: { textAlign: isRTL ? 'right' : 'left', marginHorizontal: moderateScale(5) },
                            }}
                            items={drivers}
                            alwaysShowSelectText
                            single
                            searchPlaceholderText={Strings.search}
                            uniqueKey="id"
                            subKey="children"
                            selectText={this.state.selectedDriver ? this.state.selectedDriver : Strings.driverName}

                            readOnlyHeadings={true}
                            onSelectedItemsChange={(selectedItems) => {
                                // this.setState({ countries: selectedItems });
                            }
                            }
                            onSelectedItemObjectsChange={(selectedItems) => {
                                console.log("ITEM2   ", selectedItems[0].name)
                                this.setState({ selectedDriver: selectedItems[0].name, });
                            }
                            }

                        //onConfirm={() => this.setState({ selecTitle: '' })}
                        />

                    </View>
                    {selectedDriver == null &&
                        <Text style={{ color: 'red', marginTop: moderateScale(1), marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                    }
                </View>
            </View>
        )
    }


    send = () => {
        const { currentUser, data } = this.props
        const { kg } = this.state
        if (!kg) {
            RNToasty.Error({ title: Strings.enterKg })
        }
        if (kg) {
            this.setState({ loading: true })

            console.log("DATA  ", data, " KG  ", kg)
            axios.put(`${BASE_END_POINT}orders/${data.id}/stored`, JSON.stringify({
                warehouseKgAmount: kg,
                //warehouse:this.props.currentUser.user.warehouse.id
            }), {
                headers: {
                    "Authorization": `Bearer ${currentUser.token}`,
                    "Content-Type": "application/json"
                }
            })
                .then(respose => {
                    this.setState({ loading: false })
                    resetTo('WarehouseHome')
                    RNToasty.Success({ title: Strings.done })
                })
                .catch(error => {
                    console.log('ERROR  ', error.respose)
                    console.log('ERROR  ', error)
                    this.setState({ loading: false })
                })
        }
    }


    submit_cancel_buttons = () => {
        const { isRTL } = this.props
        return (
            <View style={{ alignSelf: 'center', marginTop: moderateScale(this.state.showKgPicker ? 30 : 100), flexDirection: isRTL ? 'row-reverse' : 'row' }} >

                <TouchableOpacity onPress={() => {
                    this.send()
                }} disabled={this.state.enable} style={whiteButton} >
                    <Text style={{ color: colors.green, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.confirm}</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => {
                    pop()
                }} style={greenButton} >
                    <Text style={{ color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.cancel}</Text>
                </TouchableOpacity>


            </View>
        )
    }

    amountView = () => {
        const { isRTL, data } = this.props
        const { showKgPicker } = this.state
        return (
            <View style={{ marginTop: moderateScale(10), backgroundColor: colors.lightGray, alignSelf: 'center', width: responsiveWidth(90), height: responsiveHeight(8), flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'space-between', alignItems: 'center' }} >
                <Text style={{ marginHorizontal: moderateScale(5), fontFamily: englishFont, color: 'black' }}> {Strings.quantity} : {data.average} {Strings.Kg} </Text>
                <TouchableOpacity
                    onPress={() => {
                        this.setState({ showKgPicker: !showKgPicker })
                    }}
                    style={{ width: responsiveWidth(30), height: responsiveHeight(7), justifyContent: 'center', alignItems: 'center', backgroundColor: colors.green }} >
                    <Text style={{ color: 'white' }}>{showKgPicker ? Strings.close : Strings.update}</Text>
                </TouchableOpacity>
            </View>
        )
    }


    spinKg = () => {
        const { isRTL } = this.props
        const { selectedItem1, selectedItem2, selectedItem3, selectedItem4, selectedItem5 } = this.state
        return (
            <View style={{ width: responsiveWidth(20), alignSelf: 'center', marginTop: moderateScale(0), justifyContent: 'center', alignItems: 'center' }}>
                <View style={{ flexDirection: 'row', width: responsiveWidth(20), alignSelf: 'center', marginTop: moderateScale(0), justifyContent: 'center', alignItems: 'center' }}>
                    <View style={{ alignSelf: 'center', marginTop: moderateScale(6), justifyContent: 'center', alignItems: 'center' }}>
                        <SpinPicker
                            data={[...Array(10).keys()]}
                            value={this.state.selectedItem1}
                            onValueChange={selectedItem => this.setState({ selectedItem1: selectedItem, kg: parseFloat(selectedItem1 + '' + selectedItem2 + '' + selectedItem3 + '' + selectedItem4 + '.' + selectedItem5) })}
                            keyExtractor={number => number.toString()}
                            //showArrows={Platform.OS=='android'?true:false}
                            onInputValueChanged={this.onValueChanged}
                            renderItem={item => <Text style={{ color: colors.darkGray, textAlign: 'center', width: responsiveWidth(17), height: responsiveHeight(6), lineHeight: responsiveHeight(6), fontSize: responsiveFontSize(8) }}>{item.toString()}</Text>} />
                    </View>
                    <View style={{ alignSelf: 'center', marginTop: moderateScale(6), justifyContent: 'center', alignItems: 'center' }}>
                        <SpinPicker
                            data={[...Array(10).keys()]}
                            value={this.state.selectedItem2}
                            onValueChange={selectedItem => this.setState({ selectedItem2: selectedItem, kg: parseFloat(selectedItem1 + '' + selectedItem2 + '' + selectedItem3 + '' + selectedItem4 + '.' + selectedItem5) })}
                            keyExtractor={number => number.toString()}
                            //showArrows={Platform.OS=='android'?true:false}
                            onInputValueChanged={this.onValueChanged}
                            renderItem={item => <Text style={{ color: colors.darkGray, textAlign: 'center', width: responsiveWidth(17), height: responsiveHeight(6), lineHeight: responsiveHeight(6), fontSize: responsiveFontSize(8) }}>{item.toString()}</Text>} />
                    </View>
                    <View style={{ alignSelf: 'center', marginTop: moderateScale(6), justifyContent: 'center', alignItems: 'center' }}>
                        <SpinPicker
                            data={[...Array(10).keys()]}
                            value={this.state.selectedItem3}
                            onValueChange={selectedItem => this.setState({ selectedItem3: selectedItem, kg: parseFloat(selectedItem1 + '' + selectedItem2 + '' + selectedItem3 + '' + selectedItem4 + '.' + selectedItem5) })}
                            keyExtractor={number => number.toString()}
                            //showArrows={Platform.OS=='android'?true:false}
                            onInputValueChanged={this.onValueChanged}
                            renderItem={item => <Text style={{ color: colors.darkGray, textAlign: 'center', width: responsiveWidth(17), height: responsiveHeight(6), lineHeight: responsiveHeight(6), fontSize: responsiveFontSize(8) }}>{item.toString()}</Text>} />
                    </View>
                    <View style={{ alignSelf: 'center', marginTop: moderateScale(6), justifyContent: 'center', alignItems: 'center' }}>
                        <SpinPicker
                            data={[...Array(10).keys()]}
                            value={this.state.selectedItem4}
                            onValueChange={selectedItem => this.setState({ selectedItem4: selectedItem, kg: parseFloat(selectedItem1 + '' + selectedItem2 + '' + selectedItem3 + '' + selectedItem4 + '.' + selectedItem5) })}
                            keyExtractor={number => number.toString()}
                            //showArrows={Platform.OS=='android'?true:false}
                            onInputValueChanged={this.onValueChanged}
                            renderItem={item => <Text style={{ color: colors.darkGray, textAlign: 'center', width: responsiveWidth(16), height: responsiveHeight(6), lineHeight: responsiveHeight(6), fontSize: responsiveFontSize(8) }}>{item.toString()}</Text>} />
                    </View>

                    <View style={{ borderTopWidth: 0.7, borderBottomWidth: 0.7, borderColor: 'black', marginTop: moderateScale(6) }}>
                        <Text style={{ color: colors.darkGray, height: responsiveHeight(6), lineHeight: responsiveHeight(7), fontSize: responsiveFontSize(9) }}>.</Text>
                    </View>

                    <View style={{ alignSelf: 'center', marginTop: moderateScale(6), justifyContent: 'center', alignItems: 'center' }}>

                        <SpinPicker
                            data={[...Array(10).keys()]}
                            value={this.state.selectedItem5}
                            onValueChange={selectedItem => this.setState({ selectedItem5: selectedItem, kg: parseFloat(selectedItem1 + '' + selectedItem2 + '' + selectedItem3 + '' + selectedItem4 + '.' + selectedItem5) })}
                            keyExtractor={number => number.toString()}
                            //showArrows={Platform.OS=='android'?true:false}
                            onInputValueChanged={this.onValueChanged}
                            renderItem={item => <Text style={{ color: colors.darkGray, textAlign: 'center', width: responsiveWidth(17), height: responsiveHeight(6), lineHeight: responsiveHeight(6), fontSize: responsiveFontSize(8) }}>{item.toString()}</Text>} />
                    </View>
                </View>
            </View>
        )
    }



    render() {
        const { isRTL, userToken, currentUser } = this.props;
        const { loading, showKgPicker } = this.state;
        return (
            <LinearGradient
                colors={[colors.white, colors.white, colors.white]}
                style={{ flex: 1 }}
            >
                <Header title={Strings.driverCollections} />

                <ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: colors.white, borderTopRightRadius: moderateScale(20), borderTopLeftRadius: moderateScale(20), marginTop: moderateScale(15), width: responsiveWidth(100), marginBottom: moderateScale(20) }} >
                    {this.amountView()}
                    {/*this.driversPicker()*/}
                    {showKgPicker && <Text style={{ marginTop: moderateScale(20), color: colors.green, marginHorizontal: moderateScale(10), alignSelf: isRTL ? 'flex-end' : 'flex-start', fontFamily: arrabicFont, fontSize: responsiveFontSize(8) }} >{Strings.enterDriverQty}</Text>}
                    {showKgPicker && this.spinKg()}
                    {this.submit_cancel_buttons()}
                </ScrollView>
                {loading && <LoadingDialogOverlay title={Strings.wait} />}


                <AppFooter />
            </LinearGradient>
        );
    }
}
const mapDispatchToProps = {

}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
    //userToken: state.auth.userToken,
})


export default connect(mapToStateProps, mapDispatchToProps)(WarehouseDriverCollections);

