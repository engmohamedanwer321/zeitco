import React, { Component } from 'react';
import {
  View,TouchableOpacity,Image,Text,ScrollView,TextInput,Alert,Platform
} from 'react-native';
import { connect } from 'react-redux';
import {Icon,Button} from 'native-base';
import {login} from '../actions/AuthActions'
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import Strings from '../assets/strings';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import {enableSideMenu,resetTo,push} from '../controlls/NavigationControll'
import {arrabicFont,englishFont} from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import * as colors from '../assets/colors';
import {removeItem} from '../actions/MenuActions';
import CollaspeAppHeader from '../common/CollaspeAppHeader'
import LinearGradient from 'react-native-linear-gradient';
import FastImage from 'react-native-fast-image'
import CommanHeader from '../common/CommanHeader'
import ImagePicker from 'react-native-image-crop-picker';
import AppFooter from '../components/AppFooter'
import Header from '../common/Header'


class AccountDetails extends Component {

    state={
       
    }

    componentDidMount(){
        enableSideMenu(false, null)
    }

    basicsData = () => {
        const {isRTL} = this.props
        return(
            <View style={{width:responsiveWidth(90),alignItems:isRTL?'flex-end':'flex-start', alignSelf:'center',marginTop:moderateScale(10)}}>
                
                <Text  style={{color:'black', fontFamily:isRTL?arrabicFont:englishFont,fontSize:responsiveFontSize(8),borderBottomWidth:1,borderBottomColor:'black', paddingBottom:moderateScale(2)}} >{Strings.restaurantDetails}</Text>

                <Text  style={{color:'gray', fontFamily:isRTL?arrabicFont:englishFont,fontSize:responsiveFontSize(6),marginTop:moderateScale(5)}} >Resturant name</Text>
                <Text  style={{color:'gray', fontFamily:isRTL?arrabicFont:englishFont,fontSize:responsiveFontSize(6),marginTop:moderateScale(5)}} >Average KG</Text>
                <Text  style={{color:'gray', fontFamily:isRTL?arrabicFont:englishFont,fontSize:responsiveFontSize(6),marginTop:moderateScale(5)}} >Resturant space</Text>
                <Text  style={{color:'gray', fontFamily:isRTL?arrabicFont:englishFont,fontSize:responsiveFontSize(6),marginTop:moderateScale(5)}} >Branchs number</Text>
                <Text  style={{color:'gray', fontFamily:isRTL?arrabicFont:englishFont,fontSize:responsiveFontSize(6),marginTop:moderateScale(5)}} >Oil type</Text>
                <Text  style={{color:'gray', fontFamily:isRTL?arrabicFont:englishFont,fontSize:responsiveFontSize(6),marginTop:moderateScale(5)}} >Phone number Addsress</Text>
                <Text  style={{color:'gray', fontFamily:isRTL?arrabicFont:englishFont,fontSize:responsiveFontSize(6),marginTop:moderateScale(5)}} >Average KG</Text>

                {/*  */}

                <Text  style={{color:'black', fontFamily:isRTL?arrabicFont:englishFont,fontSize:responsiveFontSize(8),borderBottomWidth:1,borderBottomColor:'black', paddingBottom:moderateScale(2),marginTop:moderateScale(12)}} >{Strings.contactDetails}</Text>

                <Text  style={{color:'gray', fontFamily:isRTL?arrabicFont:englishFont,fontSize:responsiveFontSize(6),marginTop:moderateScale(5)}} >First name</Text>
                <Text  style={{color:'gray', fontFamily:isRTL?arrabicFont:englishFont,fontSize:responsiveFontSize(6),marginTop:moderateScale(5)}} >Last name</Text>
                <Text  style={{color:'gray', fontFamily:isRTL?arrabicFont:englishFont,fontSize:responsiveFontSize(6),marginTop:moderateScale(5)}} >email</Text>
                <Text  style={{color:'gray', fontFamily:isRTL?arrabicFont:englishFont,fontSize:responsiveFontSize(6),marginTop:moderateScale(5)}} >phone</Text>
                <Text  style={{color:'gray', fontFamily:isRTL?arrabicFont:englishFont,fontSize:responsiveFontSize(6),marginTop:moderateScale(5),marginBottom:moderateScale(10)}} >Driver name</Text>

            </View>
        )
    }
    
    edit_delete_button = () => {
        const {isRTL} = this.props
        return(
            <View style={{alignSelf:'center',width:responsiveWidth(60),justifyContent:'space-between', flexDirection: isRTL?'row-reverse':'row' ,marginTop:moderateScale(10),marginBottom:moderateScale(20),alignItems:'center'}} >
                
                <Button onPress={()=>{
                        push('ResturantAccountDetails')
                    }} style={{width:responsiveWidth(25), alignSelf:'center',height:responsiveHeight(7),justifyContent:'center',alignItems:'center',backgroundColor:colors.darkGreen,borderRadius:moderateScale(5)}} >
                    <Text style={{paddingHorizontal:moderateScale(5), color:colors.white,fontFamily:isRTL?arrabicFont:englishFont}}>{Strings.edit}</Text>
                </Button>

                <Button onPress={()=>{
                        
                    }} style={{width:responsiveWidth(25), alignSelf:'center',height:responsiveHeight(7),justifyContent:'center',alignItems:'center',backgroundColor:'red',borderRadius:moderateScale(5)}} >
                    <Text style={{paddingHorizontal:moderateScale(5), color:colors.white,fontFamily:isRTL?arrabicFont:englishFont}}>{Strings.delete}</Text>
                </Button>

                </View>
        )
    }
    

    render(){
        const  { isRTL,userToken } = this.props;
        return(
        <LinearGradient 
        colors={[colors.white, colors.white, colors.white]} 
        style={{flex:1}}
        >
         <Header  title={Strings.accountDetails} />

        <ScrollView showsVerticalScrollIndicator={false}  style={{backgroundColor:colors.white, borderTopRightRadius:moderateScale(20),borderTopLeftRadius:moderateScale(20), marginTop:moderateScale(15), width:responsiveWidth(100),marginBottom:moderateScale(20)}} >
            {this.basicsData()}
            {this.edit_delete_button()}
        </ScrollView>
        
        <AppFooter />
        </LinearGradient> 
        );
    }
}


const mapDispatchToProps = {
    login,
    removeItem
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL, 
    loading: state.auth.loading,
    errorText: state.auth.errorText,
    //userToken: state.auth.userToken,
})


export default connect(mapToStateProps,mapDispatchToProps)(AccountDetails);

