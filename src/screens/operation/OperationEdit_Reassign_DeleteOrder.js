import React, { Component } from 'react';
import {
    View, TouchableOpacity, Image, Text, ScrollView, TextInput, Alert, Platform
} from 'react-native';
import { connect } from 'react-redux';
import { Icon, Button } from 'native-base';
import { login } from '../../actions/AuthActions'
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../../utils/responsiveDimensions";
import Strings from '../../assets/strings';
import LoadingDialogOverlay from '../../components/LoadingDialogOverlay';
import { enableSideMenu, resetTo, push } from '../../controlls/NavigationControll'
import { arrabicFont, englishFont } from '../../common/AppFont'
import * as Animatable from 'react-native-animatable';
import * as colors from '../../assets/colors';
import { removeItem } from '../../actions/MenuActions';
import CollaspeAppHeader from '../../common/CollaspeAppHeader'
import LinearGradient from 'react-native-linear-gradient';
import FastImage from 'react-native-fast-image'
import CommanHeader from '../../common/CommanHeader'
import ImagePicker from 'react-native-image-crop-picker';
import AppFooter from '../../components/AppFooter'
import axios from 'axios'
import { BASE_END_POINT } from '../../AppConfig'
import { RNToasty } from 'react-native-toasty';
import Dialog, { DialogContent, DialogTitle } from 'react-native-popup-dialog';
import { whiteButton, greenButton } from '../../assets/styles'
import Header from '../../common/Header'


class OperationEdit_Reassign_DeleteOrder extends Component {

    state = {
        showDialog: false,
    }

    componentDidMount() {
        enableSideMenu(false, null)
    }

    dialog = () => {
        const { isRTL, data } = this.props;
        return (
            <Dialog
                containerStyle={{ backgroundColor: 'rgba(1,1,1,0.1)', }}
                visible={this.state.showDialog}
                onTouchOutside={() => {
                    this.setState({ showDialog: false });
                }}
                onHardwareBackPress={() => {
                    this.setState({ showDialog: false });
                }}
            >
                <View style={{ backgroundColor: 'red', justifyContent: 'center', alignItems: 'center', width: responsiveWidth(90), height: responsiveHeight(15) }}>
                    <Text style={{ color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.areYouSureToCancelOrdere}</Text>
                    <View style={{ marginTop: moderateScale(5), flexDirection: isRTL ? 'row-reverse' : 'row', width: responsiveWidth(70), justifyContent: 'space-between', alignItems: 'center' }}>
                        <Button onPress={() => {
                            this.setState({ showDialog: false })
                        }} style={{ width: responsiveWidth(28), height: responsiveHeight(5), justifyContent: 'center', alignItems: 'center', alignSelf: isRTL ? 'flex-start' : 'flex-end', borderRadius: moderateScale(3), backgroundColor: 'white' }}>
                            <Text style={{ color: colors.black, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.no}</Text>
                        </Button>
                        <Button onPress={() => {
                            this.deleteOrder()
                        }} style={{ width: responsiveWidth(28), height: responsiveHeight(5), justifyContent: 'center', alignItems: 'center', alignSelf: isRTL ? 'flex-start' : 'flex-end', borderRadius: moderateScale(3), backgroundColor: 'white' }}>
                            <Text style={{ color: colors.black, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.yes}</Text>
                        </Button>

                    </View>
                </View>
            </Dialog>
        )
    }

    deleteOrder = () => {
        axios.delete(`${BASE_END_POINT}orders/${this.props.data.id}`, {
            headers: {
                'Authorization': `Bearer ${this.props.currentUser.token}`
            }
        })
            .then(response => {
                console.log("REMOVE DONE")
                RNToasty.Success({ title: Strings.deletOrder })
                if (this.props.currentUser.user.type == 'ADMIN') {
                    resetTo('AdminHome')
                } else {
                    resetTo('OperationHome')
                }
            })
            .catch(error => {
                console.log("Error  ", error.response)
            })
    }

    basicsData = () => {
        const { isRTL, data } = this.props
        return (
            <View style={{ alignSelf: 'center', marginTop: moderateScale(5), justifyContent: 'center', width: responsiveWidth(90) }}>

                <Text style={{ color: 'black', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(8), borderBottomWidth: 1, borderBottomColor: 'black', textAlign: isRTL ? 'right' : 'left' }} >{Strings.restaurantDetails}</Text>

                <Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), marginTop: moderateScale(5), textAlign: isRTL ? 'right' : 'left' }} >{Strings.restaurantName}: {isRTL ? data.restaurant.restaurantName_ar : data.restaurant.restaurantName}</Text>
                <Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), marginTop: moderateScale(5), textAlign: isRTL ? 'right' : 'left' }} >{Strings.averageKg}: {data.average} {Strings.Kg}</Text>
                <Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), marginTop: moderateScale(5), textAlign: isRTL ? 'right' : 'left' }} >{Strings.outletCode}: {data.restaurant.outletCode}</Text>
                <Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), marginTop: moderateScale(5), textAlign: isRTL ? 'right' : 'left' }} >{Strings.oilType}: {data.restaurant.oilType}</Text>
                <Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), marginTop: moderateScale(5), textAlign: isRTL ? 'right' : 'left' }} >{Strings.addressByDetails}: {data.restaurant.branches[data.branch].address}</Text>

                {/*  */}

                <Text style={{ color: 'black', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(8), borderBottomWidth: 1, borderBottomColor: 'black', marginTop: moderateScale(12), textAlign: isRTL ? 'right' : 'left' }} >{Strings.contactDetails}</Text>

                <Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), marginTop: moderateScale(5), textAlign: isRTL ? 'right' : 'left' }} >{Strings.name}: {data.restaurant.firstname} {data.restaurant.lastname}</Text>
                {/*<Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), marginTop: moderateScale(5) }} >{Strings.lastName}: {data.restaurant.lastname}</Text>*/}
                {/*<Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), marginTop: moderateScale(5) }} >{Strings.email}: {data.restaurant.email}</Text>*/}
                <Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), marginTop: moderateScale(5), textAlign: isRTL ? 'right' : 'left' }} >{Strings.phone}: {data.restaurant.phone}</Text>

                {/*  */}

                <Text style={{ color: 'black', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(8), borderBottomWidth: 1, borderBottomColor: 'black', marginTop: moderateScale(12), textAlign: isRTL ? 'right' : 'left' }} >{Strings.driverDetail}</Text>

                <Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), marginTop: moderateScale(5), textAlign: isRTL ? 'right' : 'left' }} >{Strings.name}: {data.driver.firstname} {data.driver.lastname}</Text>

                {/*<Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), marginTop: moderateScale(5) }} >{Strings.email}: {data.driver.email}</Text>*/}
                <Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), marginTop: moderateScale(5), textAlign: isRTL ? 'right' : 'left' }} >{Strings.phone}: {data.driver.phone}</Text>

            </View>
        )
    }


    reassignButton = () => {
        const { isRTL, data } = this.props
        return (
            <View style={{ alignSelf: 'center', width: responsiveWidth(80), justifyContent: 'space-between', flexDirection: isRTL ? 'row-reverse' : 'row', marginTop: moderateScale(20), alignItems: 'center' }} >

                <Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont }}>{data.driver.firstname} {data.driver.lastname}</Text>

                <TouchableOpacity onPress={() => {
                    push('OperationDrivers', data)
                }} style={whiteButton} >
                    <Text style={{ paddingHorizontal: moderateScale(5), color: colors.green, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.reassignDriver}</Text>
                </TouchableOpacity>

            </View>
        )
    }

    edit_delete_button = () => {
        const { isRTL, data } = this.props
        console.log('DDAATT: ', data)
        return (
            <View style={{ alignSelf: 'center', width: responsiveWidth(80), justifyContent: 'space-between', flexDirection: isRTL ? 'row-reverse' : 'row', marginTop: moderateScale(10), marginBottom: moderateScale(20), alignItems: 'center' }} >

                <TouchableOpacity onPress={() => {
                    push('DriverUpdateOrder', data)
                }} style={whiteButton} >
                    <Text style={{ paddingHorizontal: moderateScale(5), color: colors.green, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.edit}</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => {
                    this.setState({ showDialog: true })
                }} style={greenButton} >
                    <Text style={{ paddingHorizontal: moderateScale(5), color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.cancelOrder}</Text>
                </TouchableOpacity>

            </View>
        )
    }




    render() {
        const { isRTL, userToken } = this.props;
        return (
            <LinearGradient
                colors={[colors.white, colors.white, colors.white]}
                style={{ flex: 1 }}
            >
                <Header title={Strings.assignOrder} />

                <ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: colors.white, borderTopRightRadius: moderateScale(20), borderTopLeftRadius: moderateScale(20), marginTop: moderateScale(5), width: responsiveWidth(100), marginBottom: moderateScale(20) }} >
                    {this.basicsData()}
                    {this.reassignButton()}
                    {this.edit_delete_button()}
                    {this.dialog()}
                </ScrollView>

                <AppFooter />
            </LinearGradient>
        );
    }
}
const mapDispatchToProps = {
    login,
    removeItem
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
})


export default connect(mapToStateProps, mapDispatchToProps)(OperationEdit_Reassign_DeleteOrder);

