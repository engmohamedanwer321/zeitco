import React, { Component } from 'react';
import {
    View, TouchableOpacity, Image, Text, ScrollView, TextInput, Alert, Platform
} from 'react-native';
import { connect } from 'react-redux';
import { Icon, Button } from 'native-base';
import { login } from '../../actions/AuthActions'
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../../utils/responsiveDimensions";
import Strings from '../../assets/strings';
import axios from 'axios';
import { BASE_END_POINT } from '../../AppConfig';
import LoadingDialogOverlay from '../../components/LoadingDialogOverlay';
import { enableSideMenu, resetTo, push } from '../../controlls/NavigationControll'
import { arrabicFont, englishFont, boldFont } from '../../common/AppFont'
import * as Animatable from 'react-native-animatable';
import * as colors from '../../assets/colors';
import { removeItem } from '../../actions/MenuActions';
import CollaspeAppHeader from '../../common/CollaspeAppHeader'
import LinearGradient from 'react-native-linear-gradient';
import FastImage from 'react-native-fast-image'
import CommanHeader from '../../common/CommanHeader'
import ImagePicker from 'react-native-image-crop-picker';
import AppFooter from '../../components/AppFooter';
import { RNToasty } from 'react-native-toasty';
import { AccountsCount } from '../../actions/AccountsAction';
import Header from '../../common/Header'


class OperationAssignAccountToPurchasing extends Component {

    state = {
        loading: false
    }

    componentDidMount() {
        enableSideMenu(false, null)
    }

    assignToPurchasing() {

        const { currentUser, data } = this.props

        this.setState({ loading: true })
        // console.log(`${BASE_END_POINT}restaurant/${order_id}/accept`)
        axios.put(`${BASE_END_POINT}restaurant/${data.id}/purchasing`, null, {
            headers: {
                "Authorization": `Bearer ${currentUser.token}`,
                "Content-Type": "application/json"
            }
        })
            .then(response => {
                console.log('DONE')
                this.setState({ loading: false })
                this.props.AccountsCount('SURVEY-ACCOUNT')
                RNToasty.Success({ title: Strings.AssignedSuccessfuly })
                if (this.props.currentUser.user.type == 'ADMIN') {
                    resetTo('AdminHome')
                } else {
                    resetTo('OperationHome')
                }
            })
            .catch(error => {
                this.setState({ loading: false })
                console.log('ERROR   ', error.response)
                RNToasty.Error({ title: Strings.errorInAssign })
            })

    }

    basicsData = () => {
        const { isRTL, data, currentUser } = this.props
        console.log('FDDDDD : ', currentUser.user.type)
        return (
            <View style={{ alignSelf: 'center', marginTop: moderateScale(20), width:responsiveWidth(85) }}>

                <Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(7),  textAlign: isRTL?'right':'left' }} ><Text style={{ fontFamily: boldFont }}>{Strings.restaurantName} </Text>{isRTL ? data.restaurantName_ar: data.restaurantName}</Text>
                <Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(7),  marginTop: moderateScale(5), textAlign: isRTL?'right':'left'  }} ><Text style={{ fontFamily: boldFont }}>{Strings.averageKg}: </Text>{data.average}</Text>
                {currentUser.user.type == 'OPERATION' &&
                    <Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(7),  marginTop: moderateScale(5), textAlign: isRTL?'right':'left'  }} ><Text style={{ fontFamily: boldFont }}>{Strings.price}</Text>{data.price}</Text>
                }
                {/*<Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(8), alignSelf: 'center', marginTop: moderateScale(5), textAlign: 'center' }} ><Text style={{ fontFamily: boldFont }}>{Strings.resSpace}</Text>{'\n'}{data.space}</Text>*/}
                {/*<Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(7),  marginTop: moderateScale(5), textAlign: isRTL?'right':'left'  }} ><Text style={{ fontFamily: boldFont }}>{Strings.branchesNo}: </Text>{data.branchesNumber}</Text>*/}
                {/*<Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(7),  marginTop: moderateScale(5), textAlign: isRTL?'right':'left'  }} ><Text style={{ fontFamily: boldFont }}>{Strings.oilType}: </Text>{data.oilType}</Text>
                <Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(7),  marginTop: moderateScale(5),textAlign: isRTL?'right':'left'  }} ><Text style={{ fontFamily: boldFont }}>{Strings.phone}: </Text>{data.phone}</Text>*/}
                {data.branches.map((item) => (
                    console.log('lllll:',item),
                <Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(7),  marginTop: moderateScale(5), textAlign: isRTL?'right':'left'  }} ><Text style={{ fontFamily: boldFont }}>{Strings.address}: </Text>{isRTL ? item.city['arabicCityName'] : item.city['cityName']} - {isRTL ? item.area['arabicAreaName'] : item.area['areaName']} - {item.address}</Text>
                ))}
                <Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(7),  marginTop: moderateScale(5), textAlign: isRTL?'right':'left'  }} ><Text style={{ fontFamily: boldFont }}>{Strings.outletCode}: </Text>{data.outletCode}</Text>


            </View>
        )
    }




    submit_cancel_buttons = () => {
        const { isRTL,data } = this.props
        return (
            <View style={{ alignSelf: 'center', margin: moderateScale(20), }} >

                <TouchableOpacity onPress={() => {
                    //this.assignToPurchasing()
                    push('OperationAssignResturantToPurchasing',data)
                }} style={{ alignSelf: 'center', paddingHorizontal: moderateScale(5), height: responsiveHeight(6), justifyContent: 'center', alignItems: 'center', backgroundColor: 'white', borderWidth: 1, borderColor: colors.lightGreenButton }} >
                    <Text style={{ color: colors.green, fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(7) }}>{'purchasing' in data ? Strings.reassign : Strings.assignToPurshing}</Text>
                </TouchableOpacity>



            </View>
        )
    }




    render() {
        const { isRTL, userToken } = this.props;
        return (
            <LinearGradient
                colors={[colors.white, colors.white, colors.white]}
                style={{ flex: 1 }}
            >
                <Header title={Strings.assignAccount} />

                <ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: colors.white, borderTopRightRadius: moderateScale(20), borderTopLeftRadius: moderateScale(20), marginTop: moderateScale(5), width: responsiveWidth(100), marginBottom: moderateScale(20) }} >
                    {this.basicsData()}

                    {this.submit_cancel_buttons()}
                </ScrollView>
                {this.state.loading == true ?
                    <LoadingDialogOverlay title={Strings.wait} />
                    :
                    null
                }
                <AppFooter />
            </LinearGradient>
        );
    }
}
const mapDispatchToProps = {
    login,
    removeItem,
    AccountsCount,
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    loading: state.auth.loading,
    errorText: state.auth.errorText,
    currentUser: state.auth.currentUser,
    accountsCount: state.accounts.accountsCount,
    //userToken: state.auth.userToken,
})


export default connect(mapToStateProps, mapDispatchToProps)(OperationAssignAccountToPurchasing);

