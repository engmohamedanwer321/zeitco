import React, { Component } from 'react';
import { View, RefreshControl, ScrollView, FlatList, Alert, Text, TouchableOpacity } from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../../utils/responsiveDimensions';
import { connect } from "react-redux";
import Strings from '../../assets/strings';

import axios from 'axios';
import { BASE_END_POINT } from '../../AppConfig';
import ListFooter from '../../components/ListFooter';
import { Icon, Thumbnail, Button } from 'native-base'
import { selectMenu, removeItem } from '../../actions/MenuActions';
import { enableSideMenu, pop } from '../../controlls/NavigationControll'
import NetInfo from "@react-native-community/netinfo";
import Loading from "../../common/Loading"
import NetworError from '../../common/NetworError'
import NoData from '../../common/NoData'
import * as colors from '../../assets/colors'
import { arrabicFont, englishFont } from '../../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image'
import AccountCard from '../../components/AccountCard'
import LinearGradient from 'react-native-linear-gradient';
import CommanHeader from '../../common/CommanHeader'
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';
import AppFooter from '../../components/AppFooter'
import Header from '../../common/Header'


class OperationPurchasings extends Component {

    _menu = null;
    page = 1;
    state = {
        networkError: null,
        members: [],
        membersLoading: true,
        membersRefresh: false,
        members404: false,
        pages: null,
    }


    componentDidMount() {
        enableSideMenu(false, null)
        this.getMembers(false, 1)
    }

    getMembers = (refresh, page) => {
        if (refresh) {
            this.setState({ membersRefresh: true })
        }

        axios.get(`${BASE_END_POINT}find?type=PURCHASING&page=${page}`)
            .then(response => {
                console.log('Done    ', response.data.data)
                this.setState({
                    members: refresh ? response.data.data : [...this.state.members, ...response.data.data],
                    membersLoading: false,
                    membersRefresh: false,
                    members404: false,
                    pages: response.data.pageCount,
                })
            })
            .catch(error => {
                console.log('Error   ', error.response)
                this.setState({ members404: true, membersLoading: false, })
            })
    }

    membersPage = () => {
        const { pages, members, members404, membersLoading, membersRefresh } = this.state
        return (
            members404 ?
                <NetworError />
                :
                membersLoading ?
                    <Loading />
                    :
                    members.length > 0 ?
                        <FlatList
                            showsVerticalScrollIndicator={false}
                            style={{ maxHeight: responsiveHeight(63), marginBottom: moderateScale(0) }}
                            data={members}
                            renderItem={({ item }) => <AccountCard order={this.props.data} data={item} socket={this.socket} />}
                            onEndReachedThreshold={.5}
                            //ListFooterComponent={()=>notificationsLoad&&<ListFooter />}
                            onEndReached={() => {
                                if (this.page <= pages) {
                                    this.page = this.page + 1;
                                    this.getMembers(false, this.page)
                                    console.log('page  ', this.page)
                                }
                            }}
                            refreshControl={
                                <RefreshControl
                                    //colors={["#B7ED03",colors.darkBlue]} 
                                    refreshing={membersRefresh}
                                    onRefresh={() => {
                                        this.page = 1
                                        this.getMembers(true, 1)
                                    }}
                                />
                            }

                        />
                        :
                        <NoData />
        )
    }

   

    render() {
        const { categoryName, isRTL } = this.props;
       
        return (
            <LinearGradient
                colors={[colors.white, colors.white, colors.white]}
                style={{ flex: 1 }}
            >
                <Header  title={Strings.purchasings} />
                <View style={{ flex: 1, minHeight: responsiveHeight(73), backgroundColor: colors.white, borderTopRightRadius: moderateScale(20), borderTopLeftRadius: moderateScale(20), marginTop: moderateScale(15), width: responsiveWidth(100) }} >

                    {this.membersPage()}
                </View>

                <AppFooter />

            </LinearGradient>
        );
    }
}

const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    barColor: state.lang.color,
    currentUser: state.auth.currentUser,
})

const mapDispatchToProps = {
    removeItem,
}

export default connect(mapStateToProps, mapDispatchToProps)(OperationPurchasings);
