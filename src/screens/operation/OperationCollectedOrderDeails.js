import React, { Component } from 'react';
import {
    View, TouchableOpacity, Image, Text, ScrollView, TextInput, Alert, Platform
} from 'react-native';
import { connect } from 'react-redux';
import { Icon, Button, Left } from 'native-base';
import { login } from '../../actions/AuthActions'
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../../utils/responsiveDimensions";
import Strings from '../../assets/strings';
import LoadingDialogOverlay from '../../components/LoadingDialogOverlay';
import { enableSideMenu, resetTo, push } from '../../controlls/NavigationControll'
import { arrabicFont, englishFont } from '../../common/AppFont'
import * as Animatable from 'react-native-animatable';
import * as colors from '../../assets/colors';
import { removeItem } from '../../actions/MenuActions';
import CollaspeAppHeader from '../../common/CollaspeAppHeader'
import LinearGradient from 'react-native-linear-gradient';
import FastImage from 'react-native-fast-image'
import CommanHeader from '../../common/CommanHeader'
import ImagePicker from 'react-native-image-crop-picker';
import AppFooter from '../../components/AppFooter'
import Header from '../../common/Header'


class OperationCollectedOrderDeails extends Component {

    state = {

    }

    componentDidMount() {
        enableSideMenu(false, null)
    }

    basicsData = () => {
        const { isRTL, data } = this.props
        return (
            <View style={{ alignSelf: 'center', marginTop: moderateScale(4), justifyContent: 'center', width: responsiveWidth(90) }}>

                <Text style={{ color: 'black', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(8), borderBottomWidth: 1, borderBottomColor: 'black', paddingBottom: moderateScale(2), textAlign: isRTL ? 'right' : 'left' }} >{Strings.restaurantDetails}</Text>


                <Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), marginTop: moderateScale(5), textAlign: isRTL ? 'right' : 'left' }} >{Strings.restaurantName}: {isRTL ? data.restaurant.restaurantName_ar : data.restaurant.restaurantName}</Text>
                <Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), marginTop: moderateScale(5), textAlign: isRTL ? 'right' : 'left' }} >{Strings.averageKg}: {data.restaurant.average} <Text style={{ fontWeight: 'bold' }}>{Strings.Kg}</Text></Text>
                {/*<Text  style={{color:'gray', fontFamily:isRTL?arrabicFont:englishFont,fontSize:responsiveFontSize(6),marginTop:moderateScale(5)}} >{Strings.resSpace}: {data.restaurant.space}</Text>*/}
                <Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), marginTop: moderateScale(5), textAlign: isRTL ? 'right' : 'left' }} >{Strings.outletCode}: {data.restaurant.outletCode}</Text>
                <Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), marginTop: moderateScale(5), textAlign: isRTL ? 'right' : 'left' }} >{Strings.oilType}: {data.restaurant.oilType}</Text>
                <Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), marginTop: moderateScale(5), textAlign: isRTL ? 'right' : 'left' }} >{Strings.addressByDetails}: {data.restaurant.branches[data.branch].address}</Text>

                {/*  */}

                <Text style={{ color: 'black', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(8), borderBottomWidth: 1, borderBottomColor: 'black', paddingBottom: moderateScale(2), marginTop: moderateScale(12), textAlign: isRTL ? 'right' : 'left' }} >{Strings.contactDetails}</Text>
                <Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), marginTop: moderateScale(3), textAlign: isRTL ? 'right' : 'left' }} >{Strings.name}: {data.restaurant.firstname} {data.restaurant.lastname}</Text>
                {/*<Text  style={{color:'gray', fontFamily:isRTL?arrabicFont:englishFont,fontSize:responsiveFontSize(6),marginTop:moderateScale(5)}} >{Strings.email}: {data.restaurant.email}</Text>*/}
                <Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), marginTop: moderateScale(5), textAlign: isRTL ? 'right' : 'left' }} >{Strings.phone}: {data.restaurant.phone}</Text>

                <Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), marginTop: moderateScale(5), textAlign: isRTL ? 'right' : 'left' }} >{Strings.driver}: {data.driver.firstname} {data.driver.lastname}</Text>

            </View>
        )
    }


    render() {
        const { isRTL, userToken } = this.props;
        return (
            <LinearGradient
                colors={[colors.white, colors.white, colors.white]}
                style={{ flex: 1 }}
            >
                <Header title={Strings.collectedOrders} />

                <ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: colors.white, borderTopRightRadius: moderateScale(20), borderTopLeftRadius: moderateScale(20), marginTop: moderateScale(5), width: responsiveWidth(100), marginBottom: moderateScale(20) }} >
                    {this.basicsData()}
                </ScrollView>

                <AppFooter />
            </LinearGradient>
        );
    }
}
const mapDispatchToProps = {
    login,
    removeItem
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    loading: state.auth.loading,
    errorText: state.auth.errorText,
    //userToken: state.auth.userToken,
})


export default connect(mapToStateProps, mapDispatchToProps)(OperationCollectedOrderDeails);

