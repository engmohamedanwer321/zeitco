import React,{Component} from 'react';
import {View,RefreshControl,ScrollView,FlatList,Text,TouchableOpacity} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight ,responsiveFontSize} from '../../utils/responsiveDimensions';
import { connect } from "react-redux";
import Strings from '../../assets/strings';

import axios from 'axios';
import { BASE_END_POINT} from '../../AppConfig';
import ListFooter from '../../components/ListFooter';
import {Icon,Thumbnail,Button} from 'native-base'
import {selectMenu,removeItem} from '../../actions/MenuActions';
import {enableSideMenu,pop} from '../../controlls/NavigationControll'
import NetInfo from "@react-native-community/netinfo";
import Loading from "../../common/Loading"
import NetworError from '../../common/NetworError'
import NoData from '../../common/NoData'
import * as colors from '../../assets/colors'
import {arrabicFont,englishFont} from  '../../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image'
import NotificationCard from '../../components/NotificationCard'
import OperationAssignWarehouseCard from '../../components/OperationAssignWarehouseCard';
import CollaspeAppHeader from '../../common/CollaspeAppHeader'
import LinearGradient from 'react-native-linear-gradient';
import CommanHeader from '../../common/CommanHeader'
import AppFooter from '../../components/AppFooter'
import io from 'socket.io-client';
import Header from '../../common/Header'

class OperationWarehouseMember extends Component {

    page=1;
    socket=null;
    state= {
        networkError:null,
        drivers:[],
        driversLoading: true,
        driversRefresh:false,
        drivers404:false,
        pages: null,
    }

    constructor(props){
        super(props);
         //connect to sicket
         this.socket = io("https://api.zeitcoapp.com/order",{ query: `id=${this.props.currentUser.user.id}`} );  
        
         //check the status (coonect or not ) when connect by socket
         this.socket.on('connect', () => {
             console.log("connect   " + this.socket.connected); // true
             console.log("connect   " + this.socket.id);
             console.log(this.socket);
             
         });
    }

    componentDidMount(){
        enableSideMenu(false, null)
        this.getDrivers(false,1)
    }


    getDrivers = (refresh,page) => {
        if(refresh){
            this.setState({driversRefresh:true})
        }
        axios.get(`${BASE_END_POINT}find?ype=WAREHOUSE&warehouse=${this.props.data.warehouse.id}&page=${page}`)
        .then(response=>{
          console.log('Done   ',response.data.data)
          this.setState({
              drivers:refresh?response.data.data:[...this.state.drivers,...response.data.data],
              driversLoading:false,
              driversRefresh:false,
              newOrdersLoading:false,
              pages:response.data.pageCount,
            })
          })
        .catch(error=>{
          console.log('Error   ',error.response)
          this.setState({drivers404:true,driversLoading:false,})
        })
    }

    driversPage = () => {
        const {pages,drivers,driversLoading,drivers404,driversRefresh} = this.state
        return (
            drivers404?
            <NetworError />
            :
            driversLoading?
            <Loading />
            :
            drivers.length>0?
            <FlatList
            showsVerticalScrollIndicator={false} 
            style={{marginVertical:moderateScale(10)}}
            data={drivers}
            renderItem={({item})=><OperationAssignWarehouseCard order={this.props.data} data={item} socket={this.socket} />}
            onEndReachedThreshold={.5}
            //ListFooterComponent={()=>notificationsLoad&&<ListFooter />}
            onEndReached={() => {     
                if(this.page <= pages){
                    this.page=this.page+1;
                    this.getDrivers(false,this.page)
                    console.log('page  ',this.page)
                }  
            }}
            refreshControl={
            <RefreshControl 
            //colors={["#B7ED03",colors.darkBlue]} 
            refreshing={driversRefresh}
            onRefresh={() => {
                this.page = 1
                this.getDrivers(true,1)
            }}
            />
            }
            
            />
            :
            <NoData />
        )
    }

    render(){
        const {categoryName,isRTL} = this.props;
        return(
            <LinearGradient 
            colors={[colors.white, colors.white, colors.white]} 
            style={{flex:1}}
            >
             <Header  title={Strings.assignWarehouse} />
            <ScrollView showsVerticalScrollIndicator={false} style={{marginBottom:moderateScale(20), backgroundColor:colors.white, borderTopRightRadius:moderateScale(20),borderTopLeftRadius:moderateScale(20), marginTop:moderateScale(15), width:responsiveWidth(100)}} >
              {this.driversPage()}
            </ScrollView>

            <AppFooter />
            </LinearGradient> 
        );
    }
}

const mapStateToProps = state => ({
    isRTL: state.lang.RTL, 
    barColor: state.lang.color ,
    currentUser:state.auth.currentUser, 
})

const mapDispatchToProps = {
    removeItem,
}

export default connect(mapStateToProps,mapDispatchToProps)(OperationWarehouseMember);
