import React, { Component } from 'react';
import { View, RefreshControl, Alert, ScrollView, FlatList, Text, TouchableOpacity, Image } from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../../utils/responsiveDimensions';
import { connect } from "react-redux";
import Strings from '../../assets/strings';

import axios from 'axios';
import { BASE_END_POINT } from '../../AppConfig';
import ListFooter from '../../components/ListFooter';
import { Icon, Thumbnail, Button } from 'native-base'
import { selectMenu, removeItem } from '../../actions/MenuActions';
import { enableSideMenu, pop, push } from '../../controlls/NavigationControll'
import NetInfo from "@react-native-community/netinfo";
import Loading from "../../common/Loading"
import NetworError from '../../common/NetworError'
import NoData from '../../common/NoData'
import { OrdersCount } from '../../actions/OrdersActions';
import { AccountsCount } from '../../actions/AccountsAction';
import * as colors from '../../assets/colors'
import { arrabicFont, englishFont } from '../../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image'
import NotificationCard from '../../components/NotificationCard'
import PurchasingAccountsCard from '../../components/PurchasingAccountsCard';
import CollaspeAppHeader from '../../common/CollaspeAppHeader'
import LinearGradient from 'react-native-linear-gradient';
import MainHeader from '../../common/MainHeader'
import Carousel from 'react-native-snap-carousel';
import BrandCard from '../../components/BrandCard'
import AppFooter from '../../components/AppFooter'
import { getUnreadNotificationsCount } from '../../actions/NotificationAction'
import {
  checkFirbaseNotificationPermision,
  getFirebaseNotificationToken,
  showFirebaseNotifcation,
  clickOnFirebaseNotification
} from '../../controlls/FirebasePushNotificationControll'
import { logout } from '../../actions/AuthActions'
import { setFirebaseToken } from '../../actions/NotificationAction'
import Header from '../../common/Header'


class OperationHome extends Component {

  page = 1;
  state = {
    networkError: null,
    showAlert: false,
  }


  componentDidMount() {
    enableSideMenu(true, this.props.isRTL)
    checkFirbaseNotificationPermision()
    getFirebaseNotificationToken(this.props.currentUser.token, this.props.setFirebaseToken)
    //showFirebaseNotifcation()
    //clickOnFirebaseNotification()
    this.props.OrdersCount(null, 'PENDING')
    this.props.AccountsCount('SURVEY-ACCOUNT')
    this.props.getUnreadNotificationsCount(this.props.currentUser.token)
  }

  buttons = () => {
    const { isRTL, ordersCount, accountsCount } = this.props
    return (
      <>
        <View style={{ marginTop: moderateScale(5), alignSelf: 'center', flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'center', alignItems: 'center', width: responsiveWidth(100) }}>


          <TouchableOpacity onPress={() => push('OperationAccounts', { accountType: 'Survey' })}
            style={{ flex:1, justifyContent: 'center', alignItems: 'center', marginTop: moderateScale(8) }}>

            <FastImage source={require('../../assets/imgs/home-viewaccount-icon.png')} style={{ width: 60, height: 60, borderRadius: 30 }} />
            <Text style={{ color: colors.black, marginTop: moderateScale(3), fontSize: responsiveFontSize(5), fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.surveyOutlets}</Text>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => push('OperationAccounts', { accountType: 'Purchasing' })} style={{ flex:1, justifyContent: 'center', alignItems: 'center', marginTop: moderateScale(8) }}>
            <FastImage source={require('../../assets/imgs/home-purchasing-icon.png')} style={{ width: 60, height: 60, borderRadius: 30 }} />
            <Text style={{ textAlign: 'center', color: colors.black, marginTop: moderateScale(3), fontSize: responsiveFontSize(5), fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.purchasingOutlets}</Text>
          </TouchableOpacity>


          <TouchableOpacity onPress={() => push('OperationAccounts', { accountType: 'Resturant' })} style={{ flex:1, justifyContent: 'center', alignItems: 'center', marginTop: moderateScale(8) }}>
            <FastImage source={require('../../assets/imgs/home-accounts-icon.png')} style={{ width: 60, height: 60, borderRadius: 30 }} />
            <View style={{ position: 'absolute', bottom: 15, zIndex: 10000, elevation: 2, shadowOffset: { height: 0, width: 2 }, alignSelf: isRTL ? 'flex-start' : 'flex-end', width: 20, height: 20, borderRadius: 10, backgroundColor: '#e20000', justifyContent: 'center', alignItems: 'center' }}>
              <Text style={{ color: colors.white, fontFamily: englishFont, fontSize: responsiveFontSize(4) }} >{accountsCount}</Text>
            </View>
            <Text style={{ color: colors.black, marginTop: moderateScale(3), fontSize: responsiveFontSize(5), fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.registeredOutlets}</Text>
          </TouchableOpacity>
        </View>

        <View style={{ marginTop: moderateScale(10), width: responsiveWidth(100), alignSelf: 'center', flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'space-between' }} >
          <TouchableOpacity onPress={() => push('MakeOrder')} style={{ flex:1, justifyContent: 'center', alignItems: 'center', marginTop: moderateScale(8) }}>
            <FastImage source={require('../../assets/imgs/new-order-icon.png')} style={{ width: 60, height: 60, borderRadius: 30 }} />
            <Text style={{ color: colors.black, marginTop: moderateScale(3), fontSize: responsiveFontSize(5), fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.manualOrder}</Text>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => push('OperationOrders')} style={{ flex:1, justifyContent: 'center', alignItems: 'center', marginTop: moderateScale(8) }}>
            <FastImage source={require('../../assets/imgs/home-orders-icon.png')} style={{ width: 60, height: 60, borderRadius: 30 }} />
            <View style={{ position: 'absolute', bottom: 15, zIndex: 10000, elevation: 2, shadowOffset: { height: 0, width: 2 }, alignSelf: isRTL ? 'flex-start' : 'flex-end', width: 20, height: 20, borderRadius: 10, backgroundColor: '#e20000', justifyContent: 'center', alignItems: 'center' }}>
              <Text style={{ color: colors.white, fontFamily: englishFont, fontSize: responsiveFontSize(4) }} >{ordersCount}</Text>
            </View>
            <Text style={{ color: colors.black, marginTop: moderateScale(3), fontSize: responsiveFontSize(5), fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.orders}</Text>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => push('History')} style={{ flex:1, justifyContent: 'center', alignItems: 'center', marginTop: moderateScale(8) }}>
            <FastImage source={require('../../assets/imgs/home-history-icon.png')} style={{ width: 60, height: 60, borderRadius: 30 }} />
            <Text style={{ color: colors.black, marginTop: moderateScale(3), fontSize: responsiveFontSize(5), fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.history}</Text>
          </TouchableOpacity>
        </View>

        <View style={{ marginTop: moderateScale(10), width: responsiveWidth(100), alignSelf: 'center', flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'space-between' }} >
          <TouchableOpacity onPress={() => push('AddResturantAccount')} style={{ flex:1, justifyContent: 'center', alignItems: 'center', marginTop: moderateScale(8) }}>
            <FastImage source={require('../../assets/imgs/home-addaccount-icon.png')} style={{ width: 60, height: 60, borderRadius: 30 }} />
            <Text style={{ color: colors.black, marginTop: moderateScale(3), fontSize: responsiveFontSize(5), fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.addAccount}</Text>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => push('OperationActiveAccounts')} style={{ flex:1, justifyContent: 'center', alignItems: 'center', marginTop: moderateScale(8) }}>
            <FastImage source={require('../../assets/imgs/home-viewaccount-icon.png')} style={{ width: 60, height: 60, borderRadius: 30 }} />
            <Text style={{ color: colors.black, marginTop: moderateScale(3), fontSize: responsiveFontSize(5), fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.viewAccounts}</Text>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => push('OperationShowContacts')} style={{ flex:1, justifyContent: 'center', alignItems: 'center', marginTop: moderateScale(8) }}>
            <FastImage source={require('../../assets/imgs/home-sendemail-icon.png')} style={{ width: 60, height: 60, borderRadius: 30 }} />
            <Text style={{ color: colors.black, marginTop: moderateScale(3), fontSize: responsiveFontSize(5), fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.contacts}</Text>
          </TouchableOpacity>
        </View>

        <View style={{ marginTop: moderateScale(10), width: responsiveWidth(70), alignSelf: 'center', flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'space-between' }} >

          <TouchableOpacity onPress={() => push('AdimnShowDriversLocation')} style={{ flex:1, justifyContent: 'center', alignItems: 'center', marginTop: moderateScale(8) }}>
            <FastImage source={require('../../assets/imgs/home-driverlocation-icon.png')} style={{ width: 60, height: 60, borderRadius: 30 }} />
            <Text style={{ color: colors.black, marginTop: moderateScale(3), fontSize: responsiveFontSize(5), fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.driverLocation}</Text>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => push('UpdateProfile')} style={{ flex:1, justifyContent: 'center', alignItems: 'center', marginTop: moderateScale(8) }}>
            <FastImage source={require('../../assets/imgs/home-profileedit-icon.png')} style={{ width: 60, height: 60, borderRadius: 30 }} />
            <Text style={{ color: colors.black, marginTop: moderateScale(3), fontSize: responsiveFontSize(5), fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.editProfile}</Text>
          </TouchableOpacity>

          {/*<TouchableOpacity onPress={() => this.props.logout(this.props.currentUser.token,'')} style={{ width: responsiveWidth(30), justifyContent: 'center', alignItems: 'center', marginTop: moderateScale(8) }}>
          <FastImage source={require('../../assets/imgs/home-logout-icon.png')} style={{ width: 60, height: 60, borderRadius: 30 }} />
          <Text style={{ color: colors.black, marginTop: moderateScale(3), fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.logout}</Text>
    </TouchableOpacity>*/}

        </View>
      </>
    )
  }


  render() {
    const { categoryName, isRTL } = this.props;
    return (
      <View style={{ flex: 1 }}>
        <Header menu title={Strings.home} />
        <ScrollView showsVerticalScrollIndicator={false} style={{ marginBottom: moderateScale(22) }} showsVerticalScrollIndicator={false} >
          {this.buttons()}
        </ScrollView>
        <AppFooter />
      </View>
    );
  }
}

const mapStateToProps = state => ({
  isRTL: state.lang.RTL,
  barColor: state.lang.color,
  currentUser: state.auth.currentUser,
  ordersCount: state.orders.ordersCount,
  accountsCount: state.accounts.accountsCount
})

const mapDispatchToProps = {
  removeItem,
  OrdersCount,
  getUnreadNotificationsCount,
  AccountsCount,
  logout,
  setFirebaseToken

}

export default connect(mapStateToProps, mapDispatchToProps)(OperationHome);
