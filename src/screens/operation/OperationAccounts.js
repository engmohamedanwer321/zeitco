import React, { Component } from 'react';
import { View, RefreshControl, Alert, Modal, ScrollView, FlatList, Text, TouchableOpacity } from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../../utils/responsiveDimensions';
import { connect } from "react-redux";
import Strings from '../../assets/strings';

import axios from 'axios';
import { BASE_END_POINT } from '../../AppConfig';
import ListFooter from '../../components/ListFooter';
import { Icon, Thumbnail, Button } from 'native-base'
import { selectMenu, removeItem } from '../../actions/MenuActions';
import { enableSideMenu, pop } from '../../controlls/NavigationControll'
import NetInfo from "@react-native-community/netinfo";
import Loading from "../../common/Loading"
import NetworError from '../../common/NetworError'
import NoData from '../../common/NoData'
import * as colors from '../../assets/colors'
import { arrabicFont, englishFont } from '../../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image'
import NotificationCard from '../../components/NotificationCard'
import NewAccountsCard from '../../components/NewAccountsCard';
import SurveyAccountsCard from '../../components/SurveyAccountCard'
import CollaspeAppHeader from '../../common/CollaspeAppHeader'
import LinearGradient from 'react-native-linear-gradient';
import CommanHeader from '../../common/CommanHeader'
import MapView, { Marker } from 'react-native-maps';
import { AccountsCount } from '../../actions/AccountsAction';
import AppFooter from '../../components/AppFooter'
import PurchasingAccountsCard from '../../components/PurchasingAccountsCard';
import Header from '../../common/Header'


class OperationAccounts extends Component {

    newAccountsPage = 1;
    surveyAccountsPage = 1;
    resturantAccountsPage = 1;

    noOilPage=1
    nonAgreementPage=1
    state = {
        networkError: null,
        showAlert: false,
        showModal: false,
        location: {
            latitude: 37.78825,
            longitude: -122.4324,
            latitudeDelta: 0.015,
            longitudeDelta: 0.0121,
        },
        newAccounts: [],
        surveyAccounts: [],
        resturantAccounts: [],
        newAccountsLoading: true,
        surveyAccountsLoading: true,
        resturantAccountsLoading: true,
        newAccountsRefresh: false,
        surveyAccountsRefresh: false,
        resturantAccountsRefresh: false,
        newAccounts404: false,
        surveyAccounts404: false,
        resturantAccounts404: false,
        newAccountsPages: null,
        surveyAccountsPages: null,
        returantAccountsPages: null,

        tabNumber: 1,
        
        noOilAccounts: [],
        noOiloading: true,
        noOil404: false,
        noOilRefresh: false,
        noOilPages: null,

        nonAgreementAccounts: [],
        nonAgreementLoading: true,
        nonAgreement404: false,
        nonAgreementRefresh: false,
        nonAgreementPages: null,
    }


    componentDidMount() {
        enableSideMenu(false, null)
       // this.newAccounts(false, 1)
        this.props.AccountsCount('SURVEY-ACCOUNT')

        if(this.props.data.accountType == 'Survey' ){
            this.surveyAccounts(true, 1)
            this.getNoOilsAccounts(true,1)
        }

        if(this.props.data.accountType == 'Purchasing' ){
            this.newAccounts(true, 1)
            this.getNonAgrrementAccounts(true, 1)
        }

        this.props.data.accountType == 'Resturant' && this.resturnatRegisteredAccounts(true, 1)
        console.log('AccountType',this.props.data)
        
    }

    /* Survey */

    surveyAccounts(refresh, page) {
        if (refresh) {
            this.setState({ surveyAccountsRefresh: true })
        }
        axios.get(`${BASE_END_POINT}restaurant?noOil=false&status=SURVEY-ACCOUNT&page=${page}`)
            .then(response => {
                console.log('Done   ', response.data.data)
                this.setState({
                    surveyAccounts: refresh ? response.data.data : [...this.state.surveyAccounts, ...response.data.data],
                    surveyAccountsLoading: false,
                    surveyAccountsRefresh: false,
                    surveyAccountsPages: response.data.pageCount,
                })
            })
            .catch(error => {
                console.log('Error   ', error)
                this.setState({ surveyAccounts404: true, surveyAccountsLoading: false, })
            })
    }

    getNoOilsAccounts(refresh, page) {
        if (refresh) {
            this.setState({ noOilRefresh: true })
        }
        axios.get(`${BASE_END_POINT}restaurant?status=SURVEY-ACCOUNT&noOil=true&page=${page}`)
            .then(response => {
                console.log('Done   ', response.data.data)
                this.setState({
                    noOilAccounts: refresh ? response.data.data : [...this.state.noOilAccounts, ...response.data.data],
                    noOiloading: false,
                    noOilRefresh: false,
                    noOilPages: response.data.pageCount,
                })
            })
            .catch(error => {
                console.log('Error   ', error)
                this.setState({ noOil404: true, noOiloading: false, })
            })
    }

    surveyTabs = () => {
        const {isRTL} = this.props;
        const {tabNumber} = this.state;
        console.log(tabNumber);
        return (
          <View style={{width: responsiveWidth(100),flexDirection: 'row',alignItems: 'center',}}
            >
            <Button
              onPress={() => {
                if (this.state.tabNumber != 1) {
                  this.setState({tabNumber: 1});
                }
              }}
              style={{flexDirection: isRTL ? 'row-reverse' : 'row',marginLeft: 1,flex: 1,height: responsiveHeight(8),justifyContent: 'center',alignItems: 'center',alignSelf: isRTL ? 'flex-start' : 'flex-end',backgroundColor: tabNumber == 1 ? colors.white : colors.lightGreen,}}
              >
              <Text style={{color: tabNumber == 1 ? colors.darkGray : colors.white,fontFamily: isRTL ? arrabicFont : englishFont,}}>{Strings.surveyAccounts}</Text>
            </Button>
    
            <Button
              onPress={() => {
                if (this.state.tabNumber != 2) {
                  this.setState({tabNumber: 2,});
                }
              }}
              style={{marginRight: 1,flex: 1,height: responsiveHeight(8),justifyContent: 'center',alignItems: 'center',alignSelf: isRTL ? 'flex-start' : 'flex-end',backgroundColor: tabNumber == 2 ? colors.white : colors.lightGreen,}}
              >
              <Text style={{color: tabNumber == 2 ? colors.darkGray : colors.white,fontFamily: isRTL ? arrabicFont : englishFont,}}> {Strings.noOil}</Text>
            </Button>
          </View>
        );
    }

    surveyAccountsViewPage = () => {
        const { surveyAccountsPages, surveyAccounts, surveyAccountsLoading, surveyAccounts404, surveyAccountsRefresh } = this.state
        return (
            <>
                {surveyAccounts404 ?
                    <NetworError />
                    :
                    surveyAccountsLoading ?
                        <Loading />
                        :
                        surveyAccounts.length > 0 ?
                            <FlatList
                                showsVerticalScrollIndicator={false}
                                contentContainerStyle={{paddingBottom:moderateScale(5),}}
                                data={surveyAccounts}
                                renderItem={({ item }) => <SurveyAccountsCard data={item} />}
                                onEndReachedThreshold={.5}
                                onEndReached={() => {
                                    if (this.surveyAccountsPage <= surveyAccountsPages) {
                                        this.surveyAccountsPage = this.surveyAccountsPage + 1;
                                        this.surveyAccounts(false, this.surveyAccountsPage)
                                        console.log('page  ', this.surveyAccountsPage)
                                    }
                                }}
                                refreshControl={
                                    <RefreshControl
                                        //colors={["#B7ED03",colors.darkBlue]} 
                                        refreshing={surveyAccountsRefresh}
                                        onRefresh={() => {
                                            this.surveyAccountsPage = 1
                                            this.surveyAccounts(true, 1)
                                        }}
                                    />
                                }
                            />
                            :
                            <NoData />
                }
            </>
        )
    }

    noOilAccountsViewPage = () => {
        const { noOil404,noOilAccounts,noOilPages,noOiloading,noOilRefresh } = this.state
        console.log("PAGE  2")
        return (
            <>
                {noOil404 ?
                    <NetworError />
                    :
                    noOiloading ?
                        <Loading />
                        :
                        noOilAccounts.length > 0 ?
                            <FlatList
                                showsVerticalScrollIndicator={false}
                                contentContainerStyle={{paddingBottom:moderateScale(5),}}
                                data={noOilAccounts}
                                renderItem={({ item }) => <SurveyAccountsCard data={item} />}
                                onEndReachedThreshold={.5}
                                onEndReached={() => {
                                    if (this.noOilPage <= noOilPages) {
                                        this.noOilPage = this.noOilPage + 1;
                                        this.getNoOilsAccounts(false, this.noOilPage)
                                        console.log('page  ', this.noOilPage)
                                    }
                                }}
                                refreshControl={
                                    <RefreshControl
                                        //colors={["#B7ED03",colors.darkBlue]} 
                                        refreshing={noOilRefresh}
                                        onRefresh={() => {
                                            this.noOilPage = 1
                                            this.getNoOilsAccounts(true, 1)
                                        }}
                                    />
                                }
                            />
                            :
                            <NoData />
                }
            </>
        )
    }

    surveyPage = () =>{
        const {tabNumber} = this.state
        return(
            <View style={{flex:1}} >
                {tabNumber==1?
                this.surveyAccountsViewPage()
                :
                this.noOilAccountsViewPage()
                }
            </View>
        )
    }

    /* Purchasing */

    newAccounts(refresh, page) {
        if (refresh) {
            this.setState({ newAccountsRefresh: true})
        }
        axios.get(`${BASE_END_POINT}restaurant?status=PROCEED&page=${page}`)
            .then(response => {
                console.log('Done   ', response.data.data)
                this.setState({
                    newAccounts: refresh ? response.data.data : [...this.state.newAccounts, ...response.data.data],
                    newAccountsLoading: false,
                    newAccountsRefresh: false,
                    newAccountsPage: response.data.pageCount,
                })
            })
            .catch(error => {
                console.log('Error   ', error)
                this.setState({ newAccounts404: true, newAccountsLoading: false, })
            })
    }

    getNonAgrrementAccounts(refresh, page) {
        if (refresh) {
            this.setState({ nonAgreementRefresh: true })
        }
        axios.get(`${BASE_END_POINT}restaurant?status=PURCHASING-ACCOUNT&Agreement=NON-AGREEMENT&page=${page}`)
            .then(response => {
                console.log('Done nonAgreement  ', response.data.data)
                this.setState({
                    nonAgreementAccounts: refresh ? response.data.data : [...this.state.nonAgreementAccounts, ...response.data.data],
                    nonAgreementLoading: false,
                    nonAgreementRefresh: false,
                    nonAgreementPages: response.data.pageCount,
                })
            })
            .catch(error => {
                console.log('Error   ', error)
                this.setState({ nonAgreement404: true, nonAgreementLoading: false, })
            })
    }

    purchasingTabs = () => {
        const {isRTL} = this.props;
        const {tabNumber} = this.state;
        console.log(tabNumber);
        return (
          <View style={{width: responsiveWidth(100),flexDirection: isRTL ?'row-reverse' : 'row',alignItems: 'center',}}
            >
            <Button
              onPress={() => {
                if (this.state.tabNumber != 1) {
                    this.newAccounts(true, 1)
                  this.setState({tabNumber: 1});
                }
              }}
              style={{flexDirection: isRTL ? 'row-reverse' : 'row',marginLeft: 1,flex: 1,height: responsiveHeight(8),justifyContent: 'center',alignItems: 'center',alignSelf: isRTL ? 'flex-start' : 'flex-end',backgroundColor: tabNumber == 1 ? colors.white : colors.lightGreen,}}
              >
              <Text style={{color: tabNumber == 1 ? colors.darkGray : colors.white,fontFamily: isRTL ? arrabicFont : englishFont,}}>{Strings.purchased}</Text>
            </Button>
    
            <Button
              onPress={() => {
                if (this.state.tabNumber != 2) {
                    this.getNonAgrrementAccounts(true, 1)
                  this.setState({tabNumber: 2,});
                }
              }}
              style={{marginRight: 1,flex: 1,height: responsiveHeight(8),justifyContent: 'center',alignItems: 'center',alignSelf: isRTL ? 'flex-start' : 'flex-end',backgroundColor: tabNumber == 2 ? colors.white : colors.lightGreen,}}
              >
              <Text style={{color: tabNumber == 2 ? colors.darkGray : colors.white,fontFamily: isRTL ? arrabicFont : englishFont,}}> {Strings.nonAgreement}</Text>
            </Button>
          </View>
        );
    }

    newAccountsViewPage = () => {
        const { newAccountsPages, newAccounts, newAccountsLoading, newAccounts404, newAccountsRefresh } = this.state
        return (
            <>
               
                {newAccounts404 ?
                    <NetworError />
                    :
                    newAccountsLoading ?
                        <Loading />
                        :
                        newAccounts.length > 0 ?
                            <FlatList
                                showsVerticalScrollIndicator={false}
                                contentContainerStyle={{paddingBottom:moderateScale(5),}}
                                data={newAccounts}
                                renderItem={({ item }) => <NewAccountsCard data={item} />}
                                onEndReachedThreshold={.5}
                                onEndReached={() => {
                                    if (this.newAccountsPage <= newAccountsPages) {
                                        this.newAccountsPage = this.newAccountsPage + 1;
                                        this.newAccounts(false, this.newAccountsPage)
                                        console.log('page  ', this.newAccountsPage)
                                    }
                                }}
                                refreshControl={
                                    <RefreshControl
                                        //colors={["#B7ED03",colors.darkBlue]} 
                                        refreshing={newAccountsRefresh}
                                        onRefresh={() => {
                                            this.newAccountsPage = 1
                                            this.newAccounts(true, 1)
                                        }}
                                    />
                                }
                            />
                            :
                            <NoData />
                }
            </>
        )
    }

    nonAgreementAccountsViewPage = () => {
        const { nonAgreement404,nonAgreementLoading,nonAgreementPages,nonAgreementAccounts,nonAgreementRefresh, } = this.state
        return (
            <>
               
                {nonAgreement404 ?
                    <NetworError />
                    :
                    nonAgreementLoading ?
                        <Loading />
                        :
                        nonAgreementAccounts.length > 0 ?
                            <FlatList
                                showsVerticalScrollIndicator={false}
                                contentContainerStyle={{paddingBottom:moderateScale(5),}}
                                data={nonAgreementAccounts}
                                renderItem={({ item }) => <SurveyAccountsCard data={item} />}
                                onEndReachedThreshold={.5}
                                onEndReached={() => {
                                    if (this.nonAgreementPage <= nonAgreementPages) {
                                        this.nonAgreementPage = this.nonAgreementPage + 1;
                                        this.getNonAgrrementAccounts(false, this.nonAgreementPage)
                                        console.log('page  ', this.nonAgreementPage)
                                    }
                                }}
                                refreshControl={
                                    <RefreshControl
                                        //colors={["#B7ED03",colors.darkBlue]} 
                                        refreshing={nonAgreementRefresh}
                                        onRefresh={() => {
                                            this.nonAgreementPage = 1
                                            this.getNonAgrrementAccounts(true, 1)
                                        }}
                                    />
                                }
                            />
                            :
                            <NoData />
                }
            </>
        )
    }

    purchasingPage = () =>{
        const {tabNumber} = this.state
        return(
            <View style={{flex:1}} >
                {tabNumber==1?
                this.newAccountsViewPage()
                :
                this.nonAgreementAccountsViewPage()
                }
            </View>
        )
    }



    

    resturnatRegisteredAccounts(refresh, page) {
        if (refresh) {
            this.setState({ resturantAccountsRefresh: true })
        }
        axios.get(`${BASE_END_POINT}restaurant?status=RESTURANT-ACCOUNT&page=${page}`)
            .then(response => {
                console.log('Done   ', response.data.data)
                this.setState({
                    resturantAccounts: refresh ? response.data.data : [...this.state.resturantAccounts, ...response.data.data],
                    resturantAccountsLoading: false,
                    resturantAccountsRefresh: false,
                    resturantAccountsPages: response.data.pageCount,
                })
            })
            .catch(error => {
                console.log('Error   ', error)
                this.setState({ resturantAccounts404: true, resturantAccountsLoading: false, })
            })
    }



    renderFooter = () => {
        return (
            this.state.loading ?
                <View style={{ alignSelf: 'center', margin: moderateScale(5) }}>
                    <ListFooter />
                </View>
                : null
        )
    }


    tabs = () => {
        const { categoryName, isRTL, accountsCount } = this.props;
        const { tabNumber } = this.state;
        return (
            <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', width: responsiveWidth(100), flexDirection: 'row', alignItems: 'center' }} >
                <Button onPress={() => {
                    [
                        this.newAccounts(true, 1),
                        tabNumber != 1 && this.setState({ tabNumber: 1 })
                    ]
                }} style={{ flexDirection: isRTL ? 'row-reverse' : 'row', marginLeft: 1, flex: 1, height: responsiveHeight(8), justifyContent: 'center', alignItems: 'center', alignSelf: isRTL ? 'flex-start' : 'flex-end', backgroundColor: tabNumber == 1 ? colors.white : colors.darkGreen }}>
                    <Text style={{ color: tabNumber == 1 ? colors.darkGray : colors.white, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.newAccounts}</Text>

                </Button>

                <Button onPress={() => {
                    [
                        this.surveyAccounts(true, 1),
                        tabNumber != 2 && this.setState({ tabNumber: 2 })

                    ]
                }} style={{ marginRight: 1, flex: 1, height: responsiveHeight(8), justifyContent: 'center', alignItems: 'center', alignSelf: isRTL ? 'flex-start' : 'flex-end', backgroundColor: tabNumber == 2 ? colors.white : colors.darkGreen }}>
                    <Text style={{ color: tabNumber == 2 ? colors.darkGray : colors.white, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.surveyAccounts}</Text>
                    <View style={{ marginHorizontal: moderateScale(2), justifyContent: 'center', alignItems: 'center', width: 18, height: 18, borderRadius: 9, backgroundColor: 'red' }}>
                        <Text style={{ color: colors.white, fontSize: responsiveFontSize(4) }} >{accountsCount}</Text>
                    </View>
                </Button>

            </View>
        )
    }



    

    


    
    resturantRegisteredAccountsViewPage = () => {
        const { resturantAccountsPages, resturantAccounts, resturantAccountsLoading, resturantAccounts404, resturantAccountsRefresh } = this.state
        return (
            <>
                {resturantAccounts404 ?
                    <NetworError />
                    :
                    resturantAccountsLoading ?
                        <Loading />
                        :
                        resturantAccounts.length > 0 ?
                            <FlatList
                                showsVerticalScrollIndicator={false}
                                contentContainerStyle={{paddingBottom:moderateScale(5),}}
                                data={resturantAccounts}
                                renderItem={({ item }) => <SurveyAccountsCard data={item} />}
                                onEndReachedThreshold={.5}
                                onEndReached={() => {
                                    if (this.resturantAccountsPage <= resturantAccountsPages) {
                                        this.resturantAccountsPage = this.resturantAccountsPage + 1;
                                        this.resturnatRegisteredAccounts(false, this.resturantAccountsPage)
                                        console.log('page  ', this.resturantAccountsPage)
                                    }
                                }}
                                refreshControl={
                                    <RefreshControl
                                        //colors={["#B7ED03",colors.darkBlue]} 
                                        refreshing={resturantAccountsRefresh}
                                        onRefresh={() => {
                                            this.resturantsAccountsPage = 1
                                            this.resturnatRegisteredAccounts(true, 1)
                                        }}
                                    />
                                }
                            />
                            :
                            <NoData />
                }
            </>
        )
    }

    render() {
        const { categoryName, isRTL, data } = this.props;
        const { tabNumber } = this.state;
        return (
            <LinearGradient
                colors={[colors.white, colors.white, colors.white]}
                style={{ flex: 1 }}
            >

                <Header title={data.accountType ? data.accountType == 'Purchasing' ? Strings.purchasingOutlets : data.accountType == 'Survey' ? Strings.surveyOutlets : Strings.registeredOutlets:Strings.registeredOutlets} />


                {data.accountType == 'Survey' && this.surveyTabs()}
                {data.accountType == 'Purchasing' && this.purchasingTabs()}

                <View style={{flex:1, height: responsiveHeight(73), backgroundColor: colors.white,  marginTop: moderateScale(0), width: responsiveWidth(100), marginBottom:responsiveHeight(10) }} >
                    {data.accountType == 'Survey' && this.surveyPage()}
                    {data.accountType == 'Purchasing' && this.purchasingPage()}
                    {data.accountType == 'Resturant' && this.resturantRegisteredAccountsViewPage()}

                </View>

                <AppFooter />
            </LinearGradient>
        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    barColor: state.lang.color,
    currentUser: state.auth.currentUser,
    accountsCount: state.accounts.accountsCount
})

const mapDispatchToProps = {
    removeItem,
    AccountsCount
}

export default connect(mapStateToProps, mapDispatchToProps)(OperationAccounts);
