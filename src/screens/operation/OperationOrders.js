import React, { Component } from 'react';
import { View, RefreshControl, Alert, Modal, ScrollView, FlatList, Text, TouchableOpacity, Platform } from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../../utils/responsiveDimensions';
import { connect } from "react-redux";
import Strings from '../../assets/strings';

import axios from 'axios';
import { BASE_END_POINT } from '../../AppConfig';
import ListFooter from '../../components/ListFooter';
import { Icon, Thumbnail, Button } from 'native-base'
import { selectMenu, removeItem } from '../../actions/MenuActions';
import { enableSideMenu, pop } from '../../controlls/NavigationControll'
import NetInfo from "@react-native-community/netinfo";
import Loading from "../../common/Loading"
import NetworError from '../../common/NetworError'
import NoData from '../../common/NoData'
import * as colors from '../../assets/colors'
import { arrabicFont, englishFont } from '../../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image'
import NotificationCard from '../../components/NotificationCard'
import NewOrderCard from '../../components/NewOrderCard';
import AssignedOrderCard from '../../components/AssignedOrderCard'
import CollaspeAppHeader from '../../common/CollaspeAppHeader'
import LinearGradient from 'react-native-linear-gradient';
import CommanHeader from '../../common/CommanHeader'
import MapView, { Marker } from 'react-native-maps';
import io from 'socket.io-client';
import AppFooter from '../../components/AppFooter';
import { OrdersCount } from '../../actions/OrdersActions';
import Header from '../../common/Header'

class OperationOrders extends Component {

    socket = null;
    newOrdersPage = 1;
    assignedOrdersPage = 1;
    onProgressOrdersPage = 1;
    collectedOrdersPage = 1;
    state = {
        networkError: null,
        tabNumber: 1,
        //mew orders
        newOrders: [],
        newOrdersLoading: true,
        newOrdersRefresh: false,
        newOrders404: false,
        newOrdersPages: null,
        newOrderType:null,
        //assigned orders
        assignedOrders: [],
        assignedOrdersLoading: true,
        assignedOrdersRefresh: false,
        assignedOrders404: false,
        assignedOrdersPages: null,
        //onProgress orders
        onProgressOrders: [],
        onProgressOrdersLoading: true,
        onProgressOrdersRefresh: false,
        onProgressOrders404: false,
        onProgressOrdersPages: null,
        //collected orders
        collectedOrders: [],
        collectedOrdersLoading: true,
        collectedOrdersRefresh: false,
        collectedOrders404: false,
        collectedOrdersPages: null,
        type:'',

    }



    componentDidMount() {
        enableSideMenu(false, null)
        //connect to sicket
        this.socket = io("https://api.zeitcoapp.com/order", { query: `id=${this.props.currentUser.user.id}` });
        this.getNewOrders(false, 1)
        this.getAssignedOrders(false, 1)
        this.getOnProgressOrders(false,1)
        this.getCollectedOrders(false, 1)
        this.props.OrdersCount(null, 'PENDING')
        //listen to order data when emit and send it 
        this.socket.on('newOrder', (data) => {
            console.log("newOrder  ", data.order[0]); // true 
            this.setState({ newOrders: [data.order[0], ...this.state.newOrders] })
            //Alert.alert('RECIVE NEW ORDER')   
        });
    }


    getNewOrders = (refresh, page,type) => {
        if (refresh) {
            this.setState({ newOrdersRefresh: true })
        }
        var link
        if(type){
            link = `${BASE_END_POINT}orders?all=true&status=PENDING&type=${type}&page=${page}`
        }else{
            link = `${BASE_END_POINT}orders?all=true&status=PENDING&page=${page}`
        }
        axios.get(link)
            .then(response => {
                console.log('Done   ', response.data.data)
                //Alert.alert('Done')
                this.setState({
                    newOrders: refresh ? response.data.data : [...this.state.newOrders, ...response.data.data],
                    newOrdersLoading: false,
                    newOrdersRefresh: false,
                    newOrdersLoading: false,
                    newOrdersPages: response.data.pageCount,
                })
            })
            .catch(error => {
                //Alert.alert('error  ',error)
                console.log('Error   ', error.response)
                this.setState({ newOrders404: true, newOrdersLoading: false, })
            })
    }

    getAssignedOrders = (refresh, page) => {
        if (refresh) {
            this.setState({ assignedOrdersRefresh: true })
        }
        axios.get(`${BASE_END_POINT}orders?all=true&status=ASSIGNED&page=${page}`)
            .then(response => {
                console.log('ASSIGN ORDERS   ', response.data.data)
                //Alert.alert('ASS Done')
                this.setState({
                    assignedOrders: refresh ? response.data.data : [...this.state.assignedOrders, ...response.data.data],
                    assignedOrdersLoading: false,
                    assignedOrders404: false,
                    assignedOrdersRefresh: false,
                    assignedOrdersPages: response.data.pageCount,
                })
            })
            .catch(error => {
                //Alert.alert('error  ',error)
                console.log('Error ASSIGN   ', error)
                this.setState({ assignedOrders404: true, assignedOrdersLoading: false, })
            })
    }


    getOnProgressOrders = (refresh, page) => {
        if (refresh) {
            this.setState({ onProgressOrdersRefresh: true })
        }
        axios.get(`${BASE_END_POINT}orders?all=true&status=ONPROGRESS&page=${page}`)
            .then(response => {
                console.log('ONPROGRESS ORDERS   ', response.data.data)
                //Alert.alert('ASS Done')
                this.setState({
                    onProgressOrders: refresh ? response.data.data : [...this.state.onProgressOrders, ...response.data.data],
                    onProgressOrdersLoading: false,
                    onProgressOrders404: false,
                    onProgressOrdersRefresh: false,
                    onProgressOrdersPages: response.data.pageCount,
                })
            })
            .catch(error => {
                //Alert.alert('error  ',error)
                console.log('Error ONPROGRESS   ', error)
                this.setState({ onProgressOrders404: true, onProgressOrdersLoading: false, })
            })
    }

    getCollectedOrders = (refresh, page) => {
        if (refresh) {
            this.setState({ collectedOrdersRefresh: true })
        }
        axios.get(`${BASE_END_POINT}orders?all=true&status=COLLECTED&page=${page}`)
            .then(response => {
                console.log('ASSIGN ORDERS   ', response.data.data)
                //Alert.alert('Coll Done')
                this.setState({
                    collectedOrders: refresh ? response.data.data : [...this.state.collectedOrders, ...response.data.data],
                    collectedOrdersLoading: false,
                    collectedOrders404: false,
                    collectedOrdersRefresh: false,
                    collectedOrdersPages: response.data.pageCount,
                })
            })
            .catch(error => {
                //Alert.alert('error  ',error)
                console.log('Error ASSIGN   ', error)
                this.setState({ collectedOrders404: true, collectedOrdersLoading: false, })
            })
    }


    tabs = () => {
        const { categoryName, isRTL, ordersCount } = this.props;
        const { tabNumber } = this.state;
        return (
            <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', width: responsiveWidth(100), alignItems: 'center' }} >
                <Button onPress={() => {
                    if (tabNumber != 1) {
                        this.setState({ tabNumber: 1 })
                        this.newOrdersPage = 1
                        this.getNewOrders(true, 1,this.state.newOrderType)
                    }

                }} style={{ flexDirection: isRTL ? 'row-reverse' : 'row', marginLeft: 1, flex: 1, height: responsiveHeight(8), justifyContent: 'center', alignItems: 'center', alignSelf: isRTL ? 'flex-start' : 'flex-end', backgroundColor: tabNumber == 1 ? colors.white : colors.darkGreen }}>
                    <Text style={{ color: tabNumber == 1 ? colors.darkGray : colors.white, fontFamily: isRTL ? arrabicFont : englishFont , textAlign:'center'}}>{Strings.newOrders}</Text>
                    <View style={{ marginHorizontal: moderateScale(2), justifyContent: 'center', alignItems: 'center', width: 18, height: 18, borderRadius: 9, backgroundColor: 'red' }}>
                        <Text style={{ color: colors.white, fontSize: responsiveFontSize(4) }} >{ordersCount}</Text>
                    </View>
                </Button>

                <Button onPress={() => {
                    if (tabNumber != 2) {
                        this.setState({ tabNumber: 2 })
                        this.assignedOrdersPage = 1
                        this.getAssignedOrders(true, 1)
                    }
                }} style={{ marginRight: 1, flex: 1, height: responsiveHeight(8), justifyContent: 'center', alignItems: 'center', alignSelf: isRTL ? 'flex-start' : 'flex-end', backgroundColor: tabNumber == 2 ? colors.white : colors.darkGreen }}>
                    <Text style={{ color: tabNumber == 2 ? colors.darkGray : colors.white, fontFamily: isRTL ? arrabicFont : englishFont , textAlign:'center'}}>{Strings.assignedOrders}</Text>
                </Button>

                <Button onPress={() => {
                    if (tabNumber != 3) {
                        this.setState({ tabNumber: 3 })
                        this.onProgressOrdersPage = 1
                        this.getOnProgressOrders(true, 1)
                    }
                }} style={{ marginRight: 1, flex: 1, height: responsiveHeight(8), justifyContent: 'center', alignItems: 'center', alignSelf: isRTL ? 'flex-start' : 'flex-end', backgroundColor: tabNumber == 3 ? colors.white : colors.darkGreen }}>
                    <Text style={{ color: tabNumber == 3 ? colors.darkGray : colors.white, fontFamily: isRTL ? arrabicFont : englishFont, textAlign:'center' }}>{Strings.onProgressOrders}</Text>
                </Button>

                <Button onPress={() => {
                    if (tabNumber != 4) {
                        this.setState({ tabNumber: 4 })
                        this.collectedOrdersPage = 1
                        this.getCollectedOrders(true, 1)
                    }
                }} style={{ marginRight: 1, flex: 1, height: responsiveHeight(8), justifyContent: 'center', alignItems: 'center', alignSelf: isRTL ? 'flex-start' : 'flex-end', backgroundColor: tabNumber == 4 ? colors.white : colors.darkGreen }}>
                    <Text style={{ color: tabNumber == 4 ? colors.darkGray : colors.white, fontFamily: isRTL ? arrabicFont : englishFont, textAlign:'center' }}>{Strings.collectedOrders}</Text>
                </Button>

            </View>
        )
    }


    newOrderstsTab = () => {
        const { newOrders,newOrderType, newOrdersPages, newOrders404, newOrdersLoading, newOrdersRefresh } = this.state
        return (
        <View style={{flex:1,}} >
            <View style={{flexDirection:'row',alignSelf:'center',marginVertical:moderateScale(10)}}>
                <TouchableOpacity 
                onPress={()=>{
                    this.newOrdersPage=1
                    this.setState({newOrderType:'MANUAL'})
                    this.getNewOrders(true,1,'MANUAL')
                }}
                style={{borderColor:colors.darkGreen,borderWidth:newOrderType=='MANUAL'?0:1, marginRight:moderateScale(7), width:70,height:70,borderRadius:35,backgroundColor:newOrderType=='MANUAL'?colors.darkGreen:'white',justifyContent:'center',alignItems:'center'}} >
                    <Text style={{color:newOrderType=='MANUAL'?'white':colors.darkGreen,fontFamily:englishFont,fontSize:responsiveFontSize(8)}} >M.O</Text>
                </TouchableOpacity>
                <TouchableOpacity
                onPress={()=>{
                    this.newOrdersPage=1
                    this.setState({newOrderType:'OTOMATIC'})
                    this.getNewOrders(true,1,'OTOMATIC')
                }}
                 style={{width:70,height:70,borderRadius:35,borderColor:colors.darkGreen,borderWidth:newOrderType=='OTOMATIC'?0:1,justifyContent:'center',alignItems:'center',backgroundColor:newOrderType=='OTOMATIC'?colors.darkGreen:'white',}} >
                    <Text style={{color:newOrderType=='OTOMATIC'?'white':colors.darkGreen,fontFamily:englishFont,fontSize:responsiveFontSize(8)}} >O.O</Text>
                </TouchableOpacity>
            </View> 
            {
            newOrders404 ?
                <NetworError />
                :
                newOrdersLoading ?
                    <Loading />
                    :
                    newOrders.length > 0 ?
                        <FlatList
                            showsVerticalScrollIndicator={false}
                            style={{ marginBottom: moderateScale(10) }}
                            data={newOrders}
                            renderItem={({ item }) => <NewOrderCard data={item} />}
                            keyExtractor={item => item.id}
                            onEndReachedThreshold={.5}
                            //ListFooterComponent={()=>notificationsLoad&&<ListFooter />}
                            onEndReached={() => {
                                if (this.newOrdersPage <= newOrdersPages) {
                                    this.newOrdersPage = this.newOrdersPage + 1;
                                    this.getNewOrders(false, this.newOrdersPage,this.state.newOrderType)
                                    console.log('page  ', this.page)
                                }
                            }}
                            refreshControl={
                                <RefreshControl
                                    //colors={["#B7ED03",colors.darkBlue]} 
                                    refreshing={newOrdersRefresh}
                                    onRefresh={() => {
                                        this.newOrdersPage = 1
                                        this.getNewOrders(true, 1,this.state.newOrderType)
                                    }}
                                />
                            }

                        />
                        :
                        <NoData />
            }
        </View>   
        )
    }

    assignedOrdersTab = () => {
        const { assignedOrders, assignedOrders404, assignedOrdersLoading, assignedOrdersRefresh, assignedOrdersPages } = this.state
        return (
            assignedOrders404 ?
                <NetworError />
                :
                assignedOrdersLoading ?
                    <Loading />
                    :
                    assignedOrders.length > 0 ?
                        <FlatList
                            showsVerticalScrollIndicator={false}
                            style={{ marginBottom: moderateScale(10) }}
                            data={assignedOrders}
                            renderItem={({ item }) => <AssignedOrderCard data={item} />}
                            onEndReachedThreshold={.5}
                            //ListFooterComponent={()=>notificationsLoad&&<ListFooter />}
                            onEndReached={() => {
                                if (this.assignedOrdersPage <= assignedOrdersPages) {
                                    this.assignedOrdersPage = this.assignedOrdersPage + 1;
                                    this.getAssignedOrders(false, this.assignedOrdersPage)
                                    console.log('page  ', this.assignedOrdersPage)
                                }
                            }}
                            refreshControl={
                                <RefreshControl
                                    //colors={["#B7ED03",colors.darkBlue]} 
                                    refreshing={assignedOrdersRefresh}
                                    onRefresh={() => {
                                        this.assignedOrdersPage = 1
                                        this.getAssignedOrders(true, 1)
                                    }}
                                />
                            }

                        />
                        :
                        <NoData />
        )
    }



    onProgressOrdersTab = () => {
        const { onProgressOrders, onProgressOrders404, onProgressOrdersLoading, onProgressOrdersRefresh, onProgressOrdersPages } = this.state
        return (
            onProgressOrders404 ?
                <NetworError />
                :
                onProgressOrdersLoading ?
                    <Loading />
                    :
                    onProgressOrders.length > 0 ?
                        <FlatList
                            showsVerticalScrollIndicator={false}
                            style={{ marginBottom: moderateScale(10) }}
                            data={onProgressOrders}
                            renderItem={({ item }) => <AssignedOrderCard onProgress data={item} />}
                            onEndReachedThreshold={.5}
                            //ListFooterComponent={()=>notificationsLoad&&<ListFooter />}
                            onEndReached={() => {
                                if (this.onProgressOrdersPage <= onProgressOrdersPages) {
                                    this.onProgressOrdersPage = this.onProgressOrdersPage + 1;
                                    this.getOnProgressOrders(false, this.onProgressOrdersPage)
                                    console.log('page  ', this.onProgressOrdersPage)
                                }
                            }}
                            refreshControl={
                                <RefreshControl
                                    //colors={["#B7ED03",colors.darkBlue]} 
                                    refreshing={onProgressOrdersRefresh}
                                    onRefresh={() => {
                                        this.onProgressOrdersPage = 1
                                        this.getAssignedOrders(true, 1)
                                    }}
                                />
                            }

                        />
                        :
                        <NoData />
        )
    }

    collectedOrdersTab = () => {
        const { collectedOrders, collectedOrdersLoading, collectedOrdersRefresh, collectedOrders404, collectedOrdersPages } = this.state
        return (
            collectedOrders404 ?
                <NetworError />
                :
                collectedOrdersLoading ?
                    <Loading />
                    :
                    collectedOrders.length > 0 ?
                        <FlatList
                            showsVerticalScrollIndicator={false}
                            style={{ marginBottom: moderateScale(10) }}
                            data={collectedOrders}
                            renderItem={({ item }) => <AssignedOrderCard collected data={item} />}
                            onEndReachedThreshold={.5}
                            //ListFooterComponent={()=>notificationsLoad&&<ListFooter />}
                            onEndReached={() => {
                                if (this.collectedOrdersPage <= collectedOrdersPages) {
                                    this.collectedOrdersPage = this.collectedOrdersPage + 1;
                                    this.getCollectedOrders(false, this.collectedOrdersPage)
                                    console.log('page  ', this.collectedOrdersPage)
                                }
                            }}
                            refreshControl={
                                <RefreshControl
                                    //colors={["#B7ED03",colors.darkBlue]} 
                                    refreshing={collectedOrdersRefresh}
                                    onRefresh={() => {
                                        this.collectedOrdersPage = 1
                                        this.getCollectedOrders(true, 1)
                                    }}
                                />
                            }

                        />
                        :
                        <NoData />
        )
    }

    render() {
        const { categoryName, isRTL } = this.props;
        const { tabNumber } = this.state;
        return (
            <LinearGradient
                colors={[colors.white, colors.white, colors.white]}
                style={{ flex: 1 }}
            >
                
                <Header  title={Strings.orders} />

                <View style={{ flex:1, height: responsiveHeight(75), backgroundColor: colors.white, marginTop: moderateScale(0), width: responsiveWidth(100),marginBottom:responsiveHeight(7) }} >
                    {this.tabs()}
                    {tabNumber == 1 && this.newOrderstsTab()}
                    {tabNumber == 2 && this.assignedOrdersTab()}
                    {tabNumber == 3 && this.onProgressOrdersTab()}
                    {tabNumber == 4 && this.collectedOrdersTab()}

                </View>
                <AppFooter />
            </LinearGradient>
        );
    }
}

const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    barColor: state.lang.color,
    currentUser: state.auth.currentUser,
    ordersCount: state.orders.ordersCount,

})

const mapDispatchToProps = {
    removeItem,
    OrdersCount,

}

export default connect(mapStateToProps, mapDispatchToProps)(OperationOrders);
