import React, { Component } from 'react';
import { View, RefreshControl, ScrollView, FlatList, Text, TouchableOpacity } from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../../utils/responsiveDimensions';
import { connect } from "react-redux";
import Strings from '../../assets/strings';

import axios from 'axios';
import { BASE_END_POINT } from '../../AppConfig';
import ListFooter from '../../components/ListFooter';
import { Icon, Thumbnail, Button } from 'native-base'
import { selectMenu, removeItem } from '../../actions/MenuActions';
import { enableSideMenu, pop } from '../../controlls/NavigationControll'
import NetInfo from "@react-native-community/netinfo";
import Loading from "../../common/Loading"
import NetworError from '../../common/NetworError'
import NoData from '../../common/NoData'
import * as colors from '../../assets/colors'
import { arrabicFont, englishFont } from '../../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image'
import NotificationCard from '../../components/NotificationCard'
import OperationAccountCard from '../../components/OperationAccountCard';
import CollaspeAppHeader from '../../common/CollaspeAppHeader'
import LinearGradient from 'react-native-linear-gradient';
import CommanHeader from '../../common/CommanHeader'
import AppFooter from '../../components/AppFooter'
import Header from '../../common/Header'


class OperationActiveAccounts extends Component {

    page = 1;
    page = 2;
    page = 3;
    state = {
        tabNumber: 1,
        networkError: null,
        accountList: [],
        accountListLoading: true,
        accountListRefresh: false,
        accountList404: false,
        pages: null,

        networkError2: null,
        accountList2: [],
        accountListLoading2: true,
        accountListRefresh2: false,
        accountList4042: false,
        pages2: null,

        networkError3: null,
        accountList3: [],
        accountListLoading3: true,
        accountListRefresh3: false,
        accountList4043: false,
        pages3: null,
    }



    componentDidMount() {
        enableSideMenu(false, null)
        this.approvedAccountList(false, 1)
        this.unCommittedAccountList(false, 1)
        this.dislinedAccountList(false, 1)
    }

    approvedAccountList(refresh, page) {
        if (refresh) {
            this.setState({ accountListRefresh: true })
        }
        axios.get(`${BASE_END_POINT}restaurant?status=APPROVED&page=${page}`)
            .then(response => {
                console.log('Done   ', response.data.data)
                this.setState({
                    accountList: refresh ? response.data.data : [...this.state.accountList, ...response.data.data],
                    accountListLoading: false,
                    accountListRefresh: false,
                    pages: response.data.pageCount,
                })
            })
            .catch(error => {
                console.log('Error   ', error)
                this.setState({ accountList404: true, accountListLoading: false, })
            })
    }

    unCommittedAccountList(refresh, page) {
        if (refresh) {
            this.setState({ accountListRefresh2: true })
        }
        axios.get(`${BASE_END_POINT}restaurant?uncommitted=true&page=${page}`)
            .then(response => {
                console.log('Done   ', response.data.data)
                this.setState({
                    accountList2: refresh ? response.data.data : [...this.state.accountList2, ...response.data.data],
                    accountListLoading2: false,
                    accountListRefresh2: false,
                    pages2: response.data.pageCount,
                })
            })
            .catch(error => {
                console.log('Error   ', error)
                this.setState({ accountList4042: true, accountListLoading2: false, })
            })
    }

    dislinedAccountList(refresh, page) {
        if (refresh) {
            this.setState({ accountListRefresh3: true })
        }
        axios.get(`${BASE_END_POINT}restaurant?decline=true&page=${page}`)
            .then(response => {
                console.log('Done   ', response.data.data)
                this.setState({
                    accountList3: refresh ? response.data.data : [...this.state.accountList2, ...response.data.data],
                    accountListLoading3: false,
                    accountListRefresh3: false,
                    pages3: response.data.pageCount,
                })
            })
            .catch(error => {
                console.log('Error   ', error)
                this.setState({ accountList4043: true, accountListLoading3: false, })
            })
    }

    renderFooter = () => {
        return (
            this.state.loading ?
                <View style={{ alignSelf: 'center', margin: moderateScale(5) }}>
                    <ListFooter />
                </View>
                : null
        )
    }

    tabs = () => {
        const {isRTL} = this.props;
        const {tabNumber} = this.state;
        console.log(tabNumber);
        return (
          <View style={{width: responsiveWidth(100),flexDirection:isRTL?'row-reverse': 'row',alignItems: 'center',}}
            >
            <Button
              onPress={() => {
                if(tabNumber!=1){
                    this.setState({tabNumber:1})
                    this.page1=1
                    this.activeOutletsPage(true,1)
                }
              }}
              style={{flexDirection: isRTL ? 'row-reverse' : 'row',marginLeft: 1,flex: 1,height: responsiveHeight(8),justifyContent: 'center',alignItems: 'center',alignSelf: isRTL ? 'flex-start' : 'flex-end',backgroundColor: tabNumber == 1 ? colors.white : colors.lightGreen,}}
              >
              <Text style={{textAlign:'center',color: tabNumber == 1 ? colors.darkGray : colors.white,fontFamily: isRTL ? arrabicFont : englishFont,}}>{Strings.activeOutlets}</Text>
            </Button>
    
            <Button
              onPress={() => {
                if(tabNumber!=2){
                    this.setState({tabNumber:2})
                    this.page2=1
                    this.unCommittedAccountList(true,1)
                }
              }}
              style={{marginRight: 1,flex: 1,height: responsiveHeight(8),justifyContent: 'center',alignItems: 'center',alignSelf: isRTL ? 'flex-start' : 'flex-end',backgroundColor: tabNumber == 2 ? colors.white : colors.lightGreen,}}
              >
              <Text style={{textAlign:'center',color: tabNumber == 2 ? colors.darkGray : colors.white,fontFamily: isRTL ? arrabicFont : englishFont,}}> {Strings.uncommittedOutlets}</Text>
            </Button>

            <Button
              onPress={() => {
                if(tabNumber!=3){
                    this.setState({tabNumber:3})
                }
              }}
              style={{flexDirection: isRTL ? 'row-reverse' : 'row',marginLeft: 1,flex: 1,height: responsiveHeight(8),justifyContent: 'center',alignItems: 'center',alignSelf: isRTL ? 'flex-start' : 'flex-end',backgroundColor: tabNumber == 3 ? colors.white : colors.lightGreen,}}
              >
              <Text style={{textAlign:'center', color: tabNumber == 3 ? colors.darkGray : colors.white,fontFamily: isRTL ? arrabicFont : englishFont,}}>{Strings.declinedOutlets}</Text>
            </Button>

          </View>
        );
    }

    activeOutletsPage = () => {
        const {isRTL} = this.props;
        const { pages, accountList, accountListLoading, accountList404, accountListRefresh } = this.state
        return (
            <View style={{ flex: 1, height: responsiveHeight(73), backgroundColor: colors.white, borderTopRightRadius: moderateScale(20), borderTopLeftRadius: moderateScale(20), marginTop: moderateScale(0), width: responsiveWidth(100), marginBottom: responsiveHeight(10) }} >
            {accountList404 ?
                <NetworError />
                :
                accountListLoading ?
                    <Loading />
                    :
                    accountList.length > 0 ?

                        <FlatList
                            showsVerticalScrollIndicator={false}
                            contentContainerStyle={{ paddingBottom: moderateScale(5) }}
                            data={accountList}
                            renderItem={({ item }) => <OperationAccountCard data={item} />}
                            onEndReachedThreshold={.5}
                            onEndReached={() => {
                                if (this.page <= pages) {
                                    this.page = this.page + 1;
                                    this.approvedAccountList(false, this.page)
                                    console.log('page  ', this.page)
                                }
                            }}
                            refreshControl={
                                <RefreshControl
                                    //colors={["#B7ED03",colors.darkBlue]} 
                                    refreshing={accountListRefresh}
                                    onRefresh={() => {
                                        this.page = 1
                                        this.approvedAccountList(true, 1)
                                    }}
                                />
                            }
                        />
                        :
                        <NoData />
            }
        </View>
        );
    }

    unCommittedOutletsPage = () => {
        const {isRTL} = this.props;
        const { pages2, accountList2, accountListLoading2, accountList4042, accountListRefresh2 } = this.state
        return (
            <View style={{ flex: 1, height: responsiveHeight(73), backgroundColor: colors.white, borderTopRightRadius: moderateScale(20), borderTopLeftRadius: moderateScale(20), marginTop: moderateScale(0), width: responsiveWidth(100), marginBottom: responsiveHeight(10) }} >
            {accountList4042 ?
                <NetworError />
                :
                accountListLoading2 ?
                    <Loading />
                    :
                    accountList2.length > 0 ?

                        <FlatList
                            showsVerticalScrollIndicator={false}
                            contentContainerStyle={{ paddingBottom: moderateScale(5) }}
                            data={accountList2}
                            renderItem={({ item }) => <OperationAccountCard data={item} />}
                            onEndReachedThreshold={.5}
                            onEndReached={() => {
                                if (this.page2 <= pages2) {
                                    this.page2 = this.page2 + 1;
                                    this.unCommittedAccountList(false, this.page2)
                                    console.log('page  ', this.page2)
                                }
                            }}
                            refreshControl={
                                <RefreshControl
                                    //colors={["#B7ED03",colors.darkBlue]} 
                                    refreshing={accountListRefresh2}
                                    onRefresh={() => {
                                        this.page2 = 1
                                        this.unCommittedAccountList(true, 1)
                                    }}
                                />
                            }
                        />
                        :
                        <NoData />
            }
        </View>
        );
    }

    declinedOutletsPage = () => {
        const {isRTL} = this.props;
        const { pages3, accountList3, accountListLoading3, accountList4043, accountListRefresh3 } = this.state
        return (
            <View style={{ flex: 1, height: responsiveHeight(73), backgroundColor: colors.white, borderTopRightRadius: moderateScale(20), borderTopLeftRadius: moderateScale(20), marginTop: moderateScale(0), width: responsiveWidth(100), marginBottom: responsiveHeight(10) }} >
            {accountList4043 ?
                <NetworError />
                :
                accountListLoading3 ?
                    <Loading />
                    :
                    accountList3.length > 0 ?

                        <FlatList
                            showsVerticalScrollIndicator={false}
                            contentContainerStyle={{ paddingBottom: moderateScale(5) }}
                            data={accountList3}
                            renderItem={({ item }) => <OperationAccountCard reassign data={item} />}
                            onEndReachedThreshold={.5}
                            onEndReached={() => {
                                if (this.page3 <= pages3) {
                                    this.page3 = this.page3 + 1;
                                    this.dislinedAccountList(false, this.page3)
                                    console.log('page  ', this.page3)
                                }
                            }}
                            refreshControl={
                                <RefreshControl
                                    //colors={["#B7ED03",colors.darkBlue]} 
                                    refreshing={accountListRefresh3}
                                    onRefresh={() => {
                                        this.page3 = 1
                                        this.dislinedAccountList(true, 1)
                                    }}
                                />
                            }
                        />
                        :
                        <NoData />
            }
        </View>
        );
    }



    render() {
        const { categoryName, isRTL } = this.props;
        const { tabNumber } = this.state
        return (
            <LinearGradient
                colors={[colors.white, colors.white, colors.white]}
                style={{ flex: 1 }}
            >
                <Header title={Strings.viewAccounts} />
                {this.tabs()}
                {tabNumber==1&&this.activeOutletsPage()}
                {tabNumber==2&&this.unCommittedOutletsPage()}
                {tabNumber==3&&this.declinedOutletsPage()}
                <AppFooter />
            </LinearGradient>
        );
    }
}

const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    barColor: state.lang.color,
    currentUser: state.auth.currentUser,
})

const mapDispatchToProps = {
    removeItem,
}

export default connect(mapStateToProps, mapDispatchToProps)(OperationActiveAccounts);
