import React, { Component } from 'react';
import {
    View, TouchableOpacity, Image, Text, ScrollView, TextInput, Alert, Platform, KeyboardAvoidingView
} from 'react-native';
import { connect } from 'react-redux';
import { Icon, Button } from 'native-base';
import { login } from '../actions/AuthActions'
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import Strings from '../assets/strings';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import Loading from "../common/Loading"
import { enableSideMenu, resetTo, push } from '../controlls/NavigationControll'
import { arrabicFont, englishFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import * as colors from '../assets/colors';
import { removeItem } from '../actions/MenuActions';
import CollaspeAppHeader from '../common/CollaspeAppHeader'
import LinearGradient from 'react-native-linear-gradient';
import FastImage from 'react-native-fast-image'
import CommanHeader from '../common/CommanHeader'
import ImagePicker from 'react-native-image-crop-picker';
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import AppFooter from '../components/AppFooter'
import MapView, { Marker } from 'react-native-maps';
import { BASE_END_POINT } from '../AppConfig';
import InputValidations from '../common/InputValidations';
import { RNToasty } from 'react-native-toasty';
//import Geolocation from '@react-native-community/geolocation';
import GetLocation from 'react-native-get-location'
import axios from 'axios'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { TextInputMask } from 'react-native-masked-text'
import { whiteButton } from '../assets/styles'
import Header from '../common/Header'

class UpdateResturantAccount extends Component {

    payTypes = [
        {
            name: Strings.payType,
            id: 0,
            children: [
                { name: 'Post Paid', id: 'PostPaid' },
                { name: 'Cash On Delivery', id: 'CashOnDelivery' },
            ],
        }
    ]

    branchsNo = [
        {
            name: Strings.branchesNo,
            id: 0,
            children: [
                { name: '1', id: 1, },
                { name: '2', id: 2, },
                { name: '3', id: 3, },
                { name: '4', id: 4, },
                { name: '5', id: 5, },
                { name: '6', id: 6, },
                { name: '7', id: 7, },
                { name: '8', id: 8, },
                { name: '9', id: 9, },
                { name: '10', id: 10, },
            ],
        }
    ]

    titles = [
        {
            name: Strings.jobTitle,
            id: 0,
            children: [
                { name: 'USER', id: 1, },
                { name: 'Title 2', id: 2, },
                { name: 'Title 3', id: 3, },
            ],
        }
    ]

    spaces = [
        {
            name: Strings.resSpace,
            id: 0,
            children: [
                { name: '0 - 50', id: 1, },
                { name: '50 - 150', id: 2, },
                { name: '200+', id: 3, },

            ],
        }
    ]

    averageKg = [
        {
            name: Strings.estimatedQuantity,
            id: 0,
            children: [
                { name: '0 - 50', id: 1, },
                { name: '50 - 100', id: 2, },
                { name: '100 - 150', id: 3, },
                { name: '150 - 200', id: 4, },
                { name: '200+', id: 5, },

            ],
        }
    ]

    oilTypes = [
        {
            name: Strings.oilType,
            id: 0,
            children: [
                { name: 'Canola', id: 'Canola', },
                { name: 'Sunflower', id: 'Sunflower', },
                { name: 'Palm', id: 'Palm', },
                { name: 'Olive', id: 'Olive', },
            ],
        }
    ]


    pageLoaded = 'first'
    state = {
        accountDetails: [],
        resturantId: 21,
        restaurantName: '', restaurantNameValidate: ' ',
        oilType: '', oilTypeValidate: ' ',
        selectPayTypeText: ' ',
        addressByDetails: '', addressByDetailsValidate: ' ',
        contactNo: ' ', contactNoValidate: ' ',
        address: ' ', addressValidate: ' ',
        city: '', cityValidate: ' ',
        firstName: '', firstNameValidate: ' ',
        lastName: '', lastNameValidate: ' ',
        email: '', emailValidate: ' ',
        userName: '', userNameValidate: ' ',
        password: '', passwordValidate: ' ',
        confirmPassword: ' ', confirmPasswordValidate: ' ', confirmPasswordEqualPasswordValidate: ' ',
        price: '', priceValidate: ' ', surveyerPrice: '',
        integerPriceValidate: ' ', decimalPriceValidate: ' ',
        integerPrice: '', decimalPrice: '',
        status: '',
        image: null,
        hidePassword: true,
        newBranches: [],
        branches: [],
        newBranches: [],
        selectPayType: '', selectPayTypeValidate: ' ',
        title: '', titleValidate: ' ',
        selectAvgKg: '', selectAvgKgValidate: ' ',
        selectBranchNo: '', selectBranchNoValidate: ' ',
        selectResturantSpace: '', selectResturantSpaceValidate: ' ',
        Coordinate: [],
        latitude: 0,
        longitude: 0,
        updateResturantLoading: false,

    }


    componentDidMount() {
        enableSideMenu(false, null)
        this.accounDetails()
        console.log('CurrentUser : ', this.props.currentUser)
        /* Geolocation.getCurrentPosition(location => {
             //Alert.alert("LNG  " + location.coords.longitude)
             // this.sendLocation(location.coords.latitude, location.coords.longitude)
             this.setState({ latitude: location.coords.latitude, longitude: location.coords.longitude })
         });*/

        GetLocation.getCurrentPosition({
            enableHighAccuracy: true,
            timeout: 200000,
        })
            .then(location => {
                //this.setState({ currentLongitude: location.longitude, currentLatitude: location.latitude, VMap: 1 });
                this.setState({ latitude: location.latitude, longitude: location.longitude })

            })
            .catch(error => {
                const { code, message } = error;
                // console.warn(code, message);
            })


    }

    accounDetails() {
        const { data, currentUser } = this.props
        console.log('DDDAASSRR:', data)
        this.setState({
            //accountList: refresh ? response.data.data : [...this.state.accountList, ...response.data.data],
            restaurantName: data.restaurantName,
            selectPayType: data.payType,

            price: currentUser.user.type == 'PURCHASING' ? '00.00' : data.price ? (data.price.toFixed(2)).toString() : '00.00',
            //integerPrice: (((data.price ? data.price : '').toString()).split("."))[0],
            //decimalPrice: (((data.price ? data.price : '').toString()).split("."))[1],
            //addressByDetails: data.address,
            contactNo: data.phone,
            firstName: data.firstname,
            lastName: data.lastname,
            email: data.status == 'APPROVED' ? data.owner.email ? data.owner.email : data.email ? data.email : '' : data.email ? data.email : '',
            userName: data.status == 'APPROVED' ? data.owner.username ? data.owner.username : data.username ? data.username : '' : data.username ? data.username : '',
            //userName: data.status == 'APPROVED' ? data.owner.username ? data.owner.username : data.userName ? data.userName : '' : '',
            title: data.title,
            oilType: data.oilType,
            image: data.img,
            status: data.status,
            selectAvgKg: data.average,
            selectBranchNo: data.branchesNumber,
            oldAddressCount: data.branches.length,
            branches: data.branches,
            selectResturantSpace: data.space,
            accountDetailsLoad: false,
            accountDetailsRefresh: false,
            loadingData: false,
            emailNotValid: false
        })
        this.addRequiredAddress(data.branchesNumber, data.branches)
        console.log('PRICE LENTGH : ', data.branches)
    }

    /////////////////////////////// Update Resturant Account Request
    updateResturantAccount = () => {
        const { resturantId, restaurantName, selectPayType, status, image, title, selectAvgKg, price, selectResturantSpace, oilType, selectBranchNo, branches, newBranches, firstName, lastName, contactNo, email, userName, password } = this.state
        const { currentUser } = this.props

        console.log('NewBranches', newBranches)

        var newStatus = status
        if (currentUser.user.type == 'SURVEY') { newStatus = 'SURVEY-ACCOUNT' }
        else if (currentUser.user.type == 'PURCHASING') { newStatus = 'PROCEED' }
        //else if (currentUser.user.type == 'ADMIN') { status = 'APPROVED' }
        // else if (currentUser.user.type == 'OPERATION') { status = 'APPROVED' }

        this.setState({ updateResturantLoading: true })
        var data = new FormData()
        data.append('restaurantName', restaurantName)
        data.append('payType', selectPayType)
        if (image) {
            data.append('img', {
                uri: image,
                type: 'multipart/form-data',
                name: 'licenseImg'
            })
        }
        data.append('average', selectAvgKg)
        // if (currentUser.user.type == 'ADMIN' || currentUser.user.type == 'OPERATION') {
        if (price != '00.00') {
            data.append('price', price)
        }
        data.append('space', selectResturantSpace)
        data.append('oilType', oilType)
        //data.append('address', addressByDetails)
        data.append('branchesNumber', selectBranchNo)
        data.append('firstname', firstName)
        data.append('lastname', lastName)
        data.append('phone', contactNo)
        data.append('status', newStatus)
        data.append('branches', JSON.stringify(newBranches))
        data.append('director', currentUser.user.id)
        //if (email.replace(/\s/g, '').length && status != 'APPROVED') {
        if (email.replace(/\s/g, '').length && status != 'APPROVED') {
            data.append('email', email)
        }
        data.append('title', title)
        data.append('type', 'USER')

        if (userName.replace(/\s/g, '').length && password.replace(/\s/g, '').length && status != 'APPROVED') {
            data.append('username', userName.toLowerCase())
            data.append('password', password)
        }
       
        



        console.log('RequestData: ', data)
        console.log(this.props.currentUser.token + ' / ' + `${BASE_END_POINT}restaurant/${this.props.data.id}`)


        axios.put(`${BASE_END_POINT}restaurant/${this.props.data.id}`, data, {
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${this.props.currentUser.token}`,
            }
        })
            .then(response => {
                this.setState({ updateResturantLoading: false })
                //const res = response.data
                // console.log('Done: ', response)

                RNToasty.Success({ title: Strings.dataUpdatedSuccessfuly })
                if (currentUser.user.type == 'PURCHASING') { resetTo('PurchasingHome') }
                else if (currentUser.user.type == 'ADMIN') { resetTo('AdminHome') }
                else if (currentUser.user.type == 'OPERATION') { resetTo('OperationHome') }
            })
            .catch(error => {
                console.log(error)
                const resError = error.response.data.errors[0]
                console.log('Error  ', resError.msg)
                RNToasty.Error({ title: resError.msg })

                //console.log(error.data)

                this.setState({ updateResturantLoading: false })
            })


    }

    restaurantNameInput = () => {
        const { isRTL } = this.props
        const { restaurantName, restaurantNameValidate } = this.state
        return (
            <View style={{ marginTop: moderateScale(15), width: responsiveWidth(80), alignSelf: 'center' }}>

                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.restaurantName}</Text>
                <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                    <TextInput
                        onChangeText={(val) => { this.setState({ restaurantName: val, restaurantNameValidate: val }) }}
                        style={{ width: responsiveWidth(80), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(6) }}
                        placeholder={Strings.restaurantName}
                        value={this.state.restaurantName}
                    />
                </View>
                {restaurantName.length == 0 &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }

    oilTypeInput = () => {
        const { isRTL } = this.props
        const { oilType } = this.state
        return (
            <View style={{ marginTop: moderateScale(5), width: responsiveWidth(80), alignSelf: 'center' }}>

                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.oilType}</Text>
                <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                    <TextInput
                        onChangeText={(val) => { this.setState({ oilType: val }) }}
                        style={{ width: responsiveWidth(80), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(6) }}
                        placeholder={Strings.oilType}
                        value={this.state.oilType}
                    />
                </View>
                {oilType.length == 0 &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }

    oilTypePicker = () => {
        const { isRTL } = this.props
        const { selectedItems } = this.state
        return (
            <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', width: responsiveWidth(80), alignSelf: 'center' }} >

                {/*Oil Type */}
                <Animatable.View style={{ marginTop: moderateScale(5), width: responsiveWidth(80), }}>

                    <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.oilType}</Text>

                    <View style={{ alignItems: 'center', justifyContent: 'center', backgroundColor: colors.lightGray, width: responsiveWidth(80), height: responsiveHeight(6), alignSelf: 'center' }}>

                        <SectionedMultiSelect
                            expandDropDowns
                            //modalAnimationType='slide'
                            //loading={loading}
                            showDropDowns={false}
                            modalWithTouchable
                            hideConfirm
                            searchPlaceholderText={Strings.search}
                            styles={{
                                selectToggle: { width: responsiveWidth(78), height: responsiveHeight(6), borderRadius: moderateScale(3), alignItems: 'center', justifyContent: 'center', },
                                selectToggleText: { textAlign: isRTL ? 'right' : 'left', fontSize: responsiveFontSize(6), color: 'gray', marginTop: moderateScale(8), fontFamily: isRTL ? arrabicFont : englishFont },
                                subItemText: { textAlign: isRTL ? 'right' : 'left' },
                                itemText: { fontSize: responsiveFontSize(10), textAlign: isRTL ? 'right' : 'left' },
                                container: { height: responsiveHeight(50), position: 'absolute', width: responsiveWidth(80), top: responsiveHeight(22), alignSelf: 'center' },
                                searchTextInput: { textAlign: isRTL ? 'right' : 'left', marginHorizontal: moderateScale(5) },
                            }}
                            items={this.oilTypes}
                            alwaysShowSelectText
                            searchPlaceholderText={Strings.search}
                            single
                            highlightChildren
                            uniqueKey="id"
                            subKey="children"
                            selectText={this.state.oilType ? this.state.oilType : Strings.oilType}
                            readOnlyHeadings={true}
                            onSelectedItemsChange={(selectedItems) => {
                                // this.setState({ countries: selectedItems });
                            }
                            }
                            onSelectedItemObjectsChange={(selectedItems) => {
                                console.log("ITEM2   ", selectedItems[0].name)
                                this.setState({ oilType: selectedItems[0].name, oilTypeValidate: selectedItems[0].name });
                            }
                            }

                        //onConfirm={() => this.setState({ selectOilType: '' })}
                        />

                    </View>
                    {this.state.oilTypeValidate.length == 0 &&
                        <Text style={{ color: 'red', marginTop: moderateScale(1), marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                    }
                </Animatable.View>
            </View>
        )
    }

    payTypePicker = () => {
        const { isRTL } = this.props
        const { selectedItems } = this.state
        return (
            <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', width: responsiveWidth(80), alignSelf: 'center' }} >

                {/*Oil Type */}
                <Animatable.View style={{ marginTop: moderateScale(5), width: responsiveWidth(80), }}>

                    <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.payType}</Text>

                    <View style={{ alignItems: 'center', justifyContent: 'center', backgroundColor: colors.lightGray, width: responsiveWidth(80), height: responsiveHeight(6), alignSelf: 'center' }}>

                        <SectionedMultiSelect
                            expandDropDowns
                            //modalAnimationType='slide'
                            //loading={loading}
                            showDropDowns={false}
                            modalWithTouchable
                            hideConfirm
                            searchPlaceholderText={Strings.search}
                            styles={{
                                selectToggle: { width: responsiveWidth(78), height: responsiveHeight(6), borderRadius: moderateScale(3), alignItems: 'center', justifyContent: 'center', },
                                selectToggleText: { textAlign: isRTL ? 'right' : 'left', fontSize: responsiveFontSize(6), color: 'gray', marginTop: moderateScale(8), fontFamily: isRTL ? arrabicFont : englishFont },
                                subItemText: { textAlign: isRTL ? 'right' : 'left' },
                                itemText: { fontSize: responsiveFontSize(10), textAlign: isRTL ? 'right' : 'left' },
                                container: { height: responsiveHeight(50), position: 'absolute', width: responsiveWidth(80), top: responsiveHeight(22), alignSelf: 'center' },
                                searchTextInput: { textAlign: isRTL ? 'right' : 'left', marginHorizontal: moderateScale(5) },
                            }}
                            items={this.payTypes}
                            searchPlaceholderText={Strings.search}
                            alwaysShowSelectText
                            single
                            highlightChildren
                            //uniqueKey= 'name'
                            uniqueKey="id"
                            subKey="children"
                            selectText={this.state.selectPayType ? this.state.selectPayType : Strings.payType}
                            //selectedItems={this.state.selectPayType}
                            selectedText={(selectPayType) => {
                                this.setState({ selectPayTypeText: selectedItems[0].name, selectPayType: selectPayType, selectPayTypeValidate: selectPayType });
                            }}
                            displayKey='name'
                            readOnlyHeadings={true}
                            onSelectedItemsChange={(selectedItems) => {
                                //this.setState({ selectPayTypeText: selectedItems[0].name });
                            }
                            }
                            onSelectedItemObjectsChange={(selectedItems) => {
                                console.log("ITEM2   ", selectedItems[0].id)
                                this.setState({ selectPayTypeText: selectedItems[0].name, selectPayType: selectedItems[0].id, selectPayTypeValidate: selectedItems[0].id });
                            }
                            }

                        // onConfirm={() => this.setState({ selectOilType: '' })}
                        />

                    </View>
                    {this.state.selectPayTypeValidate.length == 0 &&
                        <Text style={{ color: 'red', marginTop: moderateScale(1), marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                    }
                </Animatable.View>
            </View>
        )
    }

    PriceInputAvgKgPicker = () => {
        const { isRTL } = this.props
        const { integerPrice, decimalPrice, integerPriceValidate, decimalPriceValidate } = this.state
        return (
            <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'space-between', width: responsiveWidth(80), alignSelf: 'center', marginTop: moderateScale(7), }} >

                {/*Oil Type */}
                <Animatable.View style={{ width: responsiveWidth(37), }}>

                    <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.estimatedQuantity}</Text>
                    <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: colors.lightGray, width: responsiveWidth(37), height: responsiveHeight(6) }}>
                        <SectionedMultiSelect
                            expandDropDowns
                            //modalAnimationType='slide'
                            //loading={loading}
                            showDropDowns={false}
                            modalWithTouchable
                            hideConfirm
                            searchPlaceholderText={Strings.search}
                            styles={{
                                selectToggle: { width: responsiveWidth(35), height: responsiveHeight(6), borderRadius: moderateScale(3), alignItems: 'center', justifyContent: 'center', },
                                selectToggleText: { textAlign: isRTL ? 'right' : 'left', fontSize: responsiveFontSize(6), color: 'gray', marginTop: moderateScale(8), fontFamily: isRTL ? arrabicFont : englishFont },
                                subItemText: { textAlign: isRTL ? 'right' : 'left' },
                                itemText: { fontSize: responsiveFontSize(10), textAlign: isRTL ? 'right' : 'left' },
                                container: { height: responsiveHeight(60), position: 'absolute', width: responsiveWidth(80), top: responsiveHeight(18), alignSelf: 'center' },
                                searchTextInput: { textAlign: isRTL ? 'right' : 'left', marginHorizontal: moderateScale(5) },
                            }}
                            items={this.averageKg}
                            searchPlaceholderText={Strings.search}
                            alwaysShowSelectText
                            single
                            uniqueKey="id"
                            subKey="children"
                            selectText={this.state.selectAvgKg ? this.state.selectAvgKg : Strings.estimatedQuantity}

                            readOnlyHeadings={true}
                            onSelectedItemsChange={(selectedItems) => {
                                // this.setState({ countries: selectedItems });
                            }
                            }
                            onSelectedItemObjectsChange={(selectedItems) => {
                                console.log("ITEM2   ", selectedItems[0].name)
                                this.setState({ selectAvgKg: selectedItems[0].name });
                            }
                            }

                        //onConfirm={() => this.setState({ averageKg: '' })}

                        />
                    </View>
                    {this.state.selectAvgKg.length == 0 &&
                        <Text style={{ color: 'red', marginTop: moderateScale(1), marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                    }
                </Animatable.View>

                {this.priceInput()}
                { /*(this.props.currentUser.user.type == 'OPERATION' || this.props.currentUser.user.type == 'ADMIN') ?
                    <View style={{ width: responsiveWidth(37) }}>
                        <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.priceForKg}</Text>
                        <View style={{ borderRadius: moderateScale(1), flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'space-between' }} >
                            <TextInput
                                onChangeText={(val) => { this.setState({ integerPrice: val, integerPriceValidate: val }) }}
                                style={{ width: responsiveWidth(17), backgroundColor: colors.lightGray, paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(6) }}
                                placeholder={Strings.price}
                                keyboardType={'numeric'}
                                value={integerPrice.toString()}
                            />

                            <TextInput
                                onChangeText={(val) => { this.setState({ decimalPrice: val, decimalPriceValidate: val }) }}
                                style={{ width: responsiveWidth(17), backgroundColor: colors.lightGray, paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(6), textAlign: isRTL ? 'right' : 'left' }}
                                placeholder={Strings.price}
                                keyboardType={'numeric'}
                                value={decimalPrice.toString()}
                            />
                        </View>
                        {integerPriceValidate.length == 0 && decimalPriceValidate.length == 0 &&
                            <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                        }
                    </View>
                    : null*/}
            </View>
        )
    }

    priceInput = () => {
        const { isRTL } = this.props
        const { selectedResturantPrice, price } = this.state
        return (
            <View style={{ width: responsiveWidth(37), }}>
                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.priceForKg}</Text>
                <View style={{ borderRadius: moderateScale(1), alignSelf: 'center', }} >
                    <TextInputMask
                        type={'money'}
                        options={{
                            precision: 2,
                            separator: '.',
                            delimiter: '.',
                            unit: '',
                            suffixUnit: ''
                        }}
                        value={(price).toString()}
                        onChangeText={text => {
                            if (text > 11.00) {
                                this.setState({
                                    price: '11.00'
                                })
                            } else {
                                this.setState({
                                    price: text
                                })
                            }
                        }}
                        placeholder={'00.00'}
                        style={{ fontSize: responsiveFontSize(7), width: responsiveWidth(37), backgroundColor: colors.lightGray, paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(6), textAlign: 'center' }}
                    />
                </View>
                {price == '' &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }

    addressByDetailsInput = () => {
        const { isRTL } = this.props
        const { addressByDetails, addressByDetailsValidate } = this.state
        return (
            <View style={{ marginTop: moderateScale(7), width: responsiveWidth(80), alignSelf: 'center' }}>

                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.addressByDetails}</Text>
                <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                    <TextInput
                        onChangeText={(val) => { this.setState({ addressByDetails: val, addressByDetailsValidate: val }) }}
                        style={{ width: responsiveWidth(80), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(6) }}
                        placeholder={Strings.addressByDetails}
                        value={this.state.addressByDetails}
                    />
                </View>
                {addressByDetails.length == 0 &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }

    ResSpacebranchsNoPicker = () => {
        const { isRTL } = this.props
        const { selectedItems } = this.state
        return (


            <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'space-between', width: responsiveWidth(80), alignSelf: 'center', marginTop: moderateScale(7), }} >

                {/*Oil Type */}
                <Animatable.View style={{ width: responsiveWidth(37) }}>

                    <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.resSpace}</Text>

                    <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: colors.lightGray, width: responsiveWidth(37), height: responsiveHeight(6) }}>

                        <SectionedMultiSelect
                            expandDropDowns
                            //modalAnimationType='slide'
                            //loading={loading}
                            showDropDowns={false}
                            modalWithTouchable
                            hideConfirm
                            searchPlaceholderText={Strings.search}
                            styles={{
                                selectToggle: { width: responsiveWidth(35), height: responsiveHeight(6), borderRadius: moderateScale(3), alignItems: 'center', justifyContent: 'center', },
                                selectToggleText: { textAlign: isRTL ? 'right' : 'left', fontSize: responsiveFontSize(6), color: 'gray', marginTop: moderateScale(8), fontFamily: isRTL ? arrabicFont : englishFont },
                                subItemText: { textAlign: isRTL ? 'right' : 'left' },
                                itemText: { fontSize: responsiveFontSize(10), textAlign: isRTL ? 'right' : 'left' },
                                container: { height: responsiveHeight(50), position: 'absolute', width: responsiveWidth(80), top: responsiveHeight(18), alignSelf: 'center' },
                                searchTextInput: { textAlign: isRTL ? 'right' : 'left', marginHorizontal: moderateScale(5) },
                            }}
                            items={this.spaces}
                            searchPlaceholderText={Strings.search}
                            alwaysShowSelectText
                            single
                            uniqueKey="id"
                            subKey="children"
                            selectText={this.state.selectResturantSpace ? this.state.selectResturantSpace : Strings.resSpace}
                            readOnlyHeadings={true}
                            onSelectedItemsChange={(selectedItems) => {
                                // this.setState({ countries: selectedItems });
                            }
                            }
                            onSelectedItemObjectsChange={(selectedItems) => {
                                console.log("ITEM2   ", selectedItems[0].name)
                                this.setState({ selectResturantSpace: selectedItems[0].name });
                            }
                            }

                        //onConfirm={() => this.setState({ selectResturantSpace: '' })}
                        />
                    </View>
                    {this.state.selectResturantSpace.length == 0 &&
                        <Text style={{ color: 'red', marginTop: moderateScale(1), marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                    }
                </Animatable.View>


                <Animatable.View style={{ width: responsiveWidth(37) }}>

                    <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.branchesNo}</Text>

                    <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: colors.lightGray, width: responsiveWidth(37), height: responsiveHeight(6), alignSelf: 'center' }}>

                        <SectionedMultiSelect
                            expandDropDowns
                            //modalAnimationType='slide'
                            //loading={loading}
                            showDropDowns={false}
                            modalWithTouchable
                            hideConfirm
                            searchPlaceholderText={Strings.search}
                            styles={{
                                selectToggle: { width: responsiveWidth(35), height: responsiveHeight(6), borderRadius: moderateScale(3), alignItems: 'center', justifyContent: 'center', },
                                selectToggleText: { textAlign: isRTL ? 'right' : 'left', fontSize: responsiveFontSize(6), color: 'gray', marginTop: moderateScale(8), fontFamily: isRTL ? arrabicFont : englishFont },
                                subItemText: { textAlign: isRTL ? 'right' : 'left' },
                                itemText: { fontSize: responsiveFontSize(10), textAlign: isRTL ? 'right' : 'left' },
                                container: { height: responsiveHeight(60), position: 'absolute', width: responsiveWidth(80), top: responsiveHeight(18), alignSelf: 'center' },
                                searchTextInput: { textAlign: isRTL ? 'right' : 'left', marginHorizontal: moderateScale(5) },
                            }}
                            items={this.branchsNo}
                            searchPlaceholderText={Strings.search}
                            alwaysShowSelectText
                            single
                            uniqueKey="id"
                            subKey="children"
                            selectText={this.state.selectBranchNo ? this.state.selectBranchNo : Strings.branchesNo}
                            readOnlyHeadings={true}
                            onSelectedItemsChange={(selectedItems) => {
                                // this.setState({ countries: selectedItems });
                            }
                            }
                            onSelectedItemObjectsChange={(selectedItems) => {

                                console.log("ITEM2   ", selectedItems[0].id)
                                this.addRequiredAddress(selectedItems[0].id, this.state.branches)
                                this.setState({ selectBranchNo: selectedItems[0].name });

                            }
                            }

                        //onConfirm={() => this.setState({ selectBranchNo: '' })}
                        />
                    </View>
                    {this.state.selectBranchNo.length == 0 &&
                        <Text style={{ color: 'red', marginTop: moderateScale(1), marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                    }
                </Animatable.View>
            </View>
        )
    }

    addRequiredAddress = (count, branches) => {

        this.setState({ addressCount: [] })
        var Coordinate = []
        var NewAddressCount = []
        for (var i = 0; i < count; i++) {
            if (branches.length > 0 && branches[i] != null) {
                NewAddressCount.push({ 'address': branches[i].address, 'city': branches[i].city, 'destination': [branches[i].destination[0], branches[i].destination[1]] })
                Coordinate[i] = { latitude: branches[i].destination[0], longitude: branches[i].destination[1] }
            } else {
                NewAddressCount.push({ 'address': '', 'destination': [this.state.latitude, this.state.longitude] })
                Coordinate[i] = { latitude: this.state.latitude, longitude: this.state.longitude }
            }

        }

        console.log('NewAddressCount:', NewAddressCount)
        this.setState({ newBranches: NewAddressCount })


        this.setState({ Coordinate: Coordinate })

    }


    firstNameInput = () => {
        const { isRTL } = this.props
        const { firstName, firstNameValidate } = this.state
        return (
            <View style={{ marginTop: moderateScale(7), width: responsiveWidth(37), }}>

                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, marginHorizontal: moderateScale(0), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.firstName}</Text>
                <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                    <TextInput
                        onChangeText={(val) => { this.setState({ firstName: val, firstNameValidate: val }) }}
                        style={{ width: responsiveWidth(37), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(6) }}
                        placeholder={Strings.firstName}
                        value={this.state.firstName}
                    />
                </View>
                {firstName.length == 0 &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }

    lastNameInput = () => {
        const { isRTL } = this.props
        const { lastName, lastNameValidate } = this.state
        return (
            <View style={{ marginTop: moderateScale(7), width: responsiveWidth(37) }}>

                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, marginHorizontal: moderateScale(0), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.lastName}</Text>
                <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                    <TextInput
                        onChangeText={(val) => { this.setState({ lastName: val, lastNameValidate: val }) }}
                        style={{ width: responsiveWidth(37), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(6) }}
                        placeholder={Strings.lastName}
                        value={this.state.lastName}
                    />
                </View>
                {lastName.length == 0 &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }

    contactNoInput = () => {
        const { isRTL } = this.props
        const { contactNo, contactNoValidate } = this.state
        return (
            <View style={{ marginTop: moderateScale(6), width: responsiveWidth(80), alignSelf: 'center' }}>

                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.contactNo}</Text>
                <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                    <TextInput
                        onChangeText={(val) => { this.setState({ contactNo: val, contactNoValidate: val }) }}
                        style={{ width: responsiveWidth(80), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(6) }}
                        placeholder={Strings.contactNo}
                        value={this.state.contactNo}
                        keyboardType={'phone-pad'}
                    />
                </View>
                {contactNo.length == 0 &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }

    titlePicker = () => {
        const { isRTL } = this.props
        const { title } = this.state
        return (
            <View style={{ marginTop: moderateScale(5), width: responsiveWidth(80), alignSelf: 'center' }} >

                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.title}</Text>
                {/*Oil Type */}
                <Animatable.View style={{ width: responsiveWidth(80), marginLeft: 'auto', marginRight: 'auto' }}>


                    <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: colors.lightGray, width: responsiveWidth(80), height: responsiveHeight(6), alignSelf: 'center' }}>

                        <SectionedMultiSelect
                            expandDropDowns
                            //modalAnimationType='slide'
                            //loading={loading}
                            showDropDowns={false}
                            modalWithTouchable
                            hideConfirm
                            searchPlaceholderText={Strings.search}
                            styles={{
                                selectToggle: { width: responsiveWidth(74), height: responsiveHeight(6), borderRadius: moderateScale(3), alignItems: 'center', justifyContent: 'center', },
                                selectToggleText: { fontSize: responsiveFontSize(6), color: 'gray', marginTop: moderateScale(8), fontFamily: isRTL ? arrabicFont : englishFont },
                                subItemText: { textAlign: isRTL ? 'right' : 'left' },
                                itemText: { fontSize: responsiveFontSize(10), textAlign: isRTL ? 'right' : 'left' },
                                container: { height: responsiveHeight(60), position: 'absolute', width: responsiveWidth(80), top: responsiveHeight(18), alignSelf: 'center' },
                                searchTextInput: { textAlign: isRTL ? 'right' : 'left', marginHorizontal: moderateScale(5) },
                            }}
                            items={titles}
                            alwaysShowSelectText
                            single
                            uniqueKey="id"
                            subKey="children"
                            // selectText={this.state.selecTitle ? this.state.selecTitle : Strings.title}
                            readOnlyHeadings={true}
                            onSelectedItemsChange={(selectedItems) => {
                                // this.setState({ countries: selectedItems });
                            }
                            }
                            onSelectedItemObjectsChange={(selectedItems) => {
                                console.log("ITEM2   ", selectedItems[0].name)
                                this.setState({ selecTitle: selectedItems[0].name });
                            }
                            }

                        //onConfirm={() => this.setState({ selecTitle: '' })}
                        />
                    </View>
                    {this.state.selecTitle == null &&
                        <Text style={{ color: 'red', marginTop: moderateScale(1), marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                    }
                </Animatable.View>
            </View>
        )
    }


    titleInput = () => {
        const { isRTL } = this.props
        const { title } = this.state
        return (
            <View style={{ marginTop: moderateScale(5), width: responsiveWidth(80), alignSelf: 'center' }}>

                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.title}</Text>
                <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                    <TextInput
                        onChangeText={(val) => { this.setState({ title: val, titleValidate: val }) }}
                        style={{ width: responsiveWidth(80), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(6) }}
                        placeholder={Strings.title}
                        value={this.state.title}
                    />
                </View>
                {title.length == 0 &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }

    emailInput = () => {
        const { isRTL } = this.props
        const { email, emailValidate, emailNotValid, status } = this.state
        return (
            <View style={{ marginTop: moderateScale(5), width: responsiveWidth(80), alignSelf: 'center' }}>

                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.email}</Text>
                <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                    <TextInput
                        onChangeText={(val) => { this.setState({ email: val, emailValidate: val }) }}
                        style={{ width: responsiveWidth(80), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(6) }}
                        value={this.state.email}
                        placeholder={Strings.email}
                        keyboardType={'email-address'}
                        editable={status == 'APPROVED' ? false : true}
                    />
                </View>
                {emailNotValid == true &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.emailNotValid}</Text>
                }
            </View>
        )
    }

    userNameInput = () => {
        const { isRTL, currentUser } = this.props
        const { userName, userNameValidate, status } = this.state
        return (
            <View style={{ marginTop: moderateScale(5), width: responsiveWidth(80), alignSelf: 'center' }}>

                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.userName}</Text>
                <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                    <TextInput
                        onChangeText={(val) => { this.setState({ userName: val, userNameValidate: val }) }}
                        style={{ width: responsiveWidth(80), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(6) }}
                        value={this.state.userName}
                        placeholder={Strings.userName}
                        editable={status == 'APPROVED' ? false : true}
                    />
                </View>
                {userNameValidate.length == 0 && (currentUser.user.type != 'SURVEY') &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }

    passwordInput = () => {
        const { isRTL, currentUser } = this.props
        const { password, passwordValidate } = this.state
        return (
            <View style={{ marginTop: moderateScale(5), width: responsiveWidth(80), alignSelf: 'center' }}>

                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.password}</Text>
                <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                    <TextInput
                        onChangeText={(val) => { this.setState({ password: val, passwordValidate: val }) }}
                        style={{ textAlign: isRTL ? 'right' : 'left', width: responsiveWidth(80), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(6) }}
                        placeholder={Strings.password}
                        secureTextEntry
                    />
                </View>
                {passwordValidate.length == 0 && (currentUser.user.type == 'ADMIN' || currentUser.user.type == 'OPERATION') &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }

    confirmPasswordInput = () => {
        const { isRTL, currentUser } = this.props
        const { confirmPassword, confirmPasswordValidate, confirmPasswordEqualPasswordValidate, password } = this.state
        return (
            <View style={{ marginTop: moderateScale(5), width: responsiveWidth(80), alignSelf: 'center' }}>

                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.confirmPassword}</Text>
                <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                    <TextInput
                        onChangeText={(val) => { this.setState({ confirmPassword: val, confirmPasswordValidate: val }) }}
                        style={{ width: responsiveWidth(80), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(6), textAlign: isRTL ? 'right' : 'left' }}
                        placeholder={Strings.confirmPassword}
                        secureTextEntry
                    />
                </View>
                {confirmPasswordValidate.length == 0 && (currentUser.user.type == 'ADMIN' || currentUser.user.type == 'OPERATION') &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
                {confirmPasswordEqualPasswordValidate.length == 0 && confirmPasswordValidate.length > 0 && (confirmPassword != password) &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.confirmPasswordMustEqualPassword}</Text>
                }
            </View>
        )
    }


    populateBranchesAddress(val, index) {
        console.log('branches Val : ', val, index)
        var branches = this.state.newBranches
        this.setState({ newBranches: [] })
        if (branches[index]) {
            branches[index] = { "address": val, "city": branches[index].city, "destination": [branches[index].destination[0], branches[index].destination[1]] }
            this.setState({ newBranches: branches })
            console.log('branches : ', branches)
        } else {
            branches[index] = { "address": val, "city": '', "destination": [0, 0] }
            this.setState({ newBranches: branches })

            console.log('branches : ', branches)
        }
    }

    populateBranchesCity(val, index) {
        console.log('branches Val : ', val, index)
        var branches = this.state.newBranches
        this.setState({ newBranches: [] })
        if (branches[index]) {
            branches[index] = { "address": branches[index].address, "city": val, "destination": [branches[index].destination[0], branches[index].destination[1]] }
            this.setState({ newBranches: branches })
            console.log('branches : ', branches)
        } else {
            branches[index] = { "address": '', "city": val, "destination": [0, 0] }
            this.setState({ newBranches: branches })

            console.log('branches : ', branches)
        }
    }

    populateBranchesMap(latitude, longitude, index) {

        var branches = this.state.newBranches
        var Coordinate = this.state.Coordinate

        if (branches[index]) {
            branches[index] = { "address": branches[index].address,"city":branches[index].city, "destination": [latitude, longitude] }
            this.setState({ newBranches: branches })
            Coordinate[index] = { latitude: latitude, longitude: longitude }
            this.setState({ Coordinate: Coordinate })
            console.log('branches : ', branches)
        } else {
            branches[index] = { 'address': '',"city":'', 'destination': [latitude, longitude] }
            this.setState({ newBranches: branches })
            Coordinate[index] = { latitude: latitude, longitude: longitude }
            this.setState({ Coordinate: Coordinate })
            console.log('branches : ', branches)
        }
        console.log('Coordinate : ', Coordinate, index)
    }


    addressMapInput = () => {
        const { isRTL } = this.props
        const { address, newBranches, branches, addressValidate, cityValidate } = this.state

        return (
            <>
                {/*this.state.branches.map((item, index) => (
                    this.state.Coordinate.push({ latitude: item.destination[0], longitude: item.destination[1] }),
                    < View >
                        <View style={{ marginTop: moderateScale(6), width: responsiveWidth(80), alignSelf: 'center' }}>

                            <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', width: responsiveWidth(80), height: responsiveHeight(6) }}>
                                <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), lineHeight: responsiveHeight(7), alignSelf: isRTL ? 'flex-end' : 'flex-start', }}>{index + 1}</Text>
                                <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), lineHeight: responsiveHeight(7), alignSelf: isRTL ? 'flex-end' : 'flex-start', }}>. </Text>
                                <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                                    <TextInput
                                        onChangeText={(val) => { this.setState({ address: val }), this.populateBranchesAddress(val, index) }}
                                        value={item.address}
                                        style={{ width: responsiveWidth(77), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(6) }}
                                        placeholder={Strings.address}
                                    />
                                </View>
                            </View>
                            {address.length == 0 &&
                                <Text style={{ color: 'red', marginHorizontal: moderateScale(5), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                            }
                        </View>

                        <View style={{ marginTop: moderateScale(6), width: responsiveWidth(80), height: responsiveHeight(30), alignSelf: 'center' }}>

                            <MapView style={{ position: 'absolute', top: 0, left: 0, right: 0, bottom: 0 }}
                                initialRegion={{
                                    latitude: this.state.Coordinate[index].latitude,
                                    longitude: this.state.Coordinate[index].longitude,
                                    latitudeDelta: 0.0922,
                                    longitudeDelta: 0.0421,
                                }}
                                onPress={(event) => [this.setState({ Coordinate: event.nativeEvent.coordinate }), console.log(event.nativeEvent.coordinate), this.populateBranchesMap(event.nativeEvent.coordinate.latitude, event.nativeEvent.coordinate.longitude, index)]}
                            >
                                <MapView.Marker style={{ width: 4, height: 4 }}

                                    coordinate={{
                                        //latitude: 30.35689489993212,
                                        //longitude: 31.1944606972275
                                        latitude: parseFloat(this.state.Coordinate[index].latitude),
                                        longitude: parseFloat(this.state.Coordinate[index].longitude),
                                    }}
                                    //image={require('../../../../Assets/map-pin.png')}
                                    height={5}
                                    width={5}
                                />

                            </MapView>


                        </View>
                    </View>
                ))*/
                }


                {
                    this.state.newBranches.map((item, index) => (
                        <View>
                            <View style={{ marginTop: moderateScale(6), width: responsiveWidth(80), alignSelf: 'center' }}>

                                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', width: responsiveWidth(80), height: responsiveHeight(6) }}>
                                    <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), lineHeight: responsiveHeight(7), alignSelf: isRTL ? 'flex-end' : 'flex-start', }}>{index + 1}</Text>
                                    <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), lineHeight: responsiveHeight(7), alignSelf: isRTL ? 'flex-end' : 'flex-start', }}>. </Text>
                                    <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                                        <TextInput
                                            onChangeText={(val) => { this.setState({ address: val, addressValidate: val }), this.populateBranchesAddress(val, index) }}
                                            style={{ width: responsiveWidth(77), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(6) }}
                                            placeholder={Strings.address}
                                            value={item.address}
                                        />
                                    </View>
                                </View>
                                {/*address.length == 0 &&
                                    <Text style={{ color: 'red', marginHorizontal: moderateScale(5), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                                */}

                                {newBranches.length > 0 && newBranches[index] != null ?
                                    newBranches[index].address == '' ?
                                        <Text style={{ color: 'red', marginHorizontal: moderateScale(5), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                                        :
                                        null
                                    :
                                    newBranches[index] == null && addressValidate == '' &&
                                    <Text style={{ color: 'red', marginHorizontal: moderateScale(5), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                                }
                            </View>


                            <View style={{ marginTop: moderateScale(6), width: responsiveWidth(80), alignSelf: 'center' }}>

                                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', width: responsiveWidth(80), height: responsiveHeight(6) }}>
                                    <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), lineHeight: responsiveHeight(7), alignSelf: isRTL ? 'flex-end' : 'flex-start', }}>{index + 1}</Text>
                                    <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), lineHeight: responsiveHeight(7), alignSelf: isRTL ? 'flex-end' : 'flex-start', }}>. </Text>
                                    <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                                        <TextInput
                                            onChangeText={(val) => { this.setState({ city: val, cityValidate: val }), this.populateBranchesCity(val, index) }}
                                            style={{ width: responsiveWidth(77), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(6) }}
                                            placeholder={Strings.city}
                                            value={item.city}
                                        />
                                    </View>
                                </View>
                                {/*address.length == 0 &&
                                    <Text style={{ color: 'red', marginHorizontal: moderateScale(5), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                                */}

                                {newBranches.length > 0 && newBranches[index] != null ?
                                    newBranches[index].city == '' ?
                                        <Text style={{ color: 'red', marginHorizontal: moderateScale(5), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                                        :
                                        null
                                    :
                                    newBranches[index] == null && cityValidate == '' &&
                                    <Text style={{ color: 'red', marginHorizontal: moderateScale(5), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                                }
                            </View>

                            <View style={{ marginTop: moderateScale(6), width: responsiveWidth(80), height: responsiveHeight(30), alignSelf: 'center' }}>
                                {this.state.latitude > 0 ?
                                    <MapView style={{ position: 'absolute', top: 0, left: 0, right: 0, bottom: 0 }}
                                        initialRegion={{
                                            latitude: parseFloat(this.state.Coordinate.length > index && this.state.Coordinate[index] != null ? this.state.Coordinate[index].latitude : this.state.latitude),
                                            longitude: parseFloat(this.state.Coordinate.length > index && this.state.Coordinate[index] != null ? this.state.Coordinate[index].longitude : this.state.longitude),
                                            latitudeDelta: 0.0922,
                                            longitudeDelta: 0.0421,
                                        }}
                                        onPress={(event) => {
                                            this.setState({ Coordinate: event.nativeEvent.coordinate, addressValidate: event.nativeEvent.coordinate.latitude })
                                            console.log(event.nativeEvent.coordinate)
                                            this.populateBranchesMap(event.nativeEvent.coordinate.latitude, event.nativeEvent.coordinate.longitude, index)
                                        }}
                                    >
                                        <MapView.Marker style={{ width: 4, height: 4 }}

                                            coordinate={{
                                                //latitude: 30.35689489993212,
                                                //longitude: 31.1944606972275
                                                latitude: parseFloat(this.state.Coordinate.length > index && this.state.Coordinate[index] != null ? this.state.Coordinate[index].latitude : this.state.latitude),
                                                longitude: parseFloat(this.state.Coordinate.length > index && this.state.Coordinate[index] != null ? this.state.Coordinate[index].longitude : this.state.longitude),
                                            }}
                                            //image={require('../../../../Assets/map-pin.png')}
                                            height={5}
                                            width={5}
                                        />

                                    </MapView>
                                    :
                                    <Text style={{ textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.waitForGetLocation}</Text>
                                }


                            </View>
                        </View>
                    ))
                }
            </>
        )
    }




    save = () => {
        const { resturantId, restaurantName, selectPayType, image, title, selectAvgKg, price, selectResturantSpace, oilType, selectBranchNo, branches, newBranches, firstName, lastName, contactNo, email, userName, password, confirmPassword, addressValidate } = this.state
        const { currentUser, data } = this.props
        console.log(restaurantName, title, selectAvgKg, price, selectResturantSpace, oilType, selectBranchNo, newBranches, firstName, lastName, contactNo, email, userName, password, addressValidate)
        var status = ''
        var locationAddress = addressValidate
        var emailNotValid = false
        if (currentUser.user.type == 'SURVEY') { status = 'SURVEY-ACCOUNT' }
        else if (currentUser.user.type == 'PURCHASING') { status = 'PROCEED' }
        //else if (currentUser.user.type == 'ADMIN') { status = 'APPROVED' }
        //else if (currentUser.user.type == 'OPERATION') { status = 'APPROVED' }
        for (var i = 0; i < selectBranchNo; i++) {
            if (newBranches[i]) {
                if (newBranches[i].address == '') {
                    this.setState({ addressValidate: '' })
                    locationAddress = ''
                }
            }
            else {
                this.setState({ addressValidate: '' })
                locationAddress = ''
            }
        }
        console.log('locationAddress : ', userName.length)
        if (!restaurantName.replace(/\s/g, '').length) { this.setState({ restaurantNameValidate: '' }); }
        if (!selectPayType.replace(/\s/g, '').length) { this.setState({ selectPayType: '' }); }
        if (!oilType.replace(/\s/g, '').length) { this.setState({ oilType: '' }); }
        if (!selectAvgKg.replace(/\s/g, '').length) { this.setState({ selectAvgKgValidate: '' }); }
        if (!(price.toString()).replace(/\s/g, '').length) { this.setState({ price: '' }); }
        if (!selectResturantSpace.replace(/\s/g, '').length) { this.setState({ selectResturantSpaceValidate: '' }); }
        if (!(selectBranchNo.toString()).replace(/\s/g, '').length) { this.setState({ selectBranchNoValidate: '' }); }
        //if (!addressByDetails.replace(/\s/g, '').length) { this.setState({ addressByDetailsValidate: '' }); }
        if (!firstName.replace(/\s/g, '').length) { this.setState({ firstNameValidate: '' }); }
        if (!lastName.replace(/\s/g, '').length) { this.setState({ lastNameValidate: '' }); }
        if (!contactNo.replace(/\s/g, '').length) { this.setState({ contactNoValidate: '' }); }
        if (!title.replace(/\s/g, '').length) { this.setState({ titleValidate: '' }); }
        //if (!userName.replace(/\s/g, '').length) { this.setState({ userNameValidate: '' }); }
        if (email.replace(/\s/g, '').length) {
            if (InputValidations.validateEmail(email) == false) {
                //RNToasty.Error({ title: Strings.emailNotValid })
                emailNotValid = true
                this.setState({ emailNotValid: true })
            }

        }

        if (currentUser.user.type == 'OPERATION' || currentUser.user.type == 'ADMIN') {

            if (data.status == 'APPROVED') {
                if (restaurantName.replace(/\s/g, '').length && selectPayType.replace(/\s/g, '').length && oilType.replace(/\s/g, '').length && selectAvgKg.replace(/\s/g, '').length && selectResturantSpace.replace(/\s/g, '').length && `${selectBranchNo}`.replace(/\s/g, '').length && firstName.replace(/\s/g, '').length && lastName.replace(/\s/g, '').length && contactNo.replace(/\s/g, '').length && title.replace(/\s/g, '').length && locationAddress != '' && emailNotValid == false) {
                    this.updateResturantAccount()
                } else {
                    //RNToasty.Error({ title: Strings.insertTheRequiredData })
                    RNToasty.Error({ title: Strings.insertTheRequiredData })
                    this.setState({ updateResturantLoading: false })
                }
            }
            else {
                if (!userName.replace(/\s/g, '').length) { this.setState({ userNameValidate: '' }); }
                if (!password.replace(/\s/g, '').length) { this.setState({ passwordValidate: '' }); }
                if (!confirmPassword.replace(/\s/g, '').length) { this.setState({ confirmPasswordValidate: '' }); }
                if (confirmPassword != password) { this.setState({ confirmPasswordEqualPasswordValidate: '' }) }
                if (userName.replace(/\s/g, '').length && price.toString().replace(/\s/g, '').length && password.replace(/\s/g, '').length && (password == confirmPassword)) {
                    if (restaurantName.replace(/\s/g, '').length && selectPayType.replace(/\s/g, '').length && oilType.replace(/\s/g, '').length && selectAvgKg.replace(/\s/g, '').length && selectResturantSpace.replace(/\s/g, '').length && `${selectBranchNo}`.replace(/\s/g, '').length && firstName.replace(/\s/g, '').length && lastName.replace(/\s/g, '').length && contactNo.replace(/\s/g, '').length && title.replace(/\s/g, '').length && locationAddress != '' && emailNotValid == false) {
                        this.updateResturantAccount()
                    } else {
                        //RNToasty.Error({ title: Strings.insertTheRequiredData })
                        RNToasty.Error({ title: Strings.insertTheRequiredData })
                        this.setState({ updateResturantLoading: false })
                    }
                } else {
                    RNToasty.Error({ title: Strings.insertTheRequiredData })
                    this.setState({ updateResturantLoading: false })
                }
            }
        }





        else if (currentUser.user.type == 'PURCHASING') {
            //if (!(price.toString()).replace(/\s/g, '').length) { this.setState({ priceValidate: '' }); }
            //if (!userName.replace(/\s/g, '').length) { this.setState({ userNameValidate: '' }); }
            // if (!password.replace(/\s/g, '').length) { this.setState({ passwordValidate: '' }); }
            //if (userName.replace(/\s/g, '').length) {
            if (restaurantName.replace(/\s/g, '').length && selectPayType.replace(/\s/g, '').length && oilType.replace(/\s/g, '').length && selectAvgKg.replace(/\s/g, '').length && selectResturantSpace.replace(/\s/g, '').length && `${selectBranchNo}`.replace(/\s/g, '').length && firstName.replace(/\s/g, '').length && lastName.replace(/\s/g, '').length && contactNo.replace(/\s/g, '').length && title.replace(/\s/g, '').length && locationAddress != '' && emailNotValid == false) {
                console.log('ffff:', restaurantName, oilType, selectAvgKg, selectResturantSpace, selectBranchNo, firstName, lastName, contactNo, title, locationAddress, emailNotValid)
                this.updateResturantAccount()
            } else {
                RNToasty.Error({ title: Strings.insertTheRequiredData })

                this.setState({ updateResturantLoading: false })
            }
            /* } else {
                 RNToasty.Error({ title: Strings.insertTheRequiredData })
                 this.setState({ updateResturantLoading: false })
             }*/
        }
        else {
            if (restaurantName.replace(/\s/g, '').length && selectPayType.replace(/\s/g, '').length && oilType.replace(/\s/g, '').length && selectAvgKg.replace(/\s/g, '').length && selectResturantSpace.replace(/\s/g, '').length && `${selectBranchNo}`.replace(/\s/g, '').length && firstName.replace(/\s/g, '').length && lastName.replace(/\s/g, '').length && contactNo.replace(/\s/g, '').length && title.replace(/\s/g, '').length && locationAddress != '' && emailNotValid == false) {

                this.updateResturantAccount()
            } else {
                RNToasty.Error({ title: Strings.insertTheRequiredData })

                this.setState({ updateResturantLoading: false })
            }
        }

    }

    saveButton = () => {
        const { isRTL } = this.props

        return (
            <TouchableOpacity onPress={() => { this.save() }} style={[whiteButton, { alignSelf: 'center', marginVertical: moderateScale(17), marginBottom: moderateScale(35) }]} >
                <Text style={{ color: colors.green, fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(7) }}>{Strings.submit}</Text>
            </TouchableOpacity>
        )
    }

    pickImage = () => {
        ImagePicker.openPicker({
            width: 600,
            height: 600,
            cropping: true
        }).then(image => {
            this.setState({ image: image.path })
            console.log(image);
        });
    }

    profileImage = () => {
        const { isRTL } = this.props
        const { image } = this.state
        return (
            <View style={{ marginTop: moderateScale(10), alignSelf: 'center' }} >
                <FastImage
                    resizeMode='contain'
                    source={image ? { uri: image } : require('../assets/imgs/profileicon.jpg')}
                    style={{ borderWidth: 2, borderColor: colors.lightGray, width: 100, height: 100, borderRadius: 50 }}
                />
                <TouchableOpacity onPress={this.pickImage} style={{ alignSelf: 'flex-end', marginTop: moderateScale(-11), justifyContent: 'center', alignItems: 'center', backgroundColor: colors.darkGreen, height: 40, width: 40, borderRadius: 20 }}>
                    <Icon name='photo' type='FontAwesome' style={{ fontSize: responsiveFontSize(7), color: colors.white }} />
                </TouchableOpacity>
            </View>
        )
    }



    render() {
        const { isRTL, userToken, data } = this.props;
        const { phone, password, hidePassword, email } = this.state
        return (
            <LinearGradient
                colors={[colors.white, colors.white, colors.white]}
                style={{ flex: 1 }}
            >
                <Header  title={Strings.updateResturantAccount} /> 
                <KeyboardAwareScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: colors.white, borderTopRightRadius: moderateScale(20), borderTopLeftRadius: moderateScale(20), marginTop: moderateScale(12), width: responsiveWidth(100) }} >

                    <View style={{ alignSelf: 'center', width: responsiveWidth(60), borderBottomColor: colors.darkGreen, borderBottomWidth: 2, justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ marginTop: moderateScale(5), marginBottom: moderateScale(3), color: colors.darkGreen, fontSize: responsiveFontSize(8), fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.restaurantDetails}</Text>
                    </View>
                    {this.state.loadingData ?
                        <Loading />
                        : null}
                    {this.restaurantNameInput()}
                    {this.payTypePicker()}
                    {this.oilTypePicker()}
                    {this.PriceInputAvgKgPicker()}
                    {this.ResSpacebranchsNoPicker()}

                    <View style={{ marginTop: moderateScale(15), alignSelf: 'center', width: responsiveWidth(60), borderBottomColor: colors.darkGreen, borderBottomWidth: 2, justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ marginBottom: moderateScale(3), color: colors.darkGreen, fontSize: responsiveFontSize(8), fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.contactDetails}</Text>
                    </View>

                    {this.profileImage()}
                    <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', width: responsiveWidth(80), justifyContent: 'space-between', alignSelf: 'center', }}>
                        {this.firstNameInput()}
                        {this.lastNameInput()}
                    </View>

                    {this.contactNoInput()}
                    {this.titleInput()}
                    {this.emailInput()}
                    {(this.props.currentUser.user.type != 'SURVEY') && (this.props.currentUser.user.type != 'PURCHASING') && (data.status != 'APPROVED') && this.userNameInput()}
                    {(this.props.currentUser.user.type != 'SURVEY') && (this.props.currentUser.user.type != 'PURCHASING') && (data.status != 'APPROVED') && this.passwordInput()}
                    {(this.props.currentUser.user.type != 'SURVEY') && (this.props.currentUser.user.type != 'PURCHASING') && (data.status != 'APPROVED') && this.confirmPasswordInput()}

                    {this.addressMapInput()}




                    {this.saveButton()}
                </KeyboardAwareScrollView>
                {this.state.updateResturantLoading == true ?
                    <LoadingDialogOverlay title={Strings.wait} />
                    :
                    null
                }

                <AppFooter />

            </LinearGradient>
        );
    }
}
const mapDispatchToProps = {
    login,
    removeItem
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    loading: state.auth.loading,
    errorText: state.auth.errorText,
    currentUser: state.auth.currentUser,
    //userToken: state.auth.userToken,
})


export default connect(mapToStateProps, mapDispatchToProps)(UpdateResturantAccount);

