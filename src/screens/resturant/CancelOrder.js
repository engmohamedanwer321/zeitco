import React,{Component} from 'react';
import {View,RefreshControl,Alert, ScrollView,FlatList,Text,TouchableOpacity} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight ,responsiveFontSize} from '../../utils/responsiveDimensions';
import { connect } from "react-redux";
import Strings from '../../assets/strings';

import axios from 'axios';
import { BASE_END_POINT} from '../../AppConfig';
import ListFooter from '../../components/ListFooter';
import {Icon,Thumbnail,Button} from 'native-base'
import {selectMenu,removeItem} from '../../actions/MenuActions';
import {enableSideMenu,pop} from '../../controlls/NavigationControll'
import NetInfo from "@react-native-community/netinfo";
import Loading from "../../common/Loading"
import NetworError from '../../common/NetworError'
import NoData from '../../common/NoData'
import * as colors from '../../assets/colors'
import {arrabicFont,englishFont} from  '../../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image'
import NotificationCard from '../../components/NotificationCard'
import PurchasingAccountsCard from '../../components/PurchasingAccountsCard';
import CollaspeAppHeader from '../../common/CollaspeAppHeader'
import LinearGradient from 'react-native-linear-gradient';
import CommanHeader from '../../common/CommanHeader'
import AppFooter from '../../components/AppFooter'
import CancelOrderCard from '../../components/CancelOrderCard'
import Header from '../../common/Header'

 
class CancelOrder extends Component {

    page=1;
    state= {
        networkError:null,
        orders:[],
        ordersLoading: true,
        ordersRefresh:false,
        orders404:false,
        pages: null,
    }



    componentDidMount(){
        enableSideMenu(false, null)
        this.getOrders(false,1)
    }

    getOrders = (refresh,page) => {
        if(refresh){
            this.setState({ordersRefresh:true})
        }
        axios.get(`${BASE_END_POINT}orders?restaurant=${this.props.currentUser.user.restaurant.id}&finish=false&page=${page}`)
        .then(response=>{
          console.log('Done   ',response.data.data)
          //Alert.alert('Done')
          this.setState({
              orders:refresh?response.data.data:[...this.state.orders,...response.data.data],
              ordersLoading:false,
              ordersRefresh:false,
              orders404:false,
              pages:response.data.pageCount,
            })
          })
        .catch(error=>{
            //Alert.alert('error  ',error)
          console.log('Error   ',error.response)
          this.setState({orders404:true,ordersLoading:false,})
        })
    }





    cancel = () =>{
        Alert.alert(
            '',
            Strings.areYouSureToCancelOrder,
            [
              {
                text: Strings.cancel,
                onPress: () => console.log('Cancel Pressed'),
              },
              {text: Strings.yes, onPress: () => console.log('OK Pressed')},
            ],
            {cancelable: false},
          );
    }


    render(){
        const {categoryName,isRTL} = this.props;
        const {pages, orders,ordersLoading,orders404,ordersRefresh} = this.state
        return(
            <LinearGradient 
            colors={[colors.white, colors.white, colors.white]} 
            style={{flex:1}}
            >
             <Header  title={Strings.cancelOrder} />  
            
            <ScrollView showsVerticalScrollIndicator={false} style={{marginBottom:moderateScale(20),height:responsiveHeight(82), backgroundColor:colors.white, borderTopRightRadius:moderateScale(20),borderTopLeftRadius:moderateScale(20), marginTop:moderateScale(15), width:responsiveWidth(100)}} >
                {   orders404?
                    <NetworError />
                    :
                    ordersLoading?
                    <Loading />
                    :
                    orders.length>0?
                    <FlatList
                    showsVerticalScrollIndicator={false} 
                    style={{marginVertical:moderateScale(10)}}
                    data={orders}
                    renderItem={({item})=><CancelOrderCard data={item} />}
                    onEndReachedThreshold={.5}
                    //ListFooterComponent={()=>notificationsLoad&&<ListFooter />}
                    onEndReached={() => {     
                        if(this.page <= pages){
                            this.page=this.page+1;
                            this.getOrders(false,this.page)
                            console.log('page  ',this.page)
                        }  
                    }}
                    refreshControl={
                    <RefreshControl 
                    //colors={["#B7ED03",colors.darkBlue]} 
                    refreshing={ordersRefresh}
                    onRefresh={() => {
                        this.page = 1
                        this.getOrders(true,1)
                    }}
                    />
                    }
                    
                    />
                    :
                    <NoData />
                }
            </ScrollView>

            <AppFooter/>
            
            </LinearGradient> 
        );
    }
}

const mapStateToProps = state => ({
    isRTL: state.lang.RTL, 
    barColor: state.lang.color ,
    currentUser:state.auth.currentUser, 
})

const mapDispatchToProps = {
    removeItem,
}

export default connect(mapStateToProps,mapDispatchToProps)(CancelOrder);
