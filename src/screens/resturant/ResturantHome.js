import React,{Component} from 'react';
import {View,RefreshControl,Alert, ScrollView,FlatList,Text,TouchableOpacity, Image} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight ,responsiveFontSize} from '../../utils/responsiveDimensions';
import { connect } from "react-redux";
import Strings from '../../assets/strings';

import axios from 'axios';
import { BASE_END_POINT} from '../../AppConfig';
import ListFooter from '../../components/ListFooter';
import {Icon,Thumbnail,Button} from 'native-base'
import {selectMenu,removeItem} from '../../actions/MenuActions';
import {enableSideMenu,pop, push} from '../../controlls/NavigationControll'
import NetInfo from "@react-native-community/netinfo";
import Loading from "../../common/Loading"
import NetworError from '../../common/NetworError'
import NoData from '../../common/NoData'
import * as colors from '../../assets/colors'
import {arrabicFont,englishFont} from  '../../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image'
import NotificationCard from '../../components/NotificationCard'
import PurchasingAccountsCard from '../../components/PurchasingAccountsCard';
import CollaspeAppHeader from '../../common/CollaspeAppHeader'
import LinearGradient from 'react-native-linear-gradient';
import MainHeader from '../../common/MainHeader'
import Carousel from 'react-native-snap-carousel';
import AdsCard from '../../components/AdsCard'
import BrandCard from '../../components/BrandCard'
import AppFooter from '../../components/AppFooter'
import {getUnreadNotificationsCount} from '../../actions/NotificationAction'
import {
  checkFirbaseNotificationPermision,
  getFirebaseNotificationToken,
  showFirebaseNotifcation,
  clickOnFirebaseNotification
  } from '../../controlls/FirebasePushNotificationControll'
import {setFirebaseToken} from '../../actions/NotificationAction'
import Header from '../../common/Header'


class ResturantHome extends Component {

    newsPage=1
    slidersPage=1
    state= {
        networkError:null,
        news:[],
        newsLoading: true,
        newsRefresh:false,
        news404:false,
        newsPages: null,
        sliders:[],
        slidersLoading: true,
        slidersRefresh:false,
        sliders404:false,
        slidersPages: null,
    }



    componentDidMount(){
        enableSideMenu(true,this.props.isRTL)
        checkFirbaseNotificationPermision()
        getFirebaseNotificationToken(this.props.currentUser.token,this.props.setFirebaseToken)
        //showFirebaseNotifcation()
        //clickOnFirebaseNotification()
        this.props.getUnreadNotificationsCount(this.props.currentUser.token)
        this.getNews(false,1)
        //this.getSliders(false,1)
    }

    getNews = (refresh,page) => {
        if(refresh){
            this.setState({newsRefresh:true})
        }
        axios.get(`${BASE_END_POINT}news?page=${page}`)
        .then(response=>{
          console.log('Done   ',response.data.data)
          this.setState({
              news:refresh?response.data.data:[...this.state.news,...response.data.data],
              newsLoading:false,
              newsRefresh:false,
              newsPages:response.data.pageCount,
            })
          })
        .catch(error=>{
          console.log('Error   ',error.response)
          this.setState({news404:true,newsLoading:false,})
        })
    }

    getSliders = (refresh,page) => {
        if(refresh){
            this.setState({slidersRefresh:true})
        }
        axios.get(`${BASE_END_POINT}slider?page=${page}`)
        .then(response=>{
            //Alert.alert(''+response.data.data[0].img[0])
            //console.log('Data   ',response.data.data[0].img)
          this.setState({
              sliders:response.data.data,
              slidersLoading:false,
              slidersRefresh:false,
              //slidersPages:response.data.pageCount,
            })
          })
        .catch(error=>{
            console.log('Error22   ',error)
          console.log('Error   ',error.response)
          this.setState({sliders404:true,slidersLoading:false,})
        })
    }

 
    ads = () => {
        const {newsPages, news,news404,newsLoading,newsRefresh} = this.state
        return(
            <View  style={{marginTop:moderateScale(3)}}>
                {
                news404?
                <NetworError />
                :
                newsLoading?
                <Loading />
                :
                news.length>0?
                <FlatList
                horizontal
                showsHorizontalScrollIndicator={false} 
                data={news}
                renderItem={({item})=><AdsCard data={item}/>}
                onEndReachedThreshold={.5}
                //ListFooterComponent={()=>notificationsLoad&&<ListFooter />}
                onEndReached={() => {     
                    if(this.newsPage <= newsPages){
                        this.newsPage=this.newsPage+1;
                        this.getNews(false,this.newsPage)
                        console.log('page  ',this.newsPage)
                    }  
                }}
                refreshControl={
                <RefreshControl 
                //colors={["#B7ED03",colors.darkBlue]} 
                refreshing={newsRefresh}
                onRefresh={() => {
                    this.newsPage = 1
                    this.getNews(true,1)
                }}
                />
                }
                
                />
                :
                <NoData />
            }
            </View>
        )
    }

    sliders = () => {
        const {slidersPages,sliders,sliders404,slidersLoading,slidersRefresh} = this.state
        return(
            <View  style={{marginTop:moderateScale(3),marginBottom:moderateScale(10)}}>
                {
                sliders404?
                <NetworError />
                :
                slidersLoading?
                <Loading />
                :
                sliders.length>0?
                <FlatList
                horizontal
                showsHorizontalScrollIndicator={false} 
                data={sliders}
                renderItem={({item})=><BrandCard data={item}/>}
                />
                :
                <NoData />
            }
            </View>
        )
    }

    buttons = () => {
        const {isRTL} = this.props
        return(
            <View style={{marginTop:moderateScale(5), alignSelf:'center',flexDirection:isRTL?'row-reverse':'row',alignItems:'center'}}>
                <TouchableOpacity  onPress={()=>push('MakeOrder')} style={{justifyContent:'center',alignItems:'center', marginHorizontal:moderateScale(12)}}>
                    <FastImage source={require('../../assets/imgs/new-order-icon.png')} style={{width:responsiveWidth(20) , height:responsiveWidth(20), borderRadius:responsiveWidth(10)}} />
                    <Text style={{color: colors.black,marginTop:moderateScale(1), fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.requestOrder}</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={()=>push('CancelOrder')} style={{justifyContent:'center',alignItems:'center', marginHorizontal:moderateScale(12)}}>                    
                    <FastImage source={require('../../assets/imgs/cancel-order-icon.png')} style={{width:responsiveWidth(20) , height:responsiveWidth(20), borderRadius:responsiveWidth(10)}} />
                    <Text style={{color: colors.black,marginTop:moderateScale(1), fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.cancelOrder}</Text>
                </TouchableOpacity>

            </View>
        )
    }


    render(){
        const {categoryName,isRTL} = this.props;
        return(
            <View style={{flex:1}}>
              <Header menu  title={Strings.home} />
             <ScrollView showsVerticalScrollIndicator={false} style={{marginBottom:moderateScale(30)}}>
             {this.buttons()}
             
             <Text style={{marginTop:moderateScale(15),fontWeight:'bold',alignSelf:isRTL?'flex-end':'flex-start',marginHorizontal:moderateScale(10), fontFamily:isRTL?arrabicFont:englishFont,fontSize:responsiveFontSize(10)}}>{Strings.news}</Text>
             {this.ads()}

             {/*
             <Text style={{marginTop:moderateScale(3),fontWeight:'bold',alignSelf:isRTL?'flex-end':'flex-start',marginHorizontal:moderateScale(10), fontFamily:isRTL?arrabicFont:englishFont,fontSize:responsiveFontSize(10)}}>{Strings.sliders}</Text>
             this.sliders() 
             */}
             </ScrollView>
             <AppFooter />
            </View> 
        );
    }
}

const mapStateToProps = state => ({
    isRTL: state.lang.RTL, 
    barColor: state.lang.color ,
    currentUser:state.auth.currentUser, 
    firbaseToken:state.noti.firbaseToken
})

const mapDispatchToProps = {
    removeItem,
    getUnreadNotificationsCount,
    setFirebaseToken

}

export default connect(mapStateToProps,mapDispatchToProps)(ResturantHome);
