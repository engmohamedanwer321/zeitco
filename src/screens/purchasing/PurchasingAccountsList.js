import React, { Component } from 'react';
import { View, RefreshControl, ScrollView, FlatList, Text, TouchableOpacity } from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../../utils/responsiveDimensions';
import { connect } from "react-redux";
import Strings from '../../assets/strings';

import ListFooter from '../../components/ListFooter';
import { Icon, Thumbnail, Button } from 'native-base'
import { selectMenu, removeItem } from '../../actions/MenuActions';
import { enableSideMenu, pop } from '../../controlls/NavigationControll'
import NetInfo from "@react-native-community/netinfo";
import Loading from "../../common/Loading"
import NetworError from '../../common/NetworError'
import NoData from '../../common/NoData'
import * as colors from '../../assets/colors'
import { arrabicFont, englishFont } from '../../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image'
import NotificationCard from '../../components/NotificationCard'
import PurchasingAccountsCard from '../../components/PurchasingAccountsCard';
import CollaspeAppHeader from '../../common/CollaspeAppHeader'
import LinearGradient from 'react-native-linear-gradient';
import CommanHeader from '../../common/CommanHeader'
import AppFooter from '../../components/AppFooter'
import { BASE_END_POINT } from '../../AppConfig';
import InputValidations from '../../common/InputValidations';
import { RNToasty } from 'react-native-toasty'
import axios from 'axios'
import Header from '../../common/Header'
import GetLocation from 'react-native-get-location'


class PurchasingAccountsList extends Component {

    page = 1;
    state = {
        networkError: null,
        accountList: [],
        accountListLoading: true,
        accountListRefresh: false,
        accountList404: false,
        pages: null,
        latitude:0,
        longitude:0
    }

    componentDidMount() {
        enableSideMenu(false, null)
        setTimeout(() => {
            this.purchasingAccountList(false, 1)
        }, 2000);
        
        this.getCurrentLocation()
    }

   

    getCurrentLocation = () => {
        GetLocation.getCurrentPosition({
            enableHighAccuracy: true,
            timeout: 200000,
        })
            .then(location => {
                console.log("LOCATION  ", location)
                //this.setState({ currentLongitude: location.longitude, currentLatitude: location.latitude, VMap: 1 });
                this.setState({ latitude: location.latitude, longitude: location.longitude })

            })
            .catch(error => {
                const { code, message } = error;
                console.log("LOCATION error  ", error.message)
                console.log(code, message);
            })

    }

    purchasingAccountList(refresh, page) {
        const {latitude , longitude} = this.state
        if (refresh) {
            this.setState({ accountListRefresh: true })
        }
       console.log(`${BASE_END_POINT}restaurant?status=PURCHASING-ACCOUNT&purchasing=${this.props.currentUser.user.id}&Agreement=PENDING&lat=${latitude}&lang=${longitude}&page=${page}`)
        axios.get(`${BASE_END_POINT}restaurant?status=PURCHASING-ACCOUNT&purchasing=${this.props.currentUser.user.id}&Agreement=PENDING&lat=${latitude}&lang=${longitude}&page=${page}`)
            .then(response => {
                console.log('Done   ', response.data.data)
                this.setState({
                    accountList: refresh ? response.data.data : [...this.state.accountList, ...response.data.data],
                    accountListLoading: false,
                    accountListRefresh: false,
                    pages: response.data.pageCount,
                })
            })
            .catch(error => {
                console.log('Error   ', error)
                this.setState({ accountList404: true, accountListLoading: false, })
            })
    }

    renderFooter = () => {
        return (
            this.state.loading ?
                <View style={{ alignSelf: 'center', margin: moderateScale(5) }}>
                    <ListFooter />
                </View>
                : null
        )
    }

    render() {
        const { categoryName, isRTL } = this.props;
        const { pages, accountList, accountListLoading, accountList404, accountListRefresh } = this.state
        return (
            <LinearGradient
                colors={[colors.white, colors.white, colors.white]}
                style={{ flex: 1 }}
            >
                <Header title={Strings.assignOutlets} />

                <View style={{flex:1, marginBottom: moderateScale(20), backgroundColor: colors.white, borderTopRightRadius: moderateScale(20), borderTopLeftRadius: moderateScale(20), marginTop: moderateScale(5), width: responsiveWidth(100), marginBottom:responsiveHeight(7) }} >

                    {accountList404 ?
                        <NetworError />
                        :
                        accountListLoading ?
                            <Loading />
                            :
                            accountList.length > 0 ?
                                <FlatList
                                    showsVerticalScrollIndicator={false}
                                    style={{ marginBottom: moderateScale(10)}}
                                    data={accountList}
                                    renderItem={({ item }) => <PurchasingAccountsCard data={item} />}
                                    onEndReachedThreshold={.5}
                                    onEndReached={() => {
                                        if (this.page <= pages) {
                                            this.page = this.page + 1;
                                            this.purchasingAccountList(false, this.page)
                                            console.log('page  ', this.page)
                                        }
                                    }}
                                    refreshControl={
                                        <RefreshControl
                                            //colors={["#B7ED03",colors.darkBlue]} 
                                            refreshing={accountListRefresh}
                                            onRefresh={() => {
                                                this.page = 1
                                                this.purchasingAccountList(true, 1)
                                            }}
                                        />
                                    }
                                />
                                :
                                <NoData />
                    }
                </View>

                <AppFooter />
            </LinearGradient>
        );
    }
}

const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    barColor: state.lang.color,
    currentUser: state.auth.currentUser,
})

const mapDispatchToProps = {
    removeItem,
}

export default connect(mapStateToProps, mapDispatchToProps)(PurchasingAccountsList);
