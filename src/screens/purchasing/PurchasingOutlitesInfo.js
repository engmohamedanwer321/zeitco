import React, { Component } from 'react';
import {
    View, TouchableOpacity, Image, Text, ScrollView, Linking, TextInput, Alert, Platform
} from 'react-native';
import { connect } from 'react-redux';
import { Icon, Button } from 'native-base';
import { login } from '../../actions/AuthActions'
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../../utils/responsiveDimensions";
import Strings from '../../assets/strings';
import LoadingDialogOverlay from '../../components/LoadingDialogOverlay';
import { enableSideMenu, resetTo, push } from '../../controlls/NavigationControll'
import { arrabicFont, englishFont } from '../../common/AppFont'
import * as Animatable from 'react-native-animatable';
import * as colors from '../../assets/colors';
import { removeItem } from '../../actions/MenuActions';
import CollaspeAppHeader from '../../common/CollaspeAppHeader'
import LinearGradient from 'react-native-linear-gradient';
import FastImage from 'react-native-fast-image'
import CommanHeader from '../../common/CommanHeader'
import ImagePicker from 'react-native-image-crop-picker';
import AppFooter from '../../components/AppFooter'
import { BASE_END_POINT } from '../../AppConfig';
import axios from 'axios';
import { RNToasty } from 'react-native-toasty';
import Dialog, { DialogContent, DialogTitle } from 'react-native-popup-dialog';
import { whiteButton, greenButton } from '../../assets/styles'
import MapView, { Marker } from 'react-native-maps';
import Header from '../../common/Header'
import RNPickerSelect from 'react-native-picker-select';


class PurchasingOutlitesInfo extends Component {

    nonAgreementReason = [
        { label: 'Contracted with another company', value: 'Contracted with another company' },
        { label: 'Does not produce waste oils', value: 'Does not produce waste oils' },
        { label: 'Contract with management', value: 'Contract with management' },
        { label: 'Refused to conduct with our company', value: 'Refused to conduct with our company' },
        { label: 'follow up and revisit restaurant', value: 'follow up and revisit restaurant' },
        { label: 'restaurant closed', value: 'restaurant closed' },
        { label: 'Price is High', value: 'Price is High' }
    ]

    state = {
        loading: false,
        showDeleteDialog: false,
        showDialog: false,
        nonAgreementReason: ' '
    }

    componentDidMount() {
        enableSideMenu(false, null)
    }

    nonAgreementResturant = () => {
        const { nonAgreementReason } = this.state
        const { data } = this.props

        console.log(data.id)
        if (!nonAgreementReason.replace(/\s/g, '').length) {
            this.setState({ nonAgreementReason: '' })
        }

        if (nonAgreementReason.replace(/\s/g, '').length) {
            this.setState({ loading: true })
            const d = {
                nonAgreementReason: nonAgreementReason,
            }
            console.log(`${BASE_END_POINT}restaurant/${data.id}/nonAgreement`)

            axios.put(`${BASE_END_POINT}restaurant/${data.id}/nonAgreement`, JSON.stringify(d), {
                headers: {
                    "Content-Type": 'application/json',
                    Authorization: `Bearer ${this.props.currentUser.token}`,
                }
            })
                .then(response => {
                    this.setState({ loading: false, showDialog: false })
                    console.log(response)
                    resetTo('PurchasingHome')

                })
                .catch(error => {
                    this.setState({ loading: false })
                    console.log(error)
                    RNToasty.Error({ title: Strings.errorInDataReteivel })

                })
        }

    }


    dialog = () => {
        const { isRTL, data } = this.props;
        const { nonAgreementReason } = this.state
        return (
            <Dialog
                containerStyle={{ backgroundColor: 'rgba(1,1,1,0.1)', }}
                visible={this.state.showDialog}
                onTouchOutside={() => {
                    this.setState({ showDialog: false });
                }}
                onHardwareBackPress={() => {
                    this.setState({ showDialog: false });
                }}
            >

                <View style={{ justifyContent: 'center', alignItems: 'center', width: responsiveWidth(90), height: responsiveHeight(25) }}>
                    <View style={{ width: responsiveWidth(70) }}>
                        <RNPickerSelect
                            onValueChange={
                                (item) => {
                                    this.setState({
                                        nonAgreementReason: item

                                    });
                                }}
                            items={this.nonAgreementReason}
                            placeholder={{ label: 'Reason', value: '' }}
                            style={{
                                inputIOS: { textAlign: isRTL ? 'right' : 'left', fontSize: responsiveFontSize(6), height: responsiveHeight(5), fontFamily: isRTL ? arrabicFont : englishFont, paddingVertical: 9, paddingHorizontal: moderateScale(3), paddingRight: responsiveWidth(5), color: 'gray', width: responsiveWidth(70) },
                                inputAndroid: { textAlign: isRTL ? 'right' : 'left', fontSize: responsiveFontSize(6), height: responsiveHeight(5), fontFamily: isRTL ? arrabicFont : englishFont, paddingHorizontal: moderateScale(3), paddingRight: responsiveWidth(5), paddingVertical: 8, color: 'gray', width: responsiveWidth(70) }
                            }}
                            placeholderTextColor={'gray'}
                            Icon={() => { return (<Icon name='down' type='AntDesign' style={{ color: 'black', fontSize: responsiveFontSize(6), top: responsiveHeight(1.5), right: responsiveWidth(1) }} />) }}
                        />
                        {nonAgreementReason == '' &&
                            <Text style={{ color: 'red', marginTop: moderateScale(1), marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                        }
                    </View>
                    <View style={{ marginTop: moderateScale(5), flexDirection: isRTL ? 'row-reverse' : 'row', width: responsiveWidth(70), justifyContent: 'space-between', alignItems: 'center' }}>
                        <TouchableOpacity onPress={() => {
                            this.nonAgreementResturant()


                        }} disabled={this.state.enable} style={{ ...greenButton, width: responsiveWidth(25) }} >
                            <Text style={{ color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.confirm}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => {
                            this.setState({ showDialog: false })


                        }} disabled={this.state.enable} style={{ ...whiteButton, width: responsiveWidth(25) }} >
                            <Text style={{ color: colors.green, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.cancel}</Text>
                        </TouchableOpacity>

                    </View>
                </View>

            </Dialog>
        )
    }

    basicsData = () => {
        const { isRTL, data } = this.props
        console.log(data)
        return (
            <View style={{ width: responsiveWidth(90), alignItems: isRTL ? 'flex-end' : 'flex-start', alignSelf: 'center', marginTop: moderateScale(10) }}>


                {/*<Text style={{ color: 'black', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(8), borderBottomWidth: 1, borderBottomColor: 'black', paddingBottom: moderateScale(2) }} >{Strings.restaurantDetails}</Text>*/}
                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row' }}>
                    <View style={{ width: responsiveWidth(80) }}>
                        <Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), marginTop: moderateScale(5), textAlign: isRTL ? 'right' : 'left' }} >{Strings.restaurantName} : {isRTL ? data.restaurantName_ar : data.restaurantName}</Text>
                        <Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), marginTop: moderateScale(5), textAlign: isRTL ? 'right' : 'left' }} >{Strings.ContactNumber} : {data.phone}</Text>
                        <Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), marginTop: moderateScale(5), textAlign: isRTL ? 'right' : 'left' }} >{Strings.contactPerson} : {data.firstname} {data.lastname}</Text>
                        <Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), marginTop: moderateScale(5), textAlign: isRTL ? 'right' : 'left' }} >{Strings.avgKgMonth} : {data.average}</Text>
                        <Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), marginTop: moderateScale(5), textAlign: isRTL ? 'right' : 'left' }} >{Strings.price} : {data.price}</Text>
                        {/*<Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), marginTop: moderateScale(5), textAlign: isRTL ? 'right' : 'left' }} >{Strings.resSpace} : {data.space}</Text>*/}
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), marginTop: moderateScale(5), textAlign: isRTL ? 'right' : 'left' }} >{Strings.address} : </Text>
                            {data.branches.map((item) => (
                                <Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), marginTop: moderateScale(5), textAlign: isRTL ? 'right' : 'left' }} >{item.address} - </Text>
                            ))}
                        </View>
                        {/*<Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), marginTop: moderateScale(5), textAlign: isRTL ? 'right' : 'left' }} >{Strings.branchesNo} : {data.branchesNumber}</Text>*/}
                        {/*<Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), marginTop: moderateScale(5), textAlign: isRTL ? 'right' : 'left' }} >{Strings.oilType} : {data.oilType}</Text>*/}
                    </View>
                </View>

                {data.branches.map((item) => (
                    <MapView style={{ marginTop: moderateScale(10), width: responsiveWidth(90), height: responsiveHeight(30), alignSelf: 'center' }}
                        region={{
                            latitude: item.destination[0],
                            longitude: item.destination[1],
                            latitudeDelta: 0.0922,
                            longitudeDelta: 0.0421,
                        }}
                        onPress={(event) => {
                            Linking.openURL(`https://www.google.com/maps/search/?api=1&query=${item.destination[0]},${item.destination[1]}`)
                        }}
                    >
                        <MapView.Marker
                            coordinate={{
                                latitude: item.destination[0],
                                longitude: item.destination[1],
                                latitudeDelta: 0.0922,
                                longitudeDelta: 0.0421,
                            }}
                            style={{ width: 4, height: 4 }}
                            height={5}
                            width={5}

                        />

                    </MapView>
                ))}





            </View>
        )
    }


    agreement_nonAgreementButton = () => {
        const { isRTL } = this.props
        return (
            <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', alignSelf: 'center', marginVertical: moderateScale(20) }}>
                <TouchableOpacity onPress={() => {
                    push('UpdateResturantAccount', this.props.data)
                }} style={greenButton} >
                    <Text style={{ paddingHorizontal: moderateScale(5), color: 'white', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(7) }}>{Strings.agreement}</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => {
                    this.setState({ showDialog: true })
                }} style={whiteButton} >
                    <Text style={{ paddingHorizontal: moderateScale(5), color: colors.green, fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(7) }}>{Strings.nonAgreement}</Text>
                </TouchableOpacity>
            </View>
        )
    }


    render() {
        const { isRTL, userToken } = this.props;
        return (
            <LinearGradient
                colors={[colors.white, colors.white, colors.white]}
                style={{ flex: 1 }}
            >
                <Header title={Strings.confirmAccount} />

                <ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: colors.white, borderTopRightRadius: moderateScale(20), borderTopLeftRadius: moderateScale(20), marginTop: moderateScale(15), width: responsiveWidth(100), marginBottom: moderateScale(20) }} >
                    {this.basicsData()}
                    {this.agreement_nonAgreementButton()}
                    {this.dialog()}
                </ScrollView>

                <AppFooter />

            </LinearGradient>
        );
    }
}
const mapDispatchToProps = {
    login,
    removeItem
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    loading: state.auth.loading,
    errorText: state.auth.errorText,
    currentUser: state.auth.currentUser,
    //userToken: state.auth.userToken,
})


export default connect(mapToStateProps, mapDispatchToProps)(PurchasingOutlitesInfo);

