import React, { Component } from 'react';
import { View, RefreshControl, Alert, Modal, Image, FlatList, Text, TouchableOpacity, Linking } from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../../utils/responsiveDimensions';
import { connect } from "react-redux";
import Strings from '../../assets/strings';

import axios from 'axios';
import { BASE_END_POINT } from '../../AppConfig';
import ListFooter from '../../components/ListFooter';
import { Icon, Thumbnail, Button } from 'native-base'
import { selectMenu, removeItem } from '../../actions/MenuActions';
import { enableSideMenu, pop } from '../../controlls/NavigationControll'
import NetInfo from "@react-native-community/netinfo";
import Loading from "../../common/Loading"
import NetworError from '../../common/NetworError'
import NoData from '../../common/NoData'
import * as colors from '../../assets/colors'
import { arrabicFont, englishFont,boldFont } from '../../common/AppFont'
import * as Animatable from 'react-native-animatable'
import FastImage from 'react-native-fast-image'
import NotificationCard from '../../components/NotificationCard'
import PurchasingAccountsCard from '../../components/PurchasingAccountsCard';
import CollaspeAppHeader from '../../common/CollaspeAppHeader'
import { OrdersCount } from '../../actions/OrdersActions';
import LinearGradient from 'react-native-linear-gradient';
import CommanHeader from '../../common/CommanHeader'
import MapView, { Marker } from 'react-native-maps';
import AppFooter from '../../components/AppFooter'
import { push } from '../../controlls/NavigationControll';
import io from 'socket.io-client';
import DriverConfirmedOrderCard from '../../components/DriverConfirmedOrderCard'
import { RNToasty } from 'react-native-toasty';
import Header from '../../common/Header'


class DriverOrders extends Component {

    socket = null;
    newOrdersPage = 1;
    confirmedOrdersPage = 1;
    state = {
        showAlert: false,
        tabNumber: 1,
        showModal: false,
        location: {
            latitude: 37.78825,
            longitude: -122.4324,
            latitudeDelta: 0.015,
            longitudeDelta: 0.0121,
        },
        networkError: null,
        newOrders: [],
        confirmedOrders: [],
        newOrdersLoading: true,
        confirmedOrdersLoading: true,
        newOrdersRefresh: false,
        confirmedOrdersRefresh: false,
        newOrders404: false,
        confirmedOrders404: false,
        newOrdersPages: null,
        confirmedOrdersPages: null,
        ResturantOnMap: ''
    }

    
    componentDidMount() {
        enableSideMenu(false, null)
        this.newOrders(false, 1)
        console.log('ffff',this.props.isChecking)
        this.confirmedOrders(false, 1)
        this.socket = io("https://api.zeitcoapp.com/order", { query: `id=${this.props.currentUser.user.id}` });
        this.socket.on('assignOrder', (data) => {
            console.log("assignOrder  ", data.order[0]); // true 
            this.setState({ newOrders: [data.order[0], ...this.state.newOrders] })
            this.props.OrdersCount(this.props.currentUser.user.id, 'ASSIGNED')
            //Alert.alert('done')   
        });
    }

    newOrders(refresh, page) {
        
        if (refresh) {
            this.setState({ newOrdersRefresh: true })
        }
        console.log(`${BASE_END_POINT}orders?driver=${this.props.currentUser.user.id}&status=ASSIGNED&start=${this.props.isChecking}&page=${page}`)

        axios.get(`${BASE_END_POINT}orders?driver=${this.props.currentUser.user.id}&status=ASSIGNED&start=${this.props.isChecking}&page=${page}`)
            .then(response => {
                console.log('GGGGG   ', response.data.data)
                this.setState({
                    newOrders: refresh ? response.data.data : [...this.state.newOrders, ...response.data.data],
                    newOrdersLoading: false,
                    newOrdersRefresh: false,
                    newOrdersPages: response.data.pageCount,
                })
            })
            .catch(error => {
                console.log('Error new   ', error)
                this.setState({ orders404: true, newOrdersLoading: false, })
            })
    }


    confirmedOrders(refresh, page) {

        if (refresh) {
            this.setState({ confirmedOrdersRefresh: true })
        }

        axios.get(`${BASE_END_POINT}orders?driver=${this.props.currentUser.user.id}&status=ONPROGRESS&start=${this.props.isChecking}&page=${page}`)
            .then(response => {
                console.log('Done   ', response.data.data)
                this.setState({
                    confirmedOrders: refresh ? response.data.data : [...this.state.confirmedOrders, ...response.data.data],
                    confirmedOrdersLoading: false,
                    confirmedOrdersRefresh: false,
                    confirmedOrdersPages: response.data.pageCount,
                })
            })
            .catch(error => {
                console.log('Error confirm   ', error)
                this.setState({  confirmedOrdersLoading: false, })
            })
    }

    acceptOrder(order) {

        const { currentUser } = this.props

        this.setState({ loading: true })
        console.log(`${BASE_END_POINT}orders/${order.id}/accept`)
        axios.put(`${BASE_END_POINT}orders/${order.id}/accept`, null, {
            headers: {
                "Authorization": `Bearer ${currentUser.token}`,
                "Content-Type": "application/json"
            }
        })
            .then(response => {
                //console.log('LOOOcation : ', order.restaurant.branches[order.restaurant.branch][0])
                this.setState({ loading: false })
                this.setState({
                    showModal: true, ResturantOnMap: order.restaurant.restaurantName,
                    location: {
                        latitude: order.restaurant.branches[order.branch].destination[0],
                        longitude: order.restaurant.branches[order.branch].destination[1],
                        latitudeDelta: 0.015,
                        longitudeDelta: 0.0121,
                    },
                })
                this.props.OrdersCount(this.props.currentUser.user.id, 'ASSIGNED')
                //RNToasty.Success({ title: Strings.orderFinishedSuccessfuly })
            })
            .catch(error => {
                this.setState({ loading: false })
                console.log('ERROR   ', error)
                //RNToasty.Error({ title: Strings.errorInUpdate })
            })

    }

    openMap(resturant, latitude, longitude) {
        var url = 'http://maps.google.com/maps?q=' + resturant + '&daddr=' + latitude + ',' + longitude;
        Linking.openURL(url);
    }


    cancel = () => {
        Alert.alert(
            '',
            Strings.areYouSureToCancelOrder,
            [
                {
                    text: Strings.cancel,
                    onPress: () => console.log('Cancel Pressed'),
                },
                { text: Strings.yes, onPress: () => console.log('OK Pressed') },
            ],
            { cancelable: false },
        );
    }

    cancelOrderCard = () => {
        const { categoryName, isRTL } = this.props;
        return (
            <Animatable.View
                style={{ borderBottomWidth: 1, borderColor: '#cccbcb', marginTop: moderateScale(3), alignSelf: 'center', width: responsiveWidth(96) }}
                animation={"slideInLeft"}
                duration={1000}>
                <Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', color: colors.grayColor1, fontSize: responsiveFontSize(9), fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.orderCode} : 123456</Text>
                <Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', color: colors.grayColor1, fontSize: responsiveFontSize(7), fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.date} : 1/1/20</Text>
                <Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', color: colors.grayColor1, fontSize: responsiveFontSize(7), fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.quantity} : 100{Strings.Kg}</Text>
                <Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', color: colors.grayColor1, fontSize: responsiveFontSize(7), fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.price} : 100{Strings.EGP}</Text>

                <Button onPress={() => {
                    this.cancel()
                }} style={{ marginTop: moderateScale(-12), marginBottom: moderateScale(6), marginHorizontal: moderateScale(5), width: responsiveWidth(28), height: responsiveHeight(5), justifyContent: 'center', alignItems: 'center', alignSelf: isRTL ? 'flex-start' : 'flex-end', borderRadius: moderateScale(3), backgroundColor: 'red' }}>
                    <Text style={{ color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.cancel}</Text>
                </Button>

            </Animatable.View>
        )
    }

    tabs = () => {
        const { categoryName, isRTL, ordersCount } = this.props;
        const { tabNumber } = this.state;
        console.log(tabNumber)
        return (
            <View style={{ width: responsiveWidth(100), flexDirection: 'row', alignItems: 'center' }} >
                <Button onPress={() => {
                    if (this.state.tabNumber != 1) {
                        this.setState({ tabNumber: 1 })
                        this.newOrders(true, 1)
                    }



                }} style={{ flexDirection: isRTL ? 'row-reverse' : 'row', marginLeft: 1,  flex: 1, height: responsiveHeight(8), justifyContent: 'center', alignItems: 'center', alignSelf: isRTL ? 'flex-start' : 'flex-end', backgroundColor: tabNumber == 1 ? colors.white : colors.lightGreen }}>
                    <Text style={{ color: tabNumber == 1 ? colors.darkGray : colors.white, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.orders}</Text>
                    <View style={{ marginHorizontal: moderateScale(2), justifyContent: 'center', alignItems: 'center', width: 18, height: 18, borderRadius: 9, backgroundColor: 'red' }}>
                        <Text style={{ color: colors.white, fontSize: responsiveFontSize(4) }} >{ordersCount}</Text>
                    </View>
                </Button>

                <Button onPress={() => {
                    if (this.state.tabNumber != 2) {
                        this.setState({ tabNumber: 2 })
                        this.confirmedOrders(true, 1)
                    }


                }} style={{ marginRight: 1,  flex: 1, height: responsiveHeight(8), justifyContent: 'center', alignItems: 'center', alignSelf: isRTL ? 'flex-start' : 'flex-end', backgroundColor: tabNumber == 2 ? colors.white : colors.lightGreen }}>
                    <Text style={{ color: tabNumber == 2 ? colors.darkGray : colors.white, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.ordersInProgress}</Text>
                </Button>

            </View>
        )
    }

    newOrderCard = (data) => {
        const { categoryName, isRTL } = this.props;
        
        return (
            <TouchableOpacity onPress={()=>{
                push('DriverOrderDetails',data)
            }} >
                <Animatable.View
                    style={{ marginBottom: moderateScale(5), padding: moderateScale(3), flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', alignSelf: 'center', width: responsiveWidth(100) }}
                    animation={"flipInX"}
                    duration={1000}>
                    <FastImage source={require('../../assets/imgs/profileicon.jpg')} style={{width:responsiveWidth(17), height:responsiveWidth(17), borderRadius:responsiveWidth(8.5) }}  />

                    <View style={{width:responsiveWidth(45), marginHorizontal: moderateScale(5), }}>
                        <Text style={{textAlign:isRTL?'right':'left', color: colors.grayColor1, fontFamily: isRTL ? arrabicFont : englishFont }}><Text style={{ fontFamily: boldFont }}>{Strings.outletName}:</Text> {isRTL? data.restaurant.restaurantName_ar: data.restaurant.restaurantName}</Text>
                        <Text style={{textAlign:isRTL?'right':'left', color: colors.grayColor1, fontFamily: isRTL ? arrabicFont : englishFont }}><Text style={{ fontFamily: boldFont }}>{Strings.area}:</Text> {isRTL?data.restaurant.branches[0].area.arabicAreaName:data.restaurant.branches[0].area.areaName}</Text>
                        <Text style={{textAlign:isRTL?'right':'left', color: colors.grayColor1, fontFamily: isRTL ? arrabicFont : englishFont }}><Text style={{ fontFamily: boldFont }}>{Strings.phone}:</Text>{data.restaurant.phone}</Text>
                        <Text style={{textAlign:isRTL?'right':'left', color: colors.grayColor1, fontFamily: isRTL ? arrabicFont : englishFont }}><Text style={{ fontFamily: boldFont }}>{Strings.address}:</Text>{data.restaurant.branches[data.branch].address}</Text>
                        <Text style={{textAlign:isRTL?'right':'left', color: colors.grayColor1, fontFamily: isRTL ? arrabicFont : englishFont }}><Text style={{ fontFamily: boldFont }}>{Strings.date}:</Text>{data.date}</Text>
                    </View>

                    <View style={{justifyContent:'center',alignItems:'center', marginHorizontal: moderateScale(2),width:responsiveWidth(22) }}>
                        <Text style={{ color: colors.grayColor1, fontFamily: isRTL ? arrabicFont : englishFont, textAlign:'center' }}>{data.average} {Strings.Kg}</Text>
                        <Text style={{ color: colors.grayColor1, fontFamily: isRTL ? arrabicFont : englishFont, textAlign:'center' }}>{data.restaurant.price*data.average} {Strings.EGP}</Text>
                        <TouchableOpacity style={{alignSelf:'center'}} onPress={() => {
                            //this.acceptOrder(data)
                            this.setState({showModal:true})
                        }}>
                            <FastImage style={{width:20,height:20}} source={require('../../assets/imgs/home-driverlocation-icon.png')} />
                        </TouchableOpacity>
                    </View>

                </Animatable.View>
            </TouchableOpacity>

        )
    }

    confirmedOrderCard = (data) => {
        const { categoryName, isRTL } = this.props;

        return (
            <DriverConfirmedOrderCard data={data} />
        )
    }


    newOrderPage = () => {
        const { newOrders404, newOrdersLoading, newOrders, newOrdersRefresh, newOrdersPages } = this.state
        return (
            <>
                {newOrders404 ?
                    <NetworError />
                    :
                    newOrdersLoading ?
                        <Loading />
                        :
                        newOrders.length > 0 ?
                            <FlatList
                                showsVerticalScrollIndicator={false}
                                style={{ marginBottom: moderateScale(10), paddingTop: moderateScale(10) }}
                                data={newOrders}
                                renderItem={({ item }) => this.newOrderCard(item)}
                                onEndReachedThreshold={.5}
                                onEndReached={() => {
                                    if (this.newOrdersPage <= newOrdersPages) {
                                        this.newOrdersPage = this.newOrdersPage + 1;
                                        this.newOrders(false, this.newOrdersPage)
                                        console.log('page  ', this.newOrdersPage)
                                    }
                                }}
                                refreshControl={
                                    <RefreshControl
                                        //colors={["#B7ED03",colors.darkBlue]} 
                                        refreshing={newOrdersRefresh}
                                        onRefresh={() => {
                                            this.newOrdersPage = 1
                                            this.newOrders(true, 1)
                                        }}
                                    />
                                }

                            />
                            :
                            <NoData />}
            </>
        )
    }

    confirmedOrderPage = () => {
        const { confirmedOrders, confirmedOrders404, confirmedOrdersLoading, confirmedOrdersRefresh, confirmedOrdersPages } = this.state
        return (
            <>
                {confirmedOrders404 ?
                    <NetworError />
                    :
                    confirmedOrdersLoading ?
                        <Loading />
                        :
                        confirmedOrders.length > 0 ?
                            <FlatList
                                showsVerticalScrollIndicator={false}
                                style={{ marginBottom: moderateScale(10), paddingTop: moderateScale(10) }}
                                data={confirmedOrders}
                                renderItem={({ item }) => this.confirmedOrderCard(item)}
                                onEndReachedThreshold={.5}
                                onEndReached={() => {
                                    if (this.confirmedOrdersPage <= confirmedOrdersPages) {
                                        this.confirmedOrdersPage = this.confirmedOrdersPage + 1;
                                        this.confirmedOrders(false, this.confirmedOrdersPage)
                                        console.log('page  ', this.confirmedOrdersPage)
                                    }
                                }}
                                refreshControl={
                                    <RefreshControl
                                        //colors={["#B7ED03",colors.darkBlue]} 
                                        refreshing={confirmedOrdersRefresh}
                                        onRefresh={() => {
                                            this.confirmedOrdersPage = 1
                                            this.confirmedOrders(true, 1)
                                        }}
                                    />
                                }
                            />
                            :
                            <NoData />}
            </>
        )
    }

    showMap = () => (

        <Modal
            visible={this.state.showModal}
            animationType='slide'
            onRequestClose={() => {
                this.setState({ showModal: false })
                this.props.OrdersCount(this.props.currentUser.user.id, 'ASSIGNED')
            }}
        >
            <View style={{ flex: 1 }}>
                <View style={{ alignSelf: 'center', width: responsiveWidth(96), marginTop: moderateScale(5), flexDirection: this.props.isRTL ? 'row-reverse' : 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                    <Text style={{ fontSize: responsiveFontSize(10), color: colors.grayColor1, fontFamily: this.props.isRTL ? arrabicFont : englishFont }}>{this.state.ResturantOnMap}</Text>
                    <TouchableOpacity 
                    style={{ alignSelf: 'flex-end', margin: moderateScale(7) }} 
                    onPress={() => {
                        this.setState({ showModal: false })
                        this.props.OrdersCount(this.props.currentUser.user.id, 'ASSIGNED')
                    }}>
                        <Icon name='close' type='AntDesign' style={{ color: colors.darkGreen, alignSelf: 'center' }} />
                    </TouchableOpacity>
                </View>
                <MapView
                    style={{ alignSelf: 'center', width: responsiveWidth(100), height: responsiveHeight(75), marginVertical: moderateScale(5) }}
                    region={this.state.location}
                >
                    <Marker
                        coordinate={this.state.location}
                    //image={require('../assets/imgs/marker.png')}
                    />
                </MapView>

                <TouchableOpacity
                    style={{ width: responsiveWidth(60), height: responsiveHeight(8), alignSelf: 'center', alignItems: 'center', justifyContent: 'center', backgroundColor: colors.medimGreen, borderRadius: moderateScale(5) }}
                    onPress={() => {
                        this.props.OrdersCount(this.props.currentUser.user.id, 'ASSIGNED')
                        this.openMap(this.state.ResturantOnMap, this.state.location.latitude, this.state.location.longitude)
                    }}>
                    <Text style={{ fontSize: responsiveFontSize(6), color: 'white', textAlign: 'center' }}>{Strings.goToLocation}</Text>
                </TouchableOpacity>

            </View>

        </Modal>
    )

    render() {
        const { categoryName, isRTL } = this.props;
        const { tabNumber } = this.state;
        return (
            <LinearGradient
            colors={[colors.white,colors.white,colors.white]} 
            style={{ flex: 1 }}
            >
                <Header  title={Strings.orders} />  

                <View style={{flex:1, marginBottom: moderateScale(20), height: responsiveHeight(73), backgroundColor: colors.white, marginTop: moderateScale(0), width: responsiveWidth(100), marginBottom:responsiveHeight(7) }} >
                    {this.tabs()}
                    {tabNumber == 1 && this.newOrderPage()}
                    {tabNumber == 2 && this.confirmedOrderPage()}

                </View>

                {this.showMap()}
                <AppFooter />

            </LinearGradient>
        );
    }
}

const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    barColor: state.lang.color,
    currentUser: state.auth.currentUser,
    ordersCount: state.orders.ordersCount,
    isChecking: state.checking.isChecking
})

const mapDispatchToProps = {
    removeItem,
    OrdersCount
}

export default connect(mapStateToProps, mapDispatchToProps)(DriverOrders);
