import React, { Component } from 'react';
import {
    View, TouchableOpacity, Image, Text, ScrollView, TextInput, Alert, Platform
} from 'react-native';
import { connect } from 'react-redux';
import { Icon, Button } from 'native-base';
import { login } from '../../actions/AuthActions'
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../../utils/responsiveDimensions";
import Strings from '../../assets/strings';
import LoadingDialogOverlay from '../../components/LoadingDialogOverlay';
import { enableSideMenu, resetTo, push, pop } from '../../controlls/NavigationControll'
import { arrabicFont, englishFont } from '../../common/AppFont'
import * as Animatable from 'react-native-animatable';
import * as colors from '../../assets/colors';
import { removeItem } from '../../actions/MenuActions';
import CollaspeAppHeader from '../../common/CollaspeAppHeader'
import LinearGradient from 'react-native-linear-gradient';
import FastImage from 'react-native-fast-image'
import CommanHeader from '../../common/CommanHeader'
import ImagePicker from 'react-native-image-crop-picker';
import AppFooter from '../../components/AppFooter'
import io from 'socket.io-client';
import axios from 'axios';
import { BASE_END_POINT } from '../../AppConfig';
import Dialog, { DialogContent, DialogTitle } from 'react-native-popup-dialog';
import { RNToasty } from 'react-native-toasty';
import { whiteButton, greenButton } from '../../assets/styles'
import { OrdersCount } from '../../actions/OrdersActions';
import Header from '../../common/Header'


class DriverOrderDetails extends Component {

    state = {
        loading: false,
    }

    componentDidMount() {
        enableSideMenu(false, null)
        console.log('Data : ', this.props.data)
    }

    basicsData = () => {
        const { isRTL, data } = this.props
        console.log('Area : ', data)
        return (
            <View style={{ marginTop: moderateScale(10), justifyContent: 'center', paddingHorizontal: moderateScale(10) }}>

                <Text style={{ color: 'black', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(8), borderBottomWidth: 0.3, borderBottomColor: colors.darkGray, paddingBottom: moderateScale(2), textAlign: isRTL ? 'right' : 'left' }} >{Strings.restaurantDetails}</Text>

                <Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), marginTop: moderateScale(5), textAlign: isRTL ? 'right' : 'left' }} >{Strings.name}: {isRTL ? data.restaurant.restaurantName_ar : data.restaurant.restaurantName}</Text>
                <Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), marginTop: moderateScale(5), textAlign: isRTL ? 'right' : 'left' }} >{Strings.phoneNo}: {data.restaurant.phone}</Text>
                <Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), marginTop: moderateScale(5), textAlign: isRTL ? 'right' : 'left' }} >{Strings.orderQt}: {data.average} {Strings.Kg}</Text>
                <Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), marginTop: moderateScale(5), textAlign: isRTL ? 'right' : 'left' }} >{Strings.area}: {isRTL ? data.restaurant.branches[0].area.arabicAreaName : data.restaurant.branches[0].area.areaName}</Text>


                {/*  */}

                <Text style={{ color: 'black', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(8), borderBottomWidth: 0.3, borderBottomColor: colors.darkGray, paddingBottom: moderateScale(2), textAlign: isRTL ? 'right' : 'left', marginTop: moderateScale(8) }} >{Strings.orderDetails}</Text>

                <Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), marginTop: moderateScale(5), textAlign: isRTL ? 'right' : 'left' }} >{Strings.orderQt}: {data.average} {Strings.Kg}</Text>
                <Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), marginTop: moderateScale(5), textAlign: isRTL ? 'right' : 'left' }} >{Strings.price}: {data.restaurant.price * data.average} {Strings.EGP}</Text>





                <Text style={{ color: 'black', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(8), borderBottomWidth: 0.3, borderBottomColor: colors.darkGray, paddingBottom: moderateScale(2), marginTop: moderateScale(12), textAlign: isRTL ? 'right' : 'left' }} >{Strings.contactDetails}</Text>

                <Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), marginTop: moderateScale(5), textAlign: isRTL ? 'right' : 'left' }} >{Strings.contactPerson}: {data.restaurant.firstname} {data.restaurant.lastname}</Text>
                <Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), marginTop: moderateScale(5), textAlign: isRTL ? 'right' : 'left' }} >{Strings.phoneNo}: {data.restaurant.phone}</Text>

            </View>
        )
    }

    acceptOrder() {
        const { currentUser, data } = this.props
        this.setState({ loading: true })
        axios.put(`${BASE_END_POINT}orders/${data.id}/accept`, null, {
            headers: {
                "Authorization": `Bearer ${currentUser.token}`,
                "Content-Type": "application/json"
            }
        })
            .then(response => {
                this.setState({ loading: false })
                resetTo('DriverHome')
                this.props.OrdersCount(this.props.currentUser.user.id, 'ASSIGNED')
            })
            .catch(error => {
                this.setState({ loading: false })
                console.log('ERROR   ', error)
            })

    }

    edit_delete_button = () => {
        const { isRTL, data } = this.props
        return (
            <View style={{ alignSelf: 'center', justifyContent: 'space-between', flexDirection: isRTL ? 'row-reverse' : 'row', marginTop: moderateScale(10), marginBottom: moderateScale(20), alignItems: 'center' }} >

                <TouchableOpacity onPress={() => {
                    this.acceptOrder()
                }} style={whiteButton} >
                    <Text style={{ paddingHorizontal: moderateScale(5), color: colors.green, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.acceptOrder}</Text>
                </TouchableOpacity>



            </View>
        )
    }




    render() {
        const { loading } = this.state
        const { isRTL, userToken } = this.props;
        return (
            <LinearGradient
                colors={[colors.white, colors.white, colors.white]}
                style={{ flex: 1 }}
            >
                <Header title={Strings.orderDetails} />

                <ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: colors.white, borderTopRightRadius: moderateScale(20), borderTopLeftRadius: moderateScale(20), marginTop: moderateScale(15), width: responsiveWidth(100), marginBottom: moderateScale(20) }} >
                    {this.basicsData()}
                    {this.edit_delete_button()}
                </ScrollView>
                {loading && <LoadingDialogOverlay title={Strings.wait} />}
                <AppFooter />
            </LinearGradient>
        );
    }
}
const mapDispatchToProps = {
    login,
    removeItem,
    OrdersCount
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
    //userToken: state.auth.userToken,
})


export default connect(mapToStateProps, mapDispatchToProps)(DriverOrderDetails);

