import React, { Component } from 'react';
import { View, RefreshControl, Alert, ScrollView, FlatList, Text, TouchableOpacity, Image, } from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../../utils/responsiveDimensions';
import { connect } from "react-redux";
import Strings from '../../assets/strings';

import axios from 'axios';
import { BASE_END_POINT } from '../../AppConfig';
import ListFooter from '../../components/ListFooter';
import { Icon, Thumbnail, Button } from 'native-base'
import { selectMenu, removeItem } from '../../actions/MenuActions';
import { OrdersCount } from '../../actions/OrdersActions';
import { enableSideMenu, pop, push } from '../../controlls/NavigationControll'
import NetInfo from "@react-native-community/netinfo";
import Loading from "../../common/Loading"
import NetworError from '../../common/NetworError'
import NoData from '../../common/NoData'
import * as colors from '../../assets/colors'
import { arrabicFont, englishFont } from '../../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image'
import NotificationCard from '../../components/NotificationCard'
import PurchasingAccountsCard from '../../components/PurchasingAccountsCard';
import CollaspeAppHeader from '../../common/CollaspeAppHeader'
import LinearGradient from 'react-native-linear-gradient';
import MainHeader from '../../common/MainHeader'
import Carousel from 'react-native-snap-carousel';
import BrandCard from '../../components/BrandCard'
import AppFooter from '../../components/AppFooter'
import Geolocation from '@react-native-community/geolocation';
import { getUnreadNotificationsCount } from '../../actions/NotificationAction'
import {
  checkFirbaseNotificationPermision,
  getFirebaseNotificationToken,
  showFirebaseNotifcation,
  clickOnFirebaseNotification
} from '../../controlls/FirebasePushNotificationControll'
import GetLocation from 'react-native-get-location'
import {setFirebaseToken} from '../../actions/NotificationAction'
import Header from '../../common/Header'
import {whiteButton, greenButton } from '../../assets/styles'
import {checking} from '../../actions/CheckingAction'
import AsyncStorage from '@react-native-community/async-storage'
import LoadingDialogOverlay from '../../components/LoadingDialogOverlay';
import { RNToasty } from 'react-native-toasty';

class DriverHome extends Component {

  page = 1;
  state = {
    networkError: null,
    showAlert: false,
    location: null,
    orderCounts: 0,
    loading:false
  }


  componentDidMount() {
    enableSideMenu(true, this.props.isRTL)
    checkFirbaseNotificationPermision()
    getFirebaseNotificationToken(this.props.currentUser.token,this.props.setFirebaseToken)
    this.props.getUnreadNotificationsCount(this.props.currentUser.token)
    this.props.OrdersCount(this.props.currentUser.user.id, 'ASSIGNED', true)
    this.getCurrentLocation()
    setInterval(() => {
      this.getCurrentLocation()
    }, 10000)
    this.checkInCkeckoututtons()
  }

  getCurrentLocation = () => {

    GetLocation.getCurrentPosition({
      enableHighAccuracy: true,
      timeout: 200000,
    })
      .then(location => {
        this.sendLocation(location.latitude, location.longitude)
      })
      .catch(error => {
        const { code, message } = error;
       // console.warn(code, message);
      })
  }

  ckeckInCheckOut = async () => {
    const c = await AsyncStorage.getItem('@CHECKING')
    this.props.checking(c=="true"?true:false)
  }

  ckeckIn =  () => {
    this.setState({loading:true})
    axios.put(`${BASE_END_POINT}${this.props.currentUser.user.id}/start`,{},{
      headers: {
          "Authorization": `Bearer ${this.props.currentUser.token}`,
          "Content-Type": "application/json"
      }
    })
    .then(response=>{
      this.setState({loading:false})
      AsyncStorage.setItem('@CHECKING','true')
      this.props.checking(true)
      RNToasty.Success({title:Strings.attendanceHasBeenRegistered})
    })
    .catch(error=>{
      this.setState({loading:false})
      console.log("CHECK ERROR  ",error.response)
    })
    
  }

  ckeckOut =  () => {
    this.setState({loading:true})
    axios.put(`${BASE_END_POINT}${this.props.currentUser.user.id}/end`,{},{
      headers: {
          "Authorization": `Bearer ${this.props.currentUser.token}`,
          "Content-Type": "application/json"
      }
    })
    .then(response=>{
      console.log('DOne   ',response)
      this.setState({loading:false})
      AsyncStorage.setItem('@CHECKING','false')
      this.props.checking(false)
      RNToasty.Success({title:Strings.departureHasBeenRegistered})
    })
    .catch(error=>{
      this.setState({loading:false})
      console.log("CHECK ERROR  ",error.response)
      RNToasty.Success({title:Strings.done})
    })
    
  }

  

  sendLocation(latitude, longitude) {
    const data = { "location": [latitude, longitude] }
    axios.put(`${BASE_END_POINT}${this.props.currentUser.user.id}/tracking`, JSON.stringify(data), {
      headers: {
        "Content-Type": 'application/json',
        "Authorization": `Bearer ${this.props.currentUser.token}`
      }
    })
      .then(response => {})
      .catch(error => { })
  }

  buttons = () => {
    const { isRTL, ordersCount } = this.props
    const { orderCounts } = this.state
    return (
      <View style={{flexDirection:isRTL?'row-reverse':'row',  marginTop: moderateScale(10), alignSelf: 'center', justifyContent: 'space-between', alignItems: 'center',}}>


        <TouchableOpacity onPress={() => push('DriverOrders')} style={{ width: responsiveWidth(46), justifyContent: 'center', alignItems: 'center', marginTop: moderateScale(5)}}>
          <FastImage source={require('../../assets/imgs/home-orders-icon.png')} style={{  width: 80, height: 80, borderRadius: 40 }} />
          <View style={{ position: 'absolute', bottom: moderateScale(15), zIndex: 10000, elevation: 2, shadowOffset: { height: 0, width: 2 }, alignSelf: isRTL ? 'flex-start' : 'flex-end', width: 30, height: 30, borderRadius: 15, backgroundColor: '#e20000', justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ color: colors.white, fontFamily: englishFont, fontSize: responsiveFontSize(8) }} >{ordersCount}</Text>
          </View>
          <Text style={{ color: colors.black, marginTop: moderateScale(4), fontSize: responsiveFontSize(7), fontFamily: isRTL ? arrabicFont : englishFont, textAlign: 'center' }}>{Strings.orders}</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => push('DriverCompletedOrders')} style={{ width: responsiveWidth(46), justifyContent: 'center', alignItems: 'center', marginTop: moderateScale(5)  }}>
          <FastImage source={require('../../assets/imgs/home-orders-icon.png')} style={{ width: 80, height: 80, borderRadius: 40 }} />
          <Text style={{ color: colors.black, marginTop: moderateScale(4), fontSize: responsiveFontSize(7), fontFamily: isRTL ? arrabicFont : englishFont, textAlign: 'center' }}>{Strings.completedOrders}</Text>
        </TouchableOpacity>

      </View>
    )
  }

  checkInCkeckoututtons = () => {
    const {isRTL,isChecking} = this.props
    return(
        <View style={{alignSelf:'center',width:responsiveWidth(80),justifyContent:'space-between', flexDirection: isRTL?'row-reverse':'row' ,marginTop:moderateScale(30),marginBottom:moderateScale(20),alignItems:'center'}} >
            
            <TouchableOpacity 
            disabled={isChecking==false?false:true}
            onPress={()=>{
              this.ckeckIn()
            }} 
            style={whiteButton} 
            >
                <Text style={{paddingHorizontal:moderateScale(5), color:colors.green,fontFamily:isRTL?arrabicFont:englishFont}}>{Strings.checkin}</Text>
            </TouchableOpacity>

            <TouchableOpacity 
            disabled={isChecking==true?false:true}
            onPress={()=>{
              this.ckeckOut()
            }} 
            style={greenButton} 
            >
                <Text style={{paddingHorizontal:moderateScale(5), color:colors.white,fontFamily:isRTL?arrabicFont:englishFont}}>{Strings.checkout}</Text>
            </TouchableOpacity>

            </View>
    )
}


  render() {
    const { categoryName, isRTL } = this.props;
    return (
      <View style={{ flex: 1 }}>
        <Header menu title={Strings.home} green/>
        <ScrollView showsVerticalScrollIndicator={false}>
          {this.checkInCkeckoututtons()}
          {this.buttons()}
        </ScrollView>
        {this.state.loading&&<LoadingDialogOverlay title={Strings.wait} />}
        <AppFooter />
      </View>
    );
  }
}

const mapStateToProps = state => ({
  isRTL: state.lang.RTL,
  barColor: state.lang.color,
  currentUser: state.auth.currentUser,
  ordersCount: state.orders.ordersCount,
  isChecking: state.checking.isChecking
})

const mapDispatchToProps = {
  removeItem,
  OrdersCount,
  getUnreadNotificationsCount,
  setFirebaseToken,
  checking
}

export default connect(mapStateToProps, mapDispatchToProps)(DriverHome);
