import React, { Component } from 'react';
import { View, RefreshControl, Alert, ScrollView,SafeAreaView , FlatList, Text,Platform, TouchableOpacity, TextInput } from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../../utils/responsiveDimensions';
import { connect } from "react-redux";
import Strings from '../../assets/strings';

import axios from 'axios';
import { BASE_END_POINT } from '../../AppConfig';
import ListFooter from '../../components/ListFooter';
import { Icon, Thumbnail, Button } from 'native-base'
import { selectMenu, removeItem } from '../../actions/MenuActions';
import { enableSideMenu, resetTo, pop } from '../../controlls/NavigationControll'
import NetInfo from "@react-native-community/netinfo";
import Loading from "../../common/Loading"
import NetworError from '../../common/NetworError'
import NoData from '../../common/NoData'
import * as colors from '../../assets/colors'
import { arrabicFont, englishFont } from '../../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image'
import NotificationCard from '../../components/NotificationCard'
import PurchasingAccountsCard from '../../components/PurchasingAccountsCard';
import CollaspeAppHeader from '../../common/CollaspeAppHeader'
import LoadingDialogOverlay from '../../components/LoadingDialogOverlay';
import LinearGradient from 'react-native-linear-gradient';
import CommanHeader from '../../common/CommanHeader'
import AppFooter from '../../components/AppFooter'
import { RNToasty } from 'react-native-toasty';
import { SpinPicker } from 'react-native-spin-picker'
import { TextInputMask } from 'react-native-masked-text'
import {whiteButton} from '../../assets/styles'
import Header from '../../common/Header'
import RNPickerSelect from 'react-native-picker-select';
import AppCheckBox from '../../common/AppCheckBox'

class DriverUpdateOrder extends Component {
    
    
    reasons = [
        { label:Strings.propblem1, value:Strings.propblem1 },
        { label:Strings.propblem2 , value:Strings.propblem2 },
        { label:Strings.propblem3 , value:Strings.propblem3 },
        { label:Strings.propblem4, value:Strings.propblem4 },
        { label:Strings.propblem5 , value:Strings.propblem5 },
        { label:Strings.propblem6 , value:Strings.propblem6 },
    ]

    state = {
       // kg: this.props.data.kg ? this.props.data.kg.toString() : ' ',
       kg:0,
        totalPrice: this.props.data.totalPrice ? this.props.data.totalPrice.toString() : ' ',
        enablePriceInput: false,
        enableAvgKgInput: false,
        loading: false,
        amount:0,
        selectedItem1: 0,
        selectedItem2: 0,
        selectedItem3: 0,
        selectedItem4: 0,
        selectedItem5: 0,
        selectedKg: '0',
        reason:' ',
        enableReason:false,
    }

    componentDidMount() {
        enableSideMenu(false, null)
        console.log('frffr: ', this.props.data)
    }

    finishOrder() {
        const { currentUser,data } = this.props
        this.setState({ loading: true })
        axios.put(`${BASE_END_POINT}orders/${data.id}/finish`, null, {
            headers: {
                "Authorization": `Bearer ${currentUser.token}`,
                "Content-Type": "application/json"
            }
        })
            .then(response => {
                console.log('DONE')
                this.setState({ loading: false, delet: true })
                RNToasty.Success({ title: Strings.orderFinishedSuccessfuly })
                if (currentUser.user.type == 'DRIVER') { resetTo('DriverHome') }
                else if (currentUser.user.type == 'ADMIN') { resetTo('AdminHome') }
                else if (currentUser.user.type == 'OPERATION') { resetTo('OperationHome') }
            })
            .catch(error => {
                this.setState({ loading: false })
                console.log('ERROR   ', error.response)
                RNToasty.Error({ title: Strings.errorInUpdate })
            })

    }

    updateOrder() {
        const { currentUser } = this.props
        const { kg, totalPrice } = this.state
        if (kg==0) {
            RNToasty.Warn({title:Strings.selectedKg})
        }

        if (kg>0) {
            this.setState({ loading: true })
            const data = {
                restaurant: this.props.data.restaurant.id,
                kg: kg,
                totalPrice: (this.props.data.restaurant.price * kg).toString(),
                priceForKg: this.props.data.restaurant.price,
                average: this.props.data.average,
                //date: 'Monday',
            }

            axios.put(`${BASE_END_POINT}orders/${this.props.data.id}`, JSON.stringify(data), {
                headers: {
                    "Authorization": `Bearer ${currentUser.token}`,
                    "Content-Type": "application/json"
                }
            })
                .then(response => {
                    this.finishOrder()
                    RNToasty.Success({ title: Strings.orderUpdatedSuccessfuly })
                })
                .catch(error => {
                    this.setState({ loading: false })
                    console.log('ERROR   ', error.response)
                    Alert.alert("" + error)
                    RNToasty.Error({ title: Strings.errorInUpdate })
                })
        }
    }

    sendProblem() {
        const { currentUser } = this.props
        const { reason } = this.state
        if (!reason.replace(/\s/g, '').length) {
            this.setState({reason:''})
        }

        if (reason.replace(/\s/g, '').length) {
            this.setState({ loading: true })
            const data = {
               reason:reason,
            }
            axios.put(`${BASE_END_POINT}orders/${this.props.data.id}/problem`, JSON.stringify(data), {
                headers: {
                    "Authorization": `Bearer ${currentUser.token}`,
                    "Content-Type": "application/json"
                }
            })
            .then(response => {
                resetTo('DriverHome')
                RNToasty.Success({ title: Strings.yourProblemSubmited })
            })
            .catch(error => {
                this.setState({ loading: false })
                console.log('ERROR   ', error.response)
            })
        }
    }

    resturantName = () => {
        const { isRTL, data } = this.props
        return (
            <View style={{ justifyContent: 'center', marginTop: moderateScale(10), alignSelf: 'center', width: responsiveWidth(80), height: responsiveHeight(5), borderRadius: moderateScale(1), backgroundColor: colors.lightGray }}>
                <Text style={{ marginHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}>{isRTL ? data.restaurant.restaurantName_ar: data.restaurant.restaurantName}</Text>
            </View>
        )
    }

    reasonsPicker = () => {
        const { isRTL } = this.props
        const { reason } = this.state
        return (
            <View style={{marginTop:moderateScale(10), width: responsiveWidth(80), alignSelf: 'center' }} >
                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.problems}</Text>

                    <View style={{ alignItems: 'center', justifyContent: 'center', backgroundColor: colors.lightGray, width: responsiveWidth(80), height: responsiveHeight(6), alignSelf: 'center' }}>
                        <RNPickerSelect
                            onValueChange={(item) => {
                                console.log("reason  ",item)
                                this.setState({reason: item,});
                            }}
                            items={this.reasons}
                            placeholder={{ label:'', value:'' }}
                            style={{
                                inputIOS: { textAlign: isRTL ? 'right' : 'left', fontSize: responsiveFontSize(6), height: responsiveHeight(6), fontFamily: isRTL ? arrabicFont : englishFont, paddingVertical: 9, paddingHorizontal: moderateScale(3), paddingRight: responsiveWidth(5), color: 'gray', },
                                inputAndroid: { textAlign: isRTL ? 'right' : 'left', fontSize: responsiveFontSize(6), height: responsiveHeight(6), fontFamily: isRTL ? arrabicFont : englishFont, paddingHorizontal: moderateScale(3), paddingRight: responsiveWidth(5), paddingVertical: 8, color: 'gray', }
                            }}
                            placeholderTextColor={'gray'}
                            Icon={() => { return (<Icon name='down' type='AntDesign' style={{ color: 'black', fontSize: responsiveFontSize(6), top: responsiveHeight(2), right: responsiveWidth(1) }} />) }}
                        />

                    </View>
                    
                    {reason.length == 0 &&
                        <Text style={{ color: 'red', marginTop: moderateScale(1), marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                    }
            </View>
        )
    }

    reasonCheckBox = () =>{
        const {enableReason}= this.state
        return(
            <View style={{alignSelf:'center', width:responsiveWidth(80),marginTop:moderateScale(10)}} >
                <AppCheckBox 
                title={Strings.iCantCollectOrder}
                check={this.state.enableReason}
                onPress={()=>this.setState({enableReason:!this.state.enableReason})}
                textColor='black'
                boxColor={colors.darkGreen}
                style={{alignSelf:this.props.isRTL?'flex-end':'flex-start'}}
                />
            </View>
        )
    }


    spinKg = () => {
        const { isRTL } = this.props
        const { selectedItem1, selectedItem2, selectedItem3, selectedItem4, selectedItem5 } = this.state
        return (
            <View style={{ width: responsiveWidth(20), alignSelf: 'center', marginTop: moderateScale(0), justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(10), alignSelf: 'center', marginTop: moderateScale(10), color: colors.green }} >{Strings.Kg}*</Text>
                <View style={{ flexDirection: 'row', width: responsiveWidth(20), alignSelf: 'center', marginTop: moderateScale(0), justifyContent: 'center', alignItems: 'center' }}>
                    <View style={{ alignSelf: 'center', marginTop: moderateScale(0), justifyContent: 'center', alignItems: 'center' }}>
                        <SpinPicker
                            data={[...Array(10).keys()]}
                            value={this.state.selectedItem1}
                            onValueChange={selectedItem => this.setState({ selectedItem1: selectedItem, kg: parseFloat(selectedItem1 + '' + selectedItem2 + '' + selectedItem3 + '' + selectedItem4 + '.' + selectedItem5) })}
                            keyExtractor={number => number.toString()}
                            //showArrows={Platform.OS=='android'?true:false}
                            onInputValueChanged={this.onValueChanged}
                            renderItem={item => <Text style={{color: colors.darkGray, textAlign: 'center', width: responsiveWidth(14), height: responsiveHeight(6), lineHeight: responsiveHeight(6), fontSize: responsiveFontSize(8) }}>{item.toString()}</Text>} />
                    </View>
                    <View style={{ alignSelf: 'center', marginTop: moderateScale(0), justifyContent: 'center', alignItems: 'center' }}>
                        <SpinPicker
                            data={[...Array(10).keys()]}
                            value={this.state.selectedItem2}
                            onValueChange={selectedItem => this.setState({ selectedItem2: selectedItem, kg: parseFloat(selectedItem1 + '' + selectedItem2 + '' + selectedItem3 + '' + selectedItem4 + '.' + selectedItem5) })}
                            keyExtractor={number => number.toString()}
                            //showArrows={Platform.OS=='android'?true:false}
                            onInputValueChanged={this.onValueChanged}
                            renderItem={item => <Text style={{color: colors.darkGray, textAlign: 'center', width: responsiveWidth(14), height: responsiveHeight(6), lineHeight: responsiveHeight(6), fontSize: responsiveFontSize(8) }}>{item.toString()}</Text>} />
                    </View>
                    <View style={{ alignSelf: 'center', marginTop: moderateScale(0), justifyContent: 'center', alignItems: 'center' }}>
                        <SpinPicker
                            data={[...Array(10).keys()]}
                            value={this.state.selectedItem3}
                            onValueChange={selectedItem => this.setState({ selectedItem3: selectedItem, kg: parseFloat(selectedItem1 + '' + selectedItem2 + '' + selectedItem3 + '' + selectedItem4 + '.' + selectedItem5) })}
                            keyExtractor={number => number.toString()}
                            //showArrows={Platform.OS=='android'?true:false}
                            onInputValueChanged={this.onValueChanged}
                            renderItem={item => <Text style={{color: colors.darkGray, textAlign: 'center', width: responsiveWidth(14), height: responsiveHeight(6), lineHeight: responsiveHeight(6), fontSize: responsiveFontSize(8) }}>{item.toString()}</Text>} />
                    </View>
                    <View style={{ alignSelf: 'center', marginTop: moderateScale(0), justifyContent: 'center', alignItems: 'center' }}>
                        <SpinPicker
                            data={[...Array(10).keys()]}
                            value={this.state.selectedItem4}
                            onValueChange={selectedItem => this.setState({ selectedItem4: selectedItem, kg: parseFloat(selectedItem1 + '' + selectedItem2 + '' + selectedItem3 + '' + selectedItem4 + '.' + selectedItem5) })}
                            keyExtractor={number => number.toString()}
                            //showArrows={Platform.OS=='android'?true:false}
                            onInputValueChanged={this.onValueChanged}
                            renderItem={item => <Text style={{color: colors.darkGray, textAlign: 'center', width: responsiveWidth(14), height: responsiveHeight(6), lineHeight: responsiveHeight(6), fontSize: responsiveFontSize(8) }}>{item.toString()}</Text>} />
                    </View>

                   <View style={{borderTopWidth:0.7, borderBottomWidth:0.7, borderColor:'black',  marginTop: moderateScale(0)}}>
                   <Text style={{color: colors.darkGray,height:responsiveHeight(6), lineHeight:responsiveHeight(7), fontSize:responsiveFontSize(9) }}>.</Text>
                   </View>
                    
                    <View style={{ alignSelf: 'center', marginTop: moderateScale(0), justifyContent: 'center', alignItems: 'center' }}>
                    
                        <SpinPicker
                            data={[...Array(10).keys()]}
                            value={this.state.selectedItem5}
                            onValueChange={selectedItem => this.setState({ selectedItem5: selectedItem, kg: parseFloat(selectedItem1 + '' + selectedItem2 + '' + selectedItem3 + '' + selectedItem4 + '.' + selectedItem5) })}
                            keyExtractor={number => number.toString()}
                            //showArrows={Platform.OS=='android'?true:false}
                            onInputValueChanged={this.onValueChanged}
                            renderItem={item => <Text style={{color: colors.darkGray, textAlign: 'center', width: responsiveWidth(14), height: responsiveHeight(6), lineHeight: responsiveHeight(6), fontSize: responsiveFontSize(8) }}>{item.toString()}</Text>} />
                    </View>
                </View>
            </View>
        )
    }

    estimatedPrice = () => {
        const { isRTL, data } = this.props
        const { selectedResturantPrice, kg } = this.state
        console.log(parseFloat(data.restaurant.price * kg))
        
        return (
            <View style={{ width: responsiveWidth(80), alignSelf: 'center', marginTop: moderateScale(5) }}>
                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(10), textAlign: 'center', color: colors.green }}> {Strings.estimatedPrice}</Text>
                <View style={{ borderRadius: moderateScale(1), alignSelf: 'center', marginTop: moderateScale(0) }} >
                    
                    <TextInput 
                    editable={false}
                    value={(((data.restaurant.price * kg).toFixed(1)).toString())}
                    style={{ color: colors.darkGray, width: responsiveWidth(35), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(6), textAlign: 'center', borderColor: 'black', borderWidth:0, fontSize: responsiveFontSize(6) }}
                    />
                </View></View>
        )
    }

    avgKgInput = () => {
        const { isRTL, data } = this.props
        const { kg, enableAvgKgInput } = this.state
        return (
            <View style={{ width: responsiveWidth(45) }}>
                <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                    <TextInput
                        editable={enableAvgKgInput}
                        value={kg.replace(/\s/g, '').length ? kg : null}
                        onChangeText={(val) => {
                            {
                                if (Number(val)) {
                                    this.setState({ kg: val, totalPrice: (val * data.restaurant.price).toString() })
                                }
                                if (val.length == 0) {
                                    this.setState({ kg: val, totalPrice: ' ' })
                                }
                            }
                        }}
                        style={{ width: responsiveWidth(45), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(6) }}
                        placeholder={Strings.averageKg}
                        keyboardType='decimal-pad'
                    />
                </View>
                {kg.length == 0 &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }

    avgKgSection = () => {
        const { isRTL } = this.props
        const { price } = this.state
        return (
            <View style={{ marginTop: moderateScale(15), alignSelf: 'center', flexDirection: isRTL ? 'row-reverse' : 'row', width: responsiveWidth(80) }}>
                {this.avgKgInput()}
                <Button onPress={() => this.setState({ enableAvgKgInput: true })} style={{ justifyContent: 'center', alignItems: 'center', marginHorizontal: moderateScale(4), height: responsiveHeight(6), width: responsiveWidth(15), backgroundColor: 'green', borderRadius: moderateScale(2) }}>
                    <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, color: colors.white, fontSize: responsiveFontSize(6), }}>{Strings.edit}</Text>
                </Button>

                <Button onPress={() => this.setState({ enableAvgKgInput: false })} style={{ justifyContent: 'center', alignItems: 'center', height: responsiveHeight(6), width: responsiveWidth(15), backgroundColor: 'blue', borderRadius: moderateScale(2) }}>
                    <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, color: colors.white, fontSize: responsiveFontSize(6), }}>{Strings.done}</Text>
                </Button>
            </View>
        )
    }

    priceInput = () => {
        const { isRTL, data } = this.props
        const { totalPrice, enablePriceInput } = this.state
        return (
            <View style={{ width: responsiveWidth(45) }}>
                <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                    <TextInput
                        editable={enablePriceInput}
                        value={totalPrice.replace(/\s/g, '').length ? totalPrice : null}
                        onChangeText={(val) => { this.setState({ totalPrice: val }) }}
                        style={{ width: responsiveWidth(45), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(6) }}
                        placeholder={Strings.price}

                    />
                </View>
                {totalPrice.length == 0 &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }

    priceSection = () => {
        const { isRTL } = this.props
        const { price } = this.state
        return (
            <View style={{ marginTop: moderateScale(15), alignSelf: 'center', flexDirection: isRTL ? 'row-reverse' : 'row', width: responsiveWidth(80) }}>
                {this.priceInput()}
                <Button onPress={() => this.setState({ enablePriceInput: false })} style={{ justifyContent: 'center', alignItems: 'center', marginHorizontal: moderateScale(4), height: responsiveHeight(6), width: responsiveWidth(15), backgroundColor: 'green', borderRadius: moderateScale(2) }}>
                    <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, color: colors.white, fontSize: responsiveFontSize(6), }}>{Strings.edit}</Text>
                </Button>

                <Button onPress={() => this.setState({ enablePriceInput: false })} style={{ justifyContent: 'center', alignItems: 'center', height: responsiveHeight(6), width: responsiveWidth(15), backgroundColor: 'blue', borderRadius: moderateScale(2) }}>
                    <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, color: colors.white, fontSize: responsiveFontSize(6), }}>{Strings.done}</Text>
                </Button>
            </View>
        )
    }

    confirmButton = () => {
        const { isRTL } = this.props
        const { avgKg, price, loading,enableReason } = this.state
        return (
            <TouchableOpacity onPress={() => {
                if(enableReason){
                    this.sendProblem()
                }else{
                    this.updateOrder()
                }
                
            }} style={[whiteButton,{ alignSelf: 'center', marginTop: moderateScale(10),marginBottom:moderateScale(enableReason?20:0)}]}>
                <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, color: colors.green, fontSize: responsiveFontSize(6), }}>{Strings.confirm}</Text>
            </TouchableOpacity>

        )
    }



    render() {
        const { categoryName, isRTL, data } = this.props;
        const { loading,enableReason} = this.state
        return (
            <LinearGradient
            colors={[colors.white,colors.white,colors.white]} 
            style={{ flex: 1 }}
            >
                <Header  title={Strings.orderInfo} />

                <ScrollView showsVerticalScrollIndicator={false} style={{ marginBottom: moderateScale(20), height: responsiveHeight(82), backgroundColor: colors.white, borderTopRightRadius: moderateScale(20), borderTopLeftRadius: moderateScale(20), marginTop: moderateScale(15), width: responsiveWidth(100) }} >
                    {this.resturantName()}
                    {this.spinKg()}
                    {this.estimatedPrice()}
                    {this.reasonCheckBox()}
                    {enableReason&&this.reasonsPicker()}
                    {this.confirmButton()}

                </ScrollView>
                {loading &&
                    <LoadingDialogOverlay title={Strings.wait} />
                }
                <AppFooter />

            </LinearGradient>
        );
    }
}

const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    barColor: state.lang.color,
    currentUser: state.auth.currentUser,
})

const mapDispatchToProps = {
    removeItem,
}

export default connect(mapStateToProps, mapDispatchToProps)(DriverUpdateOrder);
