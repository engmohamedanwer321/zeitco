import React, { Component } from 'react';
import { View, RefreshControl, Alert, Modal, Image, FlatList, Text, TouchableOpacity, Linking } from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../../utils/responsiveDimensions';
import { connect } from "react-redux";
import Strings from '../../assets/strings';

import axios from 'axios';
import { BASE_END_POINT } from '../../AppConfig';
import ListFooter from '../../components/ListFooter';
import { Icon, Thumbnail, Button } from 'native-base'
import { selectMenu, removeItem } from '../../actions/MenuActions';
import { enableSideMenu, pop } from '../../controlls/NavigationControll'
import NetInfo from "@react-native-community/netinfo";
import Loading from "../../common/Loading"
import NetworError from '../../common/NetworError'
import NoData from '../../common/NoData'
import * as colors from '../../assets/colors'
import { arrabicFont, englishFont, boldFont } from '../../common/AppFont'
import * as Animatable from 'react-native-animatable'
import FastImage from 'react-native-fast-image'
import NotificationCard from '../../components/NotificationCard'
import PurchasingAccountsCard from '../../components/PurchasingAccountsCard';
import CollaspeAppHeader from '../../common/CollaspeAppHeader'
import { OrdersCount } from '../../actions/OrdersActions';
import LinearGradient from 'react-native-linear-gradient';
import CommanHeader from '../../common/CommanHeader'
import MapView, { Marker } from 'react-native-maps';
import AppFooter from '../../components/AppFooter'
import { push } from '../../controlls/NavigationControll';
import io from 'socket.io-client';
import DriverConfirmedOrderCard from '../../components/DriverConfirmedOrderCard'
import { RNToasty } from 'react-native-toasty';
import Header from '../../common/Header'


class DriverCompletedOrders extends Component {

    socket = null;
    newOrdersPage = 1;
    state = {
        showAlert: false,
        tabNumber: 1,
        showModal: false,
        location: {
            latitude: 37.78825,
            longitude: -122.4324,
            latitudeDelta: 0.015,
            longitudeDelta: 0.0121,
        },
        networkError: null,
        newOrders: [],
        newOrdersLoading: true,
        newOrdersRefresh: false,
        newOrders404: false,
        newOrdersPages: null,
    }

    componentDidMount() {
        enableSideMenu(false, null)
        this.completedOrders(false, 1)
    }

    completedOrders(refresh, page) {

        if (refresh) {
            this.setState({ newOrdersRefresh: true })
        }

        axios.get(`${BASE_END_POINT}orders?driver=${this.props.currentUser.user.id}&status=COLLECTED&page=${page}`)
            .then(response => {
                console.log('Done   ', response.data.data)
                this.setState({
                    newOrders: refresh ? response.data.data : [...this.state.newOrders, ...response.data.data],
                    newOrdersLoading: false,
                    newOrdersRefresh: false,
                    newOrdersPages: response.data.pageCount,
                })
            })
            .catch(error => {
                console.log('Error   ', error)
                this.setState({ orders404: true, ordersLoading: false, })
            })
    }


    completedOrderCard = (data) => {
        const { categoryName, isRTL } = this.props;
        return (
            <TouchableOpacity>
                <Animatable.View
                    style={{ marginBottom: moderateScale(5), padding: moderateScale(3), flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', alignSelf: 'center', width: responsiveWidth(100) }}
                    animation={"flipInX"}
                    duration={1000}>
                    <Thumbnail large source={require('../../assets/imgs/profileicon.jpg')} />

                    <View style={{ width: responsiveWidth(45), marginHorizontal: moderateScale(5), }}>
                        <Text style={{ textAlign: isRTL ? 'right' : 'left', color: colors.grayColor1, fontFamily: isRTL ? arrabicFont : englishFont }}><Text style={{ fontFamily: boldFont }}>{Strings.outletName}:</Text>{isRTL ? data.restaurant.restaurantName_ar : data.restaurant.restaurantName}</Text>
                        <Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', color: colors.grayColor1, fontSize: responsiveFontSize(5), fontFamily: isRTL ? arrabicFont : englishFont }}><Text style={{ fontFamily: boldFont }}>{Strings.area}:</Text> {isRTL ? data.restaurant.branches[0].area.arabicAreaName : data.restaurant.branches[0].area.areaName}</Text>
                        <Text style={{ textAlign: isRTL ? 'right' : 'left', color: colors.grayColor1, fontFamily: isRTL ? arrabicFont : englishFont }}><Text style={{ fontFamily: boldFont }}>{Strings.phone}:</Text>{data.restaurant.phone}</Text>
                        <Text style={{ textAlign: isRTL ? 'right' : 'left', color: colors.grayColor1, fontFamily: isRTL ? arrabicFont : englishFont }}><Text style={{ fontFamily: boldFont }}>{Strings.address}:</Text>{data.restaurant.branches[data.branch].address}</Text>
                        <Text style={{ textAlign: isRTL ? 'right' : 'left', color: colors.grayColor1, fontFamily: isRTL ? arrabicFont : englishFont }}><Text style={{ fontFamily: boldFont }}>{Strings.date}:</Text>{data.date}</Text>
                    </View>

                    <View style={{ justifyContent: 'center', alignItems: 'center', marginHorizontal: moderateScale(2), width: responsiveWidth(22) }}>
                        <Text style={{ color: colors.grayColor1, fontFamily: isRTL ? arrabicFont : englishFont, textAlign: 'center' }}>{data.average} {Strings.Kg}</Text>
                        <Text style={{ color: colors.grayColor1, fontFamily: isRTL ? arrabicFont : englishFont, textAlign: 'center' }}>{data.restaurant.price * data.average} {Strings.EGP}</Text>
                        {/*<TouchableOpacity style={{alignSelf:'center'}} onPress={() => {
                            this.acceptOrder(data)
                        }}>
                            <FastImage style={{width:20,height:20}} source={require('../../assets/imgs/home-driverlocation-icon.png')} />
                    </TouchableOpacity>*/}
                    </View>

                </Animatable.View>
            </TouchableOpacity>

        )
    }



    completedOrderPage = () => {
        const { newOrders404, newOrdersLoading, newOrders, newOrdersRefresh, newOrdersPages } = this.state
        return (
            <>
                {newOrders404 ?
                    <NetworError />
                    :
                    newOrdersLoading ?
                        <Loading />
                        :
                        newOrders.length > 0 ?
                            <FlatList
                                showsVerticalScrollIndicator={false}
                                style={{ marginBottom: moderateScale(10), paddingTop: moderateScale(10) }}
                                data={newOrders}
                                renderItem={({ item }) => this.completedOrderCard(item)}
                                onEndReachedThreshold={.5}
                                onEndReached={() => {
                                    if (this.newOrdersPage <= newOrdersPages) {
                                        this.newOrdersPage = this.newOrdersPage + 1;
                                        this.completedOrders(false, this.newOrdersPage)
                                        console.log('page  ', this.newOrdersPage)
                                    }
                                }}
                                refreshControl={
                                    <RefreshControl
                                        //colors={["#B7ED03",colors.darkBlue]} 
                                        refreshing={newOrdersRefresh}
                                        onRefresh={() => {
                                            this.newOrdersPage = 1
                                            this.completedOrders(true, 1)
                                        }}
                                    />
                                }

                            />
                            :
                            <NoData />}
            </>
        )
    }



    render() {
        const { categoryName, isRTL } = this.props;
        const { tabNumber } = this.state;
        return (
            <LinearGradient
                colors={[colors.white, colors.white, colors.white]}
                style={{ flex: 1 }}
            >
                <Header title={Strings.completedOrders} />
                <View style={{ flex: 1, marginBottom: moderateScale(20), backgroundColor: colors.white, borderTopRightRadius: moderateScale(0), marginTop: moderateScale(0), width: responsiveWidth(100), marginBottom: responsiveHeight(7) }} >
                    {this.completedOrderPage()}
                </View>
                <AppFooter />

            </LinearGradient>
        );
    }
}

const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    barColor: state.lang.color,
    currentUser: state.auth.currentUser,
    ordersCount: state.orders.ordersCount
})

const mapDispatchToProps = {
    removeItem,
    OrdersCount
}

export default connect(mapStateToProps, mapDispatchToProps)(DriverCompletedOrders);
