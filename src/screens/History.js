import React, {Component} from 'react';
import {
  View,
  RefreshControl,
  ScrollView,TextInput,
  FlatList,
  Text,
  TouchableOpacity,
} from 'react-native';
import {
  moderateScale,
  responsiveWidth,
  responsiveHeight,
  responsiveFontSize,
} from '../utils/responsiveDimensions';
import {connect} from 'react-redux';
import Strings from '../assets/strings';

import axios from 'axios';
import {BASE_END_POINT} from '../AppConfig';
import ListFooter from '../components/ListFooter';
import {Icon, Thumbnail, Button} from 'native-base';
import {selectMenu, removeItem} from '../actions/MenuActions';
import {enableSideMenu, pop} from '../controlls/NavigationControll';
import NetInfo from '@react-native-community/netinfo';
import Loading from '../common/Loading';
import NetworError from '../common/NetworError';
import NoData from '../common/NoData';
import * as colors from '../assets/colors';
import {arrabicFont, englishFont} from '../common/AppFont';
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image';
import OrdersHistoryCard from '../components/OrdersHistoryCard';
import OrdersHistoryCard2 from '../components/OrdersHistoryCard2';
import CollaspeAppHeader from '../common/CollaspeAppHeader';
import LinearGradient from 'react-native-linear-gradient';
import CommanHeader from '../common/CommanHeader';
import AppFooter from '../components/AppFooter';
import Header from '../common/Header';

class History extends Component {
  page = 1;
  page2 = 1;
  state = {
    tabNumber: 1,
    networkError: null,
    histories: [],
    historiesLoading: true,
    historiesRefresh: false,
    histories404: false,
    pages: null,

    networkError2: null,
    histories2: [],
    historiesLoading2: true,
    historiesRefresh2: false,
    histories4042: false,
    pages2: null,

    search:null,
  }

  componentDidMount() {
    enableSideMenu(false, null);
    this.getHistories(false, 1);
  }

  renderFooter = () => {
    return this.state.loading ? (
      <View style={{alignSelf: 'center', margin: moderateScale(5)}}>
        <ListFooter />
      </View>
    ) : null;
  }

  getHistories = (refresh, page, search) => {
    //user=${this.props.currentUser.user.id}&
    if (refresh) {
      this.setState({historiesRefresh: true});
    }
    var link = `${BASE_END_POINT}reports?type=finish&page=${page}`
    if(search){
      link = `${BASE_END_POINT}reports?type=finish&page=${page}&search=${search}`
      console.log("SEARCH   ",search)
    }
    axios
      .get(link, {
        headers: {
          Authorization: `Bearer ${this.props.currentUser.token}`,
        },
      })
      .then(response => {
        console.log('Done   ', response.data.data);
        this.setState({
          histories: refresh
            ? response.data.data
            : [...this.state.histories, ...response.data.data],
          historiesLoading: false,
          historiesRefresh: false,
          pages: response.data.pageCount,
        });
      })
      .catch(error => {
        console.log('Error   ', error.response);
        this.setState({histories404: true, historiesLoading: false});
      });
  }

  getHistoriesNotFisished = (refresh, page , search) => {
    //user=${this.props.currentUser.user.id}&
    if (refresh) {
      this.setState({historiesRefresh2: true});
    }
    var link = `${BASE_END_POINT}reports?type=problem&page=${page}`
    if(search){
      link = `${BASE_END_POINT}reports?type=problem&page=${page}&search=${search}`
      console.log("SEARCH2   ",search)
    }

    axios
      .get(link, {
        headers: {
          Authorization: `Bearer ${this.props.currentUser.token}`,
        },
      })
      .then(response => {
        console.log('Done   ', response.data.data);
        this.setState({
          histories2: refresh
            ? response.data.data
            : [...this.state.histories2, ...response.data.data],
          historiesLoading2: false,
          historiesRefresh2: false,
          pages2: response.data.pageCount,
        });
      })
      .catch(error => {
        console.log('Error   ', error.response);
        this.setState({histories4042: true, historiesLoading2: false});
      });
  }

  tabs = () => {
    const {isRTL} = this.props;
    const {tabNumber} = this.state;
    console.log(tabNumber);
    return (
      <View style={{width: responsiveWidth(100),flexDirection: 'row',alignItems: 'center',}}
        >
        <Button
          onPress={() => {
            if (this.state.tabNumber != 1) {
              this.setState({tabNumber: 1,search:''});
              this.getHistories(true, 1);
            }
          }}
          style={{flexDirection: isRTL ? 'row-reverse' : 'row',marginLeft: 1,flex: 1,height: responsiveHeight(8),justifyContent: 'center',alignItems: 'center',alignSelf: isRTL ? 'flex-start' : 'flex-end',backgroundColor: tabNumber == 1 ? colors.white : colors.lightGreen,}}
          >
          <Text style={{color: tabNumber == 1 ? colors.darkGray : colors.white,fontFamily: isRTL ? arrabicFont : englishFont,}}>{Strings.finishedOrders}</Text>
        </Button>

        <Button
          onPress={() => {
            if (this.state.tabNumber != 2) {
              this.setState({tabNumber: 2,search:''});
              this.getHistoriesNotFisished(true, 1);
            }
          }}
          style={{marginRight: 1,flex: 1,height: responsiveHeight(8),justifyContent: 'center',alignItems: 'center',alignSelf: isRTL ? 'flex-start' : 'flex-end',backgroundColor: tabNumber == 2 ? colors.white : colors.lightGreen,}}
          >
          <Text style={{color: tabNumber == 2 ? colors.darkGray : colors.white,fontFamily: isRTL ? arrabicFont : englishFont,}}> {Strings.problemOrders}</Text>
        </Button>
      </View>
    );
  }

  completedOrdersHistory = () => {
    const {pages,histories,historiesLoading, histories404, historiesRefresh,} = this.state;
    return (
      <>
        {histories404 ? 
        <NetworError />
        : 
        historiesLoading ?
        <Loading />
        : 
        histories.length > 0 ?
        <FlatList
        showsVerticalScrollIndicator={false}
            style={{marginBottom: moderateScale(10)}}
            data={histories}
            renderItem={({item}) => <OrdersHistoryCard data={item} />}
            onEndReachedThreshold={0.5}
            //ListFooterComponent={()=>notificationsLoad&&<ListFooter />}
            onEndReached={() => {
              if (this.page <= pages) {
                this.page = this.page + 1;
                this.getHistories(false, this.page);
                console.log('page  ', this.page);
              }
            }}
            refreshControl={
              <RefreshControl
                //colors={["#B7ED03",colors.darkBlue]}
                refreshing={historiesRefresh}
                onRefresh={() => {
                  this.page = 1;
                  this.getHistories(true, 1);
                }}
              />
        }
        />
        : 
        <NoData />
        }
      </>
    );
  }

  notCompletedOrdersHistory = () => {
    const {pages2,histories2,historiesLoading2, histories4042, historiesRefresh2,} = this.state;
    return (
      <>
        {histories4042 ? 
        <NetworError />
        : 
        historiesLoading2 ?
        <Loading />
        : 
        histories2.length > 0 ?
        <FlatList
        showsVerticalScrollIndicator={false}
            style={{marginBottom: moderateScale(10)}}
            data={histories2}
            renderItem={({item}) => <OrdersHistoryCard2 data={item} />}
            onEndReachedThreshold={0.5}
            //ListFooterComponent={()=>notificationsLoad&&<ListFooter />}
            onEndReached={() => {
              if (this.page2 <= pages2) {
                this.page2 = this.page2 + 1;
                this.getHistoriesNotFisished(false, this.page2);
                console.log('page  ', this.page2);
              }
            }}
            refreshControl={
              <RefreshControl
                //colors={["#B7ED03",colors.darkBlue]}
                refreshing={historiesRefresh2}
                onRefresh={() => {
                  this.page2 = 1;
                  this.getHistoriesNotFisished(true, 1);
                }}
              />
        }
        />
        : 
        <NoData />
        }
      </>
    );
  }

  searchInput = () => {
    const {tabNumber,search} = this.state;
    const {isRTL} = this.props;
    return(
      <TextInput
      value={search}
      placeholder={Strings.search}
      style={{height:responsiveHeight(6.5),backgroundColor:colors.lightGray,paddingVertical:0, textAlign:isRTL?'right':'left',width:responsiveWidth(90),borderRadius:moderateScale(2),alignSelf:'center',marginTop:moderateScale(10), color:'black', paddingHorizontal:moderateScale(2)}}
      onChangeText={(val)=>{
        this.setState({search:val})
        if(tabNumber==1){
          this.page=1
          this.getHistories(true,1,val)
        }else{
          this.page2=1
          this.getHistoriesNotFisished(true,1,val)
        }
      }}
      />
    )
  }

  render() {
    const {tabNumber} = this.state;
    return (
      <LinearGradient
        colors={[colors.white, colors.white, colors.white]}
        style={{flex: 1}}>
        <Header title={Strings.history} />
        {this.tabs()}
        {this.searchInput()}
        <View style={{ flex: 1, height: responsiveHeight(73),backgroundColor: colors.white, borderTopRightRadius: moderateScale(20),borderTopLeftRadius: moderateScale(20),marginTop: moderateScale(5),width: responsiveWidth(100),marginBottom: responsiveHeight(7.5),}}>
            {tabNumber==1&&this.completedOrdersHistory()}
            {tabNumber==2&&this.notCompletedOrdersHistory()}
        </View>
        
        <AppFooter />
      </LinearGradient>
    )
  }
}

const mapStateToProps = state => ({
  isRTL: state.lang.RTL,
  barColor: state.lang.color,
  currentUser: state.auth.currentUser,
})

const mapDispatchToProps = {
  removeItem,
}

export default connect(mapStateToProps,mapDispatchToProps,)(History)
