import React, { Component } from 'react';
import {
    View, TouchableOpacity, Image, Text, ScrollView, TextInput, Alert, Platform
} from 'react-native';
import { connect } from 'react-redux';
import { Icon, Button } from 'native-base';
import { setUser } from '../actions/AuthActions'
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import Strings from '../assets/strings';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import { enableSideMenu, resetTo, push } from '../controlls/NavigationControll'
import { arrabicFont, englishFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import * as colors from '../assets/colors';
import { removeItem } from '../actions/MenuActions';
import LinearGradient from 'react-native-linear-gradient';
import FastImage from 'react-native-fast-image'
import AppFooter from '../components/AppFooter'
import { RNToasty } from 'react-native-toasty';
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios';
import { BASE_END_POINT } from '../AppConfig';
import strings from '../assets/strings';
import { changeLanguage, changeColor } from '../actions/LanguageActions';
import GetLocation from 'react-native-get-location'


class Login extends Component {

    password = null
    state = {
        userName: ' ',
        password: ' ',
        hidePassword: true,
        loading: false,
        change: false,
        latitude: 0,
        longitude: 0
    }

    componentDidMount() {
        enableSideMenu(false, null)
        this.getCurrentLocation()
        setInterval(() => {
            this.getCurrentLocation()
        }, 1000 * 10)

    }

    /*componentDidUpdate() {
        const { latitude } = this.state
        if (latitude <= 0) {
            this.getCurrentLocation()
        }

    }*/

    Image_Title = () => {
        const { isRTL } = this.props
        return (
            <View style={{ marginTop: moderateScale(-5), alignSelf: 'center', justifyContent: 'center', alignItems: 'center' }}>
                <FastImage
                    resizeMode='contain'
                    style={{ width: responsiveWidth(50), height: responsiveHeight(25) }}
                    source={require('../assets/imgs/appLogo.png')}
                />
                {/*<Text style={{ fontFamily: englishFont, color: colors.white, marginTop: moderateScale(0), fontSize: responsiveFontSize(12) }}>Zeitco</Text>
                <Text style={{ fontFamily: englishFont, color: colors.white, marginTop: moderateScale(-2), fontSize: responsiveFontSize(5) }}>OIL RECYCLING</Text>*/}
                <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(12), color: colors.white, marginTop: moderateScale(6) }} >{Strings.welcome} </Text>
            </View>
        )
    }


    langHeader = () => {
        const { isRTL } = this.props
        return (
            <TouchableOpacity
                onPress={() => {
                    if (isRTL) {
                        this.props.changeLanguage(false);
                        //this.setState({change:'en'})
                        Strings.setLanguage('en');
                        AsyncStorage.setItem('@lang', 'en')
                    } else {
                        this.props.changeLanguage(true);
                        //this.setState({change:'ar'})
                        Strings.setLanguage('ar');
                        AsyncStorage.setItem('@lang', 'ar')
                    }
                }}
                style={{ margin: moderateScale(10), width: 40, height: 40, borderRadius: 20, backgroundColor: 'white', alignSelf: 'flex-end', justifyContent: 'center', alignItems: 'center' }}>
                {isRTL ?
                    <Text style={{ fontFamily: englishFont, color: colors.green, marginTop: moderateScale(0), fontSize: responsiveFontSize(5) }}>EN</Text>
                    :
                    <Text style={{ fontFamily: englishFont, color: colors.green, marginTop: moderateScale(0), fontSize: responsiveFontSize(5) }}>AR</Text>
                }
            </TouchableOpacity>
        )
    }

    userNameInput = () => {
        const { isRTL } = this.props
        const { userName } = this.state
        return (
            <View style={{ marginTop: moderateScale(5), width: responsiveWidth(70), alignSelf: 'center', justifyContent: 'center', alignItems: 'center' }}>
                <View style={{ borderRadius: moderateScale(8), flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', width: responsiveWidth(70), backgroundColor: colors.white }}>
                    <TextInput
                        onChangeText={(val) => { this.setState({ userName: val }) }}
                        style={{ textAlign: isRTL ? 'right' : 'left', color: 'black', width: responsiveWidth(59), marginHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(7) }}
                        placeholder={Strings.userName}
                        placeholderTextColor={colors.placeholderGray}
                        returnKeyType="next"
                        onSubmitEditing={() => { this.password.focus(); }}
                    />
                    <Icon name='user' type='AntDesign' style={{ color: 'black', fontSize: responsiveFontSize(8) }} />
                </View>
                {userName.length == 0 &&
                    <Text style={{ color: 'white', marginHorizontal: moderateScale(5), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }

    passwordInput = () => {
        const { isRTL } = this.props
        const { password, hidePassword } = this.state
        return (
            <View style={{ marginTop: moderateScale(5), width: responsiveWidth(70), alignSelf: 'center', justifyContent: 'center', alignItems: 'center' }}>
                <View style={{ borderRadius: moderateScale(8), flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', width: responsiveWidth(70), backgroundColor: colors.white }}>
                    <TextInput
                        secureTextEntry={hidePassword}
                        onChangeText={(val) => { this.setState({ password: val }) }}
                        style={{ textAlign: isRTL ? 'right' : 'left', color: 'black', width: responsiveWidth(59), marginHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(7) }}
                        placeholder={Strings.password}
                        placeholderTextColor={colors.placeholderGray}
                        ref={(input) => { this.password = input; }}
                    />
                    <TouchableOpacity onPress={() => { this.setState({ hidePassword: !hidePassword }) }} >
                        <Icon name={hidePassword ? 'ios-eye-off' : 'ios-eye'} type='Ionicons' style={{ color: 'black', fontSize: responsiveFontSize(8) }} />
                    </TouchableOpacity>
                </View>
                {password.length == 0 &&
                    <Text style={{ color: 'white', marginHorizontal: moderateScale(5), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }

    getCurrentLocation = () => {
        this.setState({ latitude: 0, longitude: 0 })
        GetLocation.getCurrentPosition({
            enableHighAccuracy: true,
            timeout: 200000,
        })
            .then(location => {
                console.log("LOCATION  ", location)
                //this.setState({ currentLongitude: location.longitude, currentLatitude: location.latitude, VMap: 1 });
                this.setState({ latitude: location.latitude, longitude: location.longitude })

            })
            .catch(error => {
                const { code, message } = error;
                console.log("LOCATION error  ", error.message)
                console.log(code, message);
            })

    }


    login = () => {
        const { password, userName } = this.state
console.log('this.state.longitude',this.state.longitude)

        if (!userName.replace(/\s/g, '').length) {
            this.setState({ userName: '' })
        }
        if (!password.replace(/\s/g, '').length) {
            this.setState({ password: '' })
        }
        if (userName.replace(/\s/g, '').length && password.replace(/\s/g, '').length) {
            this.setState({ loading: true })
            const data = {
                username: userName.toLowerCase(),
                password: password,
                token: '',
            }
            axios.post(`${BASE_END_POINT}signin`, JSON.stringify(data), {
                headers: {
                    "Content-Type": 'application/json'
                }
            })
                .then(response => {
                    this.setState({ loading: false })
                    //console.log(response.data)

                    if (response.data.user.type == 'CALL-CENTER') {
                        this.props.setUser(response.data)
                        AsyncStorage.setItem('USER', JSON.stringify(response.data))
                        resetTo('CallCenterHome')
                    }


                    if (response.data.user.type == 'ADMIN' || response.data.user.type == 'OPERATION' || response.data.user.type == 'USER' || response.data.user.type == 'WAREHOUSE') {
                        this.props.setUser(response.data)
                        AsyncStorage.setItem('USER', JSON.stringify(response.data))
                    }

                    


                    if (response.data.user.type == 'USER') {
                        resetTo('ResturantHome')
                    } else if (response.data.user.type == 'SURVEY') {
                        if (this.state.longitude > 0 || this.state.latitude > 0) {
                            this.props.setUser(response.data)
                            AsyncStorage.setItem('USER', JSON.stringify(response.data))
                            resetTo('SurveyHome')
                        } else {
                            RNToasty.Error({ title: Strings.cantLoginWithoutLocation })
                        }
                    } else if (response.data.user.type == 'ADMIN') {
                        resetTo('AdminHome')
                    } else if (response.data.user.type == 'DRIVER') {
                        if (this.state.longitude > 0 || this.state.latitude > 0) {
                            this.props.setUser(response.data)
                            AsyncStorage.setItem('USER', JSON.stringify(response.data))
                            resetTo('DriverHome')
                        } else {
                            RNToasty.Error({ title: Strings.cantLoginWithoutLocation })
                        }
                    } else if (response.data.user.type == 'OPERATION') {
                        resetTo('OperationHome')
                    }
                    else if (response.data.user.type == 'PURCHASING') {
                        if (this.state.longitude > 0 || this.state.latitude > 0) {
                            this.props.setUser(response.data)
                            AsyncStorage.setItem('USER', JSON.stringify(response.data))
                            resetTo('PurchasingHome')
                        } else {
                            RNToasty.Error({ title: Strings.cantLoginWithoutLocation })
                        }
                    }
                    else if (response.data.user.type == 'WAREHOUSE') {
                        resetTo('WarehouseHome')
                    }
                })
                .catch(error => {
                    this.setState({ loading: false })
                    if (error.response.status == 401) {
                        RNToasty.Error({ title: strings.loginUserDataIncorrect })
                    }
                })
        }

    }

    loginButton = () => {
        const { isRTL } = this.props
        

        return (
            <>
                <Button onPress={() => {  this.getCurrentLocation(),this.login() }} style={{ alignSelf: 'center', marginTop: moderateScale(10), height: responsiveHeight(7), width: responsiveWidth(70), justifyContent: 'center', alignItems: 'center', backgroundColor: colors.medimGreen, borderRadius: moderateScale(5) }} >
                    <Text style={{ color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.login}</Text>
                </Button>

                <Button transparent onPress={() => { push('RegisterResturantAccount') }} style={{ borderColor: 'white', borderWidth: 1, alignSelf: 'center', marginTop: moderateScale(5), height: responsiveHeight(7), width: responsiveWidth(70), justifyContent: 'center', alignItems: 'center', borderRadius: moderateScale(5) }} >
                    <Text style={{ color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont, }}>{Strings.registerOutlet}</Text>
                </Button>

            </>
        )
    }




    render() {
        const { loading } = this.state
        return (
            <LinearGradient
                locations={[0.0, 0.2, 1]}
                colors={[colors.medimGreen, colors.darkGreen, colors.darkGreen]}
                style={{ flex: 1 }}
            >

                <ScrollView>
                    {this.langHeader()}
                    <View style={{ alignItems: 'center' }}>
                        {this.Image_Title()}
                        {this.userNameInput()}
                        {this.passwordInput()}
                        {this.loginButton()}
                    </View>
                </ScrollView>
                {loading &&
                    <LoadingDialogOverlay title={Strings.wait} />
                }

                <AppFooter />

            </LinearGradient>
        );
    }
}
const mapDispatchToProps = {
    setUser,
    removeItem,
    changeLanguage,
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL,

    //userToken: state.auth.userToken,
})


export default connect(mapToStateProps, mapDispatchToProps)(Login);

