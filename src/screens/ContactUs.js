import React, { Component } from 'react';
import {
  View,TouchableOpacity,Image,Text,ScrollView,TextInput,Alert,Platform
} from 'react-native';
import { connect } from 'react-redux';
import {Icon,Button} from 'native-base';
import {setUser} from '../actions/AuthActions'
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import Strings from '../assets/strings';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import {enableSideMenu,resetTo,push,pop} from '../controlls/NavigationControll'
import {arrabicFont,englishFont} from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import * as colors from '../assets/colors';
import {removeItem} from '../actions/MenuActions';
import CollaspeAppHeader from '../common/CollaspeAppHeader'
import LinearGradient from 'react-native-linear-gradient';
import FastImage from 'react-native-fast-image'
import CommanHeader from '../common/CommanHeader'
import ImagePicker from 'react-native-image-crop-picker';
import AppFooter from '../components/AppFooter'
import AsyncStorage  from '@react-native-community/async-storage'
import axios from 'axios';
import {BASE_END_POINT} from '../AppConfig';
import { RNToasty } from 'react-native-toasty';
import { whiteButton, greenButton } from '../assets/styles';
import Header from '../common/Header'

class ContactUs extends Component {

    name=null
    email=null
    phone=null
    message=null

    state={
        name:' ',
        phone:' ',
        email:' ',
        message:' ',
        loading:false,

    }

    componentDidMount(){
        enableSideMenu(false, null)
    }

    nameInput = () => {
        const {isRTL} = this.props
        const {name} = this.state
        return(
            <View style={{marginTop:moderateScale(10),width:responsiveWidth(80), alignSelf:'center'}}>             
                <Text style={{marginBottom:moderateScale(3),fontFamily:isRTL?arrabicFont:englishFont, color:colors.black,fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.name}</Text>
                <View style={{borderRadius:moderateScale(1), backgroundColor:colors.lightGray }} >
                <TextInput 
                     onChangeText={(val)=>{this.setState({name:val})}}
                     style={{width:responsiveWidth(80), paddingVertical:0, fontFamily:isRTL?arrabicFont:englishFont,height:responsiveHeight(5)}}
                     placeholder={Strings.name}
                     returnKeyType='next'
                     onSubmitEditing={() => { this.email.focus(); }}
                />
                </View>
                {name.length==0&&
                <Text style={{color:'red',marginHorizontal:moderateScale(2),fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {Strings.require}</Text>
                }
            </View>
        )
    }
    

    emailInput = () => {
        const {isRTL} = this.props
        const {email} = this.state
        return(
            <View style={{marginTop:moderateScale(6),width:responsiveWidth(80), alignSelf:'center'}}>
                
                <Text style={{marginBottom:moderateScale(3),fontFamily:isRTL?arrabicFont:englishFont, color:colors.black,fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.email}</Text>
                <View style={{borderRadius:moderateScale(1), backgroundColor:colors.lightGray }} >
                <TextInput 
                    
                     onChangeText={(val)=>{this.setState({email:val})}}
                     style={{width:responsiveWidth(80), paddingVertical:0,fontFamily:isRTL?arrabicFont:englishFont,height:responsiveHeight(5)}}
                     placeholder={Strings.email}
                     ref={(input) => { this.email = input; }}
                    returnKeyType="next"
                    onSubmitEditing={() => { this.phone.focus(); }}
                />
                </View>
                {email.length==0&&
                <Text style={{color:'red',marginHorizontal:moderateScale(2),fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {Strings.require}</Text>
                }
            </View>
        )
    }

    phoneInput = () => {
        const {isRTL} = this.props
        const {phone} = this.state
        return(
            <View style={{marginTop:moderateScale(6),width:responsiveWidth(80), alignSelf:'center'}}>
                
                <Text style={{marginBottom:moderateScale(3),fontFamily:isRTL?arrabicFont:englishFont, color:colors.black,fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.phone}</Text>
                <View style={{borderRadius:moderateScale(1), backgroundColor:colors.lightGray }} >
                <TextInput 
                    
                     onChangeText={(val)=>{this.setState({phone:val})}}
                     style={{width:responsiveWidth(80), paddingVertical:0,fontFamily:isRTL?arrabicFont:englishFont,height:responsiveHeight(5)}}
                     placeholder={Strings.phone}
                     ref={(input) => { this.phone = input; }}
                    returnKeyType="next"
                    onSubmitEditing={() => { this.message.focus(); }}
                    keyboardType='phone-pad'
                />
                </View>
                {phone.length==0&&
                <Text style={{color:'red',marginHorizontal:moderateScale(2),fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {Strings.require}</Text>
                }
            </View>
        )
    }

    messageInput = () => {
        const {isRTL} = this.props
        const {message} = this.state
        return(
            <View style={{marginTop:moderateScale(7),width:responsiveWidth(80), alignSelf:'center'}}>             
                <Text style={{marginBottom:moderateScale(3),fontFamily:isRTL?arrabicFont:englishFont, color:colors.black,fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.message}</Text>
                <View style={{borderRadius:moderateScale(1), backgroundColor:colors.lightGray }} >
                <TextInput 
                    multiline
                     onChangeText={(val)=>{this.setState({message:val})}}
                     style={{textAlignVertical:'top', width:responsiveWidth(80), paddingHorizontal:moderateScale(3),fontFamily:isRTL?arrabicFont:englishFont,height:responsiveHeight(25)}}
                     placeholder={Strings.message}
                     ref={(input) => { this.message = input; }}
                />
                </View>
                {message.length==0&&
                <Text style={{color:'red',marginHorizontal:moderateScale(2),fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {Strings.require}</Text>
                }
            </View>
        )
    }



    

    save = () => {
        const {name,phone,message,email,} = this.state
        const {currentUser} = this.props

        if(!email.replace(/\s/g, '').length){
            this.setState({email:''})
        }
        if(!name.replace(/\s/g, '').length){
            this.setState({name:''})
        }

        if(!message.replace(/\s/g, '').length){
            this.setState({message:''})
        }

        if(!phone.replace(/\s/g, '').length){
            this.setState({phone:''})
        }
        if(name.replace(/\s/g, '').length&&message.replace(/\s/g, '').length&&email.replace(/\s/g, '').length&&phone.replace(/\s/g, '').length){
            this.setState({loading:true})
            const data = {
                name:name,
                number:phone,
                email:email,
                message:message
            }

            axios.post(`${BASE_END_POINT}contact-us`,JSON.stringify(data),{
                headers:{
                    //"Authorization": `Bearer ${currentUser.token}`,
                    "Content-Type": "application/json"
                  }
            })
            .then(response=>{
                console.log('DONE')
                this.setState({loading:false})
                pop()
                RNToasty.Success({title:Strings.sendSuccessfuly})
            })
            .catch(error=>{
                this.setState({loading:false})
                //Alert.alert(''+RTCPeerConnectionIceErrorEvent)
                console.log('ERROR   ',error.response)
                //RNToasty.Error({title:Strings.incorrectPassword})
            })
            

        }

       
    }

    saveButton = () => {
        const {isRTL} = this.props

        return(
        <TouchableOpacity onPress={()=>{this.save()}} 
        style={[whiteButton,{alignSelf:'center', marginVertical:moderateScale(15), height:responsiveHeight(7),width:responsiveWidth(40),justifyContent:'center',alignItems:'center',}]} >
            <Text style={{color:colors.green,fontFamily:isRTL?arrabicFont:englishFont}}>{Strings.send}</Text>
        </TouchableOpacity>
        )
    }


    

    render(){
        const  { isRTL,userToken } = this.props;
        const  {loading} = this.state
        return(
        <LinearGradient 
        colors={[colors.white, colors.white, colors.white]} 
        style={{flex:1}}
        >
         <Header  title={Strings.contactUs} />  
        
        <ScrollView showsVerticalScrollIndicator={false}  style={{backgroundColor:colors.white, borderTopRightRadius:moderateScale(20),borderTopLeftRadius:moderateScale(20), marginTop:moderateScale(15), width:responsiveWidth(100),marginBottom:moderateScale(20)}} >
            {this.nameInput()}
            {this.emailInput()}
            {this.phoneInput()}
            {this.messageInput()}
            {this.saveButton()}
        </ScrollView>
        {loading&&<LoadingDialogOverlay title={Strings.wait} />}
        <AppFooter />
        </LinearGradient> 
        );
    }
}
const mapDispatchToProps = {
    setUser,
    removeItem
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL, 
    currentUser:state.auth.currentUser, 
    //userToken: state.auth.userToken,
})


export default connect(mapToStateProps,mapDispatchToProps)(ContactUs);

