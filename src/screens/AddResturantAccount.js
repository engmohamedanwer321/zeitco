import React, { Component } from 'react';
import {
    View, TouchableOpacity, Picker, Text, ScrollView, TextInput, Alert, Platform, KeyboardAvoidingView, StyleSheet, Switch
} from 'react-native';
import { connect } from 'react-redux';
import { Icon, Button, Title } from 'native-base';
import { login } from '../actions/AuthActions'
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import Strings from '../assets/strings';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import Loading from "../common/Loading"
import { enableSideMenu, resetTo, push } from '../controlls/NavigationControll'
import { arrabicFont, englishFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import * as colors from '../assets/colors';
import { removeItem } from '../actions/MenuActions';
import CollaspeAppHeader from '../common/CollaspeAppHeader'
import LinearGradient from 'react-native-linear-gradient';
import FastImage from 'react-native-fast-image'
import CommanHeader from '../common/CommanHeader'
import ImagePicker from 'react-native-image-crop-picker';
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import AppFooter from '../components/AppFooter'
import MapView, { Marker } from 'react-native-maps';
import { BASE_END_POINT } from '../AppConfig';
import InputValidations from '../common/InputValidations';
import { RNToasty } from 'react-native-toasty'
import axios from 'axios'
//import Geolocation from '@react-native-community/geolocation';
import GetLocation from 'react-native-get-location'
import { getGreatCircleBearing } from 'geolib';
import { pop } from '../controlls/NavigationControll'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { TextInputMask } from 'react-native-masked-text'
import { whiteButton, greenButton } from '../assets/styles'
import RNPickerSelect from 'react-native-picker-select';
import strings from '../assets/strings';
//import TextInputMask from 'react-native-text-input-mask';
import Header from '../common/Header'
import { Colors } from 'react-native/Libraries/NewAppScreen';
import { SpinPicker } from 'react-native-spin-picker'
import {
    WheelPicker,
    TimePicker,
    DatePicker
  } from "@taoqf/react-native-wheel-picker";

class AddResturantAccount extends Component {

    classes = [

        { label: Strings.classA, value: 'CLASS-A' },
        { label: Strings.classB, value: 'CLASS-B' },
        { label: Strings.classC, value: 'CLASS-C' },
    ]

    payTypes = [

        { label: Strings.postPaid , value: 'PostPaid' },
        { label: Strings.cashOnDelivery, value: 'CashOnDelivery' },
        { label: Strings.UnKnown, value: 'UnKnown' },
    ]

    branchsNo = [

        { label: '1', value: 1, },
        { label: '2', value: 2, },
        { label: '3', value: 3, },
        { label: '4', value: 4, },
        { label: '5', value: 5, },
        { label: '6', value: 6, },
        { label: '7', value: 7, },
        { label: '8', value: 8, },
        { label: '9', value: 9, },
        { label: '10', value: 10, },

    ]

    /*jobTitles = [
        {
            name: Strings.jobTitle,
            id: 0,
            children: [
                { name: 'USER', id: 1, },
                { name: 'Title 2', id: 2, },
                { name: 'Title 3', id: 3, },
            ],
        }
    ]*/

    jobTitles = [
        { label: Strings.owner, value: 'Owner' },
        { label: Strings.operator, value: 'Operator' },
        { label: Strings.employee, value: 'Employee' }
    ]

    spaces = [
        { label: '0 - 50', value: '0 - 50', },
        { label: '50 - 150', value: '50 - 150', },
        { label: '200+', value: '200+', },
    ]

    spacesWheel = [
        '0 - 50', 
        '50 - 150',
       '200+'
    ]

    averageKg = [

        { label: '0 - 50', value: '0 - 50', },
        { label: '50 - 100', value: '50 - 100', },
        { label: '100 - 150', value: '100 - 150', },
        { label: '150 - 200', value: '150 - 200', },
        { label: '200+', value: '200+', },
    ]

    oilTypes = [
        { label: 'Canola', value: 'Canola', },
        { label: 'Sunflower', value: 'Sunflower', },
        { label: 'Palm', value: 'Palm', },
        { label: 'Olive', value: 'Olive', },
    ]

    inputRefs = {}

    outletname = null
    outletname2 = null
    phoneNumber = null
    firstName = null
    lastName = null
    contactNo = null
    email = null
    userName = null
    password = null
    confirmPassword = null

    state = {
        note: ' ',
        restaurantName: '', restaurantNameValidate: ' ',
        restaurantNameAr: '', restaurantNameArValidate: ' ',
        outletPhoneNumber: '', outletPhoneNumberValidate: ' ',
        oilType: '', oilTypeValidate: ' ',
        price: ' ', priceValidate: ' ',
        addressByDetails: '', addressByDetailsValidate: ' ',
        contactNo: ' ', contactNoValidate: ' ',
        address: '', addressValidate: ' ',
        city: '', cityValidate: ' ',
        firstName: '', firstNameValidate: ' ',
        lastName: '', lastNameValidate: ' ',
        email: '', emailValidate: ' ',
        userName: '', userNameValidate: ' ',
        password: ' ', passwordValidate: ' ',
        confirmPassword: ' ', confirmPasswordValidate: ' ', confirmPasswordEqualPasswordValidate: ' ',
        title: '', titleValidate: ' ',
        outletCode: '10001', outletCodeValidate: ' ',
        selectTitle: '', selecTitleValidate: ' ',
        image: null,
        hidePassword: true,
        addressCount: [1],
        branches: [], branchesValidate: ' ',
        cities: [],
        areas: [],
        selectPayType: '', selectPayTypeValidate: ' ',
        selectClass: '', selectClassValidate: ' ',
        selecTitle: '',
        selectAvgKg: '', selectAvgKgValidate: ' ',
        selectBranchNo: '1', selectBranchNoValidate: '1',
        selectResturantSpace: '', selectResturantSpaceValidate: ' ',
        Coordinate: [],
        latitude: 0,
        longitude: 0,
        mapSwitchValue: false,
        noOilSwitch: true,
        networkError: null,
        addResturantLoading: false,
        citiesLoading: true,
        areaLoading: true,
        price: '00.00',
        price1: '',
        price2: 0,
        getLocationButton: false
    }

    componentDidMount() {
        enableSideMenu(false, null)
        console.log("LOCATION ## ", this.props.currentUser.user.type)
        this.getCurrentLocation()
        this.getCities()
    }

    getCurrentLocation = () => {
        GetLocation.getCurrentPosition({
            enableHighAccuracy: true,
            timeout: 200000,
        })
            .then(location => {
                console.log("LOCATION  ", location)
                //this.setState({ currentLongitude: location.longitude, currentLatitude: location.latitude, VMap: 1 });
                this.setState({ latitude: location.latitude, longitude: location.longitude })

            })
            .catch(error => {
                const { code, message } = error;
                console.log("LOCATION error  ", error.message)
                console.log(code, message);
            })

    }


    getCities() {
        const { isRTL } = this.props
        axios.get(`${BASE_END_POINT}cities`)
            .then(response => {


                var children = []
                const res = response.data
                var allCities = []
                console.log('Cities  ', allCities)
                res.map((item) => {
                    allCities.push({ label: isRTL ? item.arabicCityName : item.cityName, value: item.id })
                    //children = [...children, { name: isRTL ? item.arabicCityName : item.cityName, id: item.id }]
                })


                /*var allCities = [{
                    name: Strings.city,
                    id: 0, children: children
                }]*/
                console.log('newCountries : ', allCities);
                this.setState({ cities: allCities, citiesLoading: false })
            })
            .catch(error => {
                console.log('Error   ', error)
                this.setState({ currentOrders404: true, currentOrdersLoading: false, areaLoading: false })
            })
    }


    getAreas(cityId) {
        const { isRTL } = this.props

        axios.get(`${BASE_END_POINT}cities/${cityId}/areas`)
            .then(response => {
                console.log('Done Current ORDERS   ', response.data.data)

                var children = []
                const res = response.data
                var newAreas = []
                res.map((item) => {
                    newAreas.push({ label: isRTL ? item.arabicAreaName : item.areaName, value: item.id })
                    //children = [...children, { name: isRTL ? item.arabicAreaName : item.areaName, id: item.id }]
                })
                // console.log('Done  ', response.data.data)

                /*var newAreas = [{
                    name: Strings.area,
                    id: 0, children: children
                }]*/
                console.log('newCountries : ', newAreas);
                this.setState({ areas: newAreas, areaLoading: false })
            })
            .catch(error => {
                console.log('Error   ', error)
                this.setState({ currentOrders404: true, currentOrdersLoading: false, areaLoading: false })
            })
    }


    addResturantAccount = () => {



        const { selectClass, restaurantName, restaurantNameAr, outletPhoneNumber, selectPayType, image, title, selectAvgKg, price, selectResturantSpace, oilType, selectBranchNo, firstName, lastName, contactNo, email, userName, password, address, branches, outletCode, noOilSwitch, note } = this.state

        const { currentUser } = this.props
        console.log(restaurantName, image, title, selectAvgKg, price, selectResturantSpace, oilType, selectBranchNo, firstName, lastName, contactNo, email, password, address, branches)
        var status = ''

        if (currentUser.user.type == 'SURVEY') {
            status = 'SURVEY-ACCOUNT'
        }
        else if (currentUser.user.type == 'PURCHASING') {
            status = 'PURCHASING-ACCOUNT'
        }
        else if (currentUser.user.type == 'ADMIN') {
            status = 'APPROVED'
        }

        /*  if (!restaurantName.replace(/\s/g, '').length) { this.setState({ restaurantNameValidate: '' }); }
          if (!selectOilType.replace(/\s/g, '').length) { this.setState({ selectOilTypeValidate: '' }); }
          if (!selectAvgKg.replace(/\s/g, '').length) { this.setState({ selectAvgKgValidate: '' }); }
          if (!selectResturantSpace.replace(/\s/g, '').length) { this.setState({ selectResturantSpaceValidate: '' }); }
          if (!selectBranchNo.replace(/\s/g, '').length) { this.setState({ selectBranchNoValidate: '' }); }
          if (!addressByDetails.replace(/\s/g, '').length) { this.setState({ addressByDetailsValidate: '' }); }
          if (!firstName.replace(/\s/g, '').length) { this.setState({ firstNameValidate: '' }); }
          if (!lastName.replace(/\s/g, '').length) { this.setState({ lastNameValidate: '' }); }
          if (!contactNo.replace(/\s/g, '').length) { this.setState({ contactNoValidate: '' }); }
          if (!title.replace(/\s/g, '').length) { this.setState({ titleValidate: '' }); }
          if (!address.replace(/\s/g, '').length) { this.setState({ branchesValidate: '' }); }
          if (email.replace(/\s/g, '').length) {
              if (InputValidations.validateEmail(email) == false) { RNToasty.Error({ title: Strings.emailNotValid }); }
          }
  
          if (restaurantName.replace(/\s/g, '').length && selectOilType.replace(/\s/g, '').length && selectAvgKg.replace(/\s/g, '').length && selectResturantSpace.replace(/\s/g, '').length && selectBranchNo.replace(/\s/g, '').length && addressByDetails.replace(/\s/g, '').length && firstName.replace(/\s/g, '').length && lastName.replace(/\s/g, '').length && contactNo.replace(/\s/g, '').length && title.replace(/\s/g, '').length) {*/
        this.setState({ addResturantLoading: true })
        var data = new FormData()
        data.append('restaurantName', restaurantName)
        data.append('restaurantName_ar', restaurantNameAr)
        if ((outletPhoneNumber.replace(/\s/g, '').toString()).length) {
            data.append('outletPhoneNumber', outletPhoneNumber)
        }
        data.append('payType', selectPayType)
        if (image) {
            data.append('img', {
                uri: image,
                type: 'multipart/form-data',
                name: 'licenseImg'
            })
        }
        data.append('noOil', noOilSwitch)
        data.append('average', selectAvgKg)
        data.append('price', price)
        data.append('space', selectResturantSpace)
        data.append('oilType', oilType)
        if (this.props.currentUser.user.type == 'ADMIN') {
            data.append('class', selectClass)
        }
        //data.append('address', addressByDetails)
        data.append('branchesNumber', selectBranchNo)
        data.append('firstname', firstName)
        data.append('lastname', lastName.toLowerCase())
        data.append('phone', contactNo)
        data.append('status', status)
        data.append('branches', JSON.stringify(branches))
        //data.append('outletnumber', outletCode)
        data.append('director', currentUser.user.id)
        //data.append('outletnumber', 10)
        if (email.replace(/\s/g, '').length) {
            data.append('email', email)

        }
        data.append('type', 'USER')
        data.append('title', title)
        if (userName.replace(/\s/g, '').length && password.replace(/\s/g, '').length) {
            data.append('username', userName)
            data.append('password', password)
        }
        if (note.replace(/\s/g, '').length) {
            data.append('note', note)
        }

        axios.post(`${BASE_END_POINT}restaurant`, data, {
            headers: {
                Authorization: `Bearer ${this.props.currentUser.token}`
            }
        })
            .then(response => {
                this.setState({ addResturantLoading: false })
                // const res = response.data
                console.log('Done: ', response)
                pop()
                RNToasty.Success({ title: Strings.dataInsertedSuccessfuly })
            })
            .catch(error => {
                const resError = error.response
                console.log('Error  ', resError)
                //RNToasty.Error({ title: resError.msg })
                this.setState({ addResturantLoading: false })
            })
        // } else { RNToasty.Error({ title: Strings.insertTheRequiredData }); }


    }



    restaurantNameInput = () => {
        const { isRTL } = this.props
        const { restaurantName, restaurantNameValidate } = this.state

        return (
            <View style={{ marginTop: moderateScale(3), width: responsiveWidth(80), alignSelf: 'center' }}>

                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.restaurantName}</Text>
                <View style={{ height: responsiveHeight(4.5), backgroundColor: colors.lightGray, borderRadius: moderateScale(4) }} >
                    <TextInput
                        onChangeText={(val) => { this.setState({ restaurantName: val, restaurantNameValidate: val }) }}
                        //onChangeText={(val)=> {this.setState({restaurantName:parseFloat(val)/100})}}
                        style={{ width: responsiveWidth(80), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(4.5), padding: 0, textAlign: isRTL ? 'right' : 'left', color:'black' }}
                        placeholder={Strings.restaurantName}
                        placeholderTextColor={colors.placeholderGray}
                        returnKeyType="next"
                        onSubmitEditing={() => { this.outletname2.focus(); }}

                    />
                </View>
                {restaurantNameValidate.length == 0 &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }

    restaurantNameArInput = () => {
        const { isRTL } = this.props
        const { restaurantNameAr, restaurantNameArValidate } = this.state

        return (
            <View style={{ marginTop: moderateScale(6), width: responsiveWidth(80), alignSelf: 'center' }}>

                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.restaurantNameArabic}</Text>
                <View style={{ height: responsiveHeight(6), borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                    <TextInput
                        onChangeText={(val) => { this.setState({ restaurantNameAr: val, restaurantNameArValidate: val }) }}
                        //onChangeText={(val)=> {this.setState({restaurantName:parseFloat(val)/100})}}
                        style={{ width: responsiveWidth(80), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(5), padding: 0, textAlign: isRTL ? 'right' : 'left', color:'black' }}
                        placeholder={Strings.restaurantNameArabic}
                        placeholderTextColor={colors.placeholderGray}
                        returnKeyType="next"
                        onSubmitEditing={() => { this.phoneNumber.focus(); }}
                        ref={(input) => { this.outletname2 = input; }}

                    />
                </View>
                {restaurantNameArValidate.length == 0 &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }


    outletPhoneNumberInput = () => {
        const { isRTL } = this.props
        const { outletPhoneNumber, outletPhoneNumberValidate } = this.state
        return (
            <View style={{ marginTop: moderateScale(6), width: responsiveWidth(37), alignSelf: 'center' }}>

                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.phoneNumber}</Text>
                <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                    <TextInput
                        onChangeText={(val) => { this.setState({ outletPhoneNumber: val, outletPhoneNumberValidate: val }) }}
                        style={{ width: responsiveWidth(37), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(5), padding: 0, textAlign: isRTL ? 'right' : 'left', color:'black' }}
                        placeholder={Strings.phoneNumber}
                        placeholderTextColor={colors.placeholderGray}
                        keyboardType={'phone-pad'}
                        ref={(input) => { this.phoneNumber = input; }}
                    />
                </View>

            </View>
        )
    }


    newPriceInput = () => {

        const { isRTL } = this.props
        const { restaurantName, restaurantNameValidate, price, price1, price2 } = this.state
        console.log('restaurantName : ', restaurantName)
        return (

            <View style={{ width: responsiveWidth(37), }}>

                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.priceForKg}</Text>
                <View style={{ flexDirection: 'row', position: 'relative', alignSelf: 'center', borderRadius: moderateScale(1), backgroundColor: colors.lightGray, width: responsiveWidth(37), justifyContent: 'center' }} >
                    <TextInput
                        placeholder={'00'}
                        onChangeText={(val) => {
                            if (val.length >= 2) { this._price2.focus() }
                            //if ((parseFloat(val + '.' + price2) > 11)) { Alert.alert(Strings.PriceCantBeOver11EGP) } else { this.setState({ price1: val, price: parseFloat(price1 + '.' + price2) }) }
                            console.log('max val : ', parseFloat(price1 + '.' + price2))
                            this.setState({ price1: val, price: parseFloat(price1 + '.' + price2) })
                        }}
                        placeholderTextColor={colors.placeholderGray}
                        style={{ color: 'black', width: responsiveWidth(12), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(5), padding: 0, textAlign: 'center' }}
                        maxLength={2}
                        ref={ref =>
                            this._price1 = ref
                        }
                        value={price1}
                        keyboardType='number-pad'
                    />
                    <Text style={{ height: responsiveHeight(5), paddingTop: responsiveHeight(1) }}>.</Text>

                    <TextInput
                        placeholder={'00'}
                        placeholderTextColor={colors.placeholderGray}
                        onChangeText={(val) => {
                            if (val == '') { this._price1.focus() }
                            //if ((parseFloat(price1 + '.' + val) > 11.00)) { Alert.alert(Strings.PriceCantBeOver11EGP) } else { this.setState({ price2: val, price: parseFloat(price1 + '.' + price2) }) }
                            //this.setState({price2:val})
                            this.setState({ price2: val, price: parseFloat(price1 + '.' + price2) })
                        }}

                        style={{ color: 'black', width: responsiveWidth(12), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(5), padding: 0, textAlign: 'center' }}
                        maxLength={2}
                        ref={ref =>
                            this._price2 = ref
                        }
                        value={price2}
                        keyboardType='number-pad'
                    />


                    <TouchableOpacity
                        style={{ position: 'absolute', width: responsiveWidth(37), height: responsiveHeight(5) }}
                        onPress={() => this._price1.focus()}

                        ref={ref =>
                            this._btnPrice = ref
                        }>
                        <Text style={{ paddingHorizontal: moderateScale(3), color: 'black', textAlign: 'center', lineHeight: responsiveHeight(6) }}></Text>
                    </TouchableOpacity>


                </View>
                {restaurantNameValidate.length == 0 &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )

    }

    newPriceInputold = () => {
        const { isRTL } = this.props
        const { restaurantName, restaurantNameValidate, price } = this.state
        console.log('restaurantName : ', restaurantName)
        return (

            <View style={{ width: responsiveWidth(37), }}>

                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.priceForKg}</Text>
                <View style={{ position: 'relative', borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                    <TextInput
                        //onChangeText={(val) => { this.setState({ newPrice: parseFloat(val) / 100 }) }}
                        //value={newPrice.toString()}
                        onChangeText={text => {
                            if (text / 100 > 11.00) {
                                //RNToasty.Error({ title: Strings.PriceCantBeOver11EGP })
                                Alert.alert(Strings.PriceCantBeOver11EGP)
                                this._btnPrice.focus()
                                this.setState({
                                    price: price
                                    //newPrice: this.state.newPrice,
                                    //priceEdit: false
                                })
                            } else if (isNaN((parseFloat(text) / 100).toFixed(2))) {
                                console.log(text)
                                this.setState({
                                    price: '00.00',

                                })

                            } else {
                                console.log(`${(parseFloat(text) / 100).toFixed(2)}`)
                                this.setState({
                                    price: (parseFloat(text) / 100).toFixed(2),

                                })
                            }
                        }
                        }
                        placeholderTextColor={colors.placeholderGray}
                        style={{ color: colors.lightGray, width: responsiveWidth(37), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(6), textAlign: 'center' }}
                        ref={ref =>
                            this._price = ref
                        }
                        maxLength={4}
                    />

                    <TouchableOpacity
                        style={{ position: 'absolute', width: responsiveWidth(37), height: responsiveHeight(6) }}
                        onPress={() => this._price.focus()}

                        ref={ref =>
                            this._btnPrice = ref
                        }>
                        <Text style={{ paddingHorizontal: moderateScale(3), color: 'black', textAlign: 'center', lineHeight: responsiveHeight(6) }}>{price.toString()}</Text>
                    </TouchableOpacity>


                </View>
                {restaurantNameValidate.length == 0 &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }


    payTypePicker = () => {
        const { isRTL } = this.props
        const { selectedItems } = this.state
        return (
            <View style={{ width: responsiveWidth(37), alignSelf: 'center' }} >

                {/*Oil Type */}
                <Animatable.View style={{ width: responsiveWidth(37), }}>

                    <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.payType}</Text>

                    <View style={{ alignItems: 'center', justifyContent: 'center', backgroundColor: colors.lightGray, width: responsiveWidth(37), height: responsiveHeight(5), alignSelf: 'center' }}>

                        <RNPickerSelect
                            onValueChange={
                                (item) => {
                                    this.setState({
                                        selectPayType: item,
                                        selectPayTypeValidate: item
                                    });
                                }}
                            items={this.payTypes}

                            placeholder={{ label: Strings.payType, value: '' }}
                            style={{
                                inputIOS: { textAlign: isRTL ? 'right' : 'left', fontSize: responsiveFontSize(6), height: responsiveHeight(5), fontFamily: isRTL ? arrabicFont : englishFont, paddingVertical: 9, paddingHorizontal: moderateScale(3), paddingRight: responsiveWidth(5), color: 'gray', },
                                inputAndroid: { textAlign: isRTL ? 'right' : 'left', fontSize: responsiveFontSize(6), height: responsiveHeight(5), fontFamily: isRTL ? arrabicFont : englishFont, paddingHorizontal: moderateScale(3), paddingRight: responsiveWidth(5), paddingVertical: 8, color: 'gray', }
                            }}
                            placeholderTextColor={colors.placeholderGray}
                            Icon={() => { return (<Icon name='down' type='AntDesign' style={{ color: 'black', fontSize: responsiveFontSize(6), top: responsiveHeight(1.5), right: responsiveWidth(1) }} />) }}
                        />

                    </View>
                    {this.state.selectPayTypeValidate.length == 0 &&
                        <Text style={{ color: 'red', marginTop: moderateScale(1), marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                    }
                </Animatable.View>
            </View>
        )
    }

    classPicker = () => {
        const { isRTL } = this.props
        const { selectedItems } = this.state
        return (
            <View style={{ width: responsiveWidth(37), alignSelf: isRTL ? 'flex-end' : 'flex-start' }} >

                {/*Oil Type */}
                <Animatable.View style={{ width: responsiveWidth(37), }}>

                    <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.selectClass}</Text>

                    <View style={{ alignItems: 'center', justifyContent: 'center', backgroundColor: colors.lightGray, width: responsiveWidth(37), height: responsiveHeight(5), alignSelf: 'center' }}>

                        <RNPickerSelect
                            onValueChange={
                                (item) => {
                                    this.setState({
                                        selectClass: item,
                                        selectClassValidate: item
                                    });
                                }}
                            items={this.classes}

                            placeholder={{ label: '', value: '' }}
                            style={{
                                inputIOS: { textAlign: isRTL ? 'right' : 'left', fontSize: responsiveFontSize(6), height: responsiveHeight(5), fontFamily: isRTL ? arrabicFont : englishFont, paddingVertical: 9, paddingHorizontal: moderateScale(3), paddingRight: responsiveWidth(5), color: 'black', },
                                inputAndroid: { textAlign: isRTL ? 'right' : 'left', fontSize: responsiveFontSize(6), height: responsiveHeight(5), fontFamily: isRTL ? arrabicFont : englishFont, paddingHorizontal: moderateScale(3), paddingRight: responsiveWidth(5), paddingVertical: 8, color: 'black', }
                            }}
                            placeholderTextColor={colors.placeholderGray}
                            Icon={() => { return (<Icon name='down' type='AntDesign' style={{ color: 'black', fontSize: responsiveFontSize(6), top: responsiveHeight(1.5), right: responsiveWidth(1) }} />) }}
                        />

                    </View>
                    {this.state.selectClassValidate.length == 0 &&
                        <Text style={{ color: 'red', marginTop: moderateScale(1), marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                    }
                </Animatable.View>
            </View>
        )
    }

    oilTypePicker = () => {
        const { isRTL } = this.props
        const { selectedItems } = this.state
        return (
            <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', width: responsiveWidth(37), alignSelf: 'center' }} >

                {/*Oil Type */}
                <Animatable.View style={{ width: responsiveWidth(37), }}>

                    <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.oilType}</Text>

                    <View style={{ alignItems: 'center', justifyContent: 'center', backgroundColor: colors.lightGray, width: responsiveWidth(37), height: responsiveHeight(5), alignSelf: 'center' }}>

                        <RNPickerSelect
                            onValueChange={
                                (item) => {
                                    this.setState({
                                        oilType: item,
                                        oilTypeValidate: item
                                    });
                                }}
                            items={this.oilTypes}

                            placeholder={{ label: Strings.oilType, value: '' }}
                            style={{
                                inputIOS: { textAlign: isRTL ? 'right' : 'left', fontSize: responsiveFontSize(6), height: responsiveHeight(5), fontFamily: isRTL ? arrabicFont : englishFont, paddingVertical: 9, paddingHorizontal: moderateScale(3), paddingRight: responsiveWidth(5), color: 'black', },
                                inputAndroid: { textAlign: isRTL ? 'right' : 'left', fontSize: responsiveFontSize(6), height: responsiveHeight(5), fontFamily: isRTL ? arrabicFont : englishFont, paddingHorizontal: moderateScale(3), paddingRight: responsiveWidth(5), paddingVertical: 8, color: 'black', }
                            }}
                            placeholderTextColor={colors.placeholderGray}
                            Icon={() => { return (<Icon name='down' type='AntDesign' style={{ color: 'black', fontSize: responsiveFontSize(6), top: responsiveHeight(1.5), right: responsiveWidth(1) }} />) }}
                        />

                    </View>
                    {this.state.oilTypeValidate.length == 0 &&
                        <Text style={{ color: 'red', marginTop: moderateScale(1), marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                    }
                </Animatable.View>
            </View>
        )

    }

    PriceInputAvgKgPicker = () => {
        const { isRTL } = this.props

        return (
            <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'space-between', width: responsiveWidth(80), alignSelf: 'center', marginTop: moderateScale(7), }} >

                {/*Oil Type */}
                <Animatable.View style={{ width: responsiveWidth(37), }}>

                    <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.estimatedQuantity}</Text>
                    <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: colors.lightGray, width: responsiveWidth(37), height: responsiveHeight(5) }}>

                        <RNPickerSelect
                            onValueChange={
                                (item) => {
                                    this.setState({
                                        selectAvgKg: item,
                                        selectAvgKgValidate: item
                                    });
                                }}
                            items={this.averageKg}

                            placeholder={{ label: Strings.estimatedQuantity, value: '' }}
                            style={{
                                inputIOS: { textAlign: 'center', fontSize: responsiveFontSize(6), height: responsiveHeight(5), fontFamily: isRTL ? arrabicFont : englishFont, justifyContent: 'center', alignItems: 'center', color: 'black', width: responsiveWidth(35) },
                                inputAndroid: { textAlign: 'center', fontSize: responsiveFontSize(6), height: responsiveHeight(5), fontFamily: isRTL ? arrabicFont : englishFont, justifyContent: 'center', alignItems: 'center', color: 'black', width: responsiveWidth(35) }
                            }}
                            placeholderTextColor={colors.placeholderGray}
                            Icon={() => { return (<Icon name='down' type='AntDesign' style={{ color: 'black', fontSize: responsiveFontSize(6), top: responsiveHeight(1.5), right: responsiveWidth(1) }} />) }}
                        />


                    </View>
                    {this.state.selectAvgKgValidate.length == 0 &&
                        <Text style={{ color: 'red', marginTop: moderateScale(1), marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                    }
                </Animatable.View>

                {this.newPriceInput()}

            </View>
        )
    }


    onChangeTextPrice(value) {
        const newPrice = parseInt(value, 10).toLocaleString(['ban', 'id']);
        console.log(value)
        this.setState({ price: newPrice });
    }

    priceInput = () => {
        const { isRTL } = this.props
        const { selectedResturantPrice, price } = this.state
        return (
            <View style={{ width: responsiveWidth(37), }}>
                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.priceForKg}</Text>
                <View style={{ borderRadius: moderateScale(1), alignSelf: 'center', position: 'relative' }} >
                    <TextInputMask
                        autoFocus={this.state.priceRef}
                        //onFocus={()=>this.setState({price:'00.00'})}
                        type={'money'}
                        options={{
                            precision: 2,
                            separator: '.',
                            delimiter: '.',
                            unit: '',
                            suffixUnit: ''
                        }}
                        value={(price).toString()}
                        onChangeText={text => {
                            if (text > 11.00) {
                                // RNToasty.Error({ title: Strings.PriceCantBeOver11EGP })
                                Alert.alert(Strings.PriceCantBeOver11EGP)
                                this.setState({
                                    price: price,
                                    // priceEdit: false
                                })
                            } else {
                                this.setState({
                                    price: text,

                                    //priceEdit: true
                                })
                            }
                        }
                        }
                        maxLength={5}
                        ref={ref =>
                            this._priceee = ref
                        }
                        placeholder={'00.00'}
                        placeholderTextColor={colors.placeholderGray}
                        caretHidden={true}
                        style={{ fontSize: responsiveFontSize(6), width: responsiveWidth(37), backgroundColor: colors.lightGray, paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(6), textAlign: 'center', color:'black' }}
                    />
                    <TouchableOpacity onPress={() => this.formatText(5)}
                        style={{ position: 'absolute', width: responsiveWidth(0), height: responsiveHeight(0) }}>

                    </TouchableOpacity>

                </View>
                {price == '' &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }

    handlePrice = () => {

        this.setState({ priceRef: true })

        //this.secondTextInput.focus();
        // console.log(this._price.focus())
    }

    formatText(props) {

        if (props.amount !== undefined && props.currencyCode !== undefined) {
            CurrencyFormatter.format(
                props.amount, props.currencyCode,
                (formattedText) => {
                    this.setState({ text: formattedText });
                    console.log(formattedText)
                },
            );
        }
    }

    addressByDetailsInput = () => {
        const { isRTL } = this.props
        const { addressByDetails, addressByDetailsValidate } = this.state
        return (
            <View style={{ marginTop: moderateScale(7), width: responsiveWidth(80), alignSelf: 'center' }}>

                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.addressByDetails}</Text>
                <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                    <TextInput
                        onChangeText={(val) => { this.setState({ addressByDetails: val, addressByDetailsValidate: val }) }}
                        style={{ width: responsiveWidth(80), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(5), color:'black' }}
                        placeholder={Strings.addressByDetails}
                        placeholderTextColor={colors.placeholderGray}
                    />
                </View>
                {addressByDetailsValidate.length == 0 &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }

    ResSpacebranchsNoPicker_wheel = () => {
        const { isRTL } = this.props
        const { selectedItems } = this.state
        return (


            <View style={{ position:'relative', flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'space-between', width: responsiveWidth(37), alignSelf: 'center',marginTop: moderateScale(7),zIndex:9999999 }} >

                {/*Oil Type */}
                <Animatable.View style={{position:'absolute', width: responsiveWidth(37) ,zIndex:9999999 }}>
                <WheelPicker
               
          selectedItem={this.state.selectedItem}
          data={this.spacesWheel}
          onItemSelected={this.onItemSelected}
        />
                </Animatable.View>
            </View>
        )
    }


    ResSpacebranchsNoPicker = () => {
        const { isRTL } = this.props
        const { selectedItems } = this.state
        return (


            <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'space-between', width: responsiveWidth(37), alignSelf: 'center', marginTop: moderateScale(7), }} >

                {/*Oil Type */}
                <Animatable.View style={{ width: responsiveWidth(37) }}>

                    <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.m2}</Text>

                    <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: colors.lightGray, width: responsiveWidth(37), height: responsiveHeight(5) }}>

                        <RNPickerSelect
                            onValueChange={
                                (item) => {
                                    this.setState({
                                        selectResturantSpace: item,
                                        selectResturantSpaceValidate: item
                                    });
                                }}
                            items={this.spaces}

                            placeholder={{ label: Strings.resSpace, value: '' }}
                            style={{
                                inputIOS: { textAlign: 'center', fontSize: responsiveFontSize(6), height: responsiveHeight(5), fontFamily: isRTL ? arrabicFont : englishFont, justifyContent: 'center', alignItems: 'center', color: 'black', width: responsiveWidth(35) },
                                inputAndroid: { textAlign: 'center', fontSize: responsiveFontSize(6), height: responsiveHeight(5), fontFamily: isRTL ? arrabicFont : englishFont, justifyContent: 'center', alignItems: 'center', color: 'black', width: responsiveWidth(35) }
                            }}
                            placeholderTextColor={colors.placeholderGray}
                            Icon={() => { return (<Icon name='down' type='AntDesign' style={{ color: 'black', fontSize: responsiveFontSize(6), top: responsiveHeight(1.5), right: responsiveWidth(0) }} />) }}
                        />



                    </View>
                    {this.state.selectResturantSpaceValidate.length == 0 &&
                        <Text style={{ color: 'red', marginTop: moderateScale(1), marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                    }
                </Animatable.View>



                {/*<Animatable.View style={{ width: responsiveWidth(37) }}>

                    <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.outletnumber}</Text>

                    <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: colors.lightGray, width: responsiveWidth(37), height: responsiveHeight(6), alignSelf: 'center' }}>

                        <TextInput
                            value={this.state.outletCode}
                            onChangeText={(val) => { this.setState({ outletCode: val, outletCodeValidate: val }) }}
                            style={{ width: responsiveWidth(35), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont,  textAlign: isRTL ? 'right' : 'left' }}
                            placeholder={Strings.outletnumber}

                        />

                    </View>
                    {this.state.outletCodeValidate.length == 0 &&
                        <Text style={{ color: 'red', marginTop: moderateScale(1), marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                    }
                </Animatable.View>*/}



                {/*<Animatable.View style={{ width: responsiveWidth(37) }}>

                    <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.branchesNo}</Text>

                    <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: colors.lightGray, width: responsiveWidth(37), height: responsiveHeight(6), alignSelf: 'center' }}>


                        <RNPickerSelect
                            onValueChange={
                                (item) => {
                                    this.setState({
                                        selectBranchNo: item,
                                        selectBranchNoValidate: item
                                    });
                                    this.addRequiredAddress(item)
                                    this.pushCoordinate(item)
                                }}
                            items={this.branchsNo}

                            placeholder={{ label: Strings.branchesNo, value: '' }}
                            style={{
                                inputIOS: { textAlign: 'center', fontSize: responsiveFontSize(6), height: responsiveHeight(6), fontFamily: isRTL ? arrabicFont : englishFont, justifyContent: 'center', alignItems: 'center', color: 'gray', width: responsiveWidth(35) },
                                inputAndroid: { textAlign: 'center', fontSize: responsiveFontSize(6), height: responsiveHeight(6), fontFamily: isRTL ? arrabicFont : englishFont, justifyContent: 'center', alignItems: 'center', color: 'gray', width: responsiveWidth(35) }
                            }}
                            placeholderTextColor={'gray'}
                            Icon={() => { return (<Icon name='down' type='AntDesign' style={{ color: 'black', fontSize: responsiveFontSize(6), top: responsiveHeight(2), right: responsiveWidth(0) }} />) }}
                        />


                    </View>
                    {this.state.selectBranchNoValidate.length == 0 &&
                        <Text style={{ color: 'red', marginTop: moderateScale(1), marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                    }
                </Animatable.View>*/}
            </View>
        )
    }

    addRequiredAddress = (count) => {
        this.setState({ addressCount: [] })
        var NewAddressCount = []
        for (var i = 1; i <= count; i++) {
            NewAddressCount.push(i)
        }
        this.setState({ addressCount: NewAddressCount })
        console.log('NewAddressCount : ' + NewAddressCount)
    }


    firstNameInput = () => {
        const { isRTL } = this.props
        const { firstName, firstNameValidate } = this.state
        return (
            <View style={{ marginTop: moderateScale(7), width: responsiveWidth(37), }}>

                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, marginHorizontal: moderateScale(0), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.firstName}</Text>
                <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                    <TextInput
                        onChangeText={(val) => { this.setState({ firstName: val, firstNameValidate: val }) }}
                        style={{ width: responsiveWidth(37), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(5), padding: 0, textAlign: isRTL ? 'right' : 'left', color:'black' }}
                        placeholder={Strings.firstName}
                        placeholderTextColor={colors.placeholderGray}
                        ref={(ref) => this.firstName = ref}
                        returnKeyType="next"
                        onSubmitEditing={() => this.lastName.focus()}
                    />
                </View>
                {firstNameValidate.length == 0 &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }

    lastNameInput = () => {
        const { isRTL } = this.props
        const { lastName, lastNameValidate } = this.state
        return (
            <View style={{ marginTop: moderateScale(7), width: responsiveWidth(37) }}>

                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, marginHorizontal: moderateScale(0), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.lastName}</Text>
                <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                    <TextInput
                        onChangeText={(val) => { this.setState({ lastName: val, lastNameValidate: val }) }}
                        style={{ width: responsiveWidth(37), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(5), padding: 0, textAlign: isRTL ? 'right' : 'left', color:'black' }}
                        placeholder={Strings.lastName}
                        placeholderTextColor={colors.placeholderGray}
                        ref={(ref) => this.lastName = ref}
                        returnKeyType="next"
                        onSubmitEditing={() => this.contactNo.focus()}
                    />
                </View>
                {lastNameValidate.length == 0 &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }

    contactNoInput = () => {
        const { isRTL } = this.props
        const { contactNo, contactNoValidate } = this.state
        return (
            <View style={{ marginTop: moderateScale(6), width: responsiveWidth(80), alignSelf: 'center' }}>

                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.contactNo}</Text>
                <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                    <TextInput
                        onChangeText={(val) => { this.setState({ contactNo: val, contactNoValidate: val }) }}
                        style={{ width: responsiveWidth(80), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(5), padding: 0, textAlign: isRTL ? 'right' : 'left',color:'black' }}
                        placeholder={Strings.contactNo}
                        placeholderTextColor={colors.placeholderGray}
                        keyboardType={'phone-pad'}
                        ref={(ref) => this.contactNo = ref}
                    />
                </View>
                {contactNoValidate.length == 0 &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }

    titlePicker = () => {
        const { isRTL } = this.props
        const { title, titleValidate } = this.state
        return (
            <View style={{ marginTop: moderateScale(5), flexDirection: isRTL ? 'row-reverse' : 'row', width: responsiveWidth(80), alignSelf: 'center' }} >

                <Animatable.View style={{ marginTop: moderateScale(5), width: responsiveWidth(80), }}>

                    <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.jobTitle}</Text>
                    {/*Oil Type */}

                    <View style={{ alignItems: 'center', justifyContent: 'center', backgroundColor: colors.lightGray, width: responsiveWidth(80), height: responsiveHeight(5), alignSelf: 'center' }}>


                        <RNPickerSelect
                            onValueChange={
                                (item) => {
                                    this.setState({
                                        title: item,
                                        titleValidate: item
                                    });
                                }}
                            items={this.jobTitles}

                            placeholder={{ label: Strings.jobTitle, value: '' }}
                            style={{
                                inputIOS: { textAlign: isRTL ? 'right' : 'left', fontSize: responsiveFontSize(6), height: responsiveHeight(5), fontFamily: isRTL ? arrabicFont : englishFont, paddingVertical: 9, paddingHorizontal: moderateScale(3), paddingRight: responsiveWidth(5), color: 'black', },
                                inputAndroid: { textAlign: isRTL ? 'right' : 'left', fontSize: responsiveFontSize(6), height: responsiveHeight(5), fontFamily: isRTL ? arrabicFont : englishFont, paddingHorizontal: moderateScale(3), paddingRight: responsiveWidth(5), paddingVertical: 8, color: 'black', }
                            }}
                            placeholderTextColor={colors.placeholderGray}
                            Icon={() => { return (<Icon name='down' type='AntDesign' style={{ color: 'black', fontSize: responsiveFontSize(6), top: responsiveHeight(1.5), right: responsiveWidth(1) }} />) }}
                        />


                        {/*<SectionedMultiSelect
                            expandDropDowns
                            //modalAnimationType='slide'
                            //loading={loading}
                            showDropDowns={false}
                            modalWithTouchable
                            hideConfirm
                            searchPlaceholderText={Strings.search}
                            styles={{
                                selectToggle: { width: responsiveWidth(74), height: responsiveHeight(6), borderRadius: moderateScale(3), alignItems: 'center', justifyContent: 'center', },
                                selectToggleText: { textAlign: isRTL ? 'right' : 'left', fontSize: responsiveFontSize(6), color: 'gray', marginTop: moderateScale(8), fontFamily: isRTL ? arrabicFont : englishFont },
                                subItemText: { textAlign: isRTL ? 'right' : 'left' },
                                itemText: { fontSize: responsiveFontSize(10), textAlign: isRTL ? 'right' : 'left' },
                                container: { height: responsiveHeight(60), position: 'absolute', width: responsiveWidth(80), top: responsiveHeight(18), alignSelf: 'center' },
                                searchTextInput: { textAlign: isRTL ? 'right' : 'left', marginHorizontal: moderateScale(5) },
                            }}
                            items={titles}
                            alwaysShowSelectText
                            single
                            searchPlaceholderText={Strings.search}
                            uniqueKey="id"
                            subKey="children"
                            selectText={this.state.selecTitle ? this.state.selecTitle : Strings.title}

                            readOnlyHeadings={true}
                            onSelectedItemsChange={(selectedItems) => {
                                // this.setState({ countries: selectedItems });
                            }
                            }
                            onSelectedItemObjectsChange={(selectedItems) => {
                                console.log("ITEM2   ", selectedItems[0].name)
                                this.setState({ selecTitle: selectedItems[0].name, selecTitleValidate: selectedItems[0].name });
                            }
                            }

                        //onConfirm={() => this.setState({ selecTitle: '' })}
                        />*/}
                    </View>
                    {titleValidate.length == 0 &&
                        <Text style={{ color: 'red', marginTop: moderateScale(1), marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                    }
                </Animatable.View>
            </View>
        )
    }

    titleInput = () => {
        const { isRTL } = this.props
        const { title, titleValidate } = this.state
        return (
            <View style={{ marginTop: moderateScale(5), width: responsiveWidth(80), alignSelf: 'center' }}>

                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.jobTitle}</Text>
                <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                    <TextInput
                        onChangeText={(val) => { this.setState({ title: val, titleValidate: val }) }}
                        style={{ width: responsiveWidth(80), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(5), textAlign: isRTL ? 'right' : 'left', color:'black' }}
                        placeholder={Strings.jobTitle}
                        placeholderTextColor={colors.placeholderGray}

                    />
                </View>
                {titleValidate.length == 0 &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }

    emailInput = () => {
        const { isRTL } = this.props
        const { email, emailValidate } = this.state
        return (
            <View style={{ marginTop: moderateScale(5), width: responsiveWidth(80), alignSelf: 'center' }}>

                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.email}</Text>
                <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                    <TextInput
                        onChangeText={(val) => { this.setState({ email: val, emailValidate: val }) }}
                        style={{ width: responsiveWidth(80), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(5), padding: 0, textAlign: isRTL ? 'right' : 'left', color:'black' }}
                        placeholder={Strings.email}
                        placeholderTextColor={colors.placeholderGray}
                        keyboardType={'email-address'}
                        ref={(ref) => this.email = ref}
                        returnKeyType="next"
                        onSubmitEditing={() => this.userName.focus()}
                    />
                </View>
                {emailValidate.length == 0 &&
                    /*<Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>*/
                    <></>
                }
            </View>
        )
    }

    userNameInput = () => {
        const { isRTL, currentUser } = this.props
        const { userName, userNameValidate } = this.state
        return (
            <View style={{ marginTop: moderateScale(5), width: responsiveWidth(80), alignSelf: 'center' }}>

                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.userName}</Text>
                <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                    <TextInput
                        onChangeText={(val) => { this.setState({ userName: val, userNameValidate: val }) }}
                        style={{ width: responsiveWidth(80), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(5), padding: 0, textAlign: isRTL ? 'right' : 'left' , color:'black'}}
                        placeholder={Strings.userName}
                        placeholderTextColor={colors.placeholderGray}
                        ref={(ref) => this.userName = ref}
                        returnKeyType="next"
                        onSubmitEditing={() => this.password.focus()}
                    />
                </View>
                {userNameValidate.length == 0 && (currentUser.user.type == 'ADMIN' || currentUser.user.type == 'OPERATION') &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }

    passwordInput = () => {
        const { isRTL, currentUser } = this.props
        const { password, passwordValidate, confirmPassword, confirmPasswordValidate, confirmPasswordEqualPasswordValidate } = this.state
        return (

            <View style={{ marginTop: moderateScale(5), width: responsiveWidth(80), alignSelf: 'center' }}>

                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.password}</Text>
                <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                    <TextInput
                        onChangeText={(val) => { this.setState({ password: val, passwordValidate: val }) }}
                        style={{ width: responsiveWidth(80), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(5), padding: 0, textAlign: isRTL ? 'right' : 'left', color:'black' }}
                        placeholder={Strings.password}
                        placeholderTextColor={colors.placeholderGray}
                        secureTextEntry
                        ref={(ref) => this.password = ref}
                        returnKeyType="next"
                        onSubmitEditing={() => this.confirmPassword.focus()}
                    />
                </View>
                {passwordValidate.length == 0 && (currentUser.user.type == 'ADMIN' || currentUser.user.type == 'OPERATION') &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>





        )
    }


    confirmPasswordInput = () => {
        const { isRTL, currentUser } = this.props
        const { confirmPassword, confirmPasswordValidate, confirmPasswordEqualPasswordValidate, password } = this.state
        return (
            <View style={{ marginTop: moderateScale(5), width: responsiveWidth(80), alignSelf: 'center' }}>

                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.confirmPassword}</Text>
                <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                    <TextInput
                        onChangeText={(val) => { this.setState({ confirmPassword: val, confirmPasswordValidate: val }) }}
                        style={{ width: responsiveWidth(80), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(5), padding: 0, textAlign: isRTL ? 'right' : 'left', color:'black' }}
                        placeholder={Strings.confirmPassword}
                        placeholderTextColor={colors.placeholderGray}
                        secureTextEntry
                        ref={(ref) => this.confirmPassword = ref}

                    />
                </View>
                {confirmPasswordValidate.length == 0 && (currentUser.user.type == 'ADMIN' || currentUser.user.type == 'OPERATION') &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
                {confirmPasswordEqualPasswordValidate.length == 0 && confirmPasswordValidate.length > 0 && (confirmPassword != password) &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.confirmPasswordMustEqualPassword}</Text>
                }
            </View>
        )
    }

    pushCoordinate(count) {
        var coordinate = []
        for (var i = 0; i < count; i++) {
            coordinate.push({ latitude: this.state.latitude, longitude: this.state.longitude })
        }
        this.setState({ Coordinate: coordinate })
    }

    populateBranchesAreas(val, index) {
        var branches = [...this.state.branches]
        if (branches[index]) {
            branches[index] = { "area": val, "city": branches[index].city, 'address': branches[index].address, "destination": [branches[index].destination[0], branches[index].destination[1]] }
            this.setState({ branches: branches })
        } else {
            branches[index] = { "area": val, "city": '', 'address': '', "destination": [this.state.latitude, this.state.longitude] }
            this.setState({ branches: branches })
        }
        console.log('adddd : ', branches)
    }

    populateBranchesCity(val, index) {
        var branches = [...this.state.branches]
        //var branches = this.state.branches
        if (branches[index]) {
            branches[index] = { "area": branches[index].area, "city": val, 'address': branches[index].address, "destination": [branches[index].destination[0], branches[index].destination[1]] }
            this.setState({ branches: branches })
        } else {
            branches[index] = { "area": '', "city": val, 'address': '', "destination": [this.state.latitude, this.state.longitude] }
            this.setState({ branches: branches })
        }
        console.log('adddd : ', branches)
    }

    populateBranchesAddress(val, index) {
        var branches = [...this.state.branches]
        //var branches = this.state.branches
        if (branches[index]) {
            branches[index] = { "area": branches[index].area, "city": branches[index].city, 'address': val, "destination": [branches[index].destination[0], branches[index].destination[1]] }
            this.setState({ branches: branches })
        } else {
            branches[index] = { "area": '', "city": '', 'address': val, "destination": [this.state.latitude, this.state.longitude] }
            this.setState({ branches: branches })
        }
        console.log('adddd : ', branches)
    }

    /*populateBranchesMap(latitude, longitude, index) {
        var branches = [...this.state.branches]
        if (branches[index]) {
            branches[index] = { "address": branches[index].address, "destination": [latitude, longitude] }
            this.setState({ branches: branches })
        } else {
            branches[index] = { 'address': '', 'destination': [latitude, longitude] }
            this.setState({ branches: branches })
        }
    }*/

    populateBranchesMap(latitude, longitude, index) {
        var branches = this.state.branches
        var Coordinate = this.state.Coordinate

        if (branches[index]) {
            branches[index] = { "area": branches[index].area, "city": branches[index].city, 'address': branches[index].address, "destination": [latitude, longitude] }
            this.setState({ branches: branches })
            Coordinate[index] = { latitude: latitude, longitude: longitude }
            this.setState({ Coordinate: Coordinate })
            console.log('Coordinate : ', Coordinate)
            console.log('branches : ', branches)
        } else {
            branches[index] = { 'area': '', "city": '', 'address': '', 'destination': [latitude, longitude] }
            this.setState({ branches: branches })
            Coordinate[index] = { latitude: latitude, longitude: longitude }
            this.setState({ Coordinate: Coordinate })
            console.log('branches : ', branches)
        }
    }


    addressMapInput = () => {
        const { isRTL, currentUser } = this.props
        const { address, branches, addressValidate, cityValidate, cities, areas, mapSwitchValue, latitude, longitude, getLocationButton } = this.state

        return (
            <>
                {this.state.addressCount.map((item, index) => (
                    <View>


                        <View style={{ marginTop: moderateScale(6), width: responsiveWidth(80), alignSelf: 'center' }}>

                            <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', width: responsiveWidth(80), height: responsiveHeight(5) }}>
                                {/*<Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), lineHeight: responsiveHeight(7), alignSelf: isRTL ? 'flex-end' : 'flex-start', }}>{item}</Text>
    <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), lineHeight: responsiveHeight(7), alignSelf: isRTL ? 'flex-end' : 'flex-start', }}>. </Text>*/}
                                <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >


                                    <RNPickerSelect

                                        onValueChange={
                                            (item) => {
                                                [
                                                    this.setState({
                                                        city: item,
                                                        cityValidate: item,
                                                        area: ''
                                                    }),
                                                    this.populateBranchesCity(item, index),
                                                    this.getAreas(item)]
                                            }}
                                        items={cities}

                                        placeholder={{ label: Strings.city, value: '' }}
                                        style={{
                                            inputIOS: { textAlign: isRTL ? 'right' : 'left', fontSize: responsiveFontSize(6), height: responsiveHeight(5), width: responsiveWidth(80), fontFamily: isRTL ? arrabicFont : englishFont, paddingVertical: 9, paddingHorizontal: moderateScale(3), paddingRight: responsiveWidth(5), color: 'black', },
                                            inputAndroid: { textAlign: isRTL ? 'right' : 'left', fontSize: responsiveFontSize(6), height: responsiveHeight(5), width: responsiveWidth(80), fontFamily: isRTL ? arrabicFont : englishFont, paddingHorizontal: moderateScale(3), paddingRight: responsiveWidth(5), paddingVertical: 8, color: 'black', }
                                        }}
                                        placeholderTextColor={colors.placeholderGray}
                                        Icon={() => { return (<Icon name='down' type='AntDesign' style={{ color: 'black', fontSize: responsiveFontSize(6), top: responsiveHeight(1.5), right: responsiveWidth(1) }} />) }}
                                    />



                                    {/*<SectionedMultiSelect
                                        expandDropDowns
                                        //modalAnimationType='slide'
                                        //loading={loading}
                                        showDropDowns={false}
                                        modalWithTouchable
                                        hideConfirm
                                        searchPlaceholderText={Strings.search}
                                        styles={{
                                            selectToggle: { width: responsiveWidth(80), height: responsiveHeight(6), borderRadius: moderateScale(3), alignItems: 'center', justifyContent: 'center', paddingHorizontal: moderateScale(3) },
                                            selectToggleText: { textAlign: isRTL ? 'right' : 'left', fontSize: responsiveFontSize(6), color: 'gray', marginTop: moderateScale(8), fontFamily: isRTL ? arrabicFont : englishFont },
                                            subItemText: { textAlign: isRTL ? 'right' : 'left' },
                                            itemText: { fontSize: responsiveFontSize(10), textAlign: isRTL ? 'right' : 'left' },
                                            container: { height: responsiveHeight(60), position: 'absolute', width: responsiveWidth(80), top: responsiveHeight(18), alignSelf: 'center' },
                                            searchTextInput: { textAlign: isRTL ? 'right' : 'left', marginHorizontal: moderateScale(5) },
                                        }}
                                        items={cities}
                                        alwaysShowSelectText
                                        single
                                        searchPlaceholderText={Strings.search}
                                        uniqueKey="id"
                                        subKey="children"
                                        selectText={this.state.city ? this.state.city : Strings.city}

                                        readOnlyHeadings={true}
                                        onSelectedItemsChange={(selectedItems) => {
                                            // this.setState({ countries: selectedItems });
                                        }
                                        }
                                        onSelectedItemObjectsChange={(selectedItems) => {
                                            console.log("ITEM2   ", selectedItems[0].name)
                                            this.setState({ city: selectedItems[0].name, cityValidate: selectedItems[0].name });
                                            this.populateBranchesCity(selectedItems[0].id, index)
                                            this.getAreas(selectedItems[0].id)
                                        }
                                        }

                                    //onConfirm={() => this.setState({ selecTitle: '' })}
                                    />*/}




                                </View>
                            </View>
                            {branches.length > 0 && branches[index] != null ?
                                branches[index].city == '' ?
                                    <Text style={{ color: 'red', marginHorizontal: moderateScale(5), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                                    :
                                    null
                                :
                                branches[index] == null && cityValidate == '' &&
                                <Text style={{ color: 'red', marginHorizontal: moderateScale(5), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                            }
                        </View>



                        <View style={{ marginTop: moderateScale(6), width: responsiveWidth(80), alignSelf: 'center' }}>




                            <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', width: responsiveWidth(80), height: responsiveHeight(5) }}>
                                {/*<Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), lineHeight: responsiveHeight(7), alignSelf: isRTL ? 'flex-end' : 'flex-start', }}>{item}</Text>
                                <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), lineHeight: responsiveHeight(7), alignSelf: isRTL ? 'flex-end' : 'flex-start', }}>. </Text>*/}
                                <View style={{ borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >


                                    <RNPickerSelect
                                        value={this.state.area}
                                        onValueChange={
                                            (item) => {
                                                [
                                                    this.setState({
                                                        area: item,
                                                        areaValidate: item
                                                    }),
                                                    this.populateBranchesAreas(item, index)]
                                            }}
                                        items={areas}

                                        placeholder={{ label: Strings.area, value: '' }}
                                        style={{
                                            inputIOS: { textAlign: isRTL ? 'right' : 'left', fontSize: responsiveFontSize(6), height: responsiveHeight(5), width: responsiveWidth(80), fontFamily: isRTL ? arrabicFont : englishFont, paddingVertical: 9, paddingHorizontal: moderateScale(3), paddingRight: responsiveWidth(5), color: 'black', },
                                            inputAndroid: { textAlign: isRTL ? 'right' : 'left', fontSize: responsiveFontSize(6), height: responsiveHeight(5), width: responsiveWidth(80), fontFamily: isRTL ? arrabicFont : englishFont, paddingHorizontal: moderateScale(3), paddingRight: responsiveWidth(5), paddingVertical: 8, color: 'black', }
                                        }}
                                        placeholderTextColor={colors.placeholderGray}
                                        Icon={() => { return (<Icon name='down' type='AntDesign' style={{ color: 'black', fontSize: responsiveFontSize(6), top: responsiveHeight(1.5), right: responsiveWidth(1) }} />) }}
                                    />

                                    {/*<SectionedMultiSelect
                                        expandDropDowns
                                        //modalAnimationType='slide'
                                        //loading={loading}
                                        showDropDowns={false}
                                        modalWithTouchable
                                        hideConfirm
                                        searchPlaceholderText={Strings.search}
                                        styles={{
                                            selectToggle: { width: responsiveWidth(80), height: responsiveHeight(6), borderRadius: moderateScale(3), alignItems: 'center', justifyContent: 'center', paddingHorizontal: moderateScale(3) },
                                            selectToggleText: { textAlign: isRTL ? 'right' : 'left', fontSize: responsiveFontSize(6), color: 'gray', marginTop: moderateScale(8), fontFamily: isRTL ? arrabicFont : englishFont },
                                            subItemText: { textAlign: isRTL ? 'right' : 'left' },
                                            itemText: { fontSize: responsiveFontSize(10), textAlign: isRTL ? 'right' : 'left' },
                                            container: { height: responsiveHeight(60), position: 'absolute', width: responsiveWidth(80), top: responsiveHeight(18), alignSelf: 'center' },
                                            searchTextInput: { textAlign: isRTL ? 'right' : 'left', marginHorizontal: moderateScale(5) },
                                        }}
                                        items={areas}
                                        alwaysShowSelectText
                                        single
                                        searchPlaceholderText={Strings.search}
                                        uniqueKey="id"
                                        subKey="children"
                                        selectText={this.state.area ? this.state.area : Strings.area}

                                        readOnlyHeadings={true}
                                        onSelectedItemsChange={(selectedItems) => {
                                            // this.setState({ countries: selectedItems });
                                        }
                                        }
                                        onSelectedItemObjectsChange={(selectedItems) => {
                                            console.log("ITEM2   ", selectedItems[0].name)
                                            this.setState({ area: selectedItems[0].name, areaValidate: selectedItems[0].name });
                                            this.populateBranchesAreas(selectedItems[0].id, index)
                                        }
                                        }

                                    //onConfirm={() => this.setState({ selecTitle: '' })}
                                    />*/}



                                </View>
                            </View>

                            {branches.length > 0 && branches[index] != null ?
                                branches[index].area == '' ?
                                    <Text style={{ color: 'red', marginHorizontal: moderateScale(5), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                                    :
                                    null
                                :
                                branches[index] == null && addressValidate == '' &&
                                <Text style={{ color: 'red', marginHorizontal: moderateScale(5), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                            }

                            <View style={{ borderRadius: moderateScale(1), marginTop: moderateScale(5), backgroundColor: colors.lightGray }} >
                                <TextInput
                                    onChangeText={(val) => [this.setState({ address: val, addressValidate: val }), this.populateBranchesAddress(val, index)]}
                                    style={{ width: responsiveWidth(77), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(5), padding: 0, textAlign: isRTL ? 'right' : 'left', color:'black' }}
                                    placeholder={Strings.address}
                                    placeholderTextColor={colors.placeholderGray}
                                />
                            </View>


                            {branches.length > 0 && branches[index] != null ?
                                branches[index].address == '' ?
                                    <Text style={{ color: 'red', marginHorizontal: moderateScale(5), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                                    :
                                    null
                                :
                                branches[index] == null && addressValidate == '' &&
                                <Text style={{ color: 'red', marginHorizontal: moderateScale(5), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                            }
                        </View>



                        <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', marginTop: moderateScale(6), width: responsiveWidth(80), height: responsiveHeight(6), alignSelf: 'center', justifyContent: 'space-between', alignItems: 'center' }}>
                            {currentUser.user.type != 'ADMIN' && currentUser.user.type != 'OPERATION' &&

                                <>
                                    <Text style={{ color: 'black', fontSize: responsiveFontSize(7), fontFamily: isRTL ? arrabicFont : englishFont, textAlign: isRTL ? 'right' : 'left' }}>{Strings.chooseCurrentLocation}</Text>
                                    {/*<Switch
                                        onValueChange={(val) => [this.setState({ mapSwitchValue: !mapSwitchValue }),
                                        this.setState({ latitude: latitude, longitude: longitude, addressValidate: latitude }),
                                        val == true ? this.populateBranchesMap(latitude, longitude, index) : this.populateBranchesMap(0, 0, index)
                                        ]}
                                    value={mapSwitchValue} />*/}


                                    <TouchableOpacity onPress={() => {
                                        this.setState({ getLocationButton: true })
                                        this.getCurrentLocation()
                                        this.populateBranchesMap(latitude, longitude, index)


                                    }} disabled={this.state.enable} style={{ ...greenButton, width: responsiveWidth(25) }} >
                                        <Icon name='map-pin' type='Feather' style={{ fontSize: responsiveFontSize(7), color: 'white' }} />
                                    </TouchableOpacity>



                                </>


                                /*<MapView style={{ position: 'absolute', top: 0, left: 0, right: 0, bottom: 0 }}
                                    region={{
                                        latitude: parseFloat(this.state.Coordinate.length > index ? this.state.Coordinate[index].latitude : this.state.latitude),
                                        longitude: parseFloat(this.state.Coordinate.length > index ? this.state.Coordinate[index].longitude : this.state.longitude),
                                        latitudeDelta: 0.0922,
                                        longitudeDelta: 0.0421,
                                    }}
                                    onPress={(event) => {

                                        this.setState({ latitude: event.nativeEvent.coordinate.latitude, longitude: event.nativeEvent.coordinate.longitude, addressValidate: event.nativeEvent.coordinate.latitude })
                                        console.log('EVEVEVE: ', event.nativeEvent.coordinate)
                                        this.populateBranchesMap(event.nativeEvent.coordinate.latitude, event.nativeEvent.coordinate.longitude, index)

                                    }}
                                >
                                    <MapView.Marker
                                        coordinate={{
                                            // latitude: this.state.latitude,
                                            // longitude: this.state.longitude,
                                            latitude: parseFloat(this.state.Coordinate.length > index ? this.state.Coordinate[index].latitude : this.state.latitude),
                                            longitude: parseFloat(this.state.Coordinate.length > index ? this.state.Coordinate[index].longitude : this.state.longitude),
                                            latitudeDelta: 0.0922,
                                            longitudeDelta: 0.0421,
                                        }}
                                        style={{ width: 4, height: 4 }}
                                        height={5}
                                        width={5}

                                    />

                                    </MapView>
                                :
                                <Text style={{ textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.waitForGetLocation}</Text>*/
                            }

                        </View>
                    </View>
                ))
                }
            </>
        )
    }



    save = () => {
        const { selectClass, selectClassValidate, resturantId, restaurantName, restaurantNameAr, selectPayType, image, title, selectAvgKg, price, selectResturantSpace, oilType, selectBranchNo, branches, firstName, lastName, contactNo, email, userName, password, confirmPassword, addressValidate, outletCode, latitude, getLocationButton } = this.state
        const { currentUser } = this.props

        var status = ''
        var emailNotValid = false
        var locationAddress = addressValidate


        if (currentUser.user.type == 'SURVEY') {
            status = 'SURVEY-ACCOUNT'
        }
        else if (currentUser.user.type == 'PURCHASING') {
            status = 'PROCEED'
        }
        else if (currentUser.user.type == 'ADMIN' || currentUser.user.type == 'OPERATION') {
            status = 'APPROVED'
        }

        for (var i = 0; i < selectBranchNo; i++) {
            if (branches[i]) {
                if (branches[i].area == '') {
                    this.setState({ addressValidate: '' })
                    locationAddress = ''
                }
            }
            else {
                this.setState({ addressValidate: '' })
                locationAddress = ''
            }
        }
        if (!restaurantName.replace(/\s/g, '').length) { this.setState({ restaurantNameValidate: '' }); }
        if (!restaurantNameAr.replace(/\s/g, '').length) { this.setState({ restaurantNameArValidate: '' }); }
        if (!selectPayType.replace(/\s/g, '').length) { this.setState({ selectPayTypeValidate: '' }); }
        if (!oilType.replace(/\s/g, '').length) { this.setState({ oilTypeValidate: '' }); }
        if (!selectAvgKg.replace(/\s/g, '').length) { this.setState({ selectAvgKgValidate: '' }); }
        if (!selectResturantSpace.replace(/\s/g, '').length) { this.setState({ selectResturantSpaceValidate: '' }); }
        if (!(selectBranchNo.toString()).replace(/\s/g, '').length) { this.setState({ selectBranchNoValidate: '' }); }
        if (getLocationButton != true) { this.setState({ getLocationButton: false }); }

        if (this.props.currentUser.user.type == 'ADMIN') {
            if (!selectClass.replace(/\s/g, '').length) { this.setState({ selectClassValidate: '' }); }
        }
        //if (!(latitude.toString()).replace(/\s/g, '').length) { this.setState({ locationAvailable: false }); }
        //if (!addressByDetails.replace(/\s/g, '').length) { this.setState({ addressByDetailsValidate: '' }); }
        //if (!firstName.replace(/\s/g, '').length) { this.setState({ firstNameValidate: '' }); }
        //if (!lastName.replace(/\s/g, '').length) { this.setState({ lastNameValidate: '' }); }
        //if (!contactNo.replace(/\s/g, '').length) { this.setState({ contactNoValidate: '' }); }
        //if (!title.replace(/\s/g, '').length) { this.setState({ titleValidate: '' }); }
        //if (!(outletCode.toString()).replace(/\s/g, '').length) { this.setState({ outletCodeValidate: '' }); }
        //if (!userName.replace(/\s/g, '').length) { this.setState({ userNameValidate: '' }); }
        if (email.replace(/\s/g, '').length) {
            if (InputValidations.validateEmail(email) == false) {
                RNToasty.Error({ title: Strings.emailNotValid })
                emailNotValid = true
            }
        }


        if (currentUser.user.type == 'OPERATION' || currentUser.user.type == 'ADMIN') {
            if (!(price.toString()).replace(/\s/g, '').length) { this.setState({ price: '' }); }
            if (!userName.replace(/\s/g, '').length) { this.setState({ userNameValidate: '' }); }
            if (!password.replace(/\s/g, '').length) { this.setState({ passwordValidate: '' }); }
            if (!confirmPassword.replace(/\s/g, '').length) { this.setState({ confirmPasswordValidate: '' }); }
            if (confirmPassword != password) { this.setState({ confirmPasswordEqualPasswordValidate: '' }) }
            if (userName.replace(/\s/g, '').length && password.replace(/\s/g, '').length && confirmPassword.replace(/\s/g, '').length && (password == confirmPassword) && price.toString().replace(/\s/g, '').length) {
                if (restaurantName.replace(/\s/g, '').length && restaurantNameAr.replace(/\s/g, '').length && selectPayType.replace(/\s/g, '').length && oilType.replace(/\s/g, '').length && selectAvgKg.replace(/\s/g, '').length && selectResturantSpace.replace(/\s/g, '').length && `${selectBranchNo}`.replace(/\s/g, '').length && firstName.replace(/\s/g, '').length && lastName.replace(/\s/g, '').length && contactNo.replace(/\s/g, '').length && title.replace(/\s/g, '').length && locationAddress != '' && emailNotValid == false) {
                    this.addResturantAccount()
                } else {
                    RNToasty.Error({ title: Strings.insertTheRequiredData })
                    this.setState({ updateResturantLoading: false })
                }
            } else {
                RNToasty.Error({ title: Strings.insertTheRequiredData })
                this.setState({ updateResturantLoading: false })
            }
        }
        else if (currentUser.user.type == 'PURCHASING') {

           // if (!userName.replace(/\s/g, '').length) { this.setState({ userNameValidate: '' }); }
            //if (!password.replace(/\s/g, '').length) { this.setState({ passwordValidate: '' }); }
            //if (userName.replace(/\s/g, '').length) {
                if (getLocationButton == true && latitude > 0) {
                if (restaurantName.replace(/\s/g, '').length && restaurantNameAr.replace(/\s/g, '').length && selectPayType.replace(/\s/g, '').length && oilType.replace(/\s/g, '').length && selectAvgKg.replace(/\s/g, '').length && selectResturantSpace.replace(/\s/g, '').length && `${selectBranchNo}`.replace(/\s/g, '').length && locationAddress != '' && emailNotValid == false && latitude > 0 && getLocationButton == true) {
                    this.addResturantAccount()
                } else {
                    RNToasty.Error({ title: Strings.insertTheRequiredData })
                    this.setState({ updateResturantLoading: false })
                }
            }
            else {
                RNToasty.Error({ title: Strings.pleaseMarkLocation })
            }
            /*} else {  For UserName
                RNToasty.Error({ title: Strings.insertTheRequiredData })
                this.setState({ updateResturantLoading: false })
            }*/
        }
        else {

            if (getLocationButton == true && latitude > 0) {
                console.log('gggggg')
                if (restaurantName.replace(/\s/g, '').length && restaurantNameAr.replace(/\s/g, '').length && selectPayType.replace(/\s/g, '').length && oilType.replace(/\s/g, '').length && selectAvgKg.replace(/\s/g, '').length && selectResturantSpace.replace(/\s/g, '').length && `${selectBranchNo}`.replace(/\s/g, '').length && (outletCode.toString()).replace(/\s/g, '').length && locationAddress != '' && emailNotValid == false && latitude > 0 && getLocationButton == true) {
                    this.addResturantAccount()
                } else {
                    RNToasty.Error({ title: Strings.insertTheRequiredData })
                    this.setState({ updateResturantLoading: false })
                }
            }
            else {
                RNToasty.Error({ title: Strings.pleaseMarkLocation })
            }
        }
    }



    saveButton = () => {
        const { isRTL } = this.props

        return (
            <TouchableOpacity onPress={() => { this.save() }} style={[whiteButton, { alignSelf: 'center', marginVertical: moderateScale(17), marginBottom: moderateScale(35) }]} >
                <Text style={{ color: colors.green, fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(7) }}>{Strings.submit}</Text>
            </TouchableOpacity>
        )
    }

    pickImage = () => {
        ImagePicker.openPicker({
            width: 600,
            height: 600,
            cropping: true
        }).then(image => {
            this.setState({ image: image.path })
            console.log(image);
        });
    }

    profileImage = () => {
        const { isRTL } = this.props
        const { image } = this.state
        return (
            <View style={{ marginTop: moderateScale(8), alignSelf: 'center' }} >
                <FastImage
                    resizeMode='center'
                    source={image ? { uri: image } : require('../assets/imgs/profileicon.jpg')}
                    style={{ borderWidth: 2, borderColor: colors.lightGray, width: 80, height: 80, borderRadius: 40 }}
                />
                <TouchableOpacity onPress={this.pickImage} style={{ alignSelf: 'flex-end', marginTop: moderateScale(-11), justifyContent: 'center', alignItems: 'center', backgroundColor: colors.darkGreen, height: 30, width: 30, borderRadius: 15 }}>
                    <Icon name='photo' type='FontAwesome' style={{ fontSize: responsiveFontSize(7), color: colors.white }} />
                </TouchableOpacity>
            </View>
        )
    }

    noteInput = () => {
        const { isRTL } = this.props
        const { note } = this.state

        return (
            <View style={{ marginTop: moderateScale(5), width: responsiveWidth(80), alignSelf: 'center' }}>

                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.note}</Text>
                <View style={{ height: responsiveHeight(6), borderRadius: moderateScale(1), backgroundColor: colors.lightGray }} >
                    <TextInput
                        onChangeText={(val) => { this.setState({ note: val, }) }}
                        //onChangeText={(val)=> {this.setState({restaurantName:parseFloat(val)/100})}}
                        style={{ width: responsiveWidth(80), paddingHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(5), padding: 0, textAlign: isRTL ? 'right' : 'left' , color:'black'}}
                        placeholder={Strings.note}
                        placeholderTextColor={colors.placeholderGray}
                    />
                </View>
                {note.length == 0 &&
                    <Text style={{ color: 'red', marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }

    noOilSwitch = () => {
        const { isRTL } = this.props
        const { noOilSwitch } = this.state
        return (
            <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', width: responsiveWidth(80), justifyContent: 'space-between', alignItems: 'center', alignSelf: 'center', marginTop: moderateScale(7) }}>
                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(7), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.noOil}</Text>
                <Switch
                    onValueChange={(val) => [this.setState({ noOilSwitch: !noOilSwitch }),


                    ]}
                    value={noOilSwitch} />
            </View>
        )
    }



    render() {
        const { isRTL, userToken, currentUser } = this.props;
        const { phone, password, hidePassword, email } = this.state
        return (
            <LinearGradient

                colors={[colors.white, colors.white, colors.white]}
                style={{ flex: 1 }}
            >
                <Header title={Strings.addAccount} />
                <KeyboardAwareScrollView

                    showsVerticalScrollIndicator={false} style={{ backgroundColor: colors.white, borderTopRightRadius: moderateScale(20), borderTopLeftRadius: moderateScale(20), marginTop: moderateScale(0), width: responsiveWidth(100) }} >
                    {/*<View style={{ alignSelf: 'center', width: responsiveWidth(60), borderBottomColor: colors.darkGreen, borderBottomWidth: 2, justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ marginTop: moderateScale(5), marginBottom: moderateScale(3), color: colors.darkGreen, fontSize: responsiveFontSize(8), fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.restaurantDetails}</Text>
                    </View>*/}
                    {this.profileImage()}

                    {this.restaurantNameInput()}
                    {this.restaurantNameArInput()}
                    <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'space-between', width: responsiveWidth(80), alignSelf: 'center', marginTop: moderateScale(6),height:responsiveHeight(10) }} >
                        {this.outletPhoneNumberInput()}
                        {this.ResSpacebranchsNoPicker()}
                    </View>
                    <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'space-between', width: responsiveWidth(80), alignSelf: 'center', marginTop: moderateScale(6), }} >
                        {this.payTypePicker()}
                        {this.oilTypePicker()}
                    </View>

                    {this.PriceInputAvgKgPicker()}

                    {this.noteInput()}

                    {currentUser.user.type == 'ADMIN' &&
                        <View style={{ width: responsiveWidth(80), alignSelf: 'center', marginTop: moderateScale(6), }} >
                            {this.classPicker()}
                        </View>
                    }

                    {(this.props.currentUser.user.type == 'SURVEY' || this.props.currentUser.user.type == 'PURCHASING') && this.noOilSwitch()}
                    <View style={{ marginTop: moderateScale(15), alignSelf: 'center', width: responsiveWidth(60), borderBottomColor: colors.darkGreen, borderBottomWidth: 2, justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ marginBottom: moderateScale(3), color: colors.darkGreen, fontSize: responsiveFontSize(8), fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.contactDetails}</Text>
                    </View>

                    <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', width: responsiveWidth(80), justifyContent: 'space-between', alignSelf: 'center', }}>
                        {this.props.currentUser.user.type != 'SURVEY' && this.firstNameInput()}
                        {this.props.currentUser.user.type != 'SURVEY' && this.lastNameInput()}
                    </View>

                    {this.props.currentUser.user.type != 'SURVEY' && this.contactNoInput()}
                    {this.titlePicker()}
                    {this.props.currentUser.user.type != 'SURVEY' && this.emailInput()}
                    {(this.props.currentUser.user.type == 'OPERATION' || this.props.currentUser.user.type == 'ADMIN') && this.userNameInput()}
                    {(this.props.currentUser.user.type == 'OPERATION' || this.props.currentUser.user.type == 'ADMIN') && this.passwordInput()}

                    <View style={{ borderRadius: moderateScale(1), height: 0.1 }} >
                        <TextInput
                            style={{ height: 0.1 }}
                        />
                    </View>

                    {(this.props.currentUser.user.type == 'OPERATION' || this.props.currentUser.user.type == 'ADMIN') && this.confirmPasswordInput()}

                    {this.addressMapInput()}

                    {this.state.addResturantLoading == true ?
                        <LoadingDialogOverlay title={Strings.wait} />
                        :
                        null
                    }


                    {this.saveButton()}
                </KeyboardAwareScrollView>

                <AppFooter />

            </LinearGradient>
        );
    }
}


const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        fontSize: responsiveFontSize(6),
        //fontFamily: isRTL ? arrabicFont : englishFont,
        paddingVertical: 9,
        paddingHorizontal: 10,
        color: 'gray',
    },
    inputAndroid: {
        fontSize: responsiveFontSize(6),
        //fontFamily: isRTL ? arrabicFont : englishFont,
        paddingHorizontal: 10,
        paddingVertical: 8,
        color: 'gray',
    },
});


const mapDispatchToProps = {
    login,
    removeItem
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    loading: state.auth.loading,
    errorText: state.auth.errorText,
    currentUser: state.auth.currentUser,
    //userToken: state.auth.userToken,
})


export default connect(mapToStateProps, mapDispatchToProps)(AddResturantAccount);

