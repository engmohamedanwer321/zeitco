import React, { Component } from 'react';
import { View, RefreshControl, Alert, ScrollView, FlatList, Text, TouchableOpacity, Image } from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../../utils/responsiveDimensions';
import { connect } from "react-redux";
import Strings from '../../assets/strings';

import axios from 'axios';
import { BASE_END_POINT } from '../../AppConfig';
import ListFooter from '../../components/ListFooter';
import { Icon, Thumbnail, Button } from 'native-base'
import { selectMenu, removeItem } from '../../actions/MenuActions';
import { enableSideMenu, pop, push } from '../../controlls/NavigationControll'
import NetInfo from "@react-native-community/netinfo";
import Loading from "../../common/Loading"
import NetworError from '../../common/NetworError'
import NoData from '../../common/NoData'
import { OrdersCount } from '../../actions/OrdersActions';
import { AccountsCount } from '../../actions/AccountsAction';
import * as colors from '../../assets/colors'
import { arrabicFont, englishFont } from '../../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image'
import NotificationCard from '../../components/NotificationCard'
import PurchasingAccountsCard from '../../components/PurchasingAccountsCard';
import CollaspeAppHeader from '../../common/CollaspeAppHeader'
import LinearGradient from 'react-native-linear-gradient';
import MainHeader from '../../common/MainHeader'
import Carousel from 'react-native-snap-carousel';
import BrandCard from '../../components/BrandCard'
import AppFooter from '../../components/AppFooter'
import { getUnreadNotificationsCount } from '../../actions/NotificationAction'
import {
  checkFirbaseNotificationPermision,
  getFirebaseNotificationToken,
  showFirebaseNotifcation,
  clickOnFirebaseNotification

} from '../../controlls/FirebasePushNotificationControll'
import { logout } from '../../actions/AuthActions'
import { setFirebaseToken } from '../../actions/NotificationAction'
import Header from '../../common/Header'

class CallCenterHome extends Component {

  page = 1;
  state = {
    networkError: null,
    showAlert: false,
  }

  componentDidMount() {
    enableSideMenu(true, this.props.isRTL)
    checkFirbaseNotificationPermision()
    getFirebaseNotificationToken(this.props.currentUser.token, this.props.setFirebaseToken)
    //showFirebaseNotifcation()
    //clickOnFirebaseNotification()
    this.props.OrdersCount(null, 'PENDING')
    this.props.AccountsCount('SURVEY-ACCOUNT')
    this.props.getUnreadNotificationsCount(this.props.currentUser.token)
  }

  buttons = () => {
    const { isRTL, ordersCount, accountsCount } = this.props
    const data = {accountType : 'Purchasing'}
    return (
      <View style={{ marginVertical: moderateScale(10), width: responsiveWidth(100) }}>
          
       

          
          <View style={{marginTop:moderateScale(6), width:responsiveWidth(100),alignSelf:'center',flexDirection:isRTL?'row-reverse':'row',justifyContent:'space-between'}} >
           
              {/*<TouchableOpacity onPress={() => push('CallCenterMakeOrder')} style={{ flex:1, alignItems:'center' }}>
              <FastImage source={require('../../assets/imgs/new-order-icon.png')} style={{ width: 60, height: 60, borderRadius: 30 }} />
              <Text style={{ color: colors.black, marginTop: moderateScale(3), fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.manualOrder}</Text>
              </TouchableOpacity>*/}

            <TouchableOpacity onPress={() => push('CallCenterActiveAccounts')} style={{flex:1, alignItems:'center' }}>
              <FastImage source={require('../../assets/imgs/home-viewaccount-icon.png')} style={{ width: 60, height: 60, borderRadius: 30 }} />
              <Text style={{ color: colors.black, marginTop: moderateScale(3), fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.viewAccounts}</Text>
            </TouchableOpacity>


          </View>
     
         

      </View>
     
    )
  }


  render() {
    const { categoryName, isRTL } = this.props;
    return (
      <View style={{ flex: 1 }}>
        <Header menu title={Strings.home} />
        <ScrollView showsVerticalScrollIndicator={false} style={{ marginBottom: moderateScale(25) }}>
          {this.buttons()}
        </ScrollView>
        <AppFooter />
      </View>
    );
  }
}

const mapStateToProps = state => ({
  isRTL: state.lang.RTL,
  currentUser: state.auth.currentUser,
  ordersCount: state.orders.ordersCount,
  accountsCount: state.accounts.accountsCount,
})

const mapDispatchToProps = {
  removeItem,
  OrdersCount,
  getUnreadNotificationsCount,
  AccountsCount,
  logout,
  setFirebaseToken,
}

export default connect(mapStateToProps, mapDispatchToProps)(CallCenterHome);
