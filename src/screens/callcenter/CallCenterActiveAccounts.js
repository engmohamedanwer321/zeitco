import React, { Component } from 'react';
import { View, RefreshControl, ScrollView, FlatList, Text, TouchableOpacity } from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../../utils/responsiveDimensions';
import { connect } from "react-redux";
import Strings from '../../assets/strings';

import axios from 'axios';
import { BASE_END_POINT } from '../../AppConfig';
import ListFooter from '../../components/ListFooter';
import { Icon, Thumbnail, Button } from 'native-base'
import { selectMenu, removeItem } from '../../actions/MenuActions';
import { enableSideMenu, pop } from '../../controlls/NavigationControll'
import NetInfo from "@react-native-community/netinfo";
import Loading from "../../common/Loading"
import NetworError from '../../common/NetworError'
import NoData from '../../common/NoData'
import * as colors from '../../assets/colors'
import { arrabicFont, englishFont } from '../../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image'
import NotificationCard from '../../components/NotificationCard'
import CallCenterAccountCard from '../../components/CallCenterAccountCard';
import CollaspeAppHeader from '../../common/CollaspeAppHeader'
import LinearGradient from 'react-native-linear-gradient';
import CommanHeader from '../../common/CommanHeader'
import AppFooter from '../../components/AppFooter'
import Header from '../../common/Header'


class CallCenterActiveAccounts extends Component {

    page = 1;
   
    state = {
        tabNumber: 1,
        networkError: null,
        accountList: [],
        accountListLoading: true,
        accountListRefresh: false,
        accountList404: false,
        pages: null,

    }



    componentDidMount() {
        enableSideMenu(false, null)
        this.approvedAccountList(false, 1)
       
    }

    approvedAccountList(refresh, page) {
        if (refresh) {
            this.setState({ accountListRefresh: true })
        }
        axios.get(`${BASE_END_POINT}restaurant?status=APPROVED&page=${page}`)
            .then(response => {
                console.log('Done   ', response.data.data)
                this.setState({
                    accountList: refresh ? response.data.data : [...this.state.accountList, ...response.data.data],
                    accountListLoading: false,
                    accountListRefresh: false,
                    pages: response.data.pageCount,
                })
            })
            .catch(error => {
                console.log('Error   ', error)
                this.setState({ accountList404: true, accountListLoading: false, })
            })
    }

   

    renderFooter = () => {
        return (
            this.state.loading ?
                <View style={{ alignSelf: 'center', margin: moderateScale(5) }}>
                    <ListFooter />
                </View>
                : null
        )
    }

    

    activeOutletsPage = () => {
        const {isRTL} = this.props;
        const { pages, accountList, accountListLoading, accountList404, accountListRefresh } = this.state
        return (
            <View style={{ flex: 1, height: responsiveHeight(73), backgroundColor: colors.white, borderTopRightRadius: moderateScale(20), borderTopLeftRadius: moderateScale(20), marginTop: moderateScale(0), width: responsiveWidth(100), marginBottom: responsiveHeight(10) }} >
            {accountList404 ?
                <NetworError />
                :
                accountListLoading ?
                    <Loading />
                    :
                    accountList.length > 0 ?

                        <FlatList
                            showsVerticalScrollIndicator={false}
                            contentContainerStyle={{ paddingBottom: moderateScale(5) }}
                            data={accountList}
                            renderItem={({ item }) => <CallCenterAccountCard data={item} />}
                            onEndReachedThreshold={.5}
                            onEndReached={() => {
                                if (this.page <= pages) {
                                    this.page = this.page + 1;
                                    this.approvedAccountList(false, this.page)
                                    console.log('page  ', this.page)
                                }
                            }}
                            refreshControl={
                                <RefreshControl
                                    //colors={["#B7ED03",colors.darkBlue]} 
                                    refreshing={accountListRefresh}
                                    onRefresh={() => {
                                        this.page = 1
                                        this.approvedAccountList(true, 1)
                                    }}
                                />
                            }
                        />
                        :
                        <NoData />
            }
        </View>
        );
    }



    render() {
        const { categoryName, isRTL } = this.props;
        const { tabNumber } = this.state
        return (
            <LinearGradient
                colors={[colors.white, colors.white, colors.white]}
                style={{ flex: 1 }}
            >
                <Header title={Strings.viewAccounts} />
                {tabNumber==1&&this.activeOutletsPage()}
               
                <AppFooter />
            </LinearGradient>
        );
    }
}

const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    barColor: state.lang.color,
    currentUser: state.auth.currentUser,
})

const mapDispatchToProps = {
    removeItem,
}

export default connect(mapStateToProps, mapDispatchToProps)(CallCenterActiveAccounts);
