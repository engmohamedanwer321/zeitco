import React, { Component } from 'react';
import {
    View, TouchableOpacity, Linking, Text, ScrollView, TextInput, Alert, Platform
} from 'react-native';
import { connect } from 'react-redux';
import { Icon, Button } from 'native-base';
import { login } from '../../actions/AuthActions'
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../../utils/responsiveDimensions";
import Strings from '../../assets/strings';
import LoadingDialogOverlay from '../../components/LoadingDialogOverlay';
import { enableSideMenu, resetTo, push } from '../../controlls/NavigationControll'
import { arrabicFont, englishFont } from '../../common/AppFont'
import * as Animatable from 'react-native-animatable';
import * as colors from '../../assets/colors';
import { removeItem } from '../../actions/MenuActions';
import CollaspeAppHeader from '../../common/CollaspeAppHeader'
import LinearGradient from 'react-native-linear-gradient';
import FastImage from 'react-native-fast-image'
import CommanHeader from '../../common/CommanHeader'
import ImagePicker from 'react-native-image-crop-picker';
import AppFooter from '../../components/AppFooter'
import { BASE_END_POINT } from '../../AppConfig';
import axios from 'axios';
import { RNToasty } from 'react-native-toasty';
import Dialog, { DialogContent, DialogTitle } from 'react-native-popup-dialog';
import { whiteButton, greenButton } from '../../assets/styles'
import Header from '../../common/Header'
import RNPickerSelect from 'react-native-picker-select';

//here

class CallCenterAccountDetails extends Component {

    reasons = [
        { label: Strings.propblem1, value: Strings.propblem1 },
        { label: Strings.propblem2, value: Strings.propblem2 },
        { label: Strings.propblem3, value: Strings.propblem3 },
        { label: Strings.propblem4, value: Strings.propblem4 },
        { label: Strings.propblem5, value: Strings.propblem5 },
        { label: Strings.propblem6, value: Strings.propblem6 },
    ]

    state = {
        loading: false,
        showDeleteDialog: false,
        reason: '',
        showDialog: false,
        nonAgreementReason: ' '
    }

    componentDidMount() {
        enableSideMenu(false, null)
        console.log('FFFF : ', this.props.data)
    }

    confirmResturant = () => {
        const { currentUser, data } = this.props

        this.setState({ loading: true })
        axios.put(`${BASE_END_POINT}restaurant/${this.props.data.id}/approved`, null, {
            headers: {
                "Authorization": `Bearer ${this.props.currentUser.token}`,
            }
        })
            .then(response => {
                RNToasty.Success({ title: Strings.resturantApprovedSuccessfuly })
                this.setState({ loading: false })
                if (currentUser.user.type == 'ADMIN') { resetTo('AdminHome') }
                else if (currentUser.user.type == 'OPERATION') { resetTo('OperationHome') }
            })
            .catch(error => {
                this.setState({ loading: false })
                RNToasty.Error({ title: Strings.setUserNamepassword })
                push('UpdateResturantAccount', data)
            })
    }


    deleteResturant = () => {
        const { currentUser } = this.props
        this.setState({ showDeleteDialog: false })
        axios.delete(`${BASE_END_POINT}restaurant/${this.props.data.id}`, {
            headers: {
                "Authorization": `Bearer ${this.props.currentUser.token}`,
            }
        })
            .then(response => {
                RNToasty.Success({ title: Strings.resturantDeletedSuccessfuly })
                if (currentUser.user.type == 'ADMIN') { resetTo('AdminHome') }
                else if (currentUser.user.type == 'OPERATION') { resetTo('OperationHome') }
            })
            .catch(error => {

                RNToasty.Error({ title: Strings.errorInDelete })
            })
    }

    declineResturant() {
        const { currentUser } = this.props
        const { reason } = this.state
        if (!reason.replace(/\s/g, '').length) {
            RNToasty.Error({ title: Strings.pleaseEnterDeclineReason })
        } else {
            const data = {
                declineReason: reason,
            }
            axios.put(`${BASE_END_POINT}restaurant/${this.props.data.id}/ignore`, JSON.stringify({ data }), {
                headers: {
                    "Authorization": `Bearer ${this.props.currentUser.token}`,
                    "Content-Type": "application/json"
                }
            })
                .then(response => {
                    console.log('DONE')
                    this.setState({ loading: false })
                    if (currentUser.user.type == 'ADMIN') { resetTo('AdminHome') }
                    else if (currentUser.user.type == 'OPERATION') { resetTo('OperationHome') }
                    RNToasty.Success({ title: Strings.resturantDeclinedSuccessfuly })
                })
                .catch(error => {
                    this.setState({ loading: false })
                    console.log('ERROR   ', error)
                    RNToasty.Error({ title: Strings.errorInDelete })
                })
        }
    }



    deleteDialog = () => {
        const { isRTL, data } = this.props;
        return (
            <Dialog
                containerStyle={{ backgroundColor: 'rgba(1,1,1,0.1)', }}
                visible={this.state.showDeleteDialog}
                onTouchOutside={() => {
                    this.setState({ showDeleteDialog: false });
                }}
                onHardwareBackPress={() => {
                    this.setState({ showDeleteDialog: false });
                }}
            >
                <View style={{ backgroundColor: 'white', justifyContent: 'center', alignItems: 'center', width: responsiveWidth(90), height: responsiveHeight(30) }}>
                    <Text style={{ color: colors.darkGreen, fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6) }}>{Strings.areYouSureToDeclineAccount}</Text>

                    <TextInput
                        placeholder={Strings.declineReason}
                        style={{ color: 'black', marginVertical: moderateScale(10), width: responsiveWidth(80), backgroundColor: colors.lightGray, height: responsiveHeight(7), paddingVertical: 0, borderRadius: moderateScale(3) }}
                        onChangeText={(val) => this.setState({ reason: val })}
                    />

                    <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', width: responsiveWidth(60), justifyContent: 'space-between', alignItems: 'center' }}>
                        <Button onPress={() => {
                            this.setState({ showDeleteDialog: false })
                        }} style={{ width: responsiveWidth(28), height: responsiveHeight(6), justifyContent: 'center', alignItems: 'center', alignSelf: isRTL ? 'flex-start' : 'flex-end', borderRadius: moderateScale(0), backgroundColor: 'white' }}>
                            <Text style={{ color: colors.black, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.no}</Text>
                        </Button>
                        <Button onPress={() => {
                            this.declineResturant()
                        }} style={{ width: responsiveWidth(28), height: responsiveHeight(6), justifyContent: 'center', alignItems: 'center', alignSelf: isRTL ? 'flex-start' : 'flex-end', borderRadius: moderateScale(0), backgroundColor: colors.lightGreenButton }}>
                            <Text style={{ color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.yes}</Text>
                        </Button>

                    </View>
                </View>
            </Dialog>
        )
    }

    basicsData = () => {
        const { isRTL, data } = this.props
        return (
            <View style={{ width: responsiveWidth(90), alignItems: isRTL ? 'flex-end' : 'flex-start', alignSelf: 'center', marginTop: moderateScale(5) }}>


                {/*<Text style={{ color: 'black', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(8), borderBottomWidth: 1, borderBottomColor: 'black', paddingBottom: moderateScale(2) }} >{Strings.restaurantDetails}</Text>*/}
                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row' }}>
                    <View style={{ width: responsiveWidth(80) }}>
                        <Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), marginTop: moderateScale(5), textAlign: isRTL ? 'right' : 'left' }} >{Strings.restaurantName} : {isRTL ? data.restaurantName_ar : data.restaurantName}</Text>
                        <Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), marginTop: moderateScale(5), textAlign: isRTL ? 'right' : 'left' }} >{Strings.averageKg} : {data.average} {Strings.Kg}</Text>
                        <Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), marginTop: moderateScale(5), textAlign: isRTL ? 'right' : 'left' }} >{Strings.price} : {data.price} {Strings.egpKg}</Text>
                        {/*<Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), marginTop: moderateScale(5), textAlign: isRTL ? 'right' : 'left' }} >{Strings.resSpace} : {data.space}</Text>*/}
                        {/*<Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), marginTop: moderateScale(5), textAlign: isRTL ? 'right' : 'left' }} >{Strings.branchesNo} : {data.branchesNumber}</Text>*/}
                        {data.branches.map((item) => (
                            <Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), marginTop: moderateScale(5), textAlign: isRTL ? 'right' : 'left' }} >{Strings.address} : {isRTL ? item.city['arabicCityName'] : item.city['cityName']} - {isRTL ? item.area['arabicAreaName'] : item.area['areaName']} - {item.address} </Text>
                        ))}
                        <Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), marginTop: moderateScale(5), textAlign: isRTL ? 'right' : 'left' }} >{Strings.outletCode} : {data.outletCode}</Text>
                        <Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), marginTop: moderateScale(5), textAlign: isRTL ? 'right' : 'left' }} >{Strings.oilType} : {data.oilType}</Text>
                    </View>
                </View>

                {/*  */}

                {/*<Text style={{ color: 'black', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(8), borderBottomWidth: 1, borderBottomColor: 'black', paddingBottom: moderateScale(2), marginTop: moderateScale(12) }} >{Strings.contactDetails}</Text>*/}
                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row' }}>
                    <View style={{ width: responsiveWidth(80) }}>
                        <Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), marginTop: moderateScale(5), textAlign: isRTL ? 'right' : 'left' }} >{Strings.contactPerson} : {data.firstname} {data.lastname}</Text>
                        <TouchableOpacity
                            onPress={() => Linking.openURL('tel:' + data.phone)}>
                            <Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), marginTop: moderateScale(5), textAlign: isRTL ? 'right' : 'left' }} >{Strings.phone} : {data.phone}</Text>
                        </TouchableOpacity>

                        {/*<Text style={{ color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), marginTop: moderateScale(5), textAlign: isRTL ? 'right' : 'left' }} >{Strings.email} : {data.email}</Text>*/}



                    </View>
                </View>



            </View>
        )
    }




    order_noOrder_buttons = () => {
        const { isRTL, data } = this.props
        return (
            <View style={{ alignSelf: 'center', marginVertical: moderateScale(20), }} >


                <View style={{ width: responsiveWidth(80), flexDirection: isRTL ? 'row-reverse' : 'row', marginVertical: moderateScale(10), justifyContent: 'space-between' }}>
                    <TouchableOpacity onPress={() => {
                        push('CallCenterMakeOrder', data)
                    }} style={whiteButton} >
                        <Text style={{ paddingHorizontal: moderateScale(5), color: colors.green, fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(7) }}>{Strings.order}</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => {
                        this.setState({ showDialog: true })
                    }} style={greenButton} >
                        <Text style={{ paddingHorizontal: moderateScale(5), color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(7) }}>{Strings.noOrder}</Text>
                    </TouchableOpacity>
                </View>


            </View>
        )
    }


    nonAgreementResturant = () => {
        const { nonAgreementReason } = this.state
        const { data } = this.props

        console.log(data.id)
        if (!nonAgreementReason.replace(/\s/g, '').length) {
            this.setState({ nonAgreementReason: '' })
        }

        if (nonAgreementReason.replace(/\s/g, '').length) {
            this.setState({ loading: true })
            const d = {
                reason: nonAgreementReason,
            }
            console.log(`${BASE_END_POINT}restaurant/${data.id}/noOrder`)

            axios.put(`${BASE_END_POINT}restaurant/${data.id}/noOrder`, JSON.stringify(d), {
                headers: {
                    "Content-Type": 'application/json',
                    Authorization: `Bearer ${this.props.currentUser.token}`,
                }
            })
                .then(response => {
                    this.setState({ loading: false, showDialog: false })
                    console.log("DONE   ", response)
                    resetTo('CallCenterHome')

                })
                .catch(error => {
                    this.setState({ loading: false })
                    console.log(error)
                    RNToasty.Error({ title: Strings.errorInDataReteivel })

                })
        }

    }


    dialog = () => {
        const { isRTL, data } = this.props;
        const { nonAgreementReason } = this.state
        return (
            <Dialog
                containerStyle={{ backgroundColor: 'rgba(1,1,1,0.1)', }}
                visible={this.state.showDialog}
                onTouchOutside={() => {
                    this.setState({ showDialog: false });
                }}
                onHardwareBackPress={() => {
                    this.setState({ showDialog: false });
                }}
            >

                <View style={{ justifyContent: 'center', alignItems: 'center', width: responsiveWidth(90), height: responsiveHeight(25) }}>
                    <View style={{ width: responsiveWidth(70) }}>
                        <RNPickerSelect
                            onValueChange={
                                (item) => {
                                    this.setState({
                                        nonAgreementReason: item

                                    });
                                }}
                            items={this.reasons}
                            placeholder={{ label: 'Reason', value: '' }}
                            style={{
                                inputIOS: { textAlign: isRTL ? 'right' : 'left', fontSize: responsiveFontSize(6), height: responsiveHeight(5), fontFamily: isRTL ? arrabicFont : englishFont, paddingVertical: 9, paddingHorizontal: moderateScale(3), paddingRight: responsiveWidth(5), color: 'gray', width: responsiveWidth(70) },
                                inputAndroid: { textAlign: isRTL ? 'right' : 'left', fontSize: responsiveFontSize(6), height: responsiveHeight(5), fontFamily: isRTL ? arrabicFont : englishFont, paddingHorizontal: moderateScale(3), paddingRight: responsiveWidth(5), paddingVertical: 8, color: 'gray', width: responsiveWidth(70) }
                            }}
                            placeholderTextColor={'gray'}
                            Icon={() => { return (<Icon name='down' type='AntDesign' style={{ color: 'black', fontSize: responsiveFontSize(6), top: responsiveHeight(1.5), right: responsiveWidth(1) }} />) }}
                        />
                        {nonAgreementReason == '' &&
                            <Text style={{ color: 'red', marginTop: moderateScale(1), marginHorizontal: moderateScale(2), fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                        }
                    </View>
                    <View style={{ marginTop: moderateScale(5), flexDirection: isRTL ? 'row-reverse' : 'row', width: responsiveWidth(70), justifyContent: 'space-between', alignItems: 'center' }}>
                        <TouchableOpacity onPress={() => {
                            this.nonAgreementResturant()


                        }} disabled={this.state.enable} style={{ ...greenButton, width: responsiveWidth(25) }} >
                            <Text style={{ color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.confirm}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => {
                            this.setState({ showDialog: false })


                        }} disabled={this.state.enable} style={{ ...whiteButton, width: responsiveWidth(25) }} >
                            <Text style={{ color: colors.green, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.cancel}</Text>
                        </TouchableOpacity>

                    </View>
                </View>

            </Dialog>
        )
    }




    render() {
        const { isRTL, userToken } = this.props;
        return (
            <LinearGradient
                colors={[colors.white, colors.white, colors.white]}
                style={{ flex: 1 }}
            >
                <Header title={Strings.restaurantDetails} />

                <ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: colors.white, borderTopRightRadius: moderateScale(20), borderTopLeftRadius: moderateScale(20), marginTop: moderateScale(5), width: responsiveWidth(100), marginBottom: moderateScale(20) }} >
                    {this.basicsData()}
                    {this.order_noOrder_buttons()}
                </ScrollView>
                {this.state.loading == true ?
                    <LoadingDialogOverlay title={Strings.wait} />
                    :
                    null
                }
                {this.dialog()}
                <AppFooter />

            </LinearGradient>
        );
    }
}
const mapDispatchToProps = {
    login,
    removeItem
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    loading: state.auth.loading,
    errorText: state.auth.errorText,
    currentUser: state.auth.currentUser,
    //userToken: state.auth.userToken,
})


export default connect(mapToStateProps, mapDispatchToProps)(CallCenterAccountDetails);

