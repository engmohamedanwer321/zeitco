import React, { Component } from 'react';
import { View, RefreshControl, Alert, ScrollView, FlatList, Text, TouchableOpacity, Image } from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../../utils/responsiveDimensions';
import { connect } from "react-redux";
import Strings from '../../assets/strings';

import axios from 'axios';
import { BASE_END_POINT } from '../../AppConfig';
import ListFooter from '../../components/ListFooter';
import { Icon, Thumbnail, Button } from 'native-base'
import { selectMenu, removeItem } from '../../actions/MenuActions';
import { enableSideMenu, pop, push } from '../../controlls/NavigationControll'
import NetInfo from "@react-native-community/netinfo";
import Loading from "../../common/Loading"
import NetworError from '../../common/NetworError'
import NoData from '../../common/NoData'
import * as colors from '../../assets/colors'
import { arrabicFont, englishFont } from '../../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image'
import NotificationCard from '../../components/NotificationCard'
import PurchasingAccountsCard from '../../components/PurchasingAccountsCard';
import CollaspeAppHeader from '../../common/CollaspeAppHeader'
import LinearGradient from 'react-native-linear-gradient';
import MainHeader from '../../common/MainHeader'
import Carousel from 'react-native-snap-carousel';
import BrandCard from '../../components/BrandCard'
import AppFooter from '../../components/AppFooter'
import {logout} from '../../actions/AuthActions'
import {getUnreadNotificationsCount} from '../../actions/NotificationAction'
import {
    checkFirbaseNotificationPermision,
    getFirebaseNotificationToken,
    showFirebaseNotifcation,
    clickOnFirebaseNotification
  } from '../../controlls/FirebasePushNotificationControll'
import {setFirebaseToken} from '../../actions/NotificationAction'
import Header from '../../common/Header'


class SurveyHome extends Component {

    page = 1;
    state = {
        networkError: null,
        showAlert: false,
    }


    componentDidMount() {
        enableSideMenu(true, this.props.isRTL)
        checkFirbaseNotificationPermision()
        getFirebaseNotificationToken(this.props.currentUser.token,this.props.setFirebaseToken)
        //showFirebaseNotifcation()
        //clickOnFirebaseNotification()
        this.props.getUnreadNotificationsCount(this.props.currentUser.token)
    }

    buttons = () => {
        const { isRTL,currentUser } = this.props
        return (
            <View style={{ marginTop: moderateScale(5), alignSelf: 'center', flexDirection: isRTL ? 'row-reverse' : 'row', flexWrap: 'wrap', justifyContent: 'space-between', alignItems: 'center', width: responsiveWidth(100) }}>
               
                <TouchableOpacity onPress={() => push('AddResturantAccount')} style={{ width: responsiveWidth(30), justifyContent: 'center', alignItems: 'center', marginTop: moderateScale(15) }}>
                    <FastImage source={require('../../assets/imgs/home-addaccount-icon.png')} style={{ width: 60, height: 60,borderRadius:30  }} />
                    <Text style={{ color: colors.black, marginTop: moderateScale(4), fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.addAccount}</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => push('UpdateProfile')} style={{ width: responsiveWidth(30), justifyContent: 'center', alignItems: 'center', marginTop: moderateScale(15) }}>
                    <FastImage source={require('../../assets/imgs/home-profileedit-icon.png')} style={{ width: 60, height: 60,borderRadius:30  }} />
                    <Text style={{ color: colors.black, marginTop: moderateScale(4), fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.editProfile}</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => {
                    this.props.logout(currentUser.token,this.props.firbaseToken)
                }} style={{ width: responsiveWidth(30), justifyContent: 'center', alignItems: 'center', marginTop: moderateScale(15) }}>
                    <FastImage source={require('../../assets/imgs/home-logout-icon.png')} style={{ width: 60, height: 60,borderRadius:30  }} />
                    <Text style={{ color: colors.black, marginTop: moderateScale(4), fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.logout}</Text>
                </TouchableOpacity>

            </View>
        )
    }


    render() {
        const { categoryName, isRTL } = this.props;
        return (
            <View style={{ flex: 1 }}>
                 <Header menu  title={Strings.home} />
                <ScrollView showsVerticalScrollIndicator={false}>
                    {this.buttons()}
                </ScrollView>
                <AppFooter />
            </View>
        );
    }
}

const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    barColor: state.lang.color,
    currentUser: state.auth.currentUser,
    firbaseToken:state.noti.firbaseToken
})

const mapDispatchToProps = {
    removeItem,
    logout,
    getUnreadNotificationsCount,
    setFirebaseToken
}

export default connect(mapStateToProps, mapDispatchToProps)(SurveyHome);
