import { combineReducers } from "redux";
import MenuReducer from "./MenuReducer";
import AuthReducer from "./AuthReducer";
import LanguageReducer from './LanguageReducer';
import NotificationsReducer from './NotificationsReducer';
import OrdersReducer from './OrdersReducer'
import ChatReducer from './ChatReducer'
import AccountsReducer from "./AccountsReducer";
import CheckingReducer from './CheckingReducer'


export default combineReducers({
    menu: MenuReducer,
    auth: AuthReducer,
    lang: LanguageReducer,
    noti: NotificationsReducer,
    chat: ChatReducer,
    orders: OrdersReducer,
    accounts: AccountsReducer,
    checking: CheckingReducer
});