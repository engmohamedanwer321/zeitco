import * as types from "../actions/types"

const initialState = {
    isChecking: false,
}

const CheckingReducer = (state = initialState, action) => {

    switch (action.type) {
        case types.CHECKING:
            return { ...state, isChecking: action.payload };       
        default:
            return state;
    }

}

export default CheckingReducer;