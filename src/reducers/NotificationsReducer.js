import {
    UNREAD_NOTIFICATIONS_COUNT,FIREBASE_TOKEN
} from '../actions/types';

const initState = {
    unreadNotificationsCount:0,
    firbaseToken:''
}

const NotificationsReducer = (state=initState, action) => {
    switch(action.type){
        case UNREAD_NOTIFICATIONS_COUNT:
            console.log("nossss "+ action.payload)
            return { ...state,unreadNotificationsCount:action.payload };
        case FIREBASE_TOKEN:
            console.log("FBT REDUCER "+ action.payload)
            return { ...state,firbaseToken:action.payload };        
        default: 
            return state; 
    }
}

export default NotificationsReducer;