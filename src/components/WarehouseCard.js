import React,{Component} from 'react';
import {View,Animated,TouchableOpacity,Text} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import * as colors from '../assets/colors'
import Strings from '../assets/strings';
import { Thumbnail,Icon } from 'native-base';
import moment from 'moment'
import "moment/locale/ar"
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import withPreventDoubleClick from './withPreventDoubleClick';
import {getUnreadNotificationsNumers} from '../actions/NotificationAction'
import {arrabicFont,englishFont} from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image';
import {push,resetTo} from '../controlls/NavigationControll'
import { OrdersCount } from '../actions/OrdersActions';
import { whiteButton, greenButton } from '../assets/styles';
import { RNToasty } from 'react-native-toasty';

class WarehouseCard extends Component {

    componentDidMount(){
        console.log('ORDER  ',this.props.order)
    }
    
    assignButton = () => {
        const { isRTL,data,order,socket,currentUser } = this.props;
        return (
            <View style={{ alignSelf: 'center', justifyContent: 'center', alignItems: 'center', }}>
                <TouchableOpacity
                    onPress={() => {
                    if(data.freeCapacity>order.average){
                       // console.log("order   ",order,"   ID   ",order.warehouse.id)
                        socket.emit("assignOrderToWarehouse",{
                            userId:currentUser.user.id,
                            orderId:order.id,
                            warehouseId:data.id
                        });
                        RNToasty.Success({title:Strings.done})
                        this.props.OrdersCount(null, 'PENDING')
                        if(currentUser.user.type=='ADMIN'){
                            resetTo('AdminHome')
                        }else{
                            resetTo('OperationHome')
                        }
                    }else{
                        RNToasty.Info({title:Strings.warehouseNotHaveSuffcientCapacity})
                    }
                    }}
                    style={[whiteButton,{ marginHorizontal: moderateScale(2), width: responsiveWidth(28), height: responsiveHeight(5), justifyContent: 'center', alignItems: 'center', alignSelf: 'center',}]}>
                    <Text style={{ color: colors.green, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.assign}</Text>
                </TouchableOpacity>


            </View>
        )
    }

    render(){
        const {isRTL,data,order,socket} = this.props;
        return(
            <TouchableOpacity
            onPress={()=>{
                const d = {
                    order:order,
                    warehouse:data
                }
               // push('OperationWarehouseMember',d)
            }}
            >
            <Animatable.View
            style={{borderBottomColor:colors.lightGray,borderBottomWidth:1, height:responsiveHeight(16),flexDirection:isRTL?'row-reverse':'row',alignItems:'center', marginTop: moderateScale(3), alignSelf: 'center', width: responsiveWidth(96) }}
            animation={"slideInLeft"}
            duration={1000}>
                <Thumbnail 
                
                source={require('../assets/imgs/profileicon.jpg')}
                />
                <View style={{width: responsiveWidth(40), marginHorizontal: moderateScale(3)}} >
                    <Text style={{alignSelf:isRTL?'flex-end':'flex-start',color:'gray',marginHorizontal:moderateScale(3), fontFamily:isRTL?arrabicFont:englishFont,fontSize:responsiveFontSize(6),fontWeight:'bold'}}>{data.name} {data.lastname}</Text>
                    <Text style={{alignSelf:isRTL?'flex-end':'flex-start', color:'gray', marginHorizontal:moderateScale(3), fontFamily:isRTL?arrabicFont:englishFont,fontSize:responsiveFontSize(5)}}>{Strings.capacity} : {data.capacity} {Strings.Kg}</Text>
                    <Text style={{alignSelf:isRTL?'flex-end':'flex-start', color:'gray', marginHorizontal:moderateScale(3), fontFamily:isRTL?arrabicFont:englishFont,fontSize:responsiveFontSize(5)}}>{Strings.freeCapacity} : {data.freeCapacity} {Strings.Kg}</Text>
                </View>
                <View style={{justifyContent:'center',alignItems:'center'}}>
                    {this.assignButton()}
                </View>
            </Animatable.View>
            </TouchableOpacity>
           
        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
});

const mapDispatchToProps = {
    getUnreadNotificationsNumers,
    OrdersCount,
}


export default connect(mapStateToProps,mapDispatchToProps)(WarehouseCard);
