import React, { Component } from 'react';
import { View, Animated, TouchableOpacity, Text, Image } from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import * as colors from '../assets/colors'
import Strings from '../assets/strings';
import { Thumbnail, Icon } from 'native-base';
import moment from 'moment'
import "moment/locale/ar"
import axios from 'axios';
import { BASE_END_POINT } from '../AppConfig';
import withPreventDoubleClick from './withPreventDoubleClick';
import { getUnreadNotificationsNumers } from '../actions/NotificationAction'
import { arrabicFont, englishFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image';
import Dialog, { DialogContent, DialogTitle } from 'react-native-popup-dialog';


const MyTouchableOpacity = withPreventDoubleClick(TouchableOpacity);

//moment(birth_date).format("YYYY-MM-DD")


class BrandCard extends Component {


    state = {
        showDialog: false
    }

    dialog = () => {
        const { isRTL, data } = this.props;
        return (
            <Dialog
                containerStyle={{ backgroundColor: 'rgba(1,1,1,0.1)', }}
                visible={this.state.showDialog}
                onTouchOutside={() => {
                    this.setState({ showDialog: false });
                }}
                onHardwareBackPress={() => {
                    this.setState({ showDialog: false });
                }}
            >
                <View style={{ justifyContent: 'center', alignItems: 'center', width: responsiveWidth(90), height: responsiveHeight(60) }}>
                    <FastImage source={{ uri: data.img[0] }} style={{width: responsiveWidth(85), height: responsiveHeight(55), resizeMode: 'contain' }} />
                </View>
            </Dialog>
        )
    }


    render() {
        const { isRTL, data } = this.props;
        return (
            <View style={{ margin: moderateScale(3), justifyContent: 'center', alignItems: 'center', backgroundColor: colors.white, borderRadius: moderateScale(3), zIndex: 10000, elevation: 8, shadowOffset: { width: 0, height: 2 }, shadowOpacity: 0.2, shadowColor: 'black', alignSelf: 'center', width: responsiveWidth(23), height: responsiveHeight(15) }}>
                <TouchableOpacity onPress={() =>
                    this.setState({ showDialog: true })

                }>
                    <FastImage
                        source={{ uri: data.img[0] }}
                        style={{ width: responsiveWidth(20), height: responsiveHeight(10) }}
                    />
                </TouchableOpacity>
                {this.dialog()}
            </View>

        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
});

const mapDispatchToProps = {
    getUnreadNotificationsNumers
}


export default connect(mapStateToProps, mapDispatchToProps)(BrandCard);
