import React, { Component } from 'react';
import { View, Alert, TouchableOpacity, Text, Image } from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import * as colors from '../assets/colors'
import Strings from '../assets/strings';
import { Thumbnail, Icon, Button } from 'native-base';
import moment from 'moment'
import "moment/locale/ar"
import axios from 'axios';
import { BASE_END_POINT } from '../AppConfig';
import withPreventDoubleClick from './withPreventDoubleClick';
import { getUnreadNotificationsNumers } from '../actions/NotificationAction'
import { arrabicFont, englishFont, boldFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image'
import { push } from '../controlls/NavigationControll'



class AssignedOrderCard extends Component {

    render() {
        const { isRTL, data, collected, onProgress } = this.props;
        console.log('DDAATTAA:', data)
        moment.locale(this.props.isRTL ? 'ar' : 'en');
        return (
            <TouchableOpacity onPress={() => {
                if (collected || onProgress) {
                    push('OperationCollectedOrderDeails', data)
                } else {
                    push('OperationEdit_Reassign_DeleteOrder', data)
                }
            }}
            >
                <Animatable.View
                    style={{ marginTop: moderateScale(3), alignSelf: 'center', width: responsiveWidth(96) }}
                    animation={"flipInX"}
                    duration={1500}>
                    <View style={{ width: responsiveWidth(96) }}>
                        <View style={{ marginTop: moderateScale(5), flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', width: responsiveWidth(94) }}>
                            <FastImage source={data.restaurant.img != '' ? { uri: data.restaurant.img } : require('../assets/imgs/profileicon.jpg')} style={{ width: responsiveWidth(17), height: responsiveWidth(17), borderRadius: responsiveWidth(8.5) }} />
                            <View style={{ width: responsiveWidth(43), marginHorizontal: moderateScale(3) }}>
                                <Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', color: colors.grayColor1, fontSize: responsiveFontSize(5), fontFamily: isRTL ? arrabicFont : englishFont }} numberOfLines={1}><Text style={{ fontFamily: boldFont }}>{Strings.outletName}:</Text> {isRTL ? data.restaurant.restaurantName_ar : data.restaurant.restaurantName}</Text>
                                <Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', color: colors.grayColor1, fontSize: responsiveFontSize(5), fontFamily: isRTL ? arrabicFont : englishFont, }}><Text style={{ fontFamily: boldFont }}>{data.status == 'COLLECTED' ? Strings.quantityWhichDriverRecivied : Strings.orderQt}:</Text>{data.status == 'COLLECTED' ? data.kg : data.average} {Strings.Kg}</Text>

                                <Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', color: colors.grayColor1, fontSize: responsiveFontSize(5), fontFamily: isRTL ? arrabicFont : englishFont }}><Text style={{ fontFamily: boldFont }}>{Strings.price}:</Text>{data.status == 'COLLECTED' ? data.totalPrice : data.restaurant.price} {Strings.EGP}</Text>
                                <Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', color: colors.grayColor1, fontSize: responsiveFontSize(5), fontFamily: isRTL ? arrabicFont : englishFont, }}><Text style={{ fontFamily: boldFont }}>{Strings.area}:</Text>{isRTL ? data.restaurant.branches[0].area.arabicAreaName : data.restaurant.branches[0].area.areaName}</Text>
                            </View>

                            <View style={{ width: responsiveWidth(30), marginHorizontal: moderateScale(3) }}>
                                <Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', color: colors.grayColor1, fontSize: responsiveFontSize(5), fontFamily: isRTL ? arrabicFont : englishFont, }}><Text style={{ fontFamily: boldFont }}>{Strings.date}:</Text>{data.date.replace('-', '.').replace('-', '.')}</Text>

                                <Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', color: colors.grayColor1, fontSize: responsiveFontSize(5), fontFamily: isRTL ? arrabicFont : englishFont, }}><Text style={{ fontFamily: boldFont }}>{Strings.driverName}:</Text>{data.driver.firstname} {data.driver.lastname}</Text>

                            </View>
                        </View>

                    </View>
                </Animatable.View>
            </TouchableOpacity>

        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
});

const mapDispatchToProps = {
    getUnreadNotificationsNumers
}


export default connect(mapStateToProps, mapDispatchToProps)(AssignedOrderCard);
