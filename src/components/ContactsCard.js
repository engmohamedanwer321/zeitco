import React, { Component } from 'react';
import { View, Alert, TouchableOpacity, Text } from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import * as colors from '../assets/colors'
import Strings from '../assets/strings';
import { Thumbnail, Icon, Button } from 'native-base';
import moment from 'moment'
import "moment/locale/ar"
import axios from 'axios';
import { BASE_END_POINT } from '../AppConfig';
import withPreventDoubleClick from './withPreventDoubleClick';
import { getUnreadNotificationsNumers } from '../actions/NotificationAction'
import { arrabicFont, englishFont, boldFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image'



const MyTouchableOpacity = withPreventDoubleClick(TouchableOpacity);

//moment(birth_date).format("YYYY-MM-DD")


class ContactsCard extends Component {


    render() {
        const { isRTL, data } = this.props;
        moment.locale(this.props.isRTL ? 'ar' : 'en');
        return (
            <View>
                <Animatable.View
                    style={{ borderBottomColor: '#cccbcb', borderBottomWidth: 1, paddingVertical: moderateScale(8), alignSelf: 'center', width: responsiveWidth(80) }}
                    animation={"slideInLeft"}
                    duration={1000}>
                     <Text style={{ textAlign:isRTL?'right':'left', color: colors.grayColor1, fontSize: responsiveFontSize(8), fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.name}: {data.name}</Text>
                     <Text style={{marginTop:moderateScale(3), textAlign:isRTL?'right':'left', color: colors.grayColor1, fontSize: responsiveFontSize(8), fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.email}: {data.email}</Text>
                     <Text style={{marginTop:moderateScale(3), textAlign:isRTL?'right':'left', color: colors.grayColor1, fontSize: responsiveFontSize(8), fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.phone}: {data.number}</Text>
                     <Text style={{marginTop:moderateScale(3), textAlign:isRTL?'right':'left', color: colors.grayColor1, fontSize: responsiveFontSize(8), fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.message}: {data.message}</Text>
                </Animatable.View>
            </View>

        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
});

const mapDispatchToProps = {
    getUnreadNotificationsNumers
}


export default connect(mapStateToProps, mapDispatchToProps)(ContactsCard);
