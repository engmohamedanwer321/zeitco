import React, { Component } from 'react';
import { View, Alert, TouchableOpacity, Text } from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import * as colors from '../assets/colors'
import Strings from '../assets/strings';
import { Thumbnail, Icon, Button } from 'native-base';
import moment from 'moment'
import "moment/locale/ar"
import axios from 'axios';
import { BASE_END_POINT } from '../AppConfig';
import withPreventDoubleClick from './withPreventDoubleClick';
import { getUnreadNotificationsNumers } from '../actions/NotificationAction'
import { arrabicFont, englishFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image'
import ImagePicker from 'react-native-image-crop-picker';
import { push } from '../controlls/NavigationControll'
import { RNToasty } from 'react-native-toasty';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';



const MyTouchableOpacity = withPreventDoubleClick(TouchableOpacity);

//moment(birth_date).format("YYYY-MM-DD")
const News = [
    {
        newsTitle: 'title'
    },
    {
        newsTitle: 'title'
    },
    {
        newsTitle: 'title'
    },
    {
        newsTitle: 'title'
    },
    {
        newsTitle: 'title'
    },
    {
        newsTitle: 'title'
    },
    {
        newsTitle: 'title'
    },
    {
        newsTitle: 'title'
    },
]


class SliderCard extends Component {

    state = {
        image: null,
        detailImage: null,
        deleteItem: false, load: false
    }

    deleteImg = () => {

        axios.delete(`${BASE_END_POINT}slider/${this.props.data.id}`, {
            headers: {
                "Authorization": `Bearer ${this.props.currentUser.token}`,
            }
        })
            .then(response => {
                this.setState({ deleteItem: true, load: false })
                RNToasty.Success({ title: Strings.deleteImageSuccessfuly })
                //resetTo('AdminHome')
            })
            .catch(error => {
                console.log('Error  ', error.response.data.errors)
                RNToasty.Error({ title: error.response.data.errors })
                //RNToasty.Success({ title: Strings.deleteImageSuccessfuly })
                this.setState({ load: false })
                //Alert.alert('' + error.response)
            })
    }


    deleteButton = () => {
        const { isRTL } = this.props;
        return (


            <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', alignSelf: 'center', justifyContent: 'space-between', alignItems: 'center', width: responsiveWidth(18) }}>
                <Button onPress={() => {
                    this.deleteImg()
                }} style={{ width: responsiveWidth(18), height: responsiveHeight(5), justifyContent: 'center', alignItems: 'center', alignSelf: 'center', borderRadius: moderateScale(3), backgroundColor: colors.darkRed }}>
                    <Text style={{ color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.delete}</Text>
                </Button>
            </View>

        )
    }


    render() {
        const { isRTL, navigator, data } = this.props;
        const { image, detailImage, deleteItem } = this.state;
        moment.locale(this.props.isRTL ? 'ar' : 'en');
        return (

            deleteItem ?
                null
                :
                <View>
                    <Animatable.View
                        style={{alignSelf: 'center', width: responsiveWidth(90) }}
                        animation={"slideInLeft"}
                        duration={1000}>
                        <View style={{ marginTop: moderateScale(8) }}>

                            <View style={{ alignItems: 'center', flexDirection: isRTL ? 'row-reverse' : 'row', width: responsiveWidth(90), justifyContent: 'space-between', alignSelf: 'center' }}>
                                <FastImage
                                    resizeMode='stretch'
                                    source={data.img[0] ? { uri: data.img[0] } : require('../assets/imgs/back-gray.jpg')}
                                    style={{ width: 70, height: 70, borderRadius: 35 }}
                                />
                                {this.deleteButton()}
                            </View>

                        </View>
                    </Animatable.View>
                </View>


        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
});

const mapDispatchToProps = {
    getUnreadNotificationsNumers
}


export default connect(mapStateToProps, mapDispatchToProps)(SliderCard);
