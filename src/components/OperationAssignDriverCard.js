import React, { Component } from 'react';
import { View, Alert, TouchableOpacity, Text,Linking } from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import * as colors from '../assets/colors'
import Strings from '../assets/strings';
import { Thumbnail, Icon, Button } from 'native-base';
import moment from 'moment'
import "moment/locale/ar"
import axios from 'axios';
import { BASE_END_POINT } from '../AppConfig';
import withPreventDoubleClick from './withPreventDoubleClick';
import { getUnreadNotificationsNumers } from '../actions/NotificationAction'
import { arrabicFont, englishFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import { push, resetTo } from '../controlls/NavigationControll'
import FastImage from 'react-native-fast-image'
import { RNToasty } from 'react-native-toasty';
import { getDistance } from 'geolib';
import { OrdersCount } from '../actions/OrdersActions';
import { whiteButton, greenButton } from '../assets/styles';


class OperationAssignDriverCard extends Component {

    assignButton = () => {

        const { isRTL,data,order,socket,currentUser } = this.props;
        return (
            <View style={{ alignSelf: 'center', justifyContent: 'center', alignItems: 'center', }}>
                <TouchableOpacity
                    onPress={() => {
                        socket.emit("assignOrder",{
                            userId:currentUser.user.id,
                            orderId:order.id,
                            driverId:data.id
                        });
                        RNToasty.Success({title:Strings.assignOrderSuccess})
                        this.props.OrdersCount(null, 'PENDING')
                        //push('OperationWarehouses',order)
                        if(currentUser.user.type=='ADMIN'){
                            resetTo('AdminHome')
                        }else if(currentUser.user.type=='OPERATION'){
                            resetTo('AdminHome')
                        }else{
                            resetTo('CallCenterHome')
                        }
                    }}
                    style={[whiteButton,{ marginHorizontal: moderateScale(2), width: responsiveWidth(28), height: responsiveHeight(5), justifyContent: 'center', alignItems: 'center', alignSelf: 'center',}]}>
                    <Text style={{ color: colors.green, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.assign}</Text>
                </TouchableOpacity>


            </View>
        )
    }

    openMap(resturant, latitude, longitude) {
        var url = 'http://maps.google.com/maps?q=' + resturant + '&daddr=' + latitude + ',' + longitude;
        Linking.openURL(url);
    }

    render() {
        const { isRTL,data,order } = this.props;
        console.log("MY SOCKET  ",this.props.socket )
        const distance = getDistance(
            { latitude: order.restaurant.branches[order.branch].destination[0], longitude: order.restaurant.branches[order.branch].destination[1] },
            { latitude: data.location.length>0?data.location[0]:0 , longitude: data.location.length>0?data.location[1]:0 }
            
        );
        return (
            <View>
                <Animatable.View
                    style={{ marginTop: moderateScale(3), alignSelf: 'center', width: responsiveWidth(96) }}
                    animation={"slideInLeft"}
                    duration={1000}>
                    <View style={{}}>
                        <View style={{ alignItems: 'center', marginTop: moderateScale(5), flexDirection: isRTL ? 'row-reverse' : 'row', width: responsiveWidth(94) }}>
                            <Thumbnail source={data.img?{uri:data.img}:require('../assets/imgs/profileicon.jpg')}  />
                            <View style={{ width: responsiveWidth(40), marginHorizontal: moderateScale(3) }}>
                                <Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', color: colors.grayColor1, fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont }}>{data.firstname} {data.lastname}</Text>
                                <Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', color: colors.grayColor1, fontSize: responsiveFontSize(5), fontFamily: isRTL ? arrabicFont : englishFont }}>{distance/1000} KM</Text>
                            </View>

                            <View style={{justifyContent:'center',alignItems:'center'}}>
                            {this.assignButton()}

                            {data.location.length>0&&
                            <TouchableOpacity style={{ alignSelf: 'center', marginTop: moderateScale(3) }} onPress={() => {
                                this.openMap(data.firstname+" "+data.firstname,data.location[0],data.location[1])
                            }}>
                                <FastImage style={{ width: 20, height: 20 }} source={require('../assets/imgs/home-driverlocation-icon.png')} />
                            </TouchableOpacity>
                            }
                            </View>

                        </View>

                    </View>


                </Animatable.View>
            </View>

        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
    ordersCount: state.orders.ordersCount,
});

const mapDispatchToProps = {
    getUnreadNotificationsNumers,
    OrdersCount,
}


export default connect(mapStateToProps, mapDispatchToProps)(OperationAssignDriverCard);
