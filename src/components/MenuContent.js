import React, { Component } from 'react';
import {
  View,
  Text, Alert,
  Image,
  ActivityIndicator,
  Platform,
  TouchableOpacity,
  ScrollView,
  Dimensions,
  Share
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage'
import { connect } from 'react-redux';
import { Button, Content, Icon, Thumbnail } from 'native-base';
import { bindActionCreators } from 'redux';
import * as Actions from '../actions/MenuActions';
import MenuItem from '../common/MenuItem';
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import * as colors from '../assets/colors';
import { rootNavigator } from '../screens/Login';
import Strings from '../assets/strings';
import { selectMenu } from '../actions/MenuActions';
import strings from '../assets/strings';
import { RNToasty } from 'react-native-toasty';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Dialog, { DialogContent, DialogTitle } from 'react-native-popup-dialog';
import { openSideMenu, push, resetTo } from '../controlls/NavigationControll'
import { arrabicFont, englishFont } from '../common/AppFont'
import { logout } from '../actions/AuthActions'


class MenuContent extends Component {


  state = {
    v: false,
    load: false,
    showLogoutDialog: false,
  }

  render() {
    const { item, currentUser, color, isRTL } = this.props;
    if (this.props.isRTL) {
      Strings.setLanguage('ar')
    } else {
      Strings.setLanguage('en')
    }
    console.log("menu Content item =>   " + item);

    return (
      <>
        <View style={{ backgroundColor: colors.darkGreen, height: Platform.OS === 'ios' ? responsiveHeight(3) : 0 }} />
        <View showsVerticalScrollIndicator={false} style={{flex:1, backgroundColor: colors.white, height: responsiveHeight(100), width: Platform.OS === 'ios' ? 310 : responsiveWidth(85)}}>


          <View
            onPress={() => {
              if (this.props.currentUser) {
                if (item[item.length - 1] == "PROFILE") {
                  openSideMenu(false)
                } else {
                  openSideMenu(false)
                  this.props.selectMenu('PROFILE');
                  push('Profile')
                }
              } else {
                push('Login')
                openSideMenu(false)
              }
            }}
            style={{ marginBottom: moderateScale(12), paddingVertical: moderateScale(8), alignSelf: 'center', backgroundColor: colors.darkGreen, justifyContent:"center", width: Platform.OS === 'ios' ? 310 : responsiveWidth(85)}}>
              

            <View style={{ alignSelf: 'center', justifyContent: 'center', alignItems: 'center' }}>
              <Thumbnail
                style={{ borderColor: colors.buttonColor }} large
                source={currentUser && currentUser.user.img ? { uri: currentUser.user.img } : require('../assets/imgs/profileicon.jpg')} />
            </View>

            {true &&
              <View style={{ marginTop: moderateScale(4), alignSelf: 'center', justifyContent: 'center' }}>
                <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, color: 'white', fontSize: responsiveFontSize(8) }}>{currentUser && currentUser.user.firstname + " " + currentUser.user.lastname}</Text>
              </View>
            }

            {true &&
              <View style={{ marginTop: moderateScale(2), alignSelf: 'center', justifyContent: 'center' }}>
                <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, color: 'white', fontSize: responsiveFontSize(6) }}>{currentUser && currentUser.user.phone}</Text>
              </View>
            }

          </View>
        

          <View style={{ backgroundColor: colors.darkGreen, height: Platform.OS === 'ios' ? responsiveHeight(0) : 0 }} />

          <View style={{ marginTop: hp(4), marginHorizontal: isRTL ? Platform.OS === 'ios' ? moderateScale(2) : moderateScale(-0) : Platform.OS === 'ios' ? moderateScale(-3) : moderateScale(-1)   }}>
          <View style={{ marginBottom: hp(3), }}>
            {currentUser && (currentUser.user.type == 'ADMIN' || currentUser.user.type == 'OPERATION') &&
              <MenuItem
                onPress={() => {
                  openSideMenu(false)
                  push('AdminNewsList')
                }}
                title={Strings.news}
                img={require('../assets/imgs/home-news-icon.png')}
             
              />
            }


            {currentUser && currentUser.user.type == 'USER' &&
              <MenuItem
                onPress={() => {
                  openSideMenu(false)
                  push('History')
                }}
                title={Strings.history}
                img={require('../assets/imgs/home-history-icon.png')}
              />
            }

            {currentUser && currentUser.user.type == 'USER' &&
              <MenuItem
                onPress={() => {
                  openSideMenu(false)
                  push('ContactUs')
                }}
                title={Strings.contactZeitco}
                img={require('../assets/imgs/home-sendemail-icon.png')}
              />
            }

            <MenuItem
              onPress={() => {
                openSideMenu(false)
                push('SelectLanguage')
              }}
              title={Strings.language}
              img={require('../assets/imgs/home-profileedit-icon.png')}
            />



            <MenuItem
              onPress={() => {
                openSideMenu(false)
                push('UpdateProfile')
              }}
              title={Strings.editProfile}
              img={require('../assets/imgs/home-profileedit-icon.png')}
            />

            <MenuItem
              onPress={() => {
                openSideMenu(false)
                this.props.logout(currentUser.token, this.props.firbaseToken)
              }}
              title={Strings.logout}
              img={require('../assets/imgs/home-logout-icon.png')}
            />

            <Dialog
              width={responsiveWidth(70)}

              visible={this.state.showLogoutDialog}
              onTouchOutside={() => {
                this.setState({ showLogoutDialog: false })
                //this.props.showDeleteNotificationDialog(0,false)
              }}
            >
              <View style={{ width: responsiveWidth(70) }}>
                <View style={{ alignSelf: 'center', marginTop: moderateScale(10) }}>
                  <Icon name='hand-paper' type='FontAwesome5' style={{ color: 'red', fontSize: responsiveFontSize(8) }} />
                </View>
                <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, textAlign: 'center', width: responsiveWidth(60), fontSize: responsiveFontSize(6), alignSelf: 'center', color: 'black', marginTop: moderateScale(3) }} >{Strings.areYouShure3}</Text>
                <View style={{ alignSelf: 'center', width: responsiveWidth(60), justifyContent: 'space-between', alignItems: 'center', flexDirection: isRTL ? 'row-reverse' : 'row', marginVertical: moderateScale(10) }} >
                  <TouchableOpacity
                    onPress={() => {
                      openSideMenu(false)
                      this.setState({ showLogoutDialog: false })
                      //this.props.showDeleteNotificationDialog(0,false)
                    }}
                  >
                    <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(7), color: 'gray' }}>{Strings.cancel}</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => {
                      console.log("FB MENUCONTENT", this.props.firbaseToken)
                      this.props.logout(currentUser.token, this.props.firbaseToken)
                      openSideMenu(false)
                    }}
                  >
                    <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(7), color: 'red' }}>{Strings.logout}</Text>
                  </TouchableOpacity>

                </View>
              </View>
            </Dialog>
          </View>
          </View>
        </View>
      </>
    );
  }
}



const mapStateToProps = state => ({
  item: state.menu.item,
  isRTL: state.lang.RTL,
  currentUser: state.auth.currentUser,
  userToken: state.auth.userToken,
  firbaseToken: state.noti.firbaseToken
});

const mapDispatchToProps = {
  selectMenu,
  logout
}

export default connect(mapStateToProps, mapDispatchToProps)(MenuContent);
// MenuContent
// connect(mapStateToProps, mapDispatchToProps, )(MenuContent);
