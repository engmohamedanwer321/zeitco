import React, { Component } from 'react';
import { View, Alert, TouchableOpacity, Text, Image } from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import * as colors from '../assets/colors'
import Strings from '../assets/strings';
import { Thumbnail, Icon, Button } from 'native-base';
import moment from 'moment'
import "moment/locale/ar"
import axios from 'axios';
import { BASE_END_POINT } from '../AppConfig';
import withPreventDoubleClick from './withPreventDoubleClick';
import { getUnreadNotificationsNumers } from '../actions/NotificationAction'
import { arrabicFont, englishFont, boldFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image'
import { push } from '../controlls/NavigationControll'



const MyTouchableOpacity = withPreventDoubleClick(TouchableOpacity);

//moment(birth_date).format("YYYY-MM-DD")


class NewAccountsCard extends Component {

    proceedButton = () => {

        const { isRTL, from, data } = this.props;
        return (
            <View style={{ alignSelf: 'center', justifyContent: 'center', alignItems: 'center', }}>
                <Button onPress={() => {
                    push('OperationConfirmResturantAccount', data)
                }} style={{ width: responsiveWidth(28), height: responsiveHeight(5), justifyContent: 'center', alignItems: 'center', alignSelf: 'center', borderRadius: moderateScale(3), backgroundColor: colors.greenButton }}>
                    <Text style={{ color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.proceed}</Text>
                </Button>


            </View>
        )
    }

    render() {
        const { isRTL, navigator, data } = this.props;
        moment.locale(this.props.isRTL ? 'ar' : 'en');
        return (
            <View>
                <Animatable.View
                    style={{ marginTop: moderateScale(3), alignSelf: 'center', width: responsiveWidth(96) }}
                    animation={"flipInX"}
                    duration={1000}>
                    <TouchableOpacity onPress={() => {
                    push('OperationConfirmResturantAccount', data)
                }}>
                        <View style={{ alignItems: 'center', marginTop: moderateScale(5), flexDirection: isRTL ? 'row-reverse' : 'row', width: responsiveWidth(96) }}>
                            <FastImage source={data.img ? { uri: data.img } : require('../assets/imgs/profileicon.jpg')} style={{width:responsiveWidth(17), height:responsiveWidth(17), borderRadius:responsiveWidth(8.5) }} />
                            <View style={{ width: responsiveWidth(43), marginHorizontal: moderateScale(4) }}>
                            <Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', color: colors.grayColor1, fontSize: responsiveFontSize(5), fontFamily: isRTL ? arrabicFont : englishFont }}><Text style={{ fontFamily: boldFont }}>{Strings.outletName}:</Text> {isRTL?data.restaurantName_ar: data.restaurantName}</Text>
                                <Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', color: colors.grayColor1, fontSize: responsiveFontSize(5), fontFamily: isRTL ? arrabicFont : englishFont}}><Text style={{ fontFamily: boldFont }}>{Strings.EstQt}:</Text> {data.average}</Text>
                                <Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', color: colors.grayColor1, fontSize: responsiveFontSize(5), fontFamily: isRTL ? arrabicFont : englishFont }}><Text style={{ fontFamily: boldFont }}>{Strings.price}:</Text> {data.price} <Text >{Strings.egpKg}</Text></Text>
                                <Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', color: colors.grayColor1, fontSize: responsiveFontSize(5), fontFamily: isRTL ? arrabicFont : englishFont }}><Text style={{ fontFamily: boldFont }}>{Strings.area}:</Text> {isRTL?data.branches[0].area.arabicAreaName:data.branches[0].area.areaName}</Text>
                                {/*<Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', color: colors.grayColor1, fontSize: responsiveFontSize(5), fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.locationArea}</Text>*/}

                            </View>
                            <View style={{ width: responsiveWidth(25), marginHorizontal: moderateScale(4) }}>
                            <Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', color: colors.grayColor1, fontSize: responsiveFontSize(5), fontFamily: isRTL ? arrabicFont : englishFont }}><Text style={{ fontFamily: boldFont }}>{Strings.purchaserName}:</Text> {data.director.firstname + ' ' +data.director.lastname }</Text>
                            <Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', color: colors.grayColor1, fontSize: responsiveFontSize(5), fontFamily: isRTL ? arrabicFont : englishFont }}><Text style={{ fontFamily: boldFont }}>{Strings.purchaserName}:</Text> {moment(data.director.updatedAt).format('(HH:MM)') }</Text>
           

                            </View>

                           

                        </View>

                    </TouchableOpacity>


                </Animatable.View>
            </View>

        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
});

const mapDispatchToProps = {
    getUnreadNotificationsNumers
}


export default connect(mapStateToProps, mapDispatchToProps)(NewAccountsCard);
