import React,{Component} from 'react';
import {View,Animated,TouchableOpacity,Text, Image} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import * as colors from '../assets/colors'
import Strings from '../assets/strings';
import { Thumbnail,Icon } from 'native-base';
import moment from 'moment'
import "moment/locale/ar"
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import withPreventDoubleClick from './withPreventDoubleClick';
import {getUnreadNotificationsNumers} from '../actions/NotificationAction'
import {arrabicFont,englishFont} from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image';
import {push} from '../controlls/NavigationControll'



class AccountCard extends Component {
    
    
    render(){
        const {isRTL,data} = this.props;
        return(
            <TouchableOpacity
            onPress={()=>{
                push('AdminUpdateMemberProfile',data)
            }}
      
            >
            <Animatable.View
            style={{ height:responsiveHeight(12),flexDirection:isRTL?'row-reverse':'row',alignItems:'center', marginTop: moderateScale(2), alignSelf: 'center', width: responsiveWidth(96)}}
            animation={"slideInLeft"}
            duration={1000}>
                <FastImage 
                large
                source={data.img?{uri:data.img}:require('../assets/imgs/profileicon.jpg')}
                style={{width:responsiveWidth(15), height:responsiveWidth(15), borderRadius:responsiveWidth(7.5) }}
                />
                <View style={{alignItems:'center'}}>
                    <Text style={{alignSelf:isRTL?'flex-end':'flex-start',color:'gray',marginHorizontal:moderateScale(3), fontFamily:isRTL?arrabicFont:englishFont,fontSize:responsiveFontSize(6)}}>{data.firstname} {data.lastname}</Text>
                    <Text style={{alignSelf:isRTL?'flex-end':'flex-start', color:'gray', marginHorizontal:moderateScale(3), fontFamily:isRTL?arrabicFont:englishFont,fontSize:responsiveFontSize(6)}}>{data.phone}</Text>
                </View>
            </Animatable.View>
            </TouchableOpacity>
           
        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
});

const mapDispatchToProps = {
    getUnreadNotificationsNumers
}


export default connect(mapStateToProps,mapDispatchToProps)(AccountCard);
