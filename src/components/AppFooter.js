import React,{Component} from 'react';
import {View,Animated,TouchableOpacity,Text} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import * as colors from '../assets/colors'
import Strings from '../assets/strings';
import { Thumbnail,Icon } from 'native-base';
import moment from 'moment'
import "moment/locale/ar"
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import withPreventDoubleClick from './withPreventDoubleClick';
import {getUnreadNotificationsNumers} from '../actions/NotificationAction'
import {arrabicFont,englishFont} from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image';




class AppFooter extends Component {
    
    
    render(){
        const {isRTL,navigator,data} = this.props;
        return(
            <View style={{width:responsiveWidth(100),height:responsiveHeight(10),position:'absolute',bottom:0,left:0,backgroundColor:colors.white,zIndex:10000,elevation:4,shadowOffset:{height:0,width:2},shadowOpacity:0.2,shadowColor:'black'}}>
                <View style={{flex:1,width:responsiveWidth(92),alignSelf:'center',flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
                    <View style={{flexDirection:'row',alignItems:'center'}}>
                        <FastImage
                        resizeMode='contain'
                        style={{height: responsiveHeight(10), width: responsiveWidth(16) }}
                        source={require('../assets/imgs/footerLogo.png')}
                        />
                        {/*<View>
                            <Text style={{ fontFamily: englishFont, color: colors.black, fontSize: responsiveFontSize(7) }}>Zeitco</Text>
                            <Text style={{ fontFamily: englishFont, color: colors.black, marginTop: moderateScale(1), fontSize: responsiveFontSize(3) }}>OIL RECYCLING</Text>
                        </View>*/}
                    </View>
                    {/*<Text style={{ fontFamily: englishFont, color: colors.black, fontSize: responsiveFontSize(3) }}>copy right 2019</Text>*/}

                </View>
            </View>
        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
});

const mapDispatchToProps = {
    getUnreadNotificationsNumers
}


export default connect(mapStateToProps,mapDispatchToProps)(AppFooter);
