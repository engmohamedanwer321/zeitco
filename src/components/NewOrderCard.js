import React, { Component } from 'react';
import { View, Alert, TouchableOpacity, Text, Image } from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import * as colors from '../assets/colors'
import Strings from '../assets/strings';
import { Thumbnail, Icon, Button } from 'native-base';
import moment from 'moment'
import "moment/locale/ar"
import axios from 'axios';
import { BASE_END_POINT } from '../AppConfig';
import withPreventDoubleClick from './withPreventDoubleClick';
import { getUnreadNotificationsNumers } from '../actions/NotificationAction'
import { arrabicFont, englishFont, boldFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image'
import { push } from '../controlls/NavigationControll'



class NewOrderCard extends Component {

    proceedButton = () => {
        const { isRTL, data } = this.props;
        return (
            <View style={{ alignSelf: 'center', justifyContent: 'center', alignItems: 'center', }}>
                <Button onPress={() => {
                    push('OperationAssignOrderToDriver', data)
                }} style={{ width: responsiveWidth(28), height: responsiveHeight(5), justifyContent: 'center', alignItems: 'center', alignSelf: 'center', borderRadius: moderateScale(3), backgroundColor: colors.greenButton }}>
                    <Text style={{ color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.proceed}</Text>
                </Button>
            </View>
        )
    }

    render() {
        const { isRTL, data, hideButtons } = this.props;
        moment.locale(this.props.isRTL ? 'ar' : 'en');
        console.log("BBBB   ", data.branch)
        return (
            <View>
                <Animatable.View
                    style={{ marginTop: moderateScale(3), alignSelf: 'center', width: responsiveWidth(96) }}
                    animation={"flipInX"}
                    duration={1000}>
                    <TouchableOpacity onPress={() => {
                        var d = { ...data }
                        if (hideButtons) {
                            d = {
                                ...d,
                                hideButtons: true,
                            }
                        }
                        push('OperationAssignOrderToDriver', d)
                    }}>
                        <View style={{ alignItems: 'center', marginTop: moderateScale(5), flexDirection: isRTL ? 'row-reverse' : 'row', width: responsiveWidth(96) }}>
                            <FastImage source={data.restaurant.img ? { uri: data.restaurant.img } : require('../assets/imgs/profileicon.jpg')} style={{ width: responsiveWidth(17), height: responsiveWidth(17), borderRadius: responsiveWidth(8.5) }} />
                            <View style={{ width: responsiveWidth(50), marginHorizontal: moderateScale(3) }}>
                                <Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', color: colors.grayColor1, fontSize: responsiveFontSize(5), fontFamily: isRTL ? arrabicFont : englishFont }} numberOfLines={1}><Text style={{ fontFamily: boldFont }}>{Strings.outletName}:</Text> {isRTL ? data.restaurant.restaurantName_ar : data.restaurant.restaurantName}</Text>
                                <Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', color: colors.grayColor1, fontSize: responsiveFontSize(5), fontFamily: isRTL ? arrabicFont : englishFont }}><Text style={{ fontFamily: boldFont }}>{Strings.orderQt}:</Text> {data.average} {Strings.Kg}</Text>
                                <Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', color: colors.grayColor1, fontSize: responsiveFontSize(5), fontFamily: isRTL ? arrabicFont : englishFont }}><Text style={{ fontFamily: boldFont }}>{Strings.price}:</Text> {data.restaurant.price} {Strings.EGP}</Text>
                                <Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', color: colors.grayColor1, fontSize: responsiveFontSize(5), fontFamily: isRTL ? arrabicFont : englishFont }}><Text style={{ fontFamily: boldFont }}>{Strings.area}:</Text> {isRTL ? data.restaurant.branches[0].area.arabicAreaName : data.restaurant.branches[0].area.areaName}</Text>


                                {/*<Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', color: colors.grayColor1, fontSize: responsiveFontSize(5), fontFamily: isRTL ? arrabicFont : englishFont }}>{data.restaurant.branches[data.branch].address}</Text>*/}

                            </View>

                            <View style={{ justifyContent: 'center', alignItems: 'center', marginHorizontal: moderateScale(2), width: responsiveWidth(22) }}>
                                <Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', color: colors.grayColor1, fontSize: responsiveFontSize(5), fontFamily: isRTL ? arrabicFont : englishFont }}><Text style={{ fontFamily: boldFont }}>{Strings.date}:</Text> {(data.date).replace('-', '.').replace('-', '.')}</Text>
                            </View>



                        </View>

                    </TouchableOpacity>


                </Animatable.View>
            </View>

        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
});

const mapDispatchToProps = {
    getUnreadNotificationsNumers
}


export default connect(mapStateToProps, mapDispatchToProps)(NewOrderCard);
