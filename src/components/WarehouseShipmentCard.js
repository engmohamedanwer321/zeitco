import React, { Component } from 'react';
import { View, Alert, TouchableOpacity, Text } from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import * as colors from '../assets/colors'
import Strings from '../assets/strings';
import { Thumbnail, Icon, Button } from 'native-base';
import moment from 'moment'
import "moment/locale/ar"
import axios from 'axios';
import { BASE_END_POINT } from '../AppConfig';
import withPreventDoubleClick from './withPreventDoubleClick';
import { getUnreadNotificationsNumers } from '../actions/NotificationAction'
import { arrabicFont, englishFont, boldFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image'
import { push } from '../controlls/NavigationControll'


class WarehouseShipmentCard extends Component {


    render() {
        const { isRTL, data, currentUser } = this.props;
        moment.locale(this.props.isRTL ? 'ar' : 'en');
        console.log('vvvvvv : ',currentUser.user.type)
        return (
            <TouchableOpacity
                onPress={() => {
                    currentUser.user.type == 'ADMIN' ?
                    push('AdminUpdateShipping',data)
                    :
                    push('WarehouseShipment', data)
                    
                }}
            >
                <Animatable.View
                    style={{ borderBottomColor: '#cccbcb', borderBottomWidth: 1, paddingVertical: moderateScale(6), alignSelf: 'center', width: responsiveWidth(80) }}
                //animation={"slideInLeft"}
                //duration={1000}
                >
                    <View style={{}}>
                        <View style={{ width: responsiveWidth(80) }}>

                            <View style={{ width: responsiveWidth(80) }}>
                                <Text style={{ textAlign: isRTL ? 'right' : 'left', color: colors.grayColor1, fontSize: responsiveFontSize(8), fontFamily: isRTL ? boldFont : boldFont }}>{data.driverName}</Text>

                                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'space-between', alignItems: 'center', marginTop: moderateScale(3) }}>
                                    <Text style={{ textAlign: isRTL ? 'right' : 'left', color: colors.grayColor1, fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont }}>- {Strings.quantity} : {data.amount} {Strings.Kg}</Text>
                                    <Text style={{ textAlign: isRTL ? 'right' : 'left', color: colors.grayColor1, fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont }}>- {Strings.date} : {data.shippingDay}</Text>
                                </View>

                                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'space-between', alignItems: 'center', marginTop: moderateScale(2) }}>
                                    <Text style={{ textAlign: isRTL ? 'right' : 'left', color: colors.grayColor1, fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont }}>- {Strings.vehicleNumber} : {data.vehicleNumber}</Text>
                                    <Text style={{ textAlign: isRTL ? 'right' : 'left', color: colors.grayColor1, fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont }}>- {Strings.tankNumber} : {data.tankNumber}</Text>
                                </View>
                                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'space-between', alignItems: 'center', marginTop: moderateScale(2) }}>
                                    <Text style={{ textAlign: isRTL ? 'right' : 'left', color: colors.grayColor1, fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont }}>- {Strings.waybill} : {data.waybill}</Text>
                                </View>


                            </View>
                        </View>

                    </View>


                </Animatable.View>
            </TouchableOpacity>

        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
});

const mapDispatchToProps = {
    getUnreadNotificationsNumers
}


export default connect(mapStateToProps, mapDispatchToProps)(WarehouseShipmentCard);
