import React, { Component } from 'react';
import { View, Alert, TouchableOpacity, Text, Linking } from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import * as colors from '../assets/colors'
import Strings from '../assets/strings';
import { Thumbnail, Icon, Button } from 'native-base';
import axios from 'axios';
import { BASE_END_POINT } from '../AppConfig';
import withPreventDoubleClick from './withPreventDoubleClick';
import { getUnreadNotificationsNumers } from '../actions/NotificationAction'
import { arrabicFont, englishFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import { push, resetTo } from '../controlls/NavigationControll'
import FastImage from 'react-native-fast-image'
import { RNToasty } from 'react-native-toasty';
import { getDistance } from 'geolib';
import { OrdersCount } from '../actions/OrdersActions';
import { whiteButton, greenButton } from '../assets/styles';
import moment from 'moment'

class DriverCheckingCard extends Component {



    render() {
        const { isRTL, data, order } = this.props;
        moment.locale('en')
        return (
            <View>
                <Animatable.View
                    style={{ marginTop: moderateScale(3), alignSelf: 'center', width: responsiveWidth(96) }}
                    animation={"slideInLeft"}
                    duration={1000}>
                    <View style={{}}>
                        <View style={{ alignItems: 'center', marginTop: moderateScale(5), flexDirection: isRTL ? 'row-reverse' : 'row', width: responsiveWidth(94) }}>
                            <Thumbnail source={data.img ? { uri: data.img } : require('../assets/imgs/profileicon.jpg')} />
                            <View style={{ width: responsiveWidth(70), marginHorizontal: moderateScale(3) }}>

                                <Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', color: colors.grayColor1, fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.name} : {data.user.firstname} {data.user.lastname}</Text>
                                {/*<Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', color: colors.grayColor1, fontSize: responsiveFontSize(5), fontFamily: isRTL ? arrabicFont : englishFont }}> {Strings.phone} : {data.user.phone}</Text>*/}


                                <Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', color: colors.grayColor1, fontSize: responsiveFontSize(5), fontFamily: isRTL ? arrabicFont : englishFont }}> {Strings.checkinTime} : {moment(data.start).format('YYYY.MM.DD (HH:MM)')}</Text>
                                <Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', color: colors.grayColor1, fontSize: responsiveFontSize(5), fontFamily: isRTL ? arrabicFont : englishFont }}> {Strings.checkoutTime} : {moment(data.end).format('YYYY.MM.DD (HH:MM)')}</Text>
                            </View>

                        </View>

                    </View>


                </Animatable.View>
            </View>

        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
    ordersCount: state.orders.ordersCount,
});

const mapDispatchToProps = {
    getUnreadNotificationsNumers,
    OrdersCount,
}


export default connect(mapStateToProps, mapDispatchToProps)(DriverCheckingCard);
