import React,{Component} from 'react';
import {View,Animated,TouchableOpacity,Text} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import * as colors from '../assets/colors'
import Strings from '../assets/strings';
import { Thumbnail,Icon } from 'native-base';
import moment from 'moment'
import "moment/locale/ar"
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import withPreventDoubleClick from './withPreventDoubleClick';
import {getUnreadNotificationsNumers} from '../actions/NotificationAction'
import {arrabicFont,englishFont} from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image';
import {push,resetTo} from '../controlls/NavigationControll'
import { OrdersCount } from '../actions/OrdersActions';
import { whiteButton, greenButton } from '../assets/styles';
import { RNToasty } from 'react-native-toasty';

class AdminWarehouseCard extends Component {

    assignButton = () => {
        const { isRTL,data,} = this.props;
        return (
            <View style={{ alignSelf: 'center', justifyContent: 'center', alignItems: 'center', }}>
                <TouchableOpacity
                    onPress={() => {
                    
                    }}
                    style={[whiteButton,{ marginHorizontal: moderateScale(2), width: responsiveWidth(28), height: responsiveHeight(5), justifyContent: 'center', alignItems: 'center', alignSelf: 'center',}]}>
                    <Text style={{ color: colors.green, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.assign}</Text>
                </TouchableOpacity>


            </View>
        )
    }

    render(){
        const {isRTL,data,order,socket} = this.props;
        return(
            <TouchableOpacity
            onPress={()=>{
              push('AdminWarehouseDetails',data)
            }}
            >
            <Animatable.View
            style={{borderBottomColor:colors.lightGray,borderBottomWidth:1, height:responsiveHeight(16),alignItems:'center',justifyContent:'center', marginTop: moderateScale(3), alignSelf: 'center', width: responsiveWidth(96) }}
            useNativeDriver
            animation={"slideInLeft"}
            duration={1000}>
               
                <View style={{alignItems:'center',justifyContent:'center'}} >
                    <Text style={{color:'gray',marginHorizontal:moderateScale(3), fontFamily:isRTL?arrabicFont:englishFont,fontSize:responsiveFontSize(7),fontWeight:'bold'}}>{data.name} {data.lastname}</Text>
                    <Text style={{color:'gray', marginHorizontal:moderateScale(3), fontFamily:isRTL?arrabicFont:englishFont,fontSize:responsiveFontSize(6)}}>{Strings.quantity} : {data.oilQuantity} {Strings.Kg}</Text>
                    
                </View>
            </Animatable.View>
            </TouchableOpacity>
           
        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
});

const mapDispatchToProps = {
    getUnreadNotificationsNumers,
    OrdersCount,
}


export default connect(mapStateToProps,mapDispatchToProps)(AdminWarehouseCard);
