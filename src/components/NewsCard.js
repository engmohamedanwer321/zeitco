import React, { Component } from 'react';
import { View, Alert, TouchableOpacity, Text } from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import * as colors from '../assets/colors'
import Strings from '../assets/strings';
import { Thumbnail, Icon, Button } from 'native-base';
import moment from 'moment'
import "moment/locale/ar"
import axios from 'axios';
import { BASE_END_POINT } from '../AppConfig';
import withPreventDoubleClick from './withPreventDoubleClick';
import { getUnreadNotificationsNumers } from '../actions/NotificationAction'
import { arrabicFont, englishFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image'
import { push } from '../controlls/NavigationControll'
import { RNToasty } from 'react-native-toasty';
import {whiteButton, greenButton} from '../assets/styles'



class NewsCard extends Component {

    state = {
        delet: false,
    }

    deleteAds = () => {
        axios.delete(`${BASE_END_POINT}news/${this.props.data.id}`, {
            headers: {
                "Authorization": `Bearer ${this.props.currentUser.token}`,
            }
        })
            .then(response => {
                this.setState({ delet: true })
                RNToasty.Success({ title: Strings.deleteNewsSuccessfuly })
                //resetTo('AdminHome')
            })
            .catch(error => {
                console.log('Error  ', error.response)
                //this.setState({load:false})
                Alert.alert('' + error.response)
            })
    }
    editDeleteButtons = () => {

        const { isRTL, data } = this.props;
        return (

            <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', alignSelf: 'center', justifyContent: 'space-between', alignItems: 'center', width: responsiveWidth(38) }}>
                <TouchableOpacity onPress={() => {
                    push('AdminUpdateNews', data)
                }} style={[whiteButton,{ width: responsiveWidth(19), height: responsiveHeight(5), justifyContent: 'center', alignItems: 'center', alignSelf: 'center', }]}>
                    <Text style={{ color: colors.green, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.edit}</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => {
                    this.deleteAds()
                }} style={[greenButton,{ width: responsiveWidth(19), height: responsiveHeight(5), justifyContent: 'center', alignItems: 'center', alignSelf: 'center', }]}>
                    <Text style={{ color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.delete}</Text>
                </TouchableOpacity>
            </View>
        )
    }



    render() {
        const { isRTL, data } = this.props;
        const { delet } = this.state
        moment.locale(this.props.isRTL ? 'ar' : 'en');
        return (
            delet ?
                null
                :
                <View>
                    <Animatable.View
                        style={{ marginTop: moderateScale(3), alignSelf: 'center', width: responsiveWidth(96) }}
                        animation={"slideInLeft"}
                        duration={1000}>

                        <View style={{ marginTop: moderateScale(5) }}>

                            <View style={{ alignItems: 'center', marginTop: moderateScale(5), flexDirection: isRTL ? 'row-reverse' : 'row', width: responsiveWidth(96), marginTop: moderateScale(5) }}>
                                <View style={{ width: responsiveWidth(50), marginHorizontal: moderateScale(3) }}>
                                    <Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', color: colors.grayColor1, fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.title} : {data.title} </Text>
                                    <Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', color: colors.grayColor1, fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, marginTop: moderateScale(4) }}>{Strings.date} : {data.createdAt.substring(0, 10)} </Text>
                                </View>

                                {this.editDeleteButtons()}
                            </View>

                            <View style={{ width: responsiveWidth(60), borderBottomColor: 'gray', borderBottomWidth: 1, borderStyle: 'solid', alignSelf: 'center', marginTop: moderateScale(7) }} />

                        </View>
                    </Animatable.View>
                </View>

        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
});

const mapDispatchToProps = {
    getUnreadNotificationsNumers
}


export default connect(mapStateToProps, mapDispatchToProps)(NewsCard);
