import React, { Component } from 'react';
import { View, Animated, TouchableOpacity, Text } from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import * as colors from '../assets/colors'
import Strings from '../assets/strings';
import { Thumbnail, Icon } from 'native-base';
import moment from 'moment'
import "moment/locale/ar"
import axios from 'axios';
import { BASE_END_POINT } from '../AppConfig';
import withPreventDoubleClick from './withPreventDoubleClick';
import { getUnreadNotificationsNumers } from '../actions/NotificationAction'
import { arrabicFont, englishFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image';



const MyTouchableOpacity = withPreventDoubleClick(TouchableOpacity);

//moment(birth_date).format("YYYY-MM-DD")


class AdsCard extends Component {


    render() {
        const { isRTL, data } = this.props;
        return (

            <View style={{ marginHorizontal: moderateScale(2), marginVertical: moderateScale(5), backgroundColor: colors.white, borderRadius: moderateScale(5), zIndex: 10000, elevation: 8, shadowOffset: { width: 0, height: 2 }, shadowOpacity: 0.2, shadowColor: 'black', alignSelf: 'center', width: responsiveWidth(45), height: responsiveHeight(25) }}>
                <Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', margin: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6) }}>{data.title}</Text>
                <Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', marginHorizontal: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(4) }}>{data.description}</Text>
                <View style={{ position: 'absolute', bottom: 0, left: 0, right: 0, }} >
                    <FastImage
                        source={{ uri: data.img[0] }}
                        style={{ width: responsiveWidth(45), height: responsiveHeight(14), marginTop: moderateScale(5), borderBottomLeftRadius: moderateScale(5), borderBottomRightRadius: moderateScale(5) }}
                    />
                </View>
            </View>


            /*<View style={{marginHorizontal:moderateScale(2),marginVertical:moderateScale(5), backgroundColor:colors.white, borderRadius:moderateScale(5), zIndex:10000,elevation:8,shadowOffset:{width:0,height:2},shadowOpacity:0.2,shadowColor:'black', alignSelf:'center', width:responsiveWidth(45),minHeight:responsiveHeight(10)}}>
                            <Text style={{alignSelf:isRTL?'flex-end':'flex-start',margin:moderateScale(3), fontFamily:isRTL?arrabicFont:englishFont,fontSize:responsiveFontSize(6)}}>{data.title}</Text>
                            <Text style={{alignSelf:isRTL?'flex-end':'flex-start',marginHorizontal:moderateScale(3), fontFamily:isRTL?arrabicFont:englishFont,fontSize:responsiveFontSize(4)}}>{data.description}</Text>
                            
                        </View>*/

        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
});

const mapDispatchToProps = {
    getUnreadNotificationsNumers
}


export default connect(mapStateToProps, mapDispatchToProps)(AdsCard);
