import React, { Component } from 'react';
import { View, Alert, TouchableOpacity, Text, Image } from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import * as colors from '../assets/colors'
import Strings from '../assets/strings';
import { Thumbnail, Icon, Button } from 'native-base';
import moment from 'moment'
import "moment/locale/ar"
import axios from 'axios';
import { BASE_END_POINT } from '../AppConfig';
import withPreventDoubleClick from './withPreventDoubleClick';
import { getUnreadNotificationsNumers } from '../actions/NotificationAction'
import { arrabicFont, englishFont, boldFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import { push } from '../controlls/NavigationControll'
import FastImage from 'react-native-fast-image'
import strings from '../assets/strings';



const MyTouchableOpacity = withPreventDoubleClick(TouchableOpacity);

//moment(birth_date).format("YYYY-MM-DD")


class PurchasingAccountsCard extends Component {

    proceedButton = () => {

        const { isRTL, data } = this.props;
        return (
            <View style={{ alignSelf: 'center', justifyContent: 'center', alignItems: 'center', }}>
                <Button
                    onPress={() => {
                        //push('UpdateResturantAccount', data)
                        push('PurchasingOutlitesInfo', data)
                        //PurchasingOutlitesInfo
                    }}
                    style={{ width: responsiveWidth(28), height: responsiveHeight(5), justifyContent: 'center', alignItems: 'center', alignSelf: 'center', borderRadius: moderateScale(3), backgroundColor: colors.greenButton }}>
                    <Text style={{ color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.proceed}</Text>
                </Button>


            </View>
        )
    }


    render() {
        const { isRTL, navigator, data } = this.props;
        moment.locale(this.props.isRTL ? 'ar' : 'en');
        return (
            <View>
                <Animatable.View
                    style={{ marginTop: moderateScale(3), alignSelf: 'center', width: responsiveWidth(96) }}
                    animation={"flipInX"}
                    duration={1500}>
                    <TouchableOpacity onPress={() => {
                        //push('UpdateResturantAccount', data)
                        push('PurchasingOutlitesInfo', data)
                    }}>
                        <View style={{ alignItems: 'center', marginTop: moderateScale(5), flexDirection: isRTL ? 'row-reverse' : 'row', width: responsiveWidth(96) }}>
                            <FastImage source={data.img ? { uri: data.img } : require('../assets/imgs/profileicon.jpg')} style={{ width: responsiveWidth(15), height: responsiveWidth(15), borderRadius: responsiveWidth(7.5) }} />
                            <View style={{ width: responsiveWidth(70), marginHorizontal: moderateScale(3) }}>

                                <Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', color: colors.grayColor1, fontSize: responsiveFontSize(5), fontFamily: isRTL ? arrabicFont : englishFont }}><Text style={{ fontFamily: boldFont }}>{Strings.outletName}:</Text>{isRTL? data.restaurantName_ar: data.restaurantName}</Text>
                                <Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', color: colors.grayColor1, fontSize: responsiveFontSize(5), fontFamily: isRTL ? arrabicFont : englishFont }}><Text style={{ fontFamily: boldFont }}>{Strings.EstQt}:</Text>{data.average}</Text>
                                <Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', color: colors.grayColor1, fontSize: responsiveFontSize(5), fontFamily: isRTL ? arrabicFont : englishFont }}><Text style={{ fontFamily: boldFont }}>{Strings.price}:</Text> {data.price} <Text>{Strings.egpKg}</Text></Text>
                                {/*<Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', textAlign: isRTL ? 'right' : 'left', color: colors.grayColor1, fontSize: responsiveFontSize(5), fontFamily: isRTL ? arrabicFont : englishFont }}>{data.address}</Text>*/}
                                {data.distance > 0 ?
                                    <Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', textAlign: isRTL ? 'right' : 'left', color: colors.grayColor1, fontSize: responsiveFontSize(5), fontFamily: isRTL ? arrabicFont : englishFont }}><Text style={{ fontFamily: boldFont }}>{Strings.distance}:</Text> {data.distance.toFixed(2)} {Strings.Km}</Text>
                                    :
                                    <Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', textAlign: isRTL ? 'right' : 'left', color: colors.grayColor1, fontSize: responsiveFontSize(5), fontFamily: isRTL ? arrabicFont : englishFont }}><Text style={{ fontFamily: boldFont }}>{Strings.distance}:</Text> 0 {Strings.Km}</Text>
                                }
                            </View>




                        </View>

                    </TouchableOpacity>


                </Animatable.View>
            </View>

        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
});

const mapDispatchToProps = {
    getUnreadNotificationsNumers
}


export default connect(mapStateToProps, mapDispatchToProps)(PurchasingAccountsCard);
