import React, { Component } from 'react';
import { View, Alert, TouchableOpacity, Text, Image } from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import * as colors from '../assets/colors'
import Strings from '../assets/strings';
import { Thumbnail, Icon, Button } from 'native-base';
import moment from 'moment'
import "moment/locale/ar"
import axios from 'axios';
import { BASE_END_POINT } from '../AppConfig';
import withPreventDoubleClick from './withPreventDoubleClick';
import { getUnreadNotificationsNumers } from '../actions/NotificationAction'
import { arrabicFont, englishFont, boldFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image'
import { push } from '../controlls/NavigationControll'



const MyTouchableOpacity = withPreventDoubleClick(TouchableOpacity);

//moment(birth_date).format("YYYY-MM-DD")


class SurveyAccountsCard extends Component {



    render() {
        const { isRTL, navigator, data } = this.props;
        moment.locale(this.props.isRTL ? 'ar' : 'en');
        return (
            <TouchableOpacity onPress={() => push('OperationAssignAccountToPurchasing', data)} >
                <Animatable.View
                    style={{ marginTop: moderateScale(3), alignSelf: 'center', width: responsiveWidth(96) }}
                    animation={"flipInX"}
                    duration={1000}>
                    <View style={{}}>
                        <View style={{ marginTop: moderateScale(5), flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', width: responsiveWidth(94) }}>
                            <FastImage source={data.img ? { uri: data.img } : require('../assets/imgs/profileicon.jpg')} style={{width:responsiveWidth(17), height:responsiveWidth(17), borderRadius:responsiveWidth(8.5) }} />
                            <View style={{ width: responsiveWidth(40), marginHorizontal: moderateScale(4) }}>
                                <Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', color: colors.grayColor1, fontSize: responsiveFontSize(5), fontFamily: isRTL ? arrabicFont : englishFont }}><Text style={{ fontFamily: boldFont }}>{Strings.outletName}:</Text> {isRTL? data.restaurantName_ar: data.restaurantName}</Text>
                                <Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', color: colors.grayColor1, fontSize: responsiveFontSize(5), fontFamily: isRTL ? arrabicFont : englishFont }}><Text style={{ fontFamily: boldFont }}>{Strings.EstQt}:</Text> {data.average}</Text>
                                <Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', color: colors.grayColor1, fontSize: responsiveFontSize(5), fontFamily: isRTL ? arrabicFont : englishFont }}><Text style={{ fontFamily: boldFont }}>{Strings.price}:</Text> {data.price} <Text>{Strings.egpKg}</Text></Text>
                                <Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', color: colors.grayColor1, fontSize: responsiveFontSize(5), fontFamily: isRTL ? arrabicFont : englishFont }}><Text style={{ fontFamily: boldFont }}>{Strings.area}:</Text> {isRTL?data.branches[0].area.arabicAreaName:data.branches[0].area.areaName}</Text>
                            </View>

                            {/*<View style={{ width: responsiveWidth(40), marginHorizontal: moderateScale(3) }}>
                                <Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', color: colors.grayColor1, fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.locationArea}</Text>
                                <Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', color: colors.grayColor1, fontSize: responsiveFontSize(5), fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.driver}</Text>
                            </View>*/}
                        </View>

                    </View>
                </Animatable.View>
            </TouchableOpacity>

        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
});

const mapDispatchToProps = {
    getUnreadNotificationsNumers
}


export default connect(mapStateToProps, mapDispatchToProps)(SurveyAccountsCard);
