import React, { Component } from 'react';
import { View, Alert, TouchableOpacity, Text, Image, Linking } from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import * as colors from '../assets/colors'
import Strings from '../assets/strings';
import { Thumbnail, Icon, Button } from 'native-base';
import moment from 'moment'
import "moment/locale/ar"
import axios from 'axios';
import { BASE_END_POINT } from '../AppConfig';
import withPreventDoubleClick from './withPreventDoubleClick';
import { getUnreadNotificationsNumers } from '../actions/NotificationAction'
import { arrabicFont, englishFont, boldFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image'
import { push } from '../controlls/NavigationControll'
import { RNToasty } from 'react-native-toasty';
import { whiteButton, greenButton } from '../assets/styles';



class DriverConfirmedOrderCard extends Component {




    openMap(resturant, latitude, longitude) {
        var url = 'http://maps.google.com/maps?q=' + resturant + '&daddr=' + latitude + ',' + longitude;
        Linking.openURL(url);
    }


    render() {
        const { isRTL, data } = this.props;
        moment.locale(this.props.isRTL ? 'ar' : 'en');
        return (
            <TouchableOpacity onPress={() => {
                push('DriverUpdateOrder', data)
            }} >
                <Animatable.View
                    style={{ marginBottom: moderateScale(5), padding: moderateScale(3), flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', alignSelf: 'center', width: responsiveWidth(100) }}
                    animation={"flipInX"}
                    duration={1000}>
                    <Thumbnail large source={require('../assets/imgs/profileicon.jpg')} />

                    <View style={{ marginHorizontal: moderateScale(3), width: responsiveWidth(60) }}>
                        <Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', color: colors.grayColor1, fontSize: responsiveFontSize(5), fontFamily: isRTL ? arrabicFont : englishFont }}><Text style={{ fontFamily: boldFont }}>{Strings.outletName}:</Text> {isRTL ? data.restaurant.restaurantName_ar : data.restaurant.restaurantName}</Text>
                        <Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', color: colors.grayColor1, fontSize: responsiveFontSize(5), fontFamily: isRTL ? arrabicFont : englishFont }}><Text style={{ fontFamily: boldFont }}>{Strings.area}:</Text> {isRTL ? data.restaurant.branches[0].area.arabicAreaName : data.restaurant.branches[0].area.areaName}</Text>
                        <Text style={{ textAlign: isRTL ? 'right' : 'left', color: colors.grayColor1, fontFamily: isRTL ? arrabicFont : englishFont }}><Text style={{ fontFamily: boldFont }}>{Strings.phone}:</Text>{data.restaurant.phone}</Text>
                        <Text style={{ textAlign: isRTL ? 'right' : 'left', color: colors.grayColor1, fontFamily: isRTL ? arrabicFont : englishFont }}><Text style={{ fontFamily: boldFont }}>{Strings.address}:</Text>{data.restaurant.branches[data.branch].address}</Text>
                        <Text style={{ textAlign: isRTL ? 'right' : 'left', color: colors.grayColor1, fontFamily: isRTL ? arrabicFont : englishFont }}><Text style={{ fontFamily: boldFont }}>{Strings.date}:</Text>{data.date}</Text>
                    </View>

                    <View>

                        <TouchableOpacity style={{ alignSelf: 'center', marginTop: moderateScale(3) }} onPress={() => {
                            this.openMap(isRTL ? data.restaurant.restaurantName_ar : data.restaurant.restaurantName, data.restaurant.branches[data.branch].destination[0], data.restaurant.branches[data.branch].destination[1])
                        }}>
                            <FastImage style={{ width: 20, height: 20 }} source={require('../assets/imgs/home-driverlocation-icon.png')} />
                        </TouchableOpacity>


                    </View>

                </Animatable.View>
            </TouchableOpacity>

        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
});

const mapDispatchToProps = {
    getUnreadNotificationsNumers
}


export default connect(mapStateToProps, mapDispatchToProps)(DriverConfirmedOrderCard);
