import React, { Component } from "react";
import { View} from "react-native";

import Animation from 'lottie-react-native';
import anim from '../assets/animations/loading.json';

export default class ListFooter extends Component {

    componentDidMount(){
        this.animation.play();
    }

    render() {
        return (
            <View>
            <Animation
                
                ref={animation => {
                this.animation = animation;
                }}
                style={{
                  
                width: 140,
                height: 100
                }}
                loop={true}
                source={anim}
            />
        </View>
            );
    }
}
