import React, { Component } from 'react';
import { View, Alert, TouchableOpacity, Text } from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import * as colors from '../assets/colors'
import Strings from '../assets/strings';
import { Thumbnail, Icon, Button } from 'native-base';
import moment from 'moment'
import "moment/locale/ar"
import axios from 'axios';
import { BASE_END_POINT } from '../AppConfig';
import withPreventDoubleClick from './withPreventDoubleClick';
import { getUnreadNotificationsNumers } from '../actions/NotificationAction'
import { arrabicFont, englishFont, boldFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image'



const MyTouchableOpacity = withPreventDoubleClick(TouchableOpacity);

//moment(birth_date).format("YYYY-MM-DD")


class OrdersHistoryCard2 extends Component {

    render() {
        const { isRTL, data } = this.props;
     
        moment.locale('en');
        return (
            <View>
                <Animatable.View
                    style={{ borderBottomColor: '#cccbcb', borderBottomWidth: 1, paddingVertical: moderateScale(8), alignSelf: 'center', width: responsiveWidth(98) }}
                    animation={"slideInLeft"}
                    duration={1000}>
                    <View style={{ alignItems:isRTL?'flex-end':'flex-start', width: responsiveWidth(98) }}>
                    
                        <Text style={{ textAlign:isRTL?'right':'left', color: colors.grayColor1, fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont }}>- {Strings.restaurantName} : {isRTL ? data.order.restaurant.restaurantName_ar: data.order.restaurant.restaurantName}</Text>
                        <Text style={{marginTop:moderateScale(2), textAlign:isRTL?'right':'left', color: colors.grayColor1, fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont }}>- {Strings.quantity} : {data.order.average} {Strings.Kg}</Text>
                        <Text style={{marginTop:moderateScale(2), textAlign:isRTL?'right':'left', color: colors.grayColor1, fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont }}>- {Strings.orderCode2} : {data.order.serialCode}</Text>
                        <Text style={{marginTop:moderateScale(2), textAlign:isRTL?'right':'left', color: colors.grayColor1, fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont }}>- {Strings.orderDate} : {moment(data.createdAt).format('YYYY.MM.DD (HH:MM)')}</Text>

                        <Text style={{marginTop:moderateScale(2), textAlign:isRTL?'right':'left', color: colors.grayColor1, fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont }}>- {Strings.driverName} : {data.order.driver.firstname+" "+data.order.driver.lastname}</Text>
                        <Text style={{marginTop:moderateScale(2), textAlign:isRTL?'right':'left', color: colors.grayColor1, fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont }}>- {Strings.timeWhichDriverAccepted} : {moment(data.order.driverTakeDate).format('YYYY.MM.DD (HH:MM)')}</Text>

                        <Text style={{marginTop:moderateScale(2), textAlign:isRTL?'right':'left', color: colors.grayColor1, fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont }}>- {Strings.warehouseName} : {data.order.wareHouse.name}</Text>

                        {/*<Text style={{ textAlign: 'center', color: colors.grayColor1, fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont }}>- {Strings.price} : {data.order.totalPrice} {Strings.EGP}</Text> */}
                    </View>

                </Animatable.View>
            </View>

        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
});

const mapDispatchToProps = {
    getUnreadNotificationsNumers
}


export default connect(mapStateToProps, mapDispatchToProps)(OrdersHistoryCard2);
