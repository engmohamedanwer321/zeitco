import React,{Component} from 'react';
import {View,Animated,TouchableOpacity,Text} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import * as colors from '../assets/colors'
import Strings from '../assets/strings';
import { Thumbnail,Icon } from 'native-base';
import moment from 'moment'
import "moment/locale/ar"
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import withPreventDoubleClick from './withPreventDoubleClick';
import {getUnreadNotificationsNumers} from '../actions/NotificationAction'
import {arrabicFont,englishFont} from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image';
import {push} from '../controlls/NavigationControll'
import MapView, { Marker } from 'react-native-maps';
import strings from '../assets/strings';



class DriverLocationCard extends Component {
    
    
    render(){
        const {isRTL,data} = this.props;
        return(
            <View
            style={{elevation:4,marginBottom:moderateScale(5),zIndex:1000,shadowOffset:{width:2,height:0},shadowOpacity:0.1,shadowColor:'black', backgroundColor:'white', justifyContent:'center',alignItems:'center', marginVertical: moderateScale(5), alignSelf: 'center', width: responsiveWidth(96) }}
            >
                <Text style={{fontWeight:'bold', color:'gray',marginHorizontal:moderateScale(3),marginTop:moderateScale(5), fontFamily:isRTL?arrabicFont:englishFont,fontSize:responsiveFontSize(7)}}>{Strings.name} : <Text style={{color:'gray',marginHorizontal:moderateScale(3),marginTop:moderateScale(5), fontFamily:isRTL?arrabicFont:englishFont,fontSize:responsiveFontSize(5.5)}}> {data.firstname} {data.lastname} </Text> </Text>
                <Text style={{fontWeight:'bold',marginTop:moderateScale(2),color:'gray',marginHorizontal:moderateScale(3), fontFamily:isRTL?arrabicFont:englishFont,fontSize:responsiveFontSize(6)}}>{Strings.phone} : <Text style={{color:'gray',marginHorizontal:moderateScale(3),marginTop:moderateScale(5), fontFamily:isRTL?arrabicFont:englishFont,fontSize:responsiveFontSize(5.5)}}> {data.phone} </Text> </Text>
                <Text style={{fontWeight:'bold', marginTop:moderateScale(2),color:'gray',marginHorizontal:moderateScale(3), fontFamily:isRTL?arrabicFont:englishFont,fontSize:responsiveFontSize(6)}}>{Strings.email} : <Text style={{color:'gray',marginHorizontal:moderateScale(3),marginTop:moderateScale(5), fontFamily:isRTL?arrabicFont:englishFont,fontSize:responsiveFontSize(5.5)}}> {data.email} </Text> </Text>
                
                <MapView
                    style={{ alignSelf: 'center', width: responsiveWidth(94), height: responsiveHeight(27), marginTop: moderateScale(7),marginBottom:moderateScale(2) }}
                    region={{
                        latitude:data.location.length>0?data.location[0]:1,
                        longitude:data.location.length>0?data.location[1]:1,
                        latitudeDelta: 0.015,
                        longitudeDelta: 0.0121,
                    }}
                >
                    <Marker
                        coordinate={{
                            latitude:data.location.length>0?data.location[0]:1,
                            longitude:data.location.length>0?data.location[1]:1,
                            latitudeDelta: 0.015,
                            longitudeDelta: 0.0121,
                        }}
                    />
                </MapView>
                
            </View>
           
        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
});

const mapDispatchToProps = {
    getUnreadNotificationsNumers
}


export default connect(mapStateToProps,mapDispatchToProps)(DriverLocationCard);
