import React, { Component } from 'react';
import { View, Alert, TouchableOpacity, Text } from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import * as colors from '../assets/colors'
import Strings from '../assets/strings';
import { Thumbnail, Icon, Button } from 'native-base';
import moment from 'moment'
import "moment/locale/ar"
import axios from 'axios';
import { BASE_END_POINT } from '../AppConfig';
import withPreventDoubleClick from './withPreventDoubleClick';
import { getUnreadNotificationsNumers } from '../actions/NotificationAction'
import { arrabicFont, englishFont, boldFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image'
import {push} from '../controlls/NavigationControll'
import { RNToasty } from 'react-native-toasty';


class WarehouseOrderCard extends Component {


    render() {
        const { isRTL, data,worker } = this.props;
        moment.locale(this.props.isRTL ? 'ar' : 'en');
        return (
            <TouchableOpacity
            onPress={()=>{
                if(data.kg==0){
                    RNToasty.Info({title:Strings.waitUntilQuantityAssigned})
                }else{
                    push('WarehouseDriverCollections',data)
                }
            }}
            >
                <Animatable.View
                    style={{ borderBottomColor: '#cccbcb', borderBottomWidth: 1, paddingVertical: moderateScale(8), alignSelf: 'center', width: responsiveWidth(80) }}
                    //animation={"slideInLeft"}
                    //duration={1000}
                >
                    <View style={{}}>
                        <View style={{ width: responsiveWidth(80) }}>

                            <View style={{ width: responsiveWidth(80) }}>
                                <Text style={{ textAlign: 'center', color: colors.grayColor1, fontSize: responsiveFontSize(8), fontFamily: isRTL ? boldFont : boldFont }}>{data.driver.firstname+"  "+data.driver.lastname}</Text>
                                <Text style={{ textAlign: 'center', color: colors.grayColor1, fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont }}>- {Strings.phone} : {data.driver.phone}</Text>
                                <Text style={{ textAlign: 'center', color: colors.grayColor1, fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont }}>- {Strings.quantity} : {data.average} {Strings.Kg}</Text>

                                <Text style={{ textAlign: 'center', color: colors.grayColor1, fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont }}>- {Strings.date} : {(data.date).replace('-','.').replace('-','.')}</Text>

                            </View>
                        </View>

                    </View>


                </Animatable.View>
            </TouchableOpacity>

        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
});

const mapDispatchToProps = {
    getUnreadNotificationsNumers
}


export default connect(mapStateToProps, mapDispatchToProps)(WarehouseOrderCard);
