import React, { Component } from 'react';
import { View, Alert, TouchableOpacity, Text,Linking } from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import * as colors from '../assets/colors'
import Strings from '../assets/strings';
import { Thumbnail, Icon, Button } from 'native-base';
import moment from 'moment'
import "moment/locale/ar"
import axios from 'axios';
import { BASE_END_POINT } from '../AppConfig';
import withPreventDoubleClick from './withPreventDoubleClick';
import { getUnreadNotificationsNumers } from '../actions/NotificationAction'
import { arrabicFont, englishFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import { push, resetTo } from '../controlls/NavigationControll'
import FastImage from 'react-native-fast-image'
import { RNToasty } from 'react-native-toasty';
import { getDistance } from 'geolib';
import { OrdersCount } from '../actions/OrdersActions';
import { whiteButton, greenButton } from '../assets/styles';
import { AccountsCount } from '../actions/AccountsAction';

//here

class OperationAssignPurchasingCard extends Component {

    assign = () => {
        const { isRTL,data,resturant,currentUser } = this.props;
        axios.put(`${BASE_END_POINT}restaurant/${resturant.id}/purchasing/${data.id}`, null, {
            headers: {
                "Authorization": `Bearer ${currentUser.token}`,
                "Content-Type": "application/json"
            }
        })
            .then(response => {
                console.log('DONE')
                //this.setState({ loading: false })
                this.props.AccountsCount('SURVEY-ACCOUNT')
                RNToasty.Success({ title: Strings.AssignedSuccessfuly })
                if (this.props.currentUser.user.type == 'ADMIN') {
                    resetTo('AdminHome')
                } else {
                    resetTo('OperationHome')
                }
            })
            .catch(error => {
                this.setState({ loading: false })
                console.log('ERROR   ', error.response)
                RNToasty.Error({ title: Strings.errorInAssign })
            })
    }

    assignButton = () => {

        const { isRTL,data,resturant,currentUser } = this.props;
        console.log("resturant  ",resturant)
        console.log("data  ",data)
        
        return (
            <View style={{ alignSelf: 'center', justifyContent: 'center', alignItems: 'center', }}>
                <TouchableOpacity
                    onPress={() => {
                        if('purchasing' in resturant){
                            if(resturant.decline || resturant.noOil || resturant.Agreement == 'NON-AGREEMENT' ){
                                if(resturant.purchasing.id==data.id){
                                    Alert.alert(
                                        '',
                                        Strings.areYouSureToAssignToTheSamePurchasing,
                                        [
                                          {
                                            text: Strings.cancel,
                                            onPress: () => console.log('Cancel Pressed'),
                                            style: 'cancel'
                                          },
                                          { text: Strings.yes, onPress: () => this.assign()}
                                        ],
                                        { cancelable: false }
                                    );
                                      
                                }else{
                                    this.assign()
                               }
                            }else{
                                this.assign()
                            }

                        }else{
                            this.assign()
                        } 
                
                    }}
                    style={[whiteButton,{ marginHorizontal: moderateScale(2), width: responsiveWidth(28), height: responsiveHeight(5), justifyContent: 'center', alignItems: 'center', alignSelf: 'center',}]}>
                    <Text style={{ color: colors.green, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.assign}</Text>
                </TouchableOpacity>


            </View>
        )
    }


    render() {
        const { isRTL,data,resturant } = this.props;
        return (
            <View>
                <Animatable.View
                    style={{ marginTop: moderateScale(3), alignSelf: 'center', width: responsiveWidth(96) }}
                    animation={"slideInLeft"}
                    duration={1000}>
                    <View style={{}}>
                        <View style={{ alignItems: 'center', marginTop: moderateScale(5), flexDirection: isRTL ? 'row-reverse' : 'row', width: responsiveWidth(94) }}>
                            <Thumbnail large source={data.img?{uri:data.img}:require('../assets/imgs/profileicon.jpg')} />
                            <View style={{ width: responsiveWidth(40), marginHorizontal: moderateScale(3) }}>
                                <Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', color: colors.grayColor1, fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont }}>{data.firstname} {data.lastname}</Text>
                                <Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', color: colors.grayColor1, fontSize: responsiveFontSize(5), fontFamily: isRTL ? arrabicFont : englishFont }}>{data.phone}</Text>
                            </View>

                            <View style={{justifyContent:'center',alignItems:'center'}}>
                            {this.assignButton()}
                            </View>

                        </View>

                    </View>


                </Animatable.View>
            </View>

        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
    ordersCount: state.orders.ordersCount,
});

const mapDispatchToProps = {
    getUnreadNotificationsNumers,
    OrdersCount,
    AccountsCount,
}


export default connect(mapStateToProps, mapDispatchToProps)(OperationAssignPurchasingCard);
