import React,{Component} from 'react';
import {View,Animated,TouchableOpacity,Alert, Text} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import * as colors from '../assets/colors'
import Strings from '../assets/strings';
import { Thumbnail,Icon,Button } from 'native-base';
import moment from 'moment'
import "moment/locale/ar"
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import withPreventDoubleClick from './withPreventDoubleClick';
import {getUnreadNotificationsNumers} from '../actions/NotificationAction'
import {arrabicFont,englishFont, boldFont} from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image';
import Dialog, { DialogContent,DialogTitle } from 'react-native-popup-dialog';
import { RNToasty } from 'react-native-toasty';
import { whiteButton, greenButton } from '../assets/styles';


class CancelOrderCard extends Component {
    
    state={
        cancel:false,
        showDialog:false,
    }
    
    cancel = () =>{
        this.setState({showDialog:false})
        axios.put(`${BASE_END_POINT}orders/${this.props.data.id}/cancel`,null,{
            headers:{
                "Authorization": `Bearer ${this.props.currentUser.token}`,
            }
        })
        .then(response=>{
            RNToasty.Success({title:Strings.orderCanceld})
            this.setState({cancel:true})
        })
        .catch(error=>{
            this.setState({showDialog:false,cancel:false})
        })
    }

    dialog = () =>{
        const {isRTL,data} = this.props;
        return(
            <Dialog
            containerStyle={{backgroundColor:'rgba(1,1,1,0.1)',}}
            visible={this.state.showDialog}
            onTouchOutside={() => {
            this.setState({ showDialog: false });
            }}
            onHardwareBackPress={()=>{
                this.setState({ showDialog: false });
            }}
            >
            <View style={{backgroundColor:'red', justifyContent:'center',alignItems:'center', width: responsiveWidth(90), height: responsiveHeight(15) }}>
                <Text style={{ color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.areYouSureToCancelOrdere}</Text>
                <View style={{ marginTop:moderateScale(5), flexDirection:isRTL?'row-reverse':'row',width:responsiveWidth(70),justifyContent:'space-between',alignItems:'center'}}>
                    <Button onPress={()=>{
                        this.setState({showDialog:false})
                    }} style={{ width: responsiveWidth(28), height: responsiveHeight(5), justifyContent: 'center', alignItems: 'center', alignSelf:isRTL?'flex-start':'flex-end', borderRadius: moderateScale(3), backgroundColor: 'white' }}>
                        <Text style={{ color: colors.black, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.no}</Text>
                    </Button>
                    <Button onPress={()=>{
                        this.cancel()
                    }} style={{width: responsiveWidth(28), height: responsiveHeight(5), justifyContent: 'center', alignItems: 'center', alignSelf:isRTL?'flex-start':'flex-end', borderRadius: moderateScale(3), backgroundColor: 'white' }}>
                        <Text style={{ color: colors.black, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.yes}</Text>
                    </Button>
                    
                </View>
            </View>
            </Dialog>
        )
    }
    
    render(){
        const {isRTL,data} = this.props;
        console.log('price:',data)
        const {cancel} = this.state;
        return(
            cancel?
            null
            :
            <Animatable.View
            style={{borderBottomWidth:1,borderColor:'#cccbcb', marginTop: moderateScale(3), alignSelf: 'center', width: responsiveWidth(94) }}
            >
                <Text style={{alignSelf: isRTL ? 'flex-end' : 'flex-start', color: colors.grayColor1, fontSize: responsiveFontSize(7), fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.orderCode} : {data.id}</Text>
                <Text style={{alignSelf: isRTL ? 'flex-end' : 'flex-start', color: colors.grayColor1, fontSize: responsiveFontSize(7), fontFamily: isRTL ? arrabicFont : englishFont }}>- {Strings.date} : {data.date.replace('-','.').replace('-','.')}</Text>
                <Text style={{alignSelf: isRTL ? 'flex-end' : 'flex-start', color: colors.grayColor1, fontSize: responsiveFontSize(7), fontFamily: isRTL ? arrabicFont : englishFont }}>- {Strings.quantity} : {data.average} <Text style={{fontSize:responsiveFontSize(5.5),fontFamily: isRTL ? boldFont : boldFont}}>{Strings.Kg}</Text></Text>
        <Text style={{alignSelf: isRTL ? 'flex-end' : 'flex-start', color: colors.grayColor1, fontSize: responsiveFontSize(7), fontFamily: isRTL ? arrabicFont : englishFont }}>- {Strings.price} : {data.totalPrice} <Text style={{fontSize:responsiveFontSize(5.5),fontFamily: isRTL ? boldFont : boldFont}}>{Strings.EGP}</Text></Text>
    
                <TouchableOpacity onPress={()=>{
                    this.setState({showDialog:true})
                }} style={[greenButton,{marginTop:moderateScale(-12),marginBottom:moderateScale(6), marginHorizontal: moderateScale(5), width: responsiveWidth(24), height: responsiveHeight(6), justifyContent: 'center', alignItems: 'center', alignSelf:isRTL?'flex-start':'flex-end',}]}>
                    <Text style={{ color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.cancel}</Text>
                </TouchableOpacity>
            {this.dialog()}
            </Animatable.View>
        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
});

const mapDispatchToProps = {
    getUnreadNotificationsNumers
}


export default connect(mapStateToProps,mapDispatchToProps)(CancelOrderCard);
