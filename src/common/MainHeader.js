import React, { Component } from 'react';
import {
  View,TouchableOpacity,Text,ImageBackground,Animated, Image
} from 'react-native';
import { Icon, Button,Badge, Thumbnail } from 'native-base';
import { connect } from 'react-redux';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import {openSideMenu,push,pop} from '../controlls/NavigationControll'
import {arrabicFont,englishFont} from './AppFont'
import FastImage from 'react-native-fast-image'
import * as colors from '../assets/colors'
import { Navigation } from "react-native-navigation";


class MainHeader extends Component {
//openSideMenu(true,isRTL)
  

  render() {
    const {isRTL,title,notifications,menu,currentUser,unreadNotificationsCount} = this.props;
    return (
      <>
      <View style={{ width: responsiveWidth(100), height: Platform.OS === 'ios' ? responsiveHeight(3) : 0,backgroundColor:colors.darkGreen }}/>
      <View style={{backgroundColor:colors.darkGreen,width:responsiveWidth(100),borderBottomLeftRadius:moderateScale(10),borderBottomRightRadius:moderateScale(10)}}>
         <View style={{marginTop:moderateScale(5),flexDirection:isRTL?'row-reverse':'row',alignItems:'center',justifyContent:'space-between', width:responsiveWidth(90),alignSelf:'center',}} >
            {menu?
            <TouchableOpacity
             style={{borderWidth:0,width:responsiveWidth(14), height:responsiveHeight(6),justifyContent:'center',}}
             onPress={()=>{
               //openSideMenu(true,isRTL)
               isRTL ?
               
              Navigation.mergeOptions('slideMenu', {
                sideMenu: {
                  right: {
                    visible:isRTL?true:false
                  }
                }
              })
              :
              Navigation.mergeOptions('slideMenu', {
                sideMenu: {
                  left: {
                    visible:isRTL?false:true
                  }
                }
              });
        
             }} >
                {/*<Icon name='sort' type='MaterialIcons' style={{color:colors.white}} />*/}
                <FastImage source={require('../assets/imgs/sidemenu-icon.png')} style={{width:responsiveWidth(6), height:responsiveHeight(2),alignSelf:isRTL?'flex-end':'flex-start', resizeMode:'stretch'}} />
            </TouchableOpacity>
            :
            <View style={{width:responsiveWidth(12)}} />
            }

            {notifications?
              <View>
                <View style={{zIndex:10000,elevation:2,shadowOffset:{height:0,width:2},alignSelf:isRTL?'flex-start':'flex-end', width:16,height:16,borderRadius:8,backgroundColor:'red',justifyContent:'center',alignItems:'center'}}>
                  <Text style={{color:colors.white,fontFamily:englishFont,fontSize:responsiveFontSize(4)}} >{unreadNotificationsCount}</Text>
                </View>
                <TouchableOpacity 
                 style={{marginTop:moderateScale(-4)}} 
                 onPress={()=>{push("Notifications")}} >
                  <Icon name='bells' type='AntDesign' style={{color:colors.white}} />
                </TouchableOpacity>
              </View>
              :
              <View style={{width:responsiveWidth(12)}} />
            }
           </View> 
         <View style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', margin:moderateScale(5),flexDirection:isRTL?'row-reverse':'row',alignItems:'center'}}>
            <Thumbnail large source={currentUser.user.img?{uri:currentUser.user.img}:require('../assets/imgs/profileicon.jpg')} />
            <View style={{marginHorizontal:moderateScale(3)}} >
              <Text style={{alignSelf:isRTL?'flex-end':'flex-start', color: colors.white, fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont }}>{currentUser.user.firstname} {currentUser.user.lastname}</Text>
              <Text style={{alignSelf:isRTL?'flex-end':'flex-start',color: colors.white, fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont }}>{currentUser.user.phone}</Text>
              <Text style={{alignSelf:isRTL?'flex-end':'flex-start', color: colors.white, fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont }}>{currentUser.user.email}</Text>
            </View>
         </View>
      </View>
      </>
    );
  }
}


const mapStateToProps = state => ({
  isRTL: state.lang.RTL,
  currentUser: state.auth.currentUser,
  unreadNotificationsCount: state.noti.unreadNotificationsCount
});

export default connect(mapStateToProps)(MainHeader);
