//
import React, { Component } from "react";
import { Text, View,ActivityIndicator} from "react-native";
import LottieView from 'lottie-react-native';
import { moderateScale, responsiveWidth, responsiveHeight,responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";


class Loading extends Component {
render(){
    return(
        <View style={{width:responsiveWidth(25), marginTop:moderateScale(10), backgroundColor:'white',justifyContent:'center',alignItems:'center',alignSelf:'center'}}>
            <ActivityIndicator />
            <Text style={{marginTop:moderateScale(5), fontSize:responsiveFontSize(7),fontWeight:'700'}} >LOADING</Text>
        </View>
    )
}
}




const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
});

export default connect(mapStateToProps)(Loading);
