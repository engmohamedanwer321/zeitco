//
import React, { Component } from "react";
import { Text, View,TouchableOpacity} from "react-native";
import { moderateScale, responsiveWidth, responsiveHeight,responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import {arabicFont,englishFont} from './AppFont'
import * as Animatable from 'react-native-animatable';
import {Icon} from 'native-base'

class AppCheckBox extends Component {
render(){
    //title: title of check box (require)
    //check: check of check box (require)
    //onPress: action when press on check box onPress (require)
    //textColor: title color of check box
    //boxColor: check box background
    const {isRTL,title,check,onPress,textColor,boxColor,style} = this.props
    return(
         <TouchableOpacity 
          onPress={()=>{onPress()}}
          style={[{flexDirection:isRTL?'row-reverse':'row',alignItems:'center',justifyContent:'center'},{...style}]}
          >
            
            <View style={{backgroundColor:check?boxColor?boxColor:'black':'transparent', justifyContent:'center',alignItems:'center', width:22,height:22,borderWidth:2,borderRadius:5,borderColor:boxColor?boxColor:'black'}}>
                {check&&
                <Animatable.View useNativeDriver duration={800} animation='flipInX' >
                    <Icon name='check' type='MaterialIcons' style={{color:'white', fontSize:responsiveFontSize(9)}}/>
                </Animatable.View>
                }
            </View>
            
            <Text style={{maxWidth:responsiveWidth(80), color:textColor?textColor:'black',fontFamily:isRTL?arabicFont:englishFont, marginHorizontal:moderateScale(3)}} >{title}</Text>
         </TouchableOpacity>
    )
}
}




const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
});

export default connect(mapStateToProps)(AppCheckBox);
