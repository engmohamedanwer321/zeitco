import React, { Component } from 'react';
import {
  View, TouchableOpacity, Text, ImageBackground, Animated
} from 'react-native';
import { Icon, Button, Badge } from 'native-base';
import { connect } from 'react-redux';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { openSideMenu, push, pop ,resetTo} from '../controlls/NavigationControll'
import { arrabicFont, englishFont } from './AppFont'
import FastImage from 'react-native-fast-image'
import * as colors from '../assets/colors'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';


class CommanHeader extends Component {
  //openSideMenu(true,isRTL)


  render() {
    const { isRTL, title,home, currentUser, notifications, unreadNotificationsCount } = this.props;
    return (
      <>
        <View style={{ width: responsiveWidth(100), height: Platform.OS === 'ios' ? responsiveHeight(3) : 0 }}/>
        <View style={{ backgroundColor: 'tranparent', width: responsiveWidth(100), height: responsiveHeight(12) }}>
          <View style={{ marginTop: moderateScale(5), flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', justifyContent: 'space-between', width: responsiveWidth(90), alignSelf: 'center', }} >
            <TouchableOpacity onPress={() => {
              if(home){
                if(currentUser.user.type=='ADMIN'){
                  resetTo("AdminHome")
                }else{
                  resetTo('OperationHome')
                }
              }else{
                pop()
              }
             }}>
              <Icon name={isRTL ? 'arrowright' : 'arrowleft'} type='AntDesign' style={{ color: colors.white }} />
            </TouchableOpacity>

            {notifications ?
              <View>
                <View style={{ zIndex: 10000, elevation: 2, shadowOffset: { height: 0, width: 2 }, alignSelf: isRTL ? 'flex-start' : 'flex-end', width: 16, height: 16, borderRadius: 8, backgroundColor: 'red', justifyContent: 'center', alignItems: 'center' }}>
                  <Text style={{ color: colors.white, fontFamily: englishFont, fontSize: responsiveFontSize(4) }} >{unreadNotificationsCount}</Text>
                </View>
                <TouchableOpacity style={{ marginTop: moderateScale(-4) }} onPress={() => { }} >
                  <Icon name='bells' type='AntDesign' style={{ color: colors.white }} />
                </TouchableOpacity>
              </View>
              :
              <View style={{ width: responsiveWidth(12) }} />
            }
          </View>
          <View style={{ justifyContent: 'center', alignItems: 'center', alignSelf: 'center', width: responsiveWidth(60), borderBottomColor: 'white', borderBottomWidth: 2 }} >
            <Text style={{ marginBottom: moderateScale(3), color: colors.white, fontSize: responsiveFontSize(8), fontFamily: isRTL ? arrabicFont : englishFont }}>{title}</Text>
          </View>
        </View>
      </>
    );
  }
}


const mapStateToProps = state => ({
  isRTL: state.lang.RTL,
  color: state.lang.color,
  currentUser: state.auth.currentUser,
  unreadNotificationsCount: state.noti.unreadNotificationsCount
});

export default connect(mapStateToProps)(CommanHeader);
