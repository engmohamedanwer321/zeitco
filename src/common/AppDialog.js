//
import React, { Component } from "react";
import { Text, View,ActivityIndicator,TouchableOpacity,TouchableWithoutFeedback , ScrollView} from "react-native";
import { moderateScale, responsiveWidth, responsiveHeight,responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import allStrings from '../assets/strings';
import * as Animatable from 'react-native-animatable';


class AppDialog extends Component {

render(){
    const {open,onOutPress} = this.props
    //opne: use it to show/hide bottom sheet (require)
    //onOutPress : action to close bottom sheet when press out of it (require)
    return(
    open?
    <TouchableOpacity
    onPress={()=>{
        if(onOutPress){
            onOutPress()
        }
    }}
    activeOpacity={1} style={{zIndex:10000000,elevation:10, position:'absolute',left:0,bottom:0,right:0,top:0, backgroundColor:'rgba(0,0,0,0.4)',alignSelf:'center',justifyContent:'center',alignItems:'center'}}
    >
        
        <TouchableOpacity activeOpacity={1}>
            <Animatable.View useNativeDriver duration={500} animation='slideInDown'>
            {this.props.children}
            </Animatable.View>
        </TouchableOpacity>
    </TouchableOpacity>
    :null
    
    )
}
}




const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
});

export default connect(mapStateToProps)(AppDialog);
