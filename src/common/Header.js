import React, { Component } from 'react';
import {
  View,TouchableOpacity,Text,ImageBackground,Animated, Image,Platform
} from 'react-native';
import { Icon, Button,Badge, Thumbnail } from 'native-base';
import { connect } from 'react-redux';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import {openSideMenu,push,pop} from '../controlls/NavigationControll'
import {arrabicFont,englishFont} from './AppFont'
import FastImage from 'react-native-fast-image'
import * as colors from '../assets/colors'
import { Navigation } from "react-native-navigation";


class Header extends Component {
//openSideMenu(true,isRTL)
  

  render() {
    const {isRTL,title,notifications,menu,unreadNotificationsCount,hideNotification} = this.props;
    return (
     <View style={{elevation:4,shadowOffset:{width:0,height:2},shadowColor:'black',shadowRadius:0.2, width:responsiveWidth(100),height:responsiveHeight(Platform.OS=='android'?10:12),backgroundColor:colors.darkGreen,justifyContent:'space-between',alignItems:'center'}} >
          <View style={{alignSelf:'center', width:responsiveWidth(95),backgroundColor:colors.darkGreen,flexDirection:isRTL?'row-reverse':'row',justifyContent:'space-between',alignItems:'center', marginTop:Platform.OS ==='ios' ? moderateScale(17): moderateScale(7)}} >
          
          {menu?
            <TouchableOpacity
             style={{borderWidth:0,height:responsiveHeight(4), alignItems:'center', justifyContent:'center'}}
             onPress={()=>{
              if(isRTL){
                Navigation.mergeOptions('slideMenu', {
                  sideMenu: {
                    right: {
                      visible:isRTL?true:false
                    }
                  }
                })
              }else{
                Navigation.mergeOptions('slideMenu', {
                  sideMenu: {
                    left: {
                      visible:isRTL?false:true
                    }
                  }
                })
              }
        
             }} >
                <FastImage source={require('../assets/imgs/sidemenu-icon.png')} style={{width:responsiveWidth(6), height:responsiveHeight(2),alignSelf:isRTL?'flex-end':'flex-start', resizeMode:'stretch'}} />
            </TouchableOpacity>
            :
            <TouchableOpacity 
            onPress={()=>{
              pop()
            }}
            style={{}}
            >
              <Icon name={isRTL?'arrowright':'arrowleft'} type='AntDesign' style={{color:colors.white}} />
            </TouchableOpacity>
          }

          <Text style={{color:'white',fontSize:responsiveFontSize(8),fontFamily:englishFont}} >{title}</Text>
          
          {hideNotification?
          <View style={{width:responsiveWidth(8)}}/>
          :
          <View style={{}} >
                <View style={{zIndex:10000,elevation:2,shadowOffset:{height:0,width:2},alignSelf:isRTL?'flex-start':'flex-end', width:responsiveWidth(5.5),height:responsiveWidth(5.5),borderRadius:responsiveWidth(2.75),backgroundColor:'red',justifyContent:'center',alignItems:'center'}}>
                  <Text style={{color:colors.white,fontFamily:englishFont,fontSize:responsiveFontSize(4), textAlign:'center'}} >{unreadNotificationsCount}</Text>
                </View>
                <TouchableOpacity 
                 style={{marginTop:moderateScale(-4)}} 
                 onPress={()=>{push("Notifications")}} >
                  <Icon name='bells' type='AntDesign' style={{color:colors.white}} />
                </TouchableOpacity>
          </View>
        }
          
          </View>
     </View>
    );
  }
}


const mapStateToProps = state => ({
  isRTL: state.lang.RTL,
  currentUser: state.auth.currentUser,
  unreadNotificationsCount: state.noti.unreadNotificationsCount
});

export default connect(mapStateToProps)(Header);
