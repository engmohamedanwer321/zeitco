//
import React, { Component } from "react";
import { Text, Platform, View, TouchableOpacity } from "react-native";

import { connect } from "react-redux";
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
//import Icon from 'react-native-vector-icons/FontAwesome5';
import withPreventDoubleClick from '../components/withPreventDoubleClick';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Icon } from 'native-base';
import { arrabicFont, englishFont } from '../common/AppFont'
import * as colors from '../assets/colors'
import FastImage from 'react-native-fast-image'


const MyTouchableOpacity = withPreventDoubleClick(TouchableOpacity);

class MenuItem extends Component {
    //color={focused? '#ff6f61' : 'white'}
    renderNormal() {
        const { onPress, isRTL, title, img } = this.props;

        return (

            <TouchableOpacity style={{ alignSelf: 'flex-start', width: 270, marginBottom: moderateScale(13), backgroundColor: "transparent", marginLeft: isRTL ? 0 : moderateScale(5) }} onPress={onPress}>
                <View style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', width: responsiveWidth(65), flexDirection: isRTL ? "row-reverse" : 'row', alignItems: 'center' }} >

                    <FastImage source={img} style={{ width: 50, height: 50, borderRadius: 25 }} />

                    <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6.5), marginHorizontal: moderateScale(5), fontWeight: '600', textAlign: 'center', color: colors.black }}>{title}</Text>



                </View>
            </TouchableOpacity>

        )
    }
    render() {
        return this.renderNormal();
    }
}




const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
});

export default connect(mapStateToProps)(MenuItem);
