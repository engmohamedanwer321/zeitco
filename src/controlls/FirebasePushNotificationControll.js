import React,{Component} from 'react';
import {Alert,Platform} from 'react-native'
import firebase,{Notification } from 'react-native-firebase';
import { push } from './NavigationControll';
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios'
import {BASE_END_POINT} from '../AppConfig'

export const checkFirbaseNotificationPermision = ()=>{
    firebase.messaging().hasPermission()
    .then(enabled => {
        if (enabled) {
        console.log("EXIST PERMISION")
        } else {
        console.log("NOT EXIST PERMISION")
        firebase.messaging().requestPermission()
        } 
    });  
}

export const getFirebaseNotificationToken = (userToken,setFirebaseToken)=>{
    firebase.messaging().getToken().then(token => {
        console.log("FIREBASE TOKEN  ", token);
        setFirebaseToken(token)
        sendFirebasTokenToServer(token,userToken)
      }).catch(error=>{
        console.log("TOKEN Error");         
        console.log(error);
      });

}

const sendFirebasTokenToServer = (firebaseToken,userToken) =>{
    //write action with api  
    axios.put(`${BASE_END_POINT}updateToken`,JSON.stringify({
        oldToken:"firebaseToken",
		newToken:firebaseToken 
    }),{
        headers:{
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${userToken}`
        }
    }
    )
    .then((res)=>{})
    .catch((error)=>{})
}

const firebaseNotificatioComponent = (title, body) => {
    console.log("TEST NOTI IN FORGROUND")
    const notification = new firebase.notifications.Notification({
        sound: 'default',
        show_in_foreground: true,  
        local: true,
    })
     .setNotificationId(new Date().valueOf().toString()) 
     .setTitle(title)
     .setBody(body);

     //android config
     if(Platform.OS=='android'){
     notification
     .android.setPriority(firebase.notifications.Android.Priority.Max)
     .android.setChannelId("reminder") // should be the same when creating channel for Android
     .android.setAutoCancel(true);
     }
    firebase.notifications().displayNotification(notification) 
 }
 
 export const showFirebaseNotifcation = () => {
     firebase.notifications().onNotification((notification) => {
         //console.log("showFirebaseNotifcation")
         //console.log(notification)
         console.log("NOTI INFO   "+notification._title,notification._body)
         firebaseNotificatioComponent(notification._title,notification._body)
     });
 }

 const doAction = async () => {
    const userJSON = await AsyncStorage.getItem('USER');
    if (userJSON) {
        push('Notifications')
    } else {
        push('Login')
    }
}

export const clickOnFirebaseNotification = ()=>{
    firebase.notifications().onNotificationOpened((notificationOpen) => {
        firebase.notifications().removeDeliveredNotification(notificationOpen.notification._notificationId)
        doAction()
    });
}


